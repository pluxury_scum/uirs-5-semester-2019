#include "pch.h"
#include "../../uirs5sem/include/Var13.h"
#include "../../uirs5sem/src/Var13.cpp"

TEST(Var13Test, isCheckSumOK)
{
	Var13 var13;
	int checksum = var13.getCheckSum();
	int csvalue = var13.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}