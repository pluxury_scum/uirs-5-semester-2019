#include "pch.h"
#include "../../uirs5sem/include/Var5.h"
#include "../../uirs5sem/src/Var5.cpp"

TEST(Var5Test, isCheckSumOK)
{
	Var5 var5;
	int checksum = var5.getCheckSum();
	int csvalue = var5.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}