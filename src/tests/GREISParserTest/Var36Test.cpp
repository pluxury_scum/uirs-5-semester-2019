#include "pch.h"
#include "../../uirs5sem/include/Var36.h"
#include "../../uirs5sem/src/Var36.cpp"

TEST(Var36Test, isCheckSumOK)
{
	Var36 var36;
	int checksum = var36.getCheckSum();
	int csvalue = var36.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}