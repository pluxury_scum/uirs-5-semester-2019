#include "pch.h"
#include "../../uirs5sem/include/Var66.h"
#include "../../uirs5sem/src/Var66.cpp"

TEST(Var66Test, isCheckSumOK)
{
	Var66 var66;
	int checksum = var66.getCheckSum();
	int csvalue = var66.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}