#include "pch.h"
#include "../../uirs5sem/include/Var21.h"
#include "../../uirs5sem/src/Var21.cpp"

TEST(Var21Test, isCheckSumOK)
{
	Var21 var21;
	int checksum = var21.getCheckSum();
	int csvalue = var21.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}