#include "pch.h"
#include "../../uirs5sem/include/Var27.h"
#include "../../uirs5sem/src/Var27.cpp"

TEST(Var27Test, isCheckSumOK)
{
	Var27 var27;
	int checksum = var27.getCheckSum();
	int csvalue = var27.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}