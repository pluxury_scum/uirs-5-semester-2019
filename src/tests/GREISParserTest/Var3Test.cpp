#include "pch.h"
#include "../../uirs5sem/include/Var3.h"
#include "../../uirs5sem/src/Var3.cpp"

TEST(Var3Test, isCheckSumOK)
{
	Var3 var3;
	int checksum = var3.getCheckSum();
	int csvalue = var3.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}