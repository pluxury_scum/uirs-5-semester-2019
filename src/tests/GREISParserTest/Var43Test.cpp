#include "pch.h"
#include "../../uirs5sem/include/Var43.h"
#include "../../uirs5sem/src/Var43.cpp"

TEST(Var43Test, isCheckSumOK)
{
	Var43 var43;
	int checksum = var43.getCheckSum();
	int csvalue = var43.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}