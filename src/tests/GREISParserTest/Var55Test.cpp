#include "pch.h"
#include "../../uirs5sem/include/Var55.h"
#include "../../uirs5sem/src/Var55.cpp"

TEST(Var55Test, isCheckSumOK)
{
	Var55 var55;
	int checksum = var55.getCheckSum();
	int csvalue = var55.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}