#include "pch.h"
#include "../../uirs5sem/include/Var44.h"
#include "../../uirs5sem/src/Var44.cpp"

TEST(Var44Test, isCheckSumOK)
{
	Var44 var44;
	int checksum = var44.getCheckSum();
	int csvalue = var44.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}