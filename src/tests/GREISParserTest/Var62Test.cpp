#include "pch.h"
#include "../../uirs5sem/include/Var62.h"
#include "../../uirs5sem/src/Var62.cpp"

TEST(Var62Test, isCheckSumOK)
{
	Var62 var62;
	int checksum = var62.getCheckSum();
	int csvalue = var62.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}