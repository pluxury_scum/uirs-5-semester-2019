#include "pch.h"
#include "../../uirs5sem/include/Var51.h"
#include "../../uirs5sem/src/Var51.cpp"

TEST(Var51Test, isCheckSumOK)
{
	Var51 var51;
	int checksum = var51.getCheckSum();
	int csvalue = var51.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}