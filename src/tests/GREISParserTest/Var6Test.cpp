#include "pch.h"
#include "../../uirs5sem/include/Var6.h"
#include "../../uirs5sem/src/Var6.cpp"

TEST(Var6Test, isCheckSumOK)
{
	Var6 var6;
	int checksum = var6.getCheckSum();
	int csvalue = var6.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}