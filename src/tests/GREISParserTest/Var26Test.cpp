#include "pch.h"
#include "../../uirs5sem/include/Var26.h"
#include "../../uirs5sem/src/Var26.cpp"

TEST(Var26Test, isCheckSumOK)
{
	Var26 var26;
	int checksum = var26.getCheckSum();
	int csvalue = var26.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}