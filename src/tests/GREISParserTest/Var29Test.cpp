#include "pch.h"
#include "../../uirs5sem/include/Var29.h"
#include "../../uirs5sem/src/Var29.cpp"

TEST(Var29Test, isCheckSumOK)
{
	Var29 var29;
	int checksum = var29.getCheckSum();
	int csvalue = var29.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}