#include "pch.h"
#include "../../uirs5sem/include/Var46.h"
#include "../../uirs5sem/src/Var46.cpp"

TEST(Var46Test, isCheckSumOK)
{
	Var46 var46;
	int checksum = var46.getCheckSum();
	int csvalue = var46.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}