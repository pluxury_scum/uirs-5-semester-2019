#include "pch.h"
#include "../../uirs5sem/include/Var41.h"
#include "../../uirs5sem/src/Var41.cpp"

TEST(Var41Test, isCheckSumOK)
{
	Var41 var41;
	int checksum = var41.getCheckSum();
	int csvalue = var41.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}