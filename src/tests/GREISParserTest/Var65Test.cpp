#include "pch.h"
#include "../../uirs5sem/include/Var65.h"
#include "../../uirs5sem/src/Var65.cpp"

TEST(Var65Test, isCheckSumOK)
{
	Var65 var65;
	int checksum = var65.getCheckSum();
	int csvalue = var65.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}