#include "pch.h"
#include "../../uirs5sem/include/Var48.h"
#include "../../uirs5sem/src/Var48.cpp"

TEST(Var48Test, isCheckSumOK)
{
	Var48 var48;
	int checksum = var48.getCheckSum();
	int csvalue = var48.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}