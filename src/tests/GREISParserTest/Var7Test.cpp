#include "pch.h"
#include "../../uirs5sem/include/Var7.h"
#include "../../uirs5sem/src/Var7.cpp"

TEST(Var7Test, isCheckSumOK)
{
	Var7 var7;
	int checksum = var7.getCheckSum();
	int csvalue = var7.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}