#include "pch.h"
#include "../../uirs5sem/include/Var50.h"
#include "../../uirs5sem/src/Var50.cpp"

TEST(Var50Test, isCheckSumOK)
{
	Var50 var50;
	int checksum = var50.getCheckSum();
	int csvalue = var50.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}