#include "pch.h"
#include "../../uirs5sem/include/Var2.h"
#include "../../uirs5sem/src/Var2.cpp"

TEST(Var2Test, isCheckSumOK) 
{
	Var2 var2;
	int checksum = var2.getCheckSum();
	int csvalue = var2.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}