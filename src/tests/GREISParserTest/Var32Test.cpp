#include "pch.h"
#include "../../uirs5sem/include/Var32.h"
#include "../../uirs5sem/src/Var32.cpp"

TEST(Var32Test, isCheckSumOK)
{
	Var32 var32;
	int checksum = var32.getCheckSum();
	int csvalue = var32.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}