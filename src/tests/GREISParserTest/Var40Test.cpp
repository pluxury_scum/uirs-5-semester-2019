#include "pch.h"
#include "../../uirs5sem/include/Var40.h"
#include "../../uirs5sem/src/Var40.cpp"

TEST(Var40Test, isCheckSumOK)
{
	Var40 var40;
	int checksum = var40.getCheckSum();
	int csvalue = var40.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}