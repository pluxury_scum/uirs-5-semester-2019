#include "pch.h"
#include "../../uirs5sem/include/Var30.h"
#include "../../uirs5sem/src/Var30.cpp"

TEST(Var30Test, isCheckSumOK)
{
	Var30 var30;
	int checksum = var30.getCheckSum();
	int csvalue = var30.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}