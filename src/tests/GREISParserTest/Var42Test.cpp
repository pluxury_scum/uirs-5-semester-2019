#include "pch.h"
#include "../../uirs5sem/include/Var42.h"
#include "../../uirs5sem/src/Var42.cpp"

TEST(Var42Test, isCheckSumOK)
{
	Var42 var42;
	int checksum = var42.getCheckSum();
	int csvalue = var42.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}