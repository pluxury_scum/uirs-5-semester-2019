#include "pch.h"
#include "../../uirs5sem/include/Var67.h"
#include "../../uirs5sem/src/Var67.cpp"

TEST(Var67Test, isCheckSumOK)
{
	Var67 var67;
	int checksum = var67.getCheckSum();
	int csvalue = var67.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}