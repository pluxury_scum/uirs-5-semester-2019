#include "pch.h"
#include "../../uirs5sem/include/Var28.h"
#include "../../uirs5sem/src/Var28.cpp"

TEST(Var28Test, isCheckSumOK)
{
	Var28 var28;
	int checksum = var28.getCheckSum();
	int csvalue = var28.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}