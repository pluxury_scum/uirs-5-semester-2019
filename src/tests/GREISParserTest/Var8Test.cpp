#include "pch.h"
#include "../../uirs5sem/include/Var8.h"
#include "../../uirs5sem/src/Var8.cpp"

TEST(Var8Test, isCheckSumOK)
{
	Var8 var8;
	int checksum = var8.getCheckSum();
	int csvalue = var8.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}