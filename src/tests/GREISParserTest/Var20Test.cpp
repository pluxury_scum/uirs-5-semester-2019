#include "pch.h"
#include "../../uirs5sem/include/Var20.h"
#include "../../uirs5sem/src/Var20.cpp"

TEST(Var20Test, isCheckSumOK)
{
	Var20 var20;
	int checksum = var20.getCheckSum();
	int csvalue = var20.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}