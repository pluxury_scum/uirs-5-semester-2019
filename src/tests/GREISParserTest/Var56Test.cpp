#include "pch.h"
#include "../../uirs5sem/include/Var56.h"
#include "../../uirs5sem/src/Var56.cpp"

TEST(Var56Test, isCheckSumOK)
{
	Var56 var56;
	int checksum = var56.getCheckSum();
	int csvalue = var56.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}