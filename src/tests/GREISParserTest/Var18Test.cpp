#include "pch.h"
#include "../../uirs5sem/include/Var18.h"
#include "../../uirs5sem/src/Var18.cpp"

TEST(Var18Test, isCheckSumOK)
{
	Var18 var18;
	int checksum = var18.getCheckSum();
	int csvalue = var18.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}