#include "pch.h"
#include "../../uirs5sem/include/Var33.h"
#include "../../uirs5sem/src/Var33.cpp"

TEST(Var33Test, isCheckSumOK)
{
	Var33 var33;
	int checksum = var33.getCheckSum();
	int csvalue = var33.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}