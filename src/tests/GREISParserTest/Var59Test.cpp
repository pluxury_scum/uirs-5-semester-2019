#include "pch.h"
#include "../../uirs5sem/include/Var59.h"
#include "../../uirs5sem/src/Var59.cpp"

TEST(Var59Test, isCheckSumOK)
{
	Var59 var59;
	int checksum = var59.getCheckSum();
	int csvalue = var59.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}