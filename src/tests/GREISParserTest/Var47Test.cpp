#include "pch.h"
#include "../../uirs5sem/include/Var47.h"
#include "../../uirs5sem/src/Var47.cpp"

TEST(Var47Test, isCheckSumOK)
{
	Var47 var47;
	int checksum = var47.getCheckSum();
	int csvalue = var47.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}