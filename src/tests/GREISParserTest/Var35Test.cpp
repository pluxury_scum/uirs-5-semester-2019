#include "pch.h"
#include "../../uirs5sem/include/Var35.h"
#include "../../uirs5sem/src/Var35.cpp"

TEST(Var35Test, isCheckSumOK)
{
	Var35 var35;
	int checksum = var35.getCheckSum();
	int csvalue = var35.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}