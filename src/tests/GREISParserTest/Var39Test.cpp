#include "pch.h"
#include "../../uirs5sem/include/Var39.h"
#include "../../uirs5sem/src/Var39.cpp"

TEST(Var39Test, isCheckSumOK)
{
	Var39 var39;
	int checksum = var39.getCheckSum();
	int csvalue = var39.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}