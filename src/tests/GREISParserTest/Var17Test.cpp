#include "pch.h"
#include "../../uirs5sem/include/Var17.h"
#include "../../uirs5sem/src/Var17.cpp"

TEST(Var17Test, isCheckSumOK)
{
	Var17 var17;
	int checksum = var17.getCheckSum();
	int csvalue = var17.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}