#include "pch.h"
#include "../../uirs5sem/include/Var54.h"
#include "../../uirs5sem/src/Var54.cpp"

TEST(Var54Test, isCheckSumOK)
{
	Var54 var54;
	int checksum = var54.getCheckSum();
	int csvalue = var54.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}