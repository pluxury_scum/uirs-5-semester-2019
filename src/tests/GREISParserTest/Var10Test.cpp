#include "pch.h"
#include "../../uirs5sem/include/Var10.h"
#include "../../uirs5sem/src/Var10.cpp"

TEST(Var10Test, isCheckSumOK)
{
	Var10 var10;
	int checksum = var10.getCheckSum();
	int csvalue = var10.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}