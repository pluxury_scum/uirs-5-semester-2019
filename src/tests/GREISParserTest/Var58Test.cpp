#include "pch.h"
#include "../../uirs5sem/include/Var58.h"
#include "../../uirs5sem/src/Var58.cpp"

TEST(Var58Test, isCheckSumOK)
{
	Var58 var58;
	int checksum = var58.getCheckSum();
	int csvalue = var58.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}