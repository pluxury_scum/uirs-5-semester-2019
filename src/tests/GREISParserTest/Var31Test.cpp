#include "pch.h"
#include "../../uirs5sem/include/Var31.h"
#include "../../uirs5sem/src/Var31.cpp"

TEST(Var31Test, isCheckSumOK)
{
	Var31 var31;
	int checksum = var31.getCheckSum();
	int csvalue = var31.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}