#include "pch.h"
#include "../../uirs5sem/include/Var15.h"
#include "../../uirs5sem/src/Var15.cpp"

TEST(Var15Test, isCheckSumOK)
{
	Var15 var15;
	int checksum = var15.getCheckSum();
	int csvalue = var15.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}