#include "pch.h"
#include "../../uirs5sem/include/Var61.h"
#include "../../uirs5sem/src/Var61.cpp"

TEST(Var61Test, isCheckSumOK)
{
	Var61 var61;
	int checksum = var61.getCheckSum();
	int csvalue = var61.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}