#include "pch.h"
#include "../../uirs5sem/include/Var16.h"
#include "../../uirs5sem/src/Var16.cpp"

TEST(Var16Test, isCheckSumOK)
{
	Var16 var16;
	int checksum = var16.getCheckSum();
	int csvalue = var16.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}