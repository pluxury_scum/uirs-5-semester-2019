#include "pch.h"
#include "../../uirs5sem/include/Var63.h"
#include "../../uirs5sem/src/Var63.cpp"

TEST(Var63Test, isCheckSumOK)
{
	Var63 var63;
	int checksum = var63.getCheckSum();
	int csvalue = var63.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}