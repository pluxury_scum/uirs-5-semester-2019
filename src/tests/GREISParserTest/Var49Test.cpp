#include "pch.h"
#include "../../uirs5sem/include/Var49.h"
#include "../../uirs5sem/src/Var49.cpp"

TEST(Var49Test, isCheckSumOK)
{
	Var49 var49;
	int checksum = var49.getCheckSum();
	int csvalue = var49.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}