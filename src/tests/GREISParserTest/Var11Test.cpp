#include "pch.h"
#include "../../uirs5sem/include/Var11.h"
#include "../../uirs5sem/src/Var11.cpp"

TEST(Var11Test, isCheckSumOK)
{
	Var11 var11;
	int checksum = var11.getCheckSum();
	int csvalue = var11.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}