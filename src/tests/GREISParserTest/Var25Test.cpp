#include "pch.h"
#include "../../uirs5sem/include/Var25.h"
#include "../../uirs5sem/src/Var25.cpp"

TEST(Var25Test, isCheckSumOK)
{
	Var25 var25;
	int checksum = var25.getCheckSum();
	int csvalue = var25.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}