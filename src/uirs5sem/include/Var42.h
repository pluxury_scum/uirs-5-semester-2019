#ifndef VAR42_H
#define VAR42_H

#include "IVar.h"

// Variant 42 - BeiDou Almanac [CA]
class Var42 : public IVar
{
private:
    u1 allValuesArr[52]{};
    u1 checkSum;
    u1 svValue;
    i2 wnaValue_1;
    i2 wnaValue_2;
    i4 toaValue_1;
    i4 toaValue_2;
    i4 toaValue_3;
    i4 toaValue_4;
    u1 healthAValue;
    u1 healthSValue;
    u1 configValue;
    f4 af1Value_1;
    f4 af1Value_2;
    f4 af1Value_3;
    f4 af1Value_4;
    f4 af0Value_1;
    f4 af0Value_2;
    f4 af0Value_3;
    f4 af0Value_4;
    f4 rootAValue_1;
    f4 rootAValue_2;
    f4 rootAValue_3;
    f4 rootAValue_4;
    f4 eccValue_1;
    f4 eccValue_2;
    f4 eccValue_3;
    f4 eccValue_4;
    f4 m0Value_1;
    f4 m0Value_2;
    f4 m0Value_3;
    f4 m0Value_4;
    f4 omega0Value_1;
    f4 omega0Value_2;
    f4 omega0Value_3;
    f4 omega0Value_4;
    f4 argPerValue_1;
    f4 argPerValue_2;
    f4 argPerValue_3;
    f4 argPerValue_4;
    f4 deliValue_1;
    f4 deliValue_2;
    f4 deliValue_3;
    f4 deliValue_4;
    f4 omegaDotValue_1;
    f4 omegaDotValue_2;
    f4 omegaDotValue_3;
    f4 omegaDotValue_4;
    u1 csValue;

public:
    int handleVar() override;
    void handleCS() override;
    void printVar() override;
    int getCheckSum() override;
    int getCsValue() override;
};

#endif // !VAR42_H