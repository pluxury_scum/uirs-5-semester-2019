#ifndef VAR54_H
#define VAR54_H

#include "IVar.h"

// Variant 54 - Accelerometer and Magnetometer Measurements [MA]
class Var54 : public IVar
{
private:
    u1 allValuesArr[43]{};
    u1 checkSum;
    u4 timeValue_1;
    u4 timeValue_2;
    u4 timeValue_3;
    u4 timeValue_4;
    f4 accelerationsValue_1;
    f4 accelerationsValue_2;
    f4 accelerationsValue_3;
    f4 accelerationsValue_4;
    f4 accelerationsValue_5;
    f4 accelerationsValue_6;
    f4 accelerationsValue_7;
    f4 accelerationsValue_8;
    f4 accelerationsValue_9;
    f4 accelerationsValue_10;
    f4 accelerationsValue_11;
    f4 accelerationsValue_12;
    f4 inductionValue_1;
    f4 inductionValue_2;
    f4 inductionValue_3;
    f4 inductionValue_4;
    f4 inductionValue_5;
    f4 inductionValue_6;
    f4 inductionValue_7;
    f4 inductionValue_8;
    f4 inductionValue_9;
    f4 inductionValue_10;
    f4 inductionValue_11;
    f4 inductionValue_12;
    f4 magnitudeValue_1;
    f4 magnitudeValue_2;
    f4 magnitudeValue_3;
    f4 magnitudeValue_4;
    f4 temperatureValue_1;
    f4 temperatureValue_2;
    f4 temperatureValue_3;
    f4 temperatureValue_4;
    u1 calibratedValue;
    u1 csValue;

public:
    int handleVar() override;
    void handleCS() override;
    void printVar() override;
    int getCheckSum() override;
    int getCsValue() override;
};

#endif // !VAR54_H