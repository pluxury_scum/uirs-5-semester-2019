#ifndef VAR30_H
#define VAR30_H

#include "IVar.h"

// Variant 30 - Position in Local Plane [mp]
class Var30 : public IVar
{
private:
    u1 allValuesArr[50]{};
    u1 checkSum;
    f8 nValue_1;
    f8 nValue_2;
    f8 nValue_3;
    f8 nValue_4;
    f8 nValue_5;
    f8 nValue_6;
    f8 nValue_7;
    f8 nValue_8;
    f8 eValue_1;
    f8 eValue_2;
    f8 eValue_3;
    f8 eValue_4;
    f8 eValue_5;
    f8 eValue_6;
    f8 eValue_7;
    f8 eValue_8;
    f8 uValue_1;
    f8 uValue_2;
    f8 uValue_3;
    f8 uValue_4;
    f8 uValue_5;
    f8 uValue_6;
    f8 uValue_7;
    f8 uValue_8;
    f8 sepValue_1;
    f8 sepValue_2;
    f8 sepValue_3;
    f8 sepValue_4;
    f8 sepValue_5;
    f8 sepValue_6;
    f8 sepValue_7;
    f8 sepValue_8;
    f4 pSigmaValue_1;
    f4 pSigmaValue_2;
    f4 pSigmaValue_3;
    f4 pSigmaValue_4;
    u1 solTypeValue;
    u1 gridValue;
    u1 geoidValue;
    u2 prjValue_1;
    u2 prjValue_2;
    u1 gridZoneValue;
    u2 chIssueValue_1;
    u2 chIssueValue_2;
    u1 csValue;

public:
    int handleVar() override;
    void handleCS() override;
    void printVar() override;
    int getCheckSum() override;
    int getCsValue() override;
};

#endif // !VAR30_H