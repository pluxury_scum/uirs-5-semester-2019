#ifndef VAR57_H
#define VAR57_H

#include "IVar.h"

// Variant 57 - Time Offset at PPS Generation Time [YA], [YB]
class Var57 : public IVar
{
private:
    u1 allValuesArr[15]{};
    u1 checkSum;
    f8 offsValue_1;
    f8 offsValue_2;
    f8 offsValue_3;
    f8 offsValue_4;
    f8 offsValue_5;
    f8 offsValue_6;
    f8 offsValue_7;
    f8 offsValue_8;
    u1 timeScaleValue;
    u1 csValue;

public:
    int handleVar() override;
    void handleCS() override;
    void printVar() override;
    int getCheckSum() override;
    int getCsValue() override;
};

#endif // !VAR57_H