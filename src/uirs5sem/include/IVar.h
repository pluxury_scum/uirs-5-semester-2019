#ifndef IVAR_H
#define IVAR_H

#include <iostream>
#include <fstream>

typedef char a1; // a1 = 1 byte
typedef int8_t i1; // i1 = 1 byte
typedef int16_t i2; // i2 = 2 bytes
typedef int32_t i4; // i4 = 4 bytes
typedef uint8_t u1; // u1 = 1 byte
typedef uint16_t u2; // u2 = 2 bytes
typedef uint32_t u4; // u4 = 4 bytes
typedef float f4; // f4 = 4 bytes
typedef double f8; // f8 = 8 bytes
typedef std::string str; // str = (var) byte(s)

// Abstract variant class
class IVar
{
public:
    virtual int handleVar() = 0;
    virtual void handleCS() = 0;
    virtual void printVar() = 0;
    virtual int getCheckSum() = 0;
    virtual int getCsValue() = 0;

    // Computing control sum
    enum
    {
        bits = 8,
        lShift = 2,
        rShift = bits - lShift
    };

#define ROT_LEFT(val) ((val << lShift) | (val >> rShift))

    static u1 cs(u1 const* src, int count);
    virtual ~IVar() {}
};

#endif // !IVAR_H
