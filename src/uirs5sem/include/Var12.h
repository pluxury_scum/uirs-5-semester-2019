#ifndef VAR12_H
#define VAR12_H

#include "IVar.h"

// Variant 12 - SBAS to Receiver Time Offset [wO]
class Var12 : public IVar
{
private:
    u1 allValuesArr[22]{};
    u1 checkSum;
    f8 valValue_1;
    f8 valValue_2;
    f8 valValue_3;
    f8 valValue_4;
    f8 valValue_5;
    f8 valValue_6;
    f8 valValue_7;
    f8 valValue_8;
    f8 svalValue_1;
    f8 svalValue_2;
    f8 svalValue_3;
    f8 svalValue_4;
    f8 svalValue_5;
    f8 svalValue_6;
    f8 svalValue_7;
    f8 svalValue_8;
    u1 csValue;

public:
    int handleVar() override;
    void handleCS() override;
    void printVar() override;
    int getCheckSum() override;
    int getCsValue() override;
};

#endif // !VAR12_H