#ifndef VAR10_H
#define VAR10_H

#include "IVar.h"

// Variant 10 - GLONASS to Receiver Time Offset [NO]
class Var10 : public IVar
{
private:
    u1 allValuesArr[22]{};
    u1 checkSum;
    f8 valValue_1;
    f8 valValue_2;
    f8 valValue_3;
    f8 valValue_4;
    f8 valValue_5;
    f8 valValue_6;
    f8 valValue_7;
    f8 valValue_8;
    f8 svalValue_1;
    f8 svalValue_2;
    f8 svalValue_3;
    f8 svalValue_4;
    f8 svalValue_5;
    f8 svalValue_6;
    f8 svalValue_7;
    f8 svalValue_8;
    u1 csValue;

public:
    int handleVar() override;
    void handleCS() override;
    void printVar() override;
    int getCheckSum() override;
    int getCsValue() override;
};

#endif // !VAR10_H