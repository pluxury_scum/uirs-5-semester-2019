#ifndef VAR38_H
#define VAR38_H

#include "IVar.h"

// Variant 38 - Time of Continuous Position Computation [PT]
class Var38 : public IVar
{
private:
    u1 allValuesArr[10]{};
    u1 checkSum;
    u4 ptValue_1;
    u4 ptValue_2;
    u4 ptValue_3;
    u4 ptValue_4;
    u1 csValue;

public:
    int handleVar() override;
    void handleCS() override;
    void printVar() override;
    int getCheckSum() override;
    int getCsValue() override;
};

#endif // !VAR38_H