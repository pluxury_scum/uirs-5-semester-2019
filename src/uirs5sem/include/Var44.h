#ifndef VAR44_H
#define VAR44_H

#include "IVar.h"

// Variant 44 - GLONASS Almanac [NA]
class Var44 : public IVar
{
private:
    u1 allValuesArr[57]{};
    u1 checkSum;
    u1 svValue;
    i1 frqNumValue;
    i2 dnaValue_1;
    i2 dnaValue_2;
    f4 tlamValue_1;
    f4 tlamValue_2;
    f4 tlamValue_3;
    f4 tlamValue_4;
    u1 flagsValue;
    f4 tauNValue_1;
    f4 tauNValue_2;
    f4 tauNValue_3;
    f4 tauNValue_4;
    f8 tauSysValue_1;
    f8 tauSysValue_2;
    f8 tauSysValue_3;
    f8 tauSysValue_4;
    f8 tauSysValue_5;
    f8 tauSysValue_6;
    f8 tauSysValue_7;
    f8 tauSysValue_8;
    f4 eccValue_1;
    f4 eccValue_2;
    f4 eccValue_3;
    f4 eccValue_4;
    f4 lambdaValue_1;
    f4 lambdaValue_2;
    f4 lambdaValue_3;
    f4 lambdaValue_4;
    f4 argPerValue_1;
    f4 argPerValue_2;
    f4 argPerValue_3;
    f4 argPerValue_4;
    f4 delTValue_1;
    f4 delTValue_2;
    f4 delTValue_3;
    f4 delTValue_4;
    f4 delTdtValue_1;
    f4 delTdtValue_2;
    f4 delTdtValue_3;
    f4 delTdtValue_4;
    f4 deliValue_1;
    f4 deliValue_2;
    f4 deliValue_3;
    f4 deliValue_4;
    u1 n4Value;
    u1 reservedValue;
    f4 gammaNValue_1;
    f4 gammaNValue_2;
    f4 gammaNValue_3;
    f4 gammaNValue_4;
    u1 csValue;

public:
    int handleVar() override;
    void handleCS() override;
    void printVar() override;
    int getCheckSum() override;
    int getCsValue() override;
};

#endif // !VAR44_H