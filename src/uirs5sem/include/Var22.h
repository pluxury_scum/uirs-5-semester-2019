#ifndef VAR22_H
#define VAR22_H

#include "IVar.h"

// Variant 22 - GLONASS UTC and GPS Time Parameters [NU]
class Var22 : public IVar
{
private:
    u1 allValuesArr[32]{};
    u1 checkSum;
    f8 tauSysValue_1;
    f8 tauSysValue_2;
    f8 tauSysValue_3;
    f8 tauSysValue_4;
    f8 tauSysValue_5;
    f8 tauSysValue_6;
    f8 tauSysValue_7;
    f8 tauSysValue_8;
    f4 tauGpsValue_1;
    f4 tauGpsValue_2;
    f4 tauGpsValue_3;
    f4 tauGpsValue_4;
    f4 B1Value_1;
    f4 B1Value_2;
    f4 B1Value_3;
    f4 B1Value_4;
    f4 B2Value_1;
    f4 B2Value_2;
    f4 B2Value_3;
    f4 B2Value_4;
    u1 KPValue;
    u1 N4Value;
    i2 DnValue_1;
    i2 DnValue_2;
    i2 NtValue_1;
    i2 NtValue_2;
    u1 csValue;

public:
    int handleVar() override;
    void handleCS() override;
    void printVar() override;
    int getCheckSum() override;
    int getCsValue() override;
};

#endif // !VAR22_H