#ifndef VAR5_H
#define VAR5_H

#include "IVar.h"

// Variant 5 - Derivative of Receiver Time Offset [DO]
class Var5 : public IVar
{
private:
    u1 allValuesArr[14]{};
    u1 checkSum;
    f4 valValue_1;
    f4 valValue_2;
    f4 valValue_3;
    f4 valValue_4;
    f4 svalValue_1;
    f4 svalValue_2;
    f4 svalValue_3;
    f4 svalValue_4;
    u1 csValue;

public:
    int handleVar() override;
    void handleCS() override;
    void printVar() override;
    int getCheckSum() override;
    int getCsValue() override;
};

#endif // !VAR5_H