#ifndef VAR35_H
#define VAR35_H

#include "IVar.h"

// Variant 35 - Baseline [BL]
class Var35 : public IVar
{
private:
    u1 allValuesArr[39]{};
    u1 checkSum;
    f8 xValue_1;
    f8 xValue_2;
    f8 xValue_3;
    f8 xValue_4;
    f8 xValue_5;
    f8 xValue_6;
    f8 xValue_7;
    f8 xValue_8;
    f8 yValue_1;
    f8 yValue_2;
    f8 yValue_3;
    f8 yValue_4;
    f8 yValue_5;
    f8 yValue_6;
    f8 yValue_7;
    f8 yValue_8;
    f8 zValue_1;
    f8 zValue_2;
    f8 zValue_3;
    f8 zValue_4;
    f8 zValue_5;
    f8 zValue_6;
    f8 zValue_7;
    f8 zValue_8;
    f4 sigmaValue_1;
    f4 sigmaValue_2;
    f4 sigmaValue_3;
    f4 sigmaValue_4;
    u1 solTypeValue;
    i4 timeValue_1;
    i4 timeValue_2;
    i4 timeValue_3;
    i4 timeValue_4;
    u1 csValue;

public:
    int handleVar() override;
    void handleCS() override;
    void printVar() override;
    int getCheckSum() override;
    int getCsValue() override;
};

#endif // !VAR35_H