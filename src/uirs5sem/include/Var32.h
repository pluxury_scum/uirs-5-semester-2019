#ifndef VAR32_H
#define VAR32_H

#include "IVar.h"

// Variant 32 - Dilution of Precision [DP]
class Var32 : public IVar
{
private:
    u1 allValuesArr[23]{};
    u1 checkSum;
    f4 hdopValue_1;
    f4 hdopValue_2;
    f4 hdopValue_3;
    f4 hdopValue_4;
    f4 vdopValue_1;
    f4 vdopValue_2;
    f4 vdopValue_3;
    f4 vdopValue_4;
    f4 tdopValue_1;
    f4 tdopValue_2;
    f4 tdopValue_3;
    f4 tdopValue_4;
    u1 solTypeValue;
    f4 edopValue_1;
    f4 edopValue_2;
    f4 edopValue_3;
    f4 edopValue_4;
    u1 csValue;

public:
    int handleVar() override;
    void handleCS() override;
    void printVar() override;
    int getCheckSum() override;
    int getCsValue() override;
};

#endif // !VAR32_H