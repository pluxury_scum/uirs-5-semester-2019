#ifndef VAR3_H
#define VAR3_H

#include "IVar.h"

// Variant 3 - Receiver Date [RD]
class Var3 : public IVar
{
private:
    u1 allValuesArr[11]{};
    u1 checkSum;
    u2 yearValue_1;
    u2 yearValue_2;
    u1 monthValue;
    u1 dayValue;
    u1 baseValue;
    u1 csValue;

public:
    int handleVar() override;
    void handleCS() override;
    void printVar() override;
    int getCheckSum() override;
    int getCsValue() override;
};

#endif // !VAR3_H