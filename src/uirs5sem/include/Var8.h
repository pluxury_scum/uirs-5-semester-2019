#ifndef VAR8_H
#define VAR8_H

#include "IVar.h"

// Variant 8 - GPS to Receiver Time Offset [GO]
class Var8 : public IVar
{
private:
    u1 allValuesArr[22]{};
    u1 checkSum;
    f8 valValue_1;
    f8 valValue_2;
    f8 valValue_3;
    f8 valValue_4;
    f8 valValue_5;
    f8 valValue_6;
    f8 valValue_7;
    f8 valValue_8;
    f8 svalValue_1;
    f8 svalValue_2;
    f8 svalValue_3;
    f8 svalValue_4;
    f8 svalValue_5;
    f8 svalValue_6;
    f8 svalValue_7;
    f8 svalValue_8;
    u1 csValue;

public:
    int handleVar() override;
    void handleCS() override;
    void printVar() override;
    int getCheckSum() override;
    int getCsValue() override;
};

#endif // !VAR8_H