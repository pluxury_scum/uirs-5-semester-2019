#ifndef VAR25_H
#define VAR25_H

#include "IVar.h"

// Variant 25 - Cartesian Position in Specific System [Po]
class Var25 : public IVar
{
private:
    u1 allValuesArr[43]{};
    u1 checkSum;
    f8 xValue_1;
    f8 xValue_2;
    f8 xValue_3;
    f8 xValue_4;
    f8 xValue_5;
    f8 xValue_6;
    f8 xValue_7;
    f8 xValue_8;
    f8 yValue_1;
    f8 yValue_2;
    f8 yValue_3;
    f8 yValue_4;
    f8 yValue_5;
    f8 yValue_6;
    f8 yValue_7;
    f8 yValue_8;
    f8 zValue_1;
    f8 zValue_2;
    f8 zValue_3;
    f8 zValue_4;
    f8 zValue_5;
    f8 zValue_6;
    f8 zValue_7;
    f8 zValue_8;
    f4 pSigmaValue_1;
    f4 pSigmaValue_2;
    f4 pSigmaValue_3;
    f4 pSigmaValue_4;
    u1 solTypeValue;
    u1 systemValue;
    a1 crsCodeValue_1;
    a1 crsCodeValue_2;
    a1 crsCodeValue_3;
    a1 crsCodeValue_4;
    a1 crsCodeValue_5;
    u2 chIssueValue_1;
    u2 chIssueValue_2;
    u1 csValue;

public:
    int handleVar() override;
    void handleCS() override;
    void printVar() override;
    int getCheckSum() override;
    int getCsValue() override;
};

#endif // !VAR25_H