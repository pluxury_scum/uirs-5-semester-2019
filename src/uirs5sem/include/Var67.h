#ifndef VAR67_H
#define VAR67_H

#include "IVar.h"

// Variant 67 - Oscillator Offset [OO]
class Var67 : public IVar
{
private:
    u1 allValuesArr[10]{};
    u1 checkSum;
    f4 valValue_1;
    f4 valValue_2;
    f4 valValue_3;
    f4 valValue_4;
    u1 csValue;

public:
    int handleVar() override;
    void handleCS() override;
    void printVar() override;
    int getCheckSum() override;
    int getCsValue() override;
};

#endif // !VAR67_H