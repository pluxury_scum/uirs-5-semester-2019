#ifndef VAR53_H
#define VAR53_H

#include "IVar.h"

// Variant 53 - Inertial Measurements [IM]
class Var53 : public IVar
{
private:
    u1 allValuesArr[30]{};
    u1 checkSum;
    f4 accelerationsValue_1;
    f4 accelerationsValue_2;
    f4 accelerationsValue_3;
    f4 accelerationsValue_4;
    f4 accelerationsValue_5;
    f4 accelerationsValue_6;
    f4 accelerationsValue_7;
    f4 accelerationsValue_8;
    f4 accelerationsValue_9;
    f4 accelerationsValue_10;
    f4 accelerationsValue_11;
    f4 accelerationsValue_12;
    f4 angularVelocitiesValue_1;
    f4 angularVelocitiesValue_2;
    f4 angularVelocitiesValue_3;
    f4 angularVelocitiesValue_4;
    f4 angularVelocitiesValue_5;
    f4 angularVelocitiesValue_6;
    f4 angularVelocitiesValue_7;
    f4 angularVelocitiesValue_8;
    f4 angularVelocitiesValue_9;
    f4 angularVelocitiesValue_10;
    f4 angularVelocitiesValue_11;
    f4 angularVelocitiesValue_12;
    u1 csValue;

public:
    int handleVar() override;
    void handleCS() override;
    void printVar() override;
    int getCheckSum() override;
    int getCsValue() override;
};

#endif // !VAR53_H