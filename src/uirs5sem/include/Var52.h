#ifndef VAR52_H
#define VAR52_H

#include "IVar.h"

// Variant 52 - Angular Velocities [AV]
class Var52 : public IVar
{
private:
    u1 allValuesArr[27]{};
    u1 checkSum;
    u4 timeValue_1;
    u4 timeValue_2;
    u4 timeValue_3;
    u4 timeValue_4;
    f4 xValue_1;
    f4 xValue_2;
    f4 xValue_3;
    f4 xValue_4;
    f4 yValue_1;
    f4 yValue_2;
    f4 yValue_3;
    f4 yValue_4;
    f4 zValue_1;
    f4 zValue_2;
    f4 zValue_3;
    f4 zValue_4;
    f4 rmsValue_1;
    f4 rmsValue_2;
    f4 rmsValue_3;
    f4 rmsValue_4;
    u1 flagsValue;
    u1 csValue;

public:
    int handleVar() override;
    void handleCS() override;
    void printVar() override;
    int getCheckSum() override;
    int getCsValue() override;
};

#endif // !VAR52_H