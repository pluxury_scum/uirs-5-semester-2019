#ifndef VAR61_H
#define VAR61_H

#include "IVar.h"

// Variant 61 - BeiDou Ionospheric Parameters [CI]
class Var61 : public IVar
{
private:
    u1 allValuesArr[44]{};
    u1 checkSum;
    u4 totValue_1;
    u4 totValue_2;
    u4 totValue_3;
    u4 totValue_4;
    u2 wnValue_1;
    u2 wnValue_2;
    f4 alpha0Value_1;
    f4 alpha0Value_2;
    f4 alpha0Value_3;
    f4 alpha0Value_4;
    f4 alpha1Value_1;
    f4 alpha1Value_2;
    f4 alpha1Value_3;
    f4 alpha1Value_4;
    f4 alpha2Value_1;
    f4 alpha2Value_2;
    f4 alpha2Value_3;
    f4 alpha2Value_4;
    f4 alpha3Value_1;
    f4 alpha3Value_2;
    f4 alpha3Value_3;
    f4 alpha3Value_4;
    f4 beta0Value_1;
    f4 beta0Value_2;
    f4 beta0Value_3;
    f4 beta0Value_4;
    f4 beta1Value_1;
    f4 beta1Value_2;
    f4 beta1Value_3;
    f4 beta1Value_4;
    f4 beta2Value_1;
    f4 beta2Value_2;
    f4 beta2Value_3;
    f4 beta2Value_4;
    f4 beta3Value_1;
    f4 beta3Value_2;
    f4 beta3Value_3;
    f4 beta3Value_4;
    u1 csValue;

public:
    int handleVar() override;
    void handleCS() override;
    void printVar() override;
    int getCheckSum() override;
    int getCsValue() override;
};

#endif // !VAR61_H