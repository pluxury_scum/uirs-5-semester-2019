#ifndef VAR45_H
#define VAR45_H

#include "IVar.h"

// Variant 45 - SBAS Almanac [WA]
class Var45 : public IVar
{
private:
    u1 allValuesArr[56]{};
    u1 checkSum;
    u1 waasPrnValue;
    u1 gpsPrnValue;
    u1 idValue;
    u1 healthSValue;
    u4 todValue_1;
    u4 todValue_2;
    u4 todValue_3;
    u4 todValue_4;
    f8 xgValue_1;
    f8 xgValue_2;
    f8 xgValue_3;
    f8 xgValue_4;
    f8 xgValue_5;
    f8 xgValue_6;
    f8 xgValue_7;
    f8 xgValue_8;
    f8 ygValue_1;
    f8 ygValue_2;
    f8 ygValue_3;
    f8 ygValue_4;
    f8 ygValue_5;
    f8 ygValue_6;
    f8 ygValue_7;
    f8 ygValue_8;
    f8 zgValue_1;
    f8 zgValue_2;
    f8 zgValue_3;
    f8 zgValue_4;
    f8 zgValue_5;
    f8 zgValue_6;
    f8 zgValue_7;
    f8 zgValue_8;
    f4 vxgValue_1;
    f4 vxgValue_2;
    f4 vxgValue_3;
    f4 vxgValue_4;
    f4 vygValue_1;
    f4 vygValue_2;
    f4 vygValue_3;
    f4 vygValue_4;
    f4 vzgValue_1;
    f4 vzgValue_2;
    f4 vzgValue_3;
    f4 vzgValue_4;
    u4 towValue_1;
    u4 towValue_2;
    u4 towValue_3;
    u4 towValue_4;
    u2 wnValue_1;
    u2 wnValue_2;
    u1 csValue;

public:
    int handleVar() override;
    void handleCS() override;
    void printVar() override;
    int getCheckSum() override;
    int getCsValue() override;
};

#endif // !VAR45_H