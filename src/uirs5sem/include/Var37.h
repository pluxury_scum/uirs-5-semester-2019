#ifndef VAR37_H
#define VAR37_H

#include "IVar.h"

// Variant 37 - Attitude Full Rotation Matrix [mR]
class Var37 : public IVar
{
private:
    u1 allValuesArr[42]{};
    u1 checkSum;
    f4 q00Value_1;
    f4 q00Value_2;
    f4 q00Value_3;
    f4 q00Value_4;
    f4 q01Value_1;
    f4 q01Value_2;
    f4 q01Value_3;
    f4 q01Value_4;
    f4 q02Value_1;
    f4 q02Value_2;
    f4 q02Value_3;
    f4 q02Value_4;
    f4 q10Value_1;
    f4 q10Value_2;
    f4 q10Value_3;
    f4 q10Value_4;
    f4 q11Value_1;
    f4 q11Value_2;
    f4 q11Value_3;
    f4 q11Value_4;
    f4 q12Value_1;
    f4 q12Value_2;
    f4 q12Value_3;
    f4 q12Value_4;
    f4 q20Value_1;
    f4 q20Value_2;
    f4 q20Value_3;
    f4 q20Value_4;
    f4 q21Value_1;
    f4 q21Value_2;
    f4 q21Value_3;
    f4 q21Value_4;
    f4 q22Value_1;
    f4 q22Value_2;
    f4 q22Value_3;
    f4 q22Value_4;
    u1 csValue;

public:
    int handleVar() override;
    void handleCS() override;
    void printVar() override;
    int getCheckSum() override;
    int getCsValue() override;
};

#endif // !VAR37_H