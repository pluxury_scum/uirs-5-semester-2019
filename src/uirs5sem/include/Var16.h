#ifndef VAR16_H
#define VAR16_H

#include "IVar.h"

// Variant 16 - GPS UTC Time Parameters [UO]
class Var16 : public IVar
{
private:
    u1 allValuesArr[29]{};
    u1 checkSum;
    f8 a0Value_1;
    f8 a0Value_2;
    f8 a0Value_3;
    f8 a0Value_4;
    f8 a0Value_5;
    f8 a0Value_6;
    f8 a0Value_7;
    f8 a0Value_8;
    f4 a1Value_1;
    f4 a1Value_2;
    f4 a1Value_3;
    f4 a1Value_4;
    u4 totValue_1;
    u4 totValue_2;
    u4 totValue_3;
    u4 totValue_4;
    u2 wntValue_1;
    u2 wntValue_2;
    i1 dtlsValue;
    u1 dnValue;
    u2 wnlsfValue_1;
    u2 wnlsfValue_2;
    i1 dtlsfValue;
    u1 csValue;

public:
    int handleVar() override;
    void handleCS() override;
    void printVar() override;
    int getCheckSum() override;
    int getCsValue() override;
};

#endif // !VAR16_H