#ifndef VAR63_H
#define VAR63_H

#include "IVar.h"

// Variant 63 - Message Output Latency [LT]
class Var63 : public IVar
{
private:
    u1 allValuesArr[7]{};
    u1 checkSum;
    u1 ltValue;
    u1 csValue;

public:
    int handleVar() override;
    void handleCS() override;
    void printVar() override;
    int getCheckSum() override;
    int getCsValue() override;
};

#endif // !VAR63_H