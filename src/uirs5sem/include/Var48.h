#ifndef VAR48_H
#define VAR48_H

#include "IVar.h"

// Variant 48 - Modem Spectrum [ms]
class Var48 : public IVar
{
private:
    u1 allValuesArr[14]{};
    u1 checkSum;
    i4 frqValue_1;
    i4 frqValue_2;
    i4 frqValue_3;
    i4 frqValue_4;
    i4 pwrValue_1;
    i4 pwrValue_2;
    i4 pwrValue_3;
    i4 pwrValue_4;
    u1 csValue;

public:
    int handleVar() override;
    void handleCS() override;
    void printVar() override;
    int getCheckSum() override;
    int getCsValue() override;
};

#endif // !VAR48_H