#ifndef VAR28_H
#define VAR28_H

#include "IVar.h"

// Variant 28 - Geodetic Position in Specific System [Pg]
class Var28 : public IVar
{
private:
    u1 allValuesArr[43]{};
    u1 checkSum;
    f8 latValue_1;
    f8 latValue_2;
    f8 latValue_3;
    f8 latValue_4;
    f8 latValue_5;
    f8 latValue_6;
    f8 latValue_7;
    f8 latValue_8;
    f8 lonValue_1;
    f8 lonValue_2;
    f8 lonValue_3;
    f8 lonValue_4;
    f8 lonValue_5;
    f8 lonValue_6;
    f8 lonValue_7;
    f8 lonValue_8;
    f8 altValue_1;
    f8 altValue_2;
    f8 altValue_3;
    f8 altValue_4;
    f8 altValue_5;
    f8 altValue_6;
    f8 altValue_7;
    f8 altValue_8;
    f4 pSigmaValue_1;
    f4 pSigmaValue_2;
    f4 pSigmaValue_3;
    f4 pSigmaValue_4;
    u1 solTypeValue;
    u1 systemValue;
    a1 crsCodeValue_1;
    a1 crsCodeValue_2;
    a1 crsCodeValue_3;
    a1 crsCodeValue_4;
    a1 crsCodeValue_5;
    u2 chIssueValue_1;
    u2 chIssueValue_2;
    u1 csValue;

public:
    int handleVar() override;
    void handleCS() override;
    void printVar() override;
    int getCheckSum() override;
    int getCsValue() override;
};

#endif // !VAR28_H