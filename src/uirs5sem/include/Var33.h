#ifndef VAR33_H
#define VAR33_H

#include "IVar.h"

// Variant 33 - Position Covariance Matrix [SP]
class Var33 : public IVar
{
private:
    u1 allValuesArr[47]{};
    u1 checkSum;
    f4 xxValue_1;
    f4 xxValue_2;
    f4 xxValue_3;
    f4 xxValue_4;
    f4 yyValue_1;
    f4 yyValue_2;
    f4 yyValue_3;
    f4 yyValue_4;
    f4 zzValue_1;
    f4 zzValue_2;
    f4 zzValue_3;
    f4 zzValue_4;
    f4 ttValue_1;
    f4 ttValue_2;
    f4 ttValue_3;
    f4 ttValue_4;
    f4 xyValue_1;
    f4 xyValue_2;
    f4 xyValue_3;
    f4 xyValue_4;
    f4 xzValue_1;
    f4 xzValue_2;
    f4 xzValue_3;
    f4 xzValue_4;
    f4 xtValue_1;
    f4 xtValue_2;
    f4 xtValue_3;
    f4 xtValue_4;
    f4 yzValue_1;
    f4 yzValue_2;
    f4 yzValue_3;
    f4 yzValue_4;
    f4 ytValue_1;
    f4 ytValue_2;
    f4 ytValue_3;
    f4 ytValue_4;
    f4 ztValue_1;
    f4 ztValue_2;
    f4 ztValue_3;
    f4 ztValue_4;
    u1 solTypeValue;
    u1 csValue;

public:
    int handleVar() override;
    void handleCS() override;
    void printVar() override;
    int getCheckSum() override;
    int getCsValue() override;
};

#endif // !VAR33_H