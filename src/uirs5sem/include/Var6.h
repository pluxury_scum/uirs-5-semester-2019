#ifndef VAR6_H
#define VAR6_H

#include "IVar.h"

// Variant 6 - Rough Accuracy of Time Approximation [BP]
class Var6 : public IVar
{
private:
    u1 allValuesArr[10]{};
    u1 checkSum;
    f4 accValue_1;
    f4 accValue_2;
    f4 accValue_3;
    f4 accValue_4;
    u1 csValue;

public:
    int handleVar() override;
    void handleCS() override;
    void printVar() override;
    int getCheckSum() override;
    int getCsValue() override;
};

#endif // !VAR6_H