#ifndef VAR64_H
#define VAR64_H

#include "IVar.h"

// Variant 64 - Base Station Information [BI]
class Var64 : public IVar
{
private:
    u1 allValuesArr[33]{};
    u1 checkSum;
    f8 xValue_1;
    f8 xValue_2;
    f8 xValue_3;
    f8 xValue_4;
    f8 xValue_5;
    f8 xValue_6;
    f8 xValue_7;
    f8 xValue_8;
    f8 yValue_1;
    f8 yValue_2;
    f8 yValue_3;
    f8 yValue_4;
    f8 yValue_5;
    f8 yValue_6;
    f8 yValue_7;
    f8 yValue_8;
    f8 zValue_1;
    f8 zValue_2;
    f8 zValue_3;
    f8 zValue_4;
    f8 zValue_5;
    f8 zValue_6;
    f8 zValue_7;
    f8 zValue_8;
    u2 idValue_1;
    u2 idValue_2;
    u1 solTypeValue;
    u1 csValue;

public:
    int handleVar() override;
    void handleCS() override;
    void printVar() override;
    int getCheckSum() override;
    int getCsValue() override;
};

#endif // !VAR64_H