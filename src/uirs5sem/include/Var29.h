#ifndef VAR29_H
#define VAR29_H

#include "IVar.h"

// Variant 29 - Geodetic Velocity [VG]
class Var29 : public IVar
{
private:
    u1 allValuesArr[23]{};
    u1 checkSum;
    f4 latValue_1;
    f4 latValue_2;
    f4 latValue_3;
    f4 latValue_4;
    f4 lonValue_1;
    f4 lonValue_2;
    f4 lonValue_3;
    f4 lonValue_4;
    f4 altValue_1;
    f4 altValue_2;
    f4 altValue_3;
    f4 altValue_4;
    f4 vSigmaValue_1;
    f4 vSigmaValue_2;
    f4 vSigmaValue_3;
    f4 vSigmaValue_4;
    u1 solTypeValue;
    u1 csValue;

public:
    int handleVar() override;
    void handleCS() override;
    void printVar() override;
    int getCheckSum() override;
    int getCsValue() override;
};

#endif // !VAR29_H