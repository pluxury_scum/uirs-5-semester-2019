#ifndef VAR55_H
#define VAR55_H

#include "IVar.h"

// Variant 55 - External Event [XA], [XB]
class Var55 : public IVar
{
private:
    u1 allValuesArr[15]{};
    u1 checkSum;
    i4 msValue_1;
    i4 msValue_2;
    i4 msValue_3;
    i4 msValue_4;
    i4 nsValue_1;
    i4 nsValue_2;
    i4 nsValue_3;
    i4 nsValue_4;
    u1 timeScaleValue;
    u1 csValue;

public:
    int handleVar() override;
    void handleCS() override;
    void printVar() override;
    int getCheckSum() override;
    int getCsValue() override;
};

#endif // !VAR55_H