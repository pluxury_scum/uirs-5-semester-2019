#ifndef VAR9_H
#define VAR9_H

#include "IVar.h"

// Variant 9 - GLONASS Time [NT]
class Var9 : public IVar
{
private:
    u1 allValuesArr[12]{};
    u1 checkSum;
    u4 todValue_1;
    u4 todValue_2;
    u4 todValue_3;
    u4 todValue_4;
    u2 dnValue_1;
    u2 dnValue_2;
    u1 csValue;

public:
    int handleVar() override;
    void handleCS() override;
    void printVar() override;
    int getCheckSum() override;
    int getCsValue() override;
};

#endif // !VAR9_H