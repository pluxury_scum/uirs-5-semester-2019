#ifndef VAR23_H
#define VAR23_H

#include "IVar.h"

// Variant 23 - Solution Time-Tag [ST]
class Var23 : public IVar
{
private:
    u1 allValuesArr[11]{};
    u1 checkSum;
    u4 timeValue_1;
    u4 timeValue_2;
    u4 timeValue_3;
    u4 timeValue_4;
    u1 solTypeValue;
    u1 csValue;

public:
    int handleVar() override;
    void handleCS() override;
    void printVar() override;
    int getCheckSum() override;
    int getCsValue() override;
};

#endif // !VAR23_H