#ifndef VAR7_H
#define VAR7_H

#include "IVar.h"

// Variant 7 - GPS Time [GT]
class Var7 : public IVar
{
private:
    u1 allValuesArr[12]{};
    u1 checkSum;
    u4 towValue_1;
    u4 towValue_2;
    u4 towValue_3;
    u4 towValue_4;
    u2 wnValue_1;
    u2 wnValue_2;
    u1 csValue;

public:
    int handleVar() override;
    void handleCS() override;
    void printVar() override;
    int getCheckSum() override;
    int getCsValue() override;
};

#endif // !VAR7_H