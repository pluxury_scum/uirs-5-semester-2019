#ifndef VARFACTORY36_H
#define VARFACTORY36_H

#include "IVarFactory.h"
#include "../Var36.h"

class VarFactory36 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY36_H