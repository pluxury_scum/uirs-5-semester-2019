#ifndef VARFACTORY17_H
#define VARFACTORY17_H

#include "IVarFactory.h"
#include "../Var17.h"

class VarFactory17 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY17_H