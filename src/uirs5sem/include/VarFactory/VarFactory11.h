#ifndef VARFACTORY11_H
#define VARFACTORY11_H

#include "IVarFactory.h"
#include "../Var11.h"

class VarFactory11 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY11_H