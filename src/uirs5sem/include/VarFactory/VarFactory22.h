#ifndef VARFACTORY22_H
#define VARFACTORY22_H

#include "IVarFactory.h"
#include "../Var22.h"

class VarFactory22 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY22_H