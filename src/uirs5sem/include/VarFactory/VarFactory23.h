#ifndef VARFACTORY23_H
#define VARFACTORY23_H

#include "IVarFactory.h"
#include "../Var23.h"

class VarFactory23 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY23_H