#ifndef VARFACTORY48_H
#define VARFACTORY48_H

#include "IVarFactory.h"
#include "../Var48.h"

class VarFactory48 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY48_H