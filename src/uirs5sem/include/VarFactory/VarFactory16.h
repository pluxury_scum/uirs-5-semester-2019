#ifndef VARFACTORY16_H
#define VARFACTORY16_H

#include "IVarFactory.h"
#include "../Var16.h"

class VarFactory16 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY16_H