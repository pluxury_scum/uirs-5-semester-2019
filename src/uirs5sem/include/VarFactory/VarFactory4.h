#ifndef VARFACTORY4_H
#define VARFACTORY4_H

#include "IVarFactory.h"
#include "../Var4.h"

class VarFactory4 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY4_H