#ifndef VARFACTORY7_H
#define VARFACTORY7_H

#include "IVarFactory.h"
#include "../Var7.h"

class VarFactory7 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY7_H