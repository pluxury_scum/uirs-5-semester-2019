#ifndef VARFACTORY68_H
#define VARFACTORY68_H

#include "IVarFactory.h"
#include "../Var68.h"

class VarFactory68 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY68_H