#ifndef VARFACTORY38_H
#define VARFACTORY38_H

#include "IVarFactory.h"
#include "../Var38.h"

class VarFactory38 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY38_H