#ifndef VARFACTORY66_H
#define VARFACTORY66_H

#include "IVarFactory.h"
#include "../Var66.h"

class VarFactory66 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY66_H