#ifndef VARFACTORY29_H
#define VARFACTORY29_H

#include "IVarFactory.h"
#include "../Var29.h"

class VarFactory29 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY29_H