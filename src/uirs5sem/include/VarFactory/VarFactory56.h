#ifndef VARFACTORY56_H
#define VARFACTORY56_H

#include "IVarFactory.h"
#include "../Var56.h"

class VarFactory56 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY56_H