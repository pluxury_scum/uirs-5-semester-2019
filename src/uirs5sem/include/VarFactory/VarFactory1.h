#ifndef VARFACTORY1_H
#define VARFACTORY1_H

#include "IVarFactory.h"
#include "../Var1.h"

class VarFactory1 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY1_H