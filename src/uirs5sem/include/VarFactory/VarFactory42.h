#ifndef VARFACTORY42_H
#define VARFACTORY42_H

#include "IVarFactory.h"
#include "../Var42.h"

class VarFactory42 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY42_H