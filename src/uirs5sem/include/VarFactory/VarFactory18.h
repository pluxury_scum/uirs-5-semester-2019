#ifndef VARFACTORY18_H
#define VARFACTORY18_H

#include "IVarFactory.h"
#include "../Var18.h"

class VarFactory18 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY18_H