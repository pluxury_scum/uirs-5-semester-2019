#ifndef VARFACTORY3_H
#define VARFACTORY3_H

#include "IVarFactory.h"
#include "../Var3.h"

class VarFactory3 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY3_H