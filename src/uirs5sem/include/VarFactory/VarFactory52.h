#ifndef VARFACTORY52_H
#define VARFACTORY52_H

#include "IVarFactory.h"
#include "../Var52.h"

class VarFactory52 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY52_H