#ifndef VARFACTORY40_H
#define VARFACTORY40_H

#include "IVarFactory.h"
#include "../Var40.h"

class VarFactory40 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY40_H