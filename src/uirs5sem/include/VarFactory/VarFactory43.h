#ifndef VARFACTORY43_H
#define VARFACTORY43_H

#include "IVarFactory.h"
#include "../Var43.h"

class VarFactory43 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY43_H