#ifndef VARFACTORY37_H
#define VARFACTORY37_H

#include "IVarFactory.h"
#include "../Var37.h"

class VarFactory37 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY37_H