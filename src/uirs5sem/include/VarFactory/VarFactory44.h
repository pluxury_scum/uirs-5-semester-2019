#ifndef VARFACTORY44_H
#define VARFACTORY44_H

#include "IVarFactory.h"
#include "../Var44.h"

class VarFactory44 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY44_H