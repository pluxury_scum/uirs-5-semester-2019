#ifndef VARFACTORY6_H
#define VARFACTORY6_H

#include "IVarFactory.h"
#include "../Var6.h"

class VarFactory6 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY6_H