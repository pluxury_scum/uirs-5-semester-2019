#ifndef VARFACTORY26_H
#define VARFACTORY26_H

#include "IVarFactory.h"
#include "../Var26.h"

class VarFactory26 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY26_H