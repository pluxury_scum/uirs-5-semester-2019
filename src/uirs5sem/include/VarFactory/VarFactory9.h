#ifndef VARFACTORY9_H
#define VARFACTORY9_H

#include "IVarFactory.h"
#include "../Var9.h"

class VarFactory9 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY9_H