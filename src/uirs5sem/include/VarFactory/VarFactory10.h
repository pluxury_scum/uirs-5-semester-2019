#ifndef VARFACTORY10_H
#define VARFACTORY10_H

#include "IVarFactory.h"
#include "../Var10.h"

class VarFactory10 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY10_H