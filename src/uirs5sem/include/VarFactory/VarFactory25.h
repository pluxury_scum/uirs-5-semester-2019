#ifndef VARFACTORY25_H
#define VARFACTORY25_H

#include "IVarFactory.h"
#include "../Var25.h"

class VarFactory25 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY25_H