#ifndef VARFACTORY65_H
#define VARFACTORY65_H

#include "IVarFactory.h"
#include "../Var65.h"

class VarFactory65 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY65_H