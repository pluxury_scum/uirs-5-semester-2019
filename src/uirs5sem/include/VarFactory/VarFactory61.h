#ifndef VARFACTORY61_H
#define VARFACTORY61_H

#include "IVarFactory.h"
#include "../Var61.h"

class VarFactory61 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY61_H