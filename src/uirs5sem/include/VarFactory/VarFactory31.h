#ifndef VARFACTORY31_H
#define VARFACTORY31_H

#include "IVarFactory.h"
#include "../Var31.h"

class VarFactory31 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY31_H