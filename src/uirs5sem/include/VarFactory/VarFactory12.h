#ifndef VARFACTORY12_H
#define VARFACTORY12_H

#include "IVarFactory.h"
#include "../Var12.h"

class VarFactory12 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY12_H