#ifndef VARFACTORY21_H
#define VARFACTORY21_H

#include "IVarFactory.h"
#include "../Var21.h"

class VarFactory21 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY21_H