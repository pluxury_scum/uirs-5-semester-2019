#ifndef VARFACTORY19_H
#define VARFACTORY19_H

#include "IVarFactory.h"
#include "../Var19.h"

class VarFactory19 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY19_H