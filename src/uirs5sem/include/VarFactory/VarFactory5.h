#ifndef VARFACTORY5_H
#define VARFACTORY5_H

#include "IVarFactory.h"
#include "../Var5.h"

class VarFactory5 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY5_H