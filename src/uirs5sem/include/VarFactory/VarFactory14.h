#ifndef VARFACTORY14_H
#define VARFACTORY14_H

#include "IVarFactory.h"
#include "../Var14.h"

class VarFactory14 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY14_H