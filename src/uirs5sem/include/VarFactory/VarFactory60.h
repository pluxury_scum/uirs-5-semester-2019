#ifndef VARFACTORY60_H
#define VARFACTORY60_H

#include "IVarFactory.h"
#include "../Var60.h"

class VarFactory60 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY60_H