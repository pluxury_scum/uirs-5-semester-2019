#ifndef VARFACTORY30_H
#define VARFACTORY30_H

#include "IVarFactory.h"
#include "../Var30.h"

class VarFactory30 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY30_H