#ifndef VARFACTORY59_H
#define VARFACTORY59_H

#include "IVarFactory.h"
#include "../Var59.h"

class VarFactory59 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY59_H