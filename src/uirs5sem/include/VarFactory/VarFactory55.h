#ifndef VARFACTORY55_H
#define VARFACTORY55_H

#include "IVarFactory.h"
#include "../Var55.h"

class VarFactory55 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY55_H