#ifndef VARFACTORY41_H
#define VARFACTORY41_H

#include "IVarFactory.h"
#include "../Var41.h"

class VarFactory41 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY41_H