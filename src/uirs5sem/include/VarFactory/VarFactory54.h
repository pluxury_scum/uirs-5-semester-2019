#ifndef VARFACTORY54_H
#define VARFACTORY54_H

#include "IVarFactory.h"
#include "../Var54.h"

class VarFactory54 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY54_H