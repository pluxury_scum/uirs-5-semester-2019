#ifndef VARFACTORY28_H
#define VARFACTORY28_H

#include "IVarFactory.h"
#include "../Var28.h"

class VarFactory28 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY28_H