#ifndef VARFACTORY62_H
#define VARFACTORY62_H

#include "IVarFactory.h"
#include "../Var62.h"

class VarFactory62 : public IVarFactory
{
public:
    IVar* createVar();
};

#endif // !VARFACTORY62_H