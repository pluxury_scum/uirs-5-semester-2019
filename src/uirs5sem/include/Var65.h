#ifndef VAR65_H
#define VAR65_H

#include "IVar.h"

// Variant 65 - Security [SE]
class Var65 : public IVar
{
private:
    u1 allValuesArr[11]{};
    u1 checkSum;
    u1 dataValue_1;
    u1 dataValue_2;
    u1 dataValue_3;
    u1 dataValue_4;
    u1 dataValue_5;
    u1 csValue;

public:
    int handleVar() override;
    void handleCS() override;
    void printVar() override;
    int getCheckSum() override;
    int getCsValue() override;
};

#endif // !VAR65_H