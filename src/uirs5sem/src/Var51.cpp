#include <iostream>
#include <fstream>
#include "../include/Var51.h"

int Var51::handleVar()
{
    std::ifstream inputFile;
    std::string inputFileName = "rks.dat";
    inputFile.open(inputFileName, std::ios::in | std::ios::binary);

    if (!inputFile.is_open())
    {
        std::cerr << "Error. File was not overwritten" << std::endl;
        exit(1);
    }
    else
    {
        int tempInt = 0; // variable for memory allocation
        std::string tempString; // char array holds all the characters of the file
        inputFile.read((char*)&tempInt, sizeof(tempInt)); // read string size from file
        tempString.resize(tempInt); // allocate memory of required size
        inputFile.read(&tempString[0], tempInt); // read string from file

        this->allValuesArr[0] = static_cast<u1>(tempString[0x4B46]);
        this->allValuesArr[1] = static_cast<u1>(tempString[0x4B47]);
        this->allValuesArr[2] = static_cast<u1>(tempString[0x4B48]);
        this->allValuesArr[3] = static_cast<u1>(tempString[0x4B49]);
        this->allValuesArr[4] = static_cast<u1>(tempString[0x4B4A]);
        this->allValuesArr[5] = static_cast<u1>(tempString[0x4B4B]);
        this->allValuesArr[6] = static_cast<u1>(tempString[0x4B4C]);
        this->allValuesArr[7] = static_cast<u1>(tempString[0x4B4D]);
        this->allValuesArr[8] = static_cast<u1>(tempString[0x4B4E]);
        this->allValuesArr[9] = static_cast<u1>(tempString[0x4B4F]);
        this->allValuesArr[10] = static_cast<u1>(tempString[0x4B50]);
        this->allValuesArr[11] = static_cast<u1>(tempString[0x4B51]);
        this->allValuesArr[12] = static_cast<u1>(tempString[0x4B52]);
        this->allValuesArr[13] = static_cast<u1>(tempString[0x4B53]);
        this->allValuesArr[14] = static_cast<u1>(tempString[0x4B54]);
        this->allValuesArr[15] = static_cast<u1>(tempString[0x4B55]);
        this->allValuesArr[16] = static_cast<u1>(tempString[0x4B56]);
        this->allValuesArr[17] = static_cast<u1>(tempString[0x4B57]);
        this->allValuesArr[18] = static_cast<u1>(tempString[0x4B58]);
        this->allValuesArr[19] = static_cast<u1>(tempString[0x4B59]);
        this->allValuesArr[20] = static_cast<u1>(tempString[0x4B5A]);
        this->allValuesArr[21] = static_cast<u1>(tempString[0x4B5B]);
        this->allValuesArr[22] = static_cast<u1>(tempString[0x4B5C]);
        this->allValuesArr[23] = static_cast<u1>(tempString[0x4B5D]);
        this->allValuesArr[24] = static_cast<u1>(tempString[0x4B5E]);
        this->allValuesArr[25] = static_cast<u1>(tempString[0x4B5F]);
        this->allValuesArr[26] = static_cast<u1>(tempString[0x4B60]);
        this->allValuesArr[27] = static_cast<u1>(tempString[0x4B61]);
        this->allValuesArr[28] = static_cast<u1>(tempString[0x4B62]);
        this->allValuesArr[29] = static_cast<u1>(tempString[0x4B63]);
        this->allValuesArr[30] = static_cast<u1>(tempString[0x4B64]);
        this->allValuesArr[31] = static_cast<u1>(tempString[0x4B65]);
        this->allValuesArr[32] = static_cast<u1>(tempString[0x4B66]);
        this->allValuesArr[33] = static_cast<u1>(tempString[0x4B67]);
        this->allValuesArr[34] = static_cast<u1>(tempString[0x4B68]);
        this->allValuesArr[35] = static_cast<u1>(tempString[0x4B69]);
        this->allValuesArr[36] = static_cast<u1>(tempString[0x4B6A]);
        this->allValuesArr[37] = static_cast<u1>(tempString[0x4B6B]);

        this->timeValue_1 = this->allValuesArr[5];
        this->timeValue_2 = this->allValuesArr[6];
        this->timeValue_3 = this->allValuesArr[7];
        this->timeValue_4 = this->allValuesArr[8];
        this->pValue_1 = this->allValuesArr[9];
        this->pValue_2 = this->allValuesArr[10];
        this->pValue_3 = this->allValuesArr[11];
        this->pValue_4 = this->allValuesArr[12];
        this->rValue_1 = this->allValuesArr[13];
        this->rValue_2 = this->allValuesArr[14];
        this->rValue_3 = this->allValuesArr[15];
        this->rValue_4 = this->allValuesArr[16];
        this->hValue_1 = this->allValuesArr[17];
        this->hValue_2 = this->allValuesArr[18];
        this->hValue_3 = this->allValuesArr[19];
        this->hValue_4 = this->allValuesArr[20];
        this->spValue_1 = this->allValuesArr[21];
        this->spValue_2 = this->allValuesArr[22];
        this->spValue_3 = this->allValuesArr[23];
        this->spValue_4 = this->allValuesArr[24];
        this->srValue_1 = this->allValuesArr[25];
        this->srValue_2 = this->allValuesArr[26];
        this->srValue_3 = this->allValuesArr[27];
        this->srValue_4 = this->allValuesArr[28];
        this->shValue_1 = this->allValuesArr[29];
        this->shValue_2 = this->allValuesArr[30];
        this->shValue_3 = this->allValuesArr[31];
        this->shValue_4 = this->allValuesArr[32];
        this->solTypeValue_1 = this->allValuesArr[33];
        this->solTypeValue_2 = this->allValuesArr[34];
        this->solTypeValue_3 = this->allValuesArr[35];
        this->flagsValue = this->allValuesArr[36];
        this->csValue = this->allValuesArr[37];

        handleCS();
        printVar();

        inputFile.close();
        return 0;
    }
}

void Var51::handleCS()
{
    this->checkSum = cs(allValuesArr, 37);
}

void Var51::printVar()
{
    std::ofstream outputFile;
    std::string outputFileName = "Var51_output.txt";
    outputFile.open(outputFileName);

    outputFile << "Var 51" << std::endl;
    outputFile << "Structure RotationAngles" << std::endl;
    outputFile << "Receiver time: "
        << static_cast<int>(timeValue_1) << " "
        << static_cast<int>(timeValue_2) << " "
        << static_cast<int>(timeValue_3) << " "
        << static_cast<int>(timeValue_4) << std::endl;
    outputFile << "Pitch angle: "
        << static_cast<int>(pValue_1) << " "
        << static_cast<int>(pValue_2) << " "
        << static_cast<int>(pValue_3) << " "
        << static_cast<int>(pValue_4) << std::endl;
    outputFile << "Roll angle: "
        << static_cast<int>(rValue_1) << " "
        << static_cast<int>(rValue_2) << " "
        << static_cast<int>(rValue_3) << " "
        << static_cast<int>(rValue_4) << std::endl;
    outputFile << "Heading angle: "
        << static_cast<int>(hValue_1) << " "
        << static_cast<int>(hValue_2) << " "
        << static_cast<int>(hValue_3) << " "
        << static_cast<int>(hValue_4) << std::endl;
    outputFile << "Pitch angle RMS: "
        << static_cast<int>(spValue_1) << " "
        << static_cast<int>(spValue_2) << " "
        << static_cast<int>(spValue_3) << " "
        << static_cast<int>(spValue_4) << std::endl;
    outputFile << "Roll angle RMS: "
        << static_cast<int>(srValue_1) << " "
        << static_cast<int>(srValue_2) << " "
        << static_cast<int>(srValue_3) << " "
        << static_cast<int>(srValue_4) << std::endl;
    outputFile << "Heading angle RMS: "
        << static_cast<int>(shValue_1) << " "
        << static_cast<int>(shValue_2) << " "
        << static_cast<int>(shValue_3) << " "
        << static_cast<int>(shValue_4) << std::endl;
    outputFile << "Solution type for 3 base lines: "
        << static_cast<int>(solTypeValue_1) << " "
        << static_cast<int>(solTypeValue_2) << " "
        << static_cast<int>(solTypeValue_3) << std::endl;
    outputFile << "Flags: " << static_cast<int>(flagsValue) << std::endl;
    outputFile << "Checksum of the string: " << static_cast<int>(checkSum) << std::endl;
    outputFile << "Parameter u1 cs: " << static_cast<int>(csValue);

    outputFile.close();
}

int Var51::getCheckSum()
{
    return this->checkSum;
}

int Var51::getCsValue()
{
    return this->csValue;
}