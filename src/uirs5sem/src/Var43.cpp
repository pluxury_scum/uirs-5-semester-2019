#include <iostream>
#include <fstream>
#include "../include/Var43.h"

int Var43::handleVar()
{
    std::ifstream inputFile;
    std::string inputFileName = "rks.dat";
    inputFile.open(inputFileName, std::ios::in | std::ios::binary);

    if (!inputFile.is_open())
    {
        std::cerr << "Error. File was not overwritten" << std::endl;
        exit(1);
    }
    else
    {
        int tempInt = 0; // variable for memory allocation
        std::string tempString; // char array holds all the characters of the file
        inputFile.read((char*)&tempInt, sizeof(tempInt)); // read string size from file
        tempString.resize(tempInt); // allocate memory of required size
        inputFile.read(&tempString[0], tempInt); // read string from file

        this->allValuesArr[0] = static_cast<u1>(tempString[0x4E21F]);
        this->allValuesArr[1] = static_cast<u1>(tempString[0x4E220]);
        this->allValuesArr[2] = static_cast<u1>(tempString[0x4E221]);
        this->allValuesArr[3] = static_cast<u1>(tempString[0x4E222]);
        this->allValuesArr[4] = static_cast<u1>(tempString[0x4E223]);
        this->allValuesArr[5] = static_cast<u1>(tempString[0x4E224]);
        this->allValuesArr[6] = static_cast<u1>(tempString[0x4E225]);
        this->allValuesArr[7] = static_cast<u1>(tempString[0x4E226]);
        this->allValuesArr[8] = static_cast<u1>(tempString[0x4E227]);
        this->allValuesArr[9] = static_cast<u1>(tempString[0x4E228]);
        this->allValuesArr[10] = static_cast<u1>(tempString[0x4E229]);
        this->allValuesArr[11] = static_cast<u1>(tempString[0x4E22A]);
        this->allValuesArr[12] = static_cast<u1>(tempString[0x4E22B]);
        this->allValuesArr[13] = static_cast<u1>(tempString[0x4E22C]);
        this->allValuesArr[14] = static_cast<u1>(tempString[0x4E22D]);
        this->allValuesArr[15] = static_cast<u1>(tempString[0x4E22E]);
        this->allValuesArr[16] = static_cast<u1>(tempString[0x4E22F]);
        this->allValuesArr[17] = static_cast<u1>(tempString[0x4E230]);
        this->allValuesArr[18] = static_cast<u1>(tempString[0x4E231]);
        this->allValuesArr[19] = static_cast<u1>(tempString[0x4E232]);
        this->allValuesArr[20] = static_cast<u1>(tempString[0x4E233]);
        this->allValuesArr[21] = static_cast<u1>(tempString[0x4E234]);
        this->allValuesArr[22] = static_cast<u1>(tempString[0x4E235]);
        this->allValuesArr[23] = static_cast<u1>(tempString[0x4E236]);
        this->allValuesArr[24] = static_cast<u1>(tempString[0x4E237]);
        this->allValuesArr[25] = static_cast<u1>(tempString[0x4E238]);
        this->allValuesArr[26] = static_cast<u1>(tempString[0x4E239]);
        this->allValuesArr[27] = static_cast<u1>(tempString[0x4E23A]);
        this->allValuesArr[28] = static_cast<u1>(tempString[0x4E23B]);
        this->allValuesArr[29] = static_cast<u1>(tempString[0x4E23C]);
        this->allValuesArr[30] = static_cast<u1>(tempString[0x4E23D]);
        this->allValuesArr[31] = static_cast<u1>(tempString[0x4E23E]);
        this->allValuesArr[32] = static_cast<u1>(tempString[0x4E23F]);
        this->allValuesArr[33] = static_cast<u1>(tempString[0x4E240]);
        this->allValuesArr[34] = static_cast<u1>(tempString[0x4E241]);
        this->allValuesArr[35] = static_cast<u1>(tempString[0x4E242]);
        this->allValuesArr[36] = static_cast<u1>(tempString[0x4E243]);
        this->allValuesArr[37] = static_cast<u1>(tempString[0x4E244]);
        this->allValuesArr[38] = static_cast<u1>(tempString[0x4E245]);
        this->allValuesArr[39] = static_cast<u1>(tempString[0x4E246]);
        this->allValuesArr[40] = static_cast<u1>(tempString[0x4E247]);
        this->allValuesArr[41] = static_cast<u1>(tempString[0x4E248]);
        this->allValuesArr[42] = static_cast<u1>(tempString[0x4E249]);
        this->allValuesArr[43] = static_cast<u1>(tempString[0x4E24A]);
        this->allValuesArr[44] = static_cast<u1>(tempString[0x4E24B]);
        this->allValuesArr[45] = static_cast<u1>(tempString[0x4E24C]);
        this->allValuesArr[46] = static_cast<u1>(tempString[0x4E24D]);
        this->allValuesArr[47] = static_cast<u1>(tempString[0x4E24E]);
        this->allValuesArr[48] = static_cast<u1>(tempString[0x4E24F]);
        this->allValuesArr[49] = static_cast<u1>(tempString[0x4E250]);
        this->allValuesArr[50] = static_cast<u1>(tempString[0x4E251]);
        this->allValuesArr[51] = static_cast<u1>(tempString[0x4E252]);

        this->svValue = this->allValuesArr[5];
        this->wnaValue_1 = this->allValuesArr[6];
        this->wnaValue_2 = this->allValuesArr[7];
        this->toaValue_1 = this->allValuesArr[8];
        this->toaValue_2 = this->allValuesArr[9];
        this->toaValue_3 = this->allValuesArr[10];
        this->toaValue_4 = this->allValuesArr[11];
        this->healthAValue = this->allValuesArr[12];
        this->healthSValue = this->allValuesArr[13];
        this->configValue = this->allValuesArr[14];
        this->af1Value_1 = this->allValuesArr[15];
        this->af1Value_2 = this->allValuesArr[16];
        this->af1Value_3 = this->allValuesArr[17];
        this->af1Value_4 = this->allValuesArr[18];
        this->af0Value_1 = this->allValuesArr[19];
        this->af0Value_2 = this->allValuesArr[20];
        this->af0Value_3 = this->allValuesArr[21];
        this->af0Value_4 = this->allValuesArr[22];
        this->rootAValue_1 = this->allValuesArr[23];
        this->rootAValue_2 = this->allValuesArr[24];
        this->rootAValue_3 = this->allValuesArr[25];
        this->rootAValue_4 = this->allValuesArr[26];
        this->eccValue_1 = this->allValuesArr[27];
        this->eccValue_2 = this->allValuesArr[28];
        this->eccValue_3 = this->allValuesArr[29];
        this->eccValue_4 = this->allValuesArr[30];
        this->m0Value_1 = this->allValuesArr[31];
        this->m0Value_2 = this->allValuesArr[32];
        this->m0Value_3 = this->allValuesArr[33];
        this->m0Value_4 = this->allValuesArr[34];
        this->omega0Value_1 = this->allValuesArr[35];
        this->omega0Value_2 = this->allValuesArr[36];
        this->omega0Value_3 = this->allValuesArr[37];
        this->omega0Value_4 = this->allValuesArr[38];
        this->argPerValue_1 = this->allValuesArr[39];
        this->argPerValue_2 = this->allValuesArr[40];
        this->argPerValue_3 = this->allValuesArr[41];
        this->argPerValue_4 = this->allValuesArr[42];
        this->deliValue_1 = this->allValuesArr[43];
        this->deliValue_2 = this->allValuesArr[44];
        this->deliValue_3 = this->allValuesArr[45];
        this->deliValue_4 = this->allValuesArr[46];
        this->omegaDotValue_1 = this->allValuesArr[47];
        this->omegaDotValue_2 = this->allValuesArr[48];
        this->omegaDotValue_3 = this->allValuesArr[49];
        this->omegaDotValue_4 = this->allValuesArr[50];
        this->csValue = this->allValuesArr[51];

        handleCS();
        printVar();

        inputFile.close();
        return 0;
    }
}

void Var43::handleCS()
{
    this->checkSum = cs(allValuesArr, 51);
}

void Var43::printVar()
{
    std::ofstream outputFile;
    std::string outputFileName = "Var43_output.txt";
    outputFile.open(outputFileName);

    outputFile << "Var 43" << std::endl;
    outputFile << "Structure IrnssAlm" << std::endl;
    outputFile << "SV PRN number within the range: " << static_cast<int>(svValue) << std::endl;
    outputFile << "Almanac reference week: "
        << static_cast<int>(wnaValue_1) << " "
        << static_cast<int>(wnaValue_2) << std::endl;
    outputFile << "Almanac reference time of week: "
        << static_cast<int>(toaValue_1) << " "
        << static_cast<int>(toaValue_2) << " "
        << static_cast<int>(toaValue_3) << " "
        << static_cast<int>(toaValue_4) << std::endl;
    outputFile << "Health summary: " << static_cast<int>(healthAValue) << std::endl;
    outputFile << "Satellite health: " << static_cast<int>(healthSValue) << std::endl;
    outputFile << "Satellite configuration: " << static_cast<int>(configValue) << std::endl;
    outputFile << "Polynomial coefficient af1: "
        << static_cast<int>(af1Value_1) << " "
        << static_cast<int>(af1Value_2) << " "
        << static_cast<int>(af1Value_3) << " "
        << static_cast<int>(af1Value_4) << std::endl;
    outputFile << "Polynomial coefficient af0: "
        << static_cast<int>(af0Value_1) << " "
        << static_cast<int>(af0Value_2) << " "
        << static_cast<int>(af0Value_3) << " "
        << static_cast<int>(af0Value_4) << std::endl;
    outputFile << "Square root of the semi-major axis: "
        << static_cast<int>(rootAValue_1) << " "
        << static_cast<int>(rootAValue_2) << " "
        << static_cast<int>(rootAValue_3) << " "
        << static_cast<int>(rootAValue_4) << std::endl;
    outputFile << "Eccentricity: "
        << static_cast<int>(eccValue_1) << " "
        << static_cast<int>(eccValue_2) << " "
        << static_cast<int>(eccValue_3) << " "
        << static_cast<int>(eccValue_4) << std::endl;
    outputFile << "Mean Anomaly at reference time: "
        << static_cast<int>(m0Value_1) << " "
        << static_cast<int>(m0Value_2) << " "
        << static_cast<int>(m0Value_3) << " "
        << static_cast<int>(m0Value_4) << std::endl;
    outputFile << "Longitude of ascending node of orbit plane: "
        << static_cast<int>(omega0Value_1) << " "
        << static_cast<int>(omega0Value_2) << " "
        << static_cast<int>(omega0Value_3) << " "
        << static_cast<int>(omega0Value_4) << std::endl;
    outputFile << "Argument of perigee: "
        << static_cast<int>(argPerValue_1) << " "
        << static_cast<int>(argPerValue_2) << " "
        << static_cast<int>(argPerValue_3) << " "
        << static_cast<int>(argPerValue_4) << std::endl;
    outputFile << "Correction to inclination angle: "
        << static_cast<int>(deliValue_1) << " "
        << static_cast<int>(deliValue_2) << " "
        << static_cast<int>(deliValue_3) << " "
        << static_cast<int>(deliValue_4) << std::endl;
    outputFile << "Rate of right ascension: "
        << static_cast<int>(omegaDotValue_1) << " "
        << static_cast<int>(omegaDotValue_2) << " "
        << static_cast<int>(omegaDotValue_3) << " "
        << static_cast<int>(omegaDotValue_4) << std::endl;
    outputFile << "Checksum of the string: " << static_cast<int>(checkSum) << std::endl;
    outputFile << "Parameter u1 cs: " << static_cast<int>(csValue);

    outputFile.close();
}

int Var43::getCheckSum()
{
    return this->checkSum;
}

int Var43::getCsValue()
{
    return this->csValue;
}