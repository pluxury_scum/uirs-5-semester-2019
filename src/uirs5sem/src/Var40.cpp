#include <iostream>
#include <fstream>
#include "../include/Var40.h"

int Var40::handleVar()
{
    std::ifstream inputFile;
    std::string inputFileName = "rks.dat";
    inputFile.open(inputFileName, std::ios::in | std::ios::binary);

    if (!inputFile.is_open())
    {
        std::cerr << "Error. File was not overwritten" << std::endl;
        exit(1);
    }
    else
    {
        int tempInt = 0; // variable for memory allocation
        std::string tempString; // char array holds all the characters of the file
        inputFile.read((char*)&tempInt, sizeof(tempInt)); // read string size from file
        tempString.resize(tempInt); // allocate memory of required size
        inputFile.read(&tempString[0], tempInt); // read string from file

        this->allValuesArr[0] = static_cast<u1>(tempString[0x6C2]);
        this->allValuesArr[1] = static_cast<u1>(tempString[0x6C3]);
        this->allValuesArr[2] = static_cast<u1>(tempString[0x6C4]);
        this->allValuesArr[3] = static_cast<u1>(tempString[0x6C5]);
        this->allValuesArr[4] = static_cast<u1>(tempString[0x6C6]);
        this->allValuesArr[5] = static_cast<u1>(tempString[0x6C7]);
        this->allValuesArr[6] = static_cast<u1>(tempString[0x6C8]);
        this->allValuesArr[7] = static_cast<u1>(tempString[0x6C9]);
        this->allValuesArr[8] = static_cast<u1>(tempString[0x6CA]);
        this->allValuesArr[9] = static_cast<u1>(tempString[0x6CB]);
        this->allValuesArr[10] = static_cast<u1>(tempString[0x6CC]);
        this->allValuesArr[11] = static_cast<u1>(tempString[0x6CD]);
        this->allValuesArr[12] = static_cast<u1>(tempString[0x6CE]);
        this->allValuesArr[13] = static_cast<u1>(tempString[0x6CF]);
        this->allValuesArr[14] = static_cast<u1>(tempString[0x6D0]);
        this->allValuesArr[15] = static_cast<u1>(tempString[0x6D1]);
        this->allValuesArr[16] = static_cast<u1>(tempString[0x6D2]);
        this->allValuesArr[17] = static_cast<u1>(tempString[0x6D3]);
        this->allValuesArr[18] = static_cast<u1>(tempString[0x6D4]);
        this->allValuesArr[19] = static_cast<u1>(tempString[0x6D5]);
        this->allValuesArr[20] = static_cast<u1>(tempString[0x6D6]);
        this->allValuesArr[21] = static_cast<u1>(tempString[0x6D7]);
        this->allValuesArr[22] = static_cast<u1>(tempString[0x6D8]);
        this->allValuesArr[23] = static_cast<u1>(tempString[0x6D9]);
        this->allValuesArr[24] = static_cast<u1>(tempString[0x6DA]);
        this->allValuesArr[25] = static_cast<u1>(tempString[0x6DB]);
        this->allValuesArr[26] = static_cast<u1>(tempString[0x6DC]);
        this->allValuesArr[27] = static_cast<u1>(tempString[0x6DD]);
        this->allValuesArr[28] = static_cast<u1>(tempString[0x6DE]);
        this->allValuesArr[29] = static_cast<u1>(tempString[0x6DF]);
        this->allValuesArr[30] = static_cast<u1>(tempString[0x6E0]);
        this->allValuesArr[31] = static_cast<u1>(tempString[0x6E1]);
        this->allValuesArr[32] = static_cast<u1>(tempString[0x6E2]);
        this->allValuesArr[33] = static_cast<u1>(tempString[0x6E3]);
        this->allValuesArr[34] = static_cast<u1>(tempString[0x6E4]);
        this->allValuesArr[35] = static_cast<u1>(tempString[0x6E5]);
        this->allValuesArr[36] = static_cast<u1>(tempString[0x6E6]);
        this->allValuesArr[37] = static_cast<u1>(tempString[0x6E7]);
        this->allValuesArr[38] = static_cast<u1>(tempString[0x6E8]);
        this->allValuesArr[39] = static_cast<u1>(tempString[0x6E9]);
        this->allValuesArr[40] = static_cast<u1>(tempString[0x6EA]);
        this->allValuesArr[41] = static_cast<u1>(tempString[0x6EB]);
        this->allValuesArr[42] = static_cast<u1>(tempString[0x6EC]);
        this->allValuesArr[43] = static_cast<u1>(tempString[0x6ED]);
        this->allValuesArr[44] = static_cast<u1>(tempString[0x6EE]);
        this->allValuesArr[45] = static_cast<u1>(tempString[0x6EF]);
        this->allValuesArr[46] = static_cast<u1>(tempString[0x6F0]);
        this->allValuesArr[47] = static_cast<u1>(tempString[0x6F1]);
        this->allValuesArr[48] = static_cast<u1>(tempString[0x6F2]);
        this->allValuesArr[49] = static_cast<u1>(tempString[0x6F3]);
        this->allValuesArr[50] = static_cast<u1>(tempString[0x6F4]);
        this->allValuesArr[51] = static_cast<u1>(tempString[0x6F5]);
        this->allValuesArr[52] = static_cast<u1>(tempString[0x6F6]);
        this->allValuesArr[53] = static_cast<u1>(tempString[0x6F7]);

        this->svValue = this->allValuesArr[5];
        this->wnaValue_1 = this->allValuesArr[6];
        this->wnaValue_2 = this->allValuesArr[7];
        this->toaValue_1 = this->allValuesArr[8];
        this->toaValue_2 = this->allValuesArr[9];
        this->toaValue_3 = this->allValuesArr[10];
        this->toaValue_4 = this->allValuesArr[11];
        this->healthAValue = this->allValuesArr[12];
        this->healthSValue = this->allValuesArr[13];
        this->configValue = this->allValuesArr[14];
        this->af1Value_1 = this->allValuesArr[15];
        this->af1Value_2 = this->allValuesArr[16];
        this->af1Value_3 = this->allValuesArr[17];
        this->af1Value_4 = this->allValuesArr[18];
        this->af0Value_1 = this->allValuesArr[19];
        this->af0Value_2 = this->allValuesArr[20];
        this->af0Value_3 = this->allValuesArr[21];
        this->af0Value_4 = this->allValuesArr[22];
        this->rootAValue_1 = this->allValuesArr[23];
        this->rootAValue_2 = this->allValuesArr[24];
        this->rootAValue_3 = this->allValuesArr[25];
        this->rootAValue_4 = this->allValuesArr[26];
        this->eccValue_1 = this->allValuesArr[27];
        this->eccValue_2 = this->allValuesArr[28];
        this->eccValue_3 = this->allValuesArr[29];
        this->eccValue_4 = this->allValuesArr[30];
        this->m0Value_1 = this->allValuesArr[31];
        this->m0Value_2 = this->allValuesArr[32];
        this->m0Value_3 = this->allValuesArr[33];
        this->m0Value_4 = this->allValuesArr[34];
        this->omega0Value_1 = this->allValuesArr[35];
        this->omega0Value_2 = this->allValuesArr[36];
        this->omega0Value_3 = this->allValuesArr[37];
        this->omega0Value_4 = this->allValuesArr[38];
        this->argPerValue_1 = this->allValuesArr[39];
        this->argPerValue_2 = this->allValuesArr[40];
        this->argPerValue_3 = this->allValuesArr[41];
        this->argPerValue_4 = this->allValuesArr[42];
        this->deliValue_1 = this->allValuesArr[43];
        this->deliValue_2 = this->allValuesArr[44];
        this->deliValue_3 = this->allValuesArr[45];
        this->deliValue_4 = this->allValuesArr[46];
        this->omegaDotValue_1 = this->allValuesArr[47];
        this->omegaDotValue_2 = this->allValuesArr[48];
        this->omegaDotValue_3 = this->allValuesArr[49];
        this->omegaDotValue_4 = this->allValuesArr[50];
        this->iodValue_1 = this->allValuesArr[51];
        this->iodValue_2 = this->allValuesArr[52];
        this->csValue = this->allValuesArr[53];

        handleCS();
        printVar();

        inputFile.close();
        return 0;
    }
}

void Var40::handleCS()
{
    this->checkSum = cs(allValuesArr, 53);
}

void Var40::printVar()
{
    std::ofstream outputFile;
    std::string outputFileName = "Var40_output.txt";
    outputFile.open(outputFileName);

    outputFile << "Var 40" << std::endl;
    outputFile << "Structure GALAlm" << std::endl;
    outputFile << "SV PRN number within the range: " << static_cast<int>(svValue) << std::endl;
    outputFile << "Almanac reference week: "
        << static_cast<int>(wnaValue_1) << " "
        << static_cast<int>(wnaValue_2) << std::endl;
    outputFile << "Almanac reference time of week: "
        << static_cast<int>(toaValue_1) << " "
        << static_cast<int>(toaValue_2) << " "
        << static_cast<int>(toaValue_3) << " "
        << static_cast<int>(toaValue_4) << std::endl;
    outputFile << "Health summary: " << static_cast<int>(healthAValue) << std::endl;
    outputFile << "Satellite health: " << static_cast<int>(healthSValue) << std::endl;
    outputFile << "Satellite configuration: " << static_cast<int>(configValue) << std::endl;
    outputFile << "Polynomial coefficient af1: "
        << static_cast<int>(af1Value_1) << " "
        << static_cast<int>(af1Value_2) << " "
        << static_cast<int>(af1Value_3) << " "
        << static_cast<int>(af1Value_4) << std::endl;
    outputFile << "Polynomial coefficient af0: "
        << static_cast<int>(af0Value_1) << " "
        << static_cast<int>(af0Value_2) << " "
        << static_cast<int>(af0Value_3) << " "
        << static_cast<int>(af0Value_4) << std::endl;
    outputFile << "Square root of the semi-major axis: "
        << static_cast<int>(rootAValue_1) << " "
        << static_cast<int>(rootAValue_2) << " "
        << static_cast<int>(rootAValue_3) << " "
        << static_cast<int>(rootAValue_4) << std::endl;
    outputFile << "Eccentricity: "
        << static_cast<int>(eccValue_1) << " "
        << static_cast<int>(eccValue_2) << " "
        << static_cast<int>(eccValue_3) << " "
        << static_cast<int>(eccValue_4) << std::endl;
    outputFile << "Mean Anomaly at reference time: "
        << static_cast<int>(m0Value_1) << " "
        << static_cast<int>(m0Value_2) << " "
        << static_cast<int>(m0Value_3) << " "
        << static_cast<int>(m0Value_4) << std::endl;
    outputFile << "Longitude of ascending node of orbit plane: "
        << static_cast<int>(omega0Value_1) << " "
        << static_cast<int>(omega0Value_2) << " "
        << static_cast<int>(omega0Value_3) << " "
        << static_cast<int>(omega0Value_4) << std::endl;
    outputFile << "Argument of perigee: "
        << static_cast<int>(argPerValue_1) << " "
        << static_cast<int>(argPerValue_2) << " "
        << static_cast<int>(argPerValue_3) << " "
        << static_cast<int>(argPerValue_4) << std::endl;
    outputFile << "Correction to inclination angle: "
        << static_cast<int>(deliValue_1) << " "
        << static_cast<int>(deliValue_2) << " "
        << static_cast<int>(deliValue_3) << " "
        << static_cast<int>(deliValue_4) << std::endl;
    outputFile << "Rate of right ascension: "
        << static_cast<int>(omegaDotValue_1) << " "
        << static_cast<int>(omegaDotValue_2) << " "
        << static_cast<int>(omegaDotValue_3) << " "
        << static_cast<int>(omegaDotValue_4) << std::endl;
    outputFile << "Issue of almanac data: "
        << static_cast<int>(iodValue_1) << " "
        << static_cast<int>(iodValue_2) << std::endl;
    outputFile << "Checksum of the string: " << static_cast<int>(checkSum) << std::endl;
    outputFile << "Parameter u1 cs: " << static_cast<int>(csValue);

    outputFile.close();
}

int Var40::getCheckSum()
{
    return this->checkSum;
}

int Var40::getCsValue()
{
    return this->csValue;
}