#include <iostream>
#include <fstream>
#include "../include/Var44.h"

int Var44::handleVar()
{
    std::ifstream inputFile;
    std::string inputFileName = "rks.dat";
    inputFile.open(inputFileName, std::ios::in | std::ios::binary);

    if (!inputFile.is_open())
    {
        std::cerr << "Error. File was not overwritten" << std::endl;
        exit(1);
    }
    else
    {
        int tempInt = 0; // variable for memory allocation
        std::string tempString; // char array holds all the characters of the file
        inputFile.read((char*)&tempInt, sizeof(tempInt)); // read string size from file
        tempString.resize(tempInt); // allocate memory of required size
        inputFile.read(&tempString[0], tempInt); // read string from file

        this->allValuesArr[0] = static_cast<u1>(tempString[0x2002]);
        this->allValuesArr[1] = static_cast<u1>(tempString[0x2003]);
        this->allValuesArr[2] = static_cast<u1>(tempString[0x2004]);
        this->allValuesArr[3] = static_cast<u1>(tempString[0x2005]);
        this->allValuesArr[4] = static_cast<u1>(tempString[0x2006]);
        this->allValuesArr[5] = static_cast<u1>(tempString[0x2007]);
        this->allValuesArr[6] = static_cast<u1>(tempString[0x2008]);
        this->allValuesArr[7] = static_cast<u1>(tempString[0x2009]);
        this->allValuesArr[8] = static_cast<u1>(tempString[0x200A]);
        this->allValuesArr[9] = static_cast<u1>(tempString[0x200B]);
        this->allValuesArr[10] = static_cast<u1>(tempString[0x200C]);
        this->allValuesArr[11] = static_cast<u1>(tempString[0x200D]);
        this->allValuesArr[12] = static_cast<u1>(tempString[0x200E]);
        this->allValuesArr[13] = static_cast<u1>(tempString[0x200F]);
        this->allValuesArr[14] = static_cast<u1>(tempString[0x2010]);
        this->allValuesArr[15] = static_cast<u1>(tempString[0x2011]);
        this->allValuesArr[16] = static_cast<u1>(tempString[0x2012]);
        this->allValuesArr[17] = static_cast<u1>(tempString[0x2013]);
        this->allValuesArr[18] = static_cast<u1>(tempString[0x2014]);
        this->allValuesArr[19] = static_cast<u1>(tempString[0x2015]);
        this->allValuesArr[20] = static_cast<u1>(tempString[0x2016]);
        this->allValuesArr[21] = static_cast<u1>(tempString[0x2017]);
        this->allValuesArr[22] = static_cast<u1>(tempString[0x2018]);
        this->allValuesArr[23] = static_cast<u1>(tempString[0x2019]);
        this->allValuesArr[24] = static_cast<u1>(tempString[0x201A]);
        this->allValuesArr[25] = static_cast<u1>(tempString[0x201B]);
        this->allValuesArr[26] = static_cast<u1>(tempString[0x201C]);
        this->allValuesArr[27] = static_cast<u1>(tempString[0x201D]);
        this->allValuesArr[28] = static_cast<u1>(tempString[0x201E]);
        this->allValuesArr[29] = static_cast<u1>(tempString[0x201F]);
        this->allValuesArr[30] = static_cast<u1>(tempString[0x2020]);
        this->allValuesArr[31] = static_cast<u1>(tempString[0x2021]);
        this->allValuesArr[32] = static_cast<u1>(tempString[0x2022]);
        this->allValuesArr[33] = static_cast<u1>(tempString[0x2023]);
        this->allValuesArr[34] = static_cast<u1>(tempString[0x2024]);
        this->allValuesArr[35] = static_cast<u1>(tempString[0x2025]);
        this->allValuesArr[36] = static_cast<u1>(tempString[0x2026]);
        this->allValuesArr[37] = static_cast<u1>(tempString[0x2027]);
        this->allValuesArr[38] = static_cast<u1>(tempString[0x2028]);
        this->allValuesArr[39] = static_cast<u1>(tempString[0x2029]);
        this->allValuesArr[40] = static_cast<u1>(tempString[0x202A]);
        this->allValuesArr[41] = static_cast<u1>(tempString[0x202B]);
        this->allValuesArr[42] = static_cast<u1>(tempString[0x202C]);
        this->allValuesArr[43] = static_cast<u1>(tempString[0x202D]);
        this->allValuesArr[44] = static_cast<u1>(tempString[0x202E]);
        this->allValuesArr[45] = static_cast<u1>(tempString[0x202F]);
        this->allValuesArr[46] = static_cast<u1>(tempString[0x2030]);
        this->allValuesArr[47] = static_cast<u1>(tempString[0x2031]);
        this->allValuesArr[48] = static_cast<u1>(tempString[0x2032]);
        this->allValuesArr[49] = static_cast<u1>(tempString[0x2033]);
        this->allValuesArr[50] = static_cast<u1>(tempString[0x2034]);
        this->allValuesArr[51] = static_cast<u1>(tempString[0x2035]);
        this->allValuesArr[52] = static_cast<u1>(tempString[0x2036]);
        this->allValuesArr[53] = static_cast<u1>(tempString[0x2037]);
        this->allValuesArr[54] = static_cast<u1>(tempString[0x2038]);
        this->allValuesArr[55] = static_cast<u1>(tempString[0x2039]);
        this->allValuesArr[56] = static_cast<u1>(tempString[0x203A]);

        this->svValue = this->allValuesArr[5];
        this->frqNumValue = this->allValuesArr[6];
        this->dnaValue_1 = this->allValuesArr[7];
        this->dnaValue_2 = this->allValuesArr[8];
        this->tlamValue_1 = this->allValuesArr[9];
        this->tlamValue_2 = this->allValuesArr[10];
        this->tlamValue_3 = this->allValuesArr[11];
        this->tlamValue_4 = this->allValuesArr[12];
        this->flagsValue = this->allValuesArr[13];
        this->tauNValue_1 = this->allValuesArr[14];
        this->tauNValue_2 = this->allValuesArr[15];
        this->tauNValue_3 = this->allValuesArr[16];
        this->tauNValue_4 = this->allValuesArr[17];
        this->tauSysValue_1 = this->allValuesArr[18];
        this->tauSysValue_2 = this->allValuesArr[19];
        this->tauSysValue_3 = this->allValuesArr[20];
        this->tauSysValue_4 = this->allValuesArr[21];
        this->tauSysValue_5 = this->allValuesArr[22];
        this->tauSysValue_6 = this->allValuesArr[23];
        this->tauSysValue_7 = this->allValuesArr[24];
        this->tauSysValue_8 = this->allValuesArr[25];
        this->eccValue_1 = this->allValuesArr[26];
        this->eccValue_2 = this->allValuesArr[27];
        this->eccValue_3 = this->allValuesArr[28];
        this->eccValue_4 = this->allValuesArr[29];
        this->lambdaValue_1 = this->allValuesArr[30];
        this->lambdaValue_2 = this->allValuesArr[31];
        this->lambdaValue_3 = this->allValuesArr[32];
        this->lambdaValue_4 = this->allValuesArr[33];
        this->argPerValue_1 = this->allValuesArr[34];
        this->argPerValue_2 = this->allValuesArr[35];
        this->argPerValue_3 = this->allValuesArr[36];
        this->argPerValue_4 = this->allValuesArr[37];
        this->delTValue_1 = this->allValuesArr[38];
        this->delTValue_2 = this->allValuesArr[39];
        this->delTValue_3 = this->allValuesArr[40];
        this->delTValue_4 = this->allValuesArr[41];
        this->delTdtValue_1 = this->allValuesArr[42];
        this->delTdtValue_2 = this->allValuesArr[43];
        this->delTdtValue_3 = this->allValuesArr[44];
        this->delTdtValue_4 = this->allValuesArr[45];
        this->deliValue_1 = this->allValuesArr[46];
        this->deliValue_2 = this->allValuesArr[47];
        this->deliValue_3 = this->allValuesArr[48];
        this->deliValue_4 = this->allValuesArr[49];
        this->n4Value = this->allValuesArr[50];
        this->reservedValue = this->allValuesArr[51];
        this->gammaNValue_1 = this->allValuesArr[52];
        this->gammaNValue_2 = this->allValuesArr[53];
        this->gammaNValue_3 = this->allValuesArr[54];
        this->gammaNValue_4 = this->allValuesArr[55];
        this->csValue = this->allValuesArr[56];

        handleCS();
        printVar();

        inputFile.close();
        return 0;
    }
}

void Var44::handleCS()
{
    this->checkSum = cs(allValuesArr, 56);
}

void Var44::printVar()
{
    std::ofstream outputFile;
    std::string outputFileName = "Var44_output.txt";
    outputFile.open(outputFileName);

    outputFile << "Var 44" << std::endl;
    outputFile << "Structure GLOAlmanac" << std::endl;
    outputFile << "Satellite orbit slot number within: " << static_cast<int>(svValue) << std::endl;
    outputFile << "Satellite frequency channel number: " << static_cast<int>(frqNumValue) << std::endl;
    outputFile << "Day number within 4-year period starting with the leap year: "
        << static_cast<int>(dnaValue_1) << " "
        << static_cast<int>(dnaValue_2) << std::endl;
    outputFile << "Time of the first ascending node passage on day �dna�: "
        << static_cast<int>(tlamValue_1) << " "
        << static_cast<int>(tlamValue_2) << " "
        << static_cast<int>(tlamValue_3) << " "
        << static_cast<int>(tlamValue_4) << std::endl;
    outputFile << "Satellite flags: " << static_cast<int>(flagsValue) << std::endl;
    outputFile << "Coarse time correction to SV clock with respect to GLONASS system time: "
        << static_cast<int>(tauNValue_1) << " "
        << static_cast<int>(tauNValue_2) << " "
        << static_cast<int>(tauNValue_3) << " "
        << static_cast<int>(tauNValue_4) << std::endl;
    outputFile << "Correction to GLONASS system time with respect to UTC(SU): "
        << static_cast<int>(tauSysValue_1) << " "
        << static_cast<int>(tauSysValue_2) << " "
        << static_cast<int>(tauSysValue_3) << " "
        << static_cast<int>(tauSysValue_4) << " "
        << static_cast<int>(tauSysValue_5) << " "
        << static_cast<int>(tauSysValue_6) << " "
        << static_cast<int>(tauSysValue_7) << " "
        << static_cast<int>(tauSysValue_8) << std::endl;
    outputFile << "Eccentricity at reference time �tlam�: "
        << static_cast<int>(eccValue_1) << " "
        << static_cast<int>(eccValue_2) << " "
        << static_cast<int>(eccValue_3) << " "
        << static_cast<int>(eccValue_4) << std::endl;
    outputFile << "Longitude of ascending node at reference time �tlam�: "
        << static_cast<int>(lambdaValue_1) << " "
        << static_cast<int>(lambdaValue_2) << " "
        << static_cast<int>(lambdaValue_3) << " "
        << static_cast<int>(lambdaValue_4) << std::endl;
    outputFile << "Argument of perigee at reference time �tlam�: "
        << static_cast<int>(argPerValue_1) << " "
        << static_cast<int>(argPerValue_2) << " "
        << static_cast<int>(argPerValue_3) << " "
        << static_cast<int>(argPerValue_4) << std::endl;
    outputFile << "Correction to mean Draconic period at reference time �tlam�: "
        << static_cast<int>(delTValue_1) << " "
        << static_cast<int>(delTValue_2) << " "
        << static_cast<int>(delTValue_3) << " "
        << static_cast<int>(delTValue_4) << std::endl;
    outputFile << "Rate of change of Draconic period: "
        << static_cast<int>(delTdtValue_1) << " "
        << static_cast<int>(delTdtValue_2) << " "
        << static_cast<int>(delTdtValue_3) << " "
        << static_cast<int>(delTdtValue_4) << std::endl;
    outputFile << "Correction to inclination at reference time �tlam�: "
        << static_cast<int>(deliValue_1) << " "
        << static_cast<int>(deliValue_2) << " "
        << static_cast<int>(deliValue_3) << " "
        << static_cast<int>(deliValue_4) << std::endl;
    outputFile << "Number of 4-year period: " << static_cast<int>(n4Value) << std::endl;
    outputFile << "<reserved>: " << static_cast<int>(reservedValue) << std::endl;
    outputFile << "Rate of coarse satellite clock correction to GLONASS time scale: "
        << static_cast<int>(gammaNValue_1) << " "
        << static_cast<int>(gammaNValue_2) << " "
        << static_cast<int>(gammaNValue_3) << " "
        << static_cast<int>(gammaNValue_4) << std::endl;
    outputFile << "Checksum of the string: " << static_cast<int>(checkSum) << std::endl;
    outputFile << "Parameter u1 cs: " << static_cast<int>(csValue);

    outputFile.close();
}

int Var44::getCheckSum()
{
    return this->checkSum;
}

int Var44::getCsValue()
{
    return this->csValue;
}