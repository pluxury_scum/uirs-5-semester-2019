#include <iostream>
#include <fstream>
#include "../include/Var32.h"

int Var32::handleVar()
{
    std::ifstream inputFile;
    std::string inputFileName = "rks.dat";
    inputFile.open(inputFileName, std::ios::in | std::ios::binary);

    if (!inputFile.is_open())
    {
        std::cerr << "Error. File was not overwritten" << std::endl;
        exit(1);
    }
    else
    {
        int tempInt = 0; // variable for memory allocation
        std::string tempString; // char array holds all the characters of the file
        inputFile.read((char*)&tempInt, sizeof(tempInt)); // read string size from file
        tempString.resize(tempInt); // allocate memory of required size
        inputFile.read(&tempString[0], tempInt); // read string from file

        this->allValuesArr[0] = static_cast<u1>(tempString[0x19F0]);
        this->allValuesArr[1] = static_cast<u1>(tempString[0x19F1]);
        this->allValuesArr[2] = static_cast<u1>(tempString[0x19F2]);
        this->allValuesArr[3] = static_cast<u1>(tempString[0x19F3]);
        this->allValuesArr[4] = static_cast<u1>(tempString[0x19F4]);
        this->allValuesArr[5] = static_cast<u1>(tempString[0x19F5]);
        this->allValuesArr[6] = static_cast<u1>(tempString[0x19F6]);
        this->allValuesArr[7] = static_cast<u1>(tempString[0x19F7]);
        this->allValuesArr[8] = static_cast<u1>(tempString[0x19F8]);
        this->allValuesArr[9] = static_cast<u1>(tempString[0x19F9]);
        this->allValuesArr[10] = static_cast<u1>(tempString[0x19FA]);
        this->allValuesArr[11] = static_cast<u1>(tempString[0x19FB]);
        this->allValuesArr[12] = static_cast<u1>(tempString[0x19FC]);
        this->allValuesArr[13] = static_cast<u1>(tempString[0x19FD]);
        this->allValuesArr[14] = static_cast<u1>(tempString[0x19FE]);
        this->allValuesArr[15] = static_cast<u1>(tempString[0x19FF]);
        this->allValuesArr[16] = static_cast<u1>(tempString[0x1A00]);
        this->allValuesArr[17] = static_cast<u1>(tempString[0x1A01]);
        this->allValuesArr[18] = static_cast<u1>(tempString[0x1A02]);
        this->allValuesArr[19] = static_cast<u1>(tempString[0x1A03]);
        this->allValuesArr[20] = static_cast<u1>(tempString[0x1A04]);
        this->allValuesArr[21] = static_cast<u1>(tempString[0x1A05]);
        this->allValuesArr[22] = static_cast<u1>(tempString[0x1A06]);

        this->hdopValue_1 = this->allValuesArr[5];
        this->hdopValue_2 = this->allValuesArr[6];
        this->hdopValue_3 = this->allValuesArr[7];
        this->hdopValue_4 = this->allValuesArr[8];
        this->vdopValue_1 = this->allValuesArr[9];
        this->vdopValue_2 = this->allValuesArr[10];
        this->vdopValue_3 = this->allValuesArr[11];
        this->vdopValue_4 = this->allValuesArr[12];
        this->tdopValue_1 = this->allValuesArr[13];
        this->tdopValue_2 = this->allValuesArr[14];
        this->tdopValue_3 = this->allValuesArr[15];
        this->tdopValue_4 = this->allValuesArr[16];
        this->solTypeValue = this->allValuesArr[17];
        this->edopValue_1 = this->allValuesArr[18];
        this->edopValue_2 = this->allValuesArr[19];
        this->edopValue_3 = this->allValuesArr[20];
        this->edopValue_4 = this->allValuesArr[21];
        this->csValue = this->allValuesArr[22];

        handleCS();
        printVar();

        inputFile.close();
        return 0;
    }
}

void Var32::handleCS()
{
    this->checkSum = cs(allValuesArr, 22);
}

void Var32::printVar()
{
    std::ofstream outputFile;
    std::string outputFileName = "Var32_output.txt";
    outputFile.open(outputFileName);

    outputFile << "Var 32" << std::endl;
    outputFile << "Structure Dops" << std::endl;
    outputFile << "Horizontal dilution of precision: "
        << static_cast<int>(hdopValue_1) << " "
        << static_cast<int>(hdopValue_2) << " "
        << static_cast<int>(hdopValue_3) << " "
        << static_cast<int>(hdopValue_4) << std::endl;
    outputFile << "Vertical dilution of precision: "
        << static_cast<int>(vdopValue_1) << " "
        << static_cast<int>(vdopValue_2) << " "
        << static_cast<int>(vdopValue_3) << " "
        << static_cast<int>(vdopValue_4) << std::endl;
    outputFile << "Time dilution of precision: "
        << static_cast<int>(tdopValue_1) << " "
        << static_cast<int>(tdopValue_2) << " "
        << static_cast<int>(tdopValue_3) << " "
        << static_cast<int>(tdopValue_4) << std::endl;
    outputFile << "Solution type: " << static_cast<int>(checkSum) << std::endl;
    outputFile << "East dilution of precision: "
        << static_cast<int>(edopValue_1) << " "
        << static_cast<int>(edopValue_2) << " "
        << static_cast<int>(edopValue_3) << " "
        << static_cast<int>(edopValue_4) << std::endl;
    outputFile << "Checksum of the string: " << static_cast<int>(checkSum) << std::endl;
    outputFile << "Parameter u1 cs: " << static_cast<int>(csValue);

    outputFile.close();
}

int Var32::getCheckSum()
{
    return this->checkSum;
}

int Var32::getCsValue()
{
    return this->csValue;
}