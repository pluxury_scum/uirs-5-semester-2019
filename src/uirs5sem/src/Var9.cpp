#include <iostream>
#include <fstream>
#include "../include/Var9.h"

int Var9::handleVar()
{
    std::ifstream inputFile;
    std::string inputFileName = "rks.dat";
    inputFile.open(inputFileName, std::ios::in | std::ios::binary);

    if (!inputFile.is_open())
    {
        std::cerr << "Error. File was not overwritten" << std::endl;
        exit(1);
    }
    else
    {
        int tempInt = 0; // variable for memory allocation
        std::string tempString; // char array holds all the characters of the file
        inputFile.read((char*)&tempInt, sizeof(tempInt)); // read string size from file
        tempString.resize(tempInt); // allocate memory of required size
        inputFile.read(&tempString[0], tempInt); // read string from file

        this->allValuesArr[0] = static_cast<u1>(tempString[0x70F]);
        this->allValuesArr[1] = static_cast<u1>(tempString[0x710]);
        this->allValuesArr[2] = static_cast<u1>(tempString[0x711]);
        this->allValuesArr[3] = static_cast<u1>(tempString[0x712]);
        this->allValuesArr[4] = static_cast<u1>(tempString[0x713]);
        this->allValuesArr[5] = static_cast<u1>(tempString[0x714]);
        this->allValuesArr[6] = static_cast<u1>(tempString[0x715]);
        this->allValuesArr[7] = static_cast<u1>(tempString[0x716]);
        this->allValuesArr[8] = static_cast<u1>(tempString[0x717]);
        this->allValuesArr[9] = static_cast<u1>(tempString[0x718]);
        this->allValuesArr[10] = static_cast<u1>(tempString[0x719]);
        this->allValuesArr[11] = static_cast<u1>(tempString[0x71A]);

        this->todValue_1 = this->allValuesArr[5];
        this->todValue_2 = this->allValuesArr[6];
        this->todValue_3 = this->allValuesArr[7];
        this->todValue_4 = this->allValuesArr[8];
        this->dnValue_1 = this->allValuesArr[9];
        this->dnValue_2 = this->allValuesArr[10];
        this->csValue = this->allValuesArr[11];

        handleCS();
        printVar();

        inputFile.close();
        return 0;
    }
}

void Var9::handleCS()
{
    this->checkSum = cs(allValuesArr, 11);
}

void Var9::printVar()
{
    std::ofstream outputFile;
    std::string outputFileName = "Var9_output.txt";
    outputFile.open(outputFileName);

    outputFile << "Var 9" << std::endl;
    outputFile << "Structure GLOTime" << std::endl;
    outputFile << "Time of day: "
        << static_cast<int>(todValue_1) << " "
        << static_cast<int>(todValue_2) << " "
        << static_cast<int>(todValue_3) << " "
        << static_cast<int>(todValue_4) << std::endl;
    outputFile << "GLONASS day number: "
        << static_cast<int>(dnValue_1) << " "
        << static_cast<int>(dnValue_2) << std::endl;
    outputFile << "Checksum of the string: " << static_cast<int>(checkSum) << std::endl;
    outputFile << "Parameter u1 cs: " << static_cast<int>(csValue);

    outputFile.close();
}

int Var9::getCheckSum()
{
    return this->checkSum;
}

int Var9::getCsValue()
{
    return this->csValue;
}