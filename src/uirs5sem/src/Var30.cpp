#include <iostream>
#include <fstream>
#include "../include/Var30.h"

int Var30::handleVar()
{
    std::ifstream inputFile;
    std::string inputFileName = "rks.dat";
    inputFile.open(inputFileName, std::ios::in | std::ios::binary);

    if (!inputFile.is_open())
    {
        std::cerr << "Error. File was not overwritten" << std::endl;
        exit(1);
    }
    else
    {
        int tempInt = 0; // variable for memory allocation
        std::string tempString; // char array holds all the characters of the file
        inputFile.read((char*)&tempInt, sizeof(tempInt)); // read string size from file
        tempString.resize(tempInt); // allocate memory of required size
        inputFile.read(&tempString[0], tempInt); // read string from file

        this->allValuesArr[0] = static_cast<u1>(tempString[0x330F0]);
        this->allValuesArr[1] = static_cast<u1>(tempString[0x330F1]);
        this->allValuesArr[2] = static_cast<u1>(tempString[0x330F2]);
        this->allValuesArr[3] = static_cast<u1>(tempString[0x330F3]);
        this->allValuesArr[4] = static_cast<u1>(tempString[0x330F4]);
        this->allValuesArr[5] = static_cast<u1>(tempString[0x330F5]);
        this->allValuesArr[6] = static_cast<u1>(tempString[0x330F6]);
        this->allValuesArr[7] = static_cast<u1>(tempString[0x330F7]);
        this->allValuesArr[8] = static_cast<u1>(tempString[0x330F8]);
        this->allValuesArr[9] = static_cast<u1>(tempString[0x330F9]);
        this->allValuesArr[10] = static_cast<u1>(tempString[0x330FA]);
        this->allValuesArr[11] = static_cast<u1>(tempString[0x330FB]);
        this->allValuesArr[12] = static_cast<u1>(tempString[0x330FC]);
        this->allValuesArr[13] = static_cast<u1>(tempString[0x330FD]);
        this->allValuesArr[14] = static_cast<u1>(tempString[0x330FE]);
        this->allValuesArr[15] = static_cast<u1>(tempString[0x330FF]);
        this->allValuesArr[16] = static_cast<u1>(tempString[0x33100]);
        this->allValuesArr[17] = static_cast<u1>(tempString[0x33101]);
        this->allValuesArr[18] = static_cast<u1>(tempString[0x33102]);
        this->allValuesArr[19] = static_cast<u1>(tempString[0x33103]);
        this->allValuesArr[20] = static_cast<u1>(tempString[0x33104]);
        this->allValuesArr[21] = static_cast<u1>(tempString[0x33105]);
        this->allValuesArr[22] = static_cast<u1>(tempString[0x33106]);
        this->allValuesArr[23] = static_cast<u1>(tempString[0x33107]);
        this->allValuesArr[24] = static_cast<u1>(tempString[0x33108]);
        this->allValuesArr[25] = static_cast<u1>(tempString[0x33109]);
        this->allValuesArr[26] = static_cast<u1>(tempString[0x3310A]);
        this->allValuesArr[27] = static_cast<u1>(tempString[0x3310B]);
        this->allValuesArr[28] = static_cast<u1>(tempString[0x3310C]);
        this->allValuesArr[29] = static_cast<u1>(tempString[0x3310D]);
        this->allValuesArr[30] = static_cast<u1>(tempString[0x3310E]);
        this->allValuesArr[31] = static_cast<u1>(tempString[0x3310F]);
        this->allValuesArr[32] = static_cast<u1>(tempString[0x33110]);
        this->allValuesArr[33] = static_cast<u1>(tempString[0x33111]);
        this->allValuesArr[34] = static_cast<u1>(tempString[0x33112]);
        this->allValuesArr[35] = static_cast<u1>(tempString[0x33113]);
        this->allValuesArr[36] = static_cast<u1>(tempString[0x33114]);
        this->allValuesArr[37] = static_cast<u1>(tempString[0x33115]);
        this->allValuesArr[38] = static_cast<u1>(tempString[0x33116]);
        this->allValuesArr[39] = static_cast<u1>(tempString[0x33117]);
        this->allValuesArr[40] = static_cast<u1>(tempString[0x33118]);
        this->allValuesArr[41] = static_cast<u1>(tempString[0x33119]);
        this->allValuesArr[42] = static_cast<u1>(tempString[0x3311A]);
        this->allValuesArr[43] = static_cast<u1>(tempString[0x3311B]);
        this->allValuesArr[44] = static_cast<u1>(tempString[0x3311C]);
        this->allValuesArr[45] = static_cast<u1>(tempString[0x3311D]);
        this->allValuesArr[46] = static_cast<u1>(tempString[0x3311E]);
        this->allValuesArr[47] = static_cast<u1>(tempString[0x3311F]);
        this->allValuesArr[48] = static_cast<u1>(tempString[0x33120]);
        this->allValuesArr[49] = static_cast<u1>(tempString[0x33121]);

        this->nValue_1 = this->allValuesArr[5];
        this->nValue_2 = this->allValuesArr[6];
        this->nValue_3 = this->allValuesArr[7];
        this->nValue_4 = this->allValuesArr[8];
        this->nValue_5 = this->allValuesArr[9];
        this->nValue_6 = this->allValuesArr[10];
        this->nValue_7 = this->allValuesArr[11];
        this->nValue_8 = this->allValuesArr[12];
        this->eValue_1 = this->allValuesArr[13];
        this->eValue_2 = this->allValuesArr[14];
        this->eValue_3 = this->allValuesArr[15];
        this->eValue_4 = this->allValuesArr[16];
        this->eValue_5 = this->allValuesArr[17];
        this->eValue_6 = this->allValuesArr[18];
        this->eValue_7 = this->allValuesArr[19];
        this->eValue_8 = this->allValuesArr[20];
        this->uValue_1 = this->allValuesArr[21];
        this->uValue_2 = this->allValuesArr[22];
        this->uValue_3 = this->allValuesArr[23];
        this->uValue_4 = this->allValuesArr[24];
        this->uValue_5 = this->allValuesArr[25];
        this->uValue_6 = this->allValuesArr[26];
        this->uValue_7 = this->allValuesArr[27];
        this->uValue_8 = this->allValuesArr[28];
        this->sepValue_1 = this->allValuesArr[29];
        this->sepValue_2 = this->allValuesArr[30];
        this->sepValue_3 = this->allValuesArr[31];
        this->sepValue_4 = this->allValuesArr[32];
        this->sepValue_5 = this->allValuesArr[33];
        this->sepValue_6 = this->allValuesArr[34];
        this->sepValue_7 = this->allValuesArr[35];
        this->sepValue_8 = this->allValuesArr[36];
        this->pSigmaValue_1 = this->allValuesArr[37];
        this->pSigmaValue_2 = this->allValuesArr[38];
        this->pSigmaValue_3 = this->allValuesArr[39];
        this->pSigmaValue_4 = this->allValuesArr[40];
        this->solTypeValue = this->allValuesArr[41];
        this->gridValue = this->allValuesArr[42];
        this->geoidValue = this->allValuesArr[43];
        this->prjValue_1 = this->allValuesArr[44];
        this->prjValue_2 = this->allValuesArr[45];
        this->gridZoneValue = this->allValuesArr[46];
        this->chIssueValue_1 = this->allValuesArr[47];
        this->chIssueValue_2 = this->allValuesArr[48];
        this->csValue = this->allValuesArr[49];

        handleCS();
        printVar();

        inputFile.close();
        return 0;
    }
}

void Var30::handleCS()
{
    this->checkSum = cs(allValuesArr, 49);
}

void Var30::printVar()
{
    std::ofstream outputFile;
    std::string outputFileName = "Var30_output.txt";
    outputFile.open(outputFileName);

    outputFile << "Var 30" << std::endl;
    outputFile << "Structure LocalPlanePos" << std::endl;
    outputFile << "Northern coordinate: "
        << static_cast<int>(nValue_1) << " "
        << static_cast<int>(nValue_2) << " "
        << static_cast<int>(nValue_3) << " "
        << static_cast<int>(nValue_4) << " "
        << static_cast<int>(nValue_5) << " "
        << static_cast<int>(nValue_6) << " "
        << static_cast<int>(nValue_7) << " "
        << static_cast<int>(nValue_8) << std::endl;
    outputFile << "Eastern coordinate: "
        << static_cast<int>(eValue_1) << " "
        << static_cast<int>(eValue_2) << " "
        << static_cast<int>(eValue_3) << " "
        << static_cast<int>(eValue_4) << " "
        << static_cast<int>(eValue_5) << " "
        << static_cast<int>(eValue_6) << " "
        << static_cast<int>(eValue_7) << " "
        << static_cast<int>(eValue_8) << std::endl;
    outputFile << "Altitude above local ellipsoid: "
        << static_cast<int>(uValue_1) << " "
        << static_cast<int>(uValue_2) << " "
        << static_cast<int>(uValue_3) << " "
        << static_cast<int>(uValue_4) << " "
        << static_cast<int>(uValue_5) << " "
        << static_cast<int>(uValue_6) << " "
        << static_cast<int>(uValue_7) << " "
        << static_cast<int>(uValue_8) << std::endl;
    outputFile << "Geoid separation relatively to local ellipsoid: "
        << static_cast<int>(sepValue_1) << " "
        << static_cast<int>(sepValue_2) << " "
        << static_cast<int>(sepValue_3) << " "
        << static_cast<int>(sepValue_4) << " "
        << static_cast<int>(sepValue_5) << " "
        << static_cast<int>(sepValue_6) << " "
        << static_cast<int>(sepValue_7) << " "
        << static_cast<int>(sepValue_8) << std::endl;
    outputFile << "Position SEP: "
        << static_cast<int>(pSigmaValue_1) << " "
        << static_cast<int>(pSigmaValue_2) << " "
        << static_cast<int>(pSigmaValue_3) << " "
        << static_cast<int>(pSigmaValue_4) << std::endl;
    outputFile << "Solution type: " << static_cast<int>(solTypeValue) << std::endl;
    outputFile << "Grid source: " << static_cast<int>(gridValue) << std::endl;
    outputFile << "Geoid source: " << static_cast<int>(geoidValue) << std::endl;
    outputFile << "EPSG code of used projection: "
        << static_cast<int>(prjValue_1) << " "
        << static_cast<int>(prjValue_2) << std::endl;
    outputFile << "Grid zone for global systems UTM and UPS: " << static_cast<int>(gridZoneValue) << std::endl;
    outputFile << "Counter incrementing on every potential change of user grid system: "
        << static_cast<int>(chIssueValue_1) << " "
        << static_cast<int>(chIssueValue_2) << std::endl;
    outputFile << "Checksum of the string: " << static_cast<int>(checkSum) << std::endl;
    outputFile << "Parameter u1 cs: " << static_cast<int>(csValue);

    outputFile.close();
}

int Var30::getCheckSum()
{
    return this->checkSum;
}

int Var30::getCsValue()
{
    return this->csValue;
}