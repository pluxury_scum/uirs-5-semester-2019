#include <iostream>
#include <fstream>
#include "../include/Var35.h"

int Var35::handleVar()
{
    std::ifstream inputFile;
    std::string inputFileName = "rks.dat";
    inputFile.open(inputFileName, std::ios::in | std::ios::binary);

    if (!inputFile.is_open())
    {
        std::cerr << "Error. File was not overwritten" << std::endl;
        exit(1);
    }
    else
    {
        int tempInt = 0; // variable for memory allocation
        std::string tempString; // char array holds all the characters of the file
        inputFile.read((char*)&tempInt, sizeof(tempInt)); // read string size from file
        tempString.resize(tempInt); // allocate memory of required size
        inputFile.read(&tempString[0], tempInt); // read string from file

        this->allValuesArr[0] = static_cast<u1>(tempString[0x91262]);
        this->allValuesArr[1] = static_cast<u1>(tempString[0x91263]);
        this->allValuesArr[2] = static_cast<u1>(tempString[0x91264]);
        this->allValuesArr[3] = static_cast<u1>(tempString[0x91265]);
        this->allValuesArr[4] = static_cast<u1>(tempString[0x91266]);
        this->allValuesArr[5] = static_cast<u1>(tempString[0x91267]);
        this->allValuesArr[6] = static_cast<u1>(tempString[0x91268]);
        this->allValuesArr[7] = static_cast<u1>(tempString[0x91269]);
        this->allValuesArr[8] = static_cast<u1>(tempString[0x9126A]);
        this->allValuesArr[9] = static_cast<u1>(tempString[0x9126B]);
        this->allValuesArr[10] = static_cast<u1>(tempString[0x9126C]);
        this->allValuesArr[11] = static_cast<u1>(tempString[0x9126D]);
        this->allValuesArr[12] = static_cast<u1>(tempString[0x9126E]);
        this->allValuesArr[13] = static_cast<u1>(tempString[0x9126F]);
        this->allValuesArr[14] = static_cast<u1>(tempString[0x91270]);
        this->allValuesArr[15] = static_cast<u1>(tempString[0x91271]);
        this->allValuesArr[16] = static_cast<u1>(tempString[0x91272]);
        this->allValuesArr[17] = static_cast<u1>(tempString[0x91273]);
        this->allValuesArr[18] = static_cast<u1>(tempString[0x91274]);
        this->allValuesArr[19] = static_cast<u1>(tempString[0x91275]);
        this->allValuesArr[20] = static_cast<u1>(tempString[0x91276]);
        this->allValuesArr[21] = static_cast<u1>(tempString[0x91277]);
        this->allValuesArr[22] = static_cast<u1>(tempString[0x91278]);
        this->allValuesArr[23] = static_cast<u1>(tempString[0x91279]);
        this->allValuesArr[24] = static_cast<u1>(tempString[0x9127A]);
        this->allValuesArr[25] = static_cast<u1>(tempString[0x9127B]);
        this->allValuesArr[26] = static_cast<u1>(tempString[0x9127C]);
        this->allValuesArr[27] = static_cast<u1>(tempString[0x9127D]);
        this->allValuesArr[28] = static_cast<u1>(tempString[0x9127E]);
        this->allValuesArr[29] = static_cast<u1>(tempString[0x9127F]);
        this->allValuesArr[30] = static_cast<u1>(tempString[0x91280]);
        this->allValuesArr[31] = static_cast<u1>(tempString[0x91281]);
        this->allValuesArr[32] = static_cast<u1>(tempString[0x91282]);
        this->allValuesArr[33] = static_cast<u1>(tempString[0x91283]);
        this->allValuesArr[34] = static_cast<u1>(tempString[0x91284]);
        this->allValuesArr[35] = static_cast<u1>(tempString[0x91285]);
        this->allValuesArr[36] = static_cast<u1>(tempString[0x91286]);
        this->allValuesArr[37] = static_cast<u1>(tempString[0x91287]);
        this->allValuesArr[38] = static_cast<u1>(tempString[0x91288]);

        this->xValue_1 = this->allValuesArr[5];
        this->xValue_2 = this->allValuesArr[6];
        this->xValue_3 = this->allValuesArr[7];
        this->xValue_4 = this->allValuesArr[8];
        this->xValue_5 = this->allValuesArr[9];
        this->xValue_6 = this->allValuesArr[10];
        this->xValue_7 = this->allValuesArr[11];
        this->xValue_8 = this->allValuesArr[12];
        this->yValue_1 = this->allValuesArr[13];
        this->yValue_2 = this->allValuesArr[14];
        this->yValue_3 = this->allValuesArr[15];
        this->yValue_4 = this->allValuesArr[16];
        this->yValue_5 = this->allValuesArr[17];
        this->yValue_6 = this->allValuesArr[18];
        this->yValue_7 = this->allValuesArr[19];
        this->yValue_8 = this->allValuesArr[20];
        this->zValue_1 = this->allValuesArr[21];
        this->zValue_2 = this->allValuesArr[22];
        this->zValue_3 = this->allValuesArr[23];
        this->zValue_4 = this->allValuesArr[24];
        this->zValue_5 = this->allValuesArr[25];
        this->zValue_6 = this->allValuesArr[26];
        this->zValue_7 = this->allValuesArr[27];
        this->zValue_8 = this->allValuesArr[28];
        this->sigmaValue_1 = this->allValuesArr[29];
        this->sigmaValue_2 = this->allValuesArr[30];
        this->sigmaValue_3 = this->allValuesArr[31];
        this->sigmaValue_4 = this->allValuesArr[32];
        this->solTypeValue = this->allValuesArr[33];
        this->timeValue_1 = this->allValuesArr[34];
        this->timeValue_2 = this->allValuesArr[35];
        this->timeValue_3 = this->allValuesArr[36];
        this->timeValue_4 = this->allValuesArr[37];
        this->csValue = this->allValuesArr[38];

        handleCS();
        printVar();

        inputFile.close();
        return 0;
    }
}

void Var35::handleCS()
{
    this->checkSum = cs(allValuesArr, 38);
}

void Var35::printVar()
{
    std::ofstream outputFile;
    std::string outputFileName = "Var35_output.txt";
    outputFile.open(outputFileName);

    outputFile << "Var 35" << std::endl;
    outputFile << "Structure Baseline" << std::endl;
    outputFile << "Calculated baseline vector coordinates" << std::endl;
    outputFile << "x: "
        << static_cast<int>(xValue_1) << " "
        << static_cast<int>(xValue_2) << " "
        << static_cast<int>(xValue_3) << " "
        << static_cast<int>(xValue_4) << " "
        << static_cast<int>(xValue_5) << " "
        << static_cast<int>(xValue_6) << " "
        << static_cast<int>(xValue_7) << " "
        << static_cast<int>(xValue_8) << std::endl;
    outputFile << "y: "
        << static_cast<int>(yValue_1) << " "
        << static_cast<int>(yValue_2) << " "
        << static_cast<int>(yValue_3) << " "
        << static_cast<int>(yValue_4) << " "
        << static_cast<int>(yValue_5) << " "
        << static_cast<int>(yValue_6) << " "
        << static_cast<int>(yValue_7) << " "
        << static_cast<int>(yValue_8) << std::endl;
    outputFile << "z: "
        << static_cast<int>(zValue_1) << " "
        << static_cast<int>(zValue_2) << " "
        << static_cast<int>(zValue_3) << " "
        << static_cast<int>(zValue_4) << " "
        << static_cast<int>(zValue_5) << " "
        << static_cast<int>(zValue_6) << " "
        << static_cast<int>(zValue_7) << " "
        << static_cast<int>(zValue_8) << std::endl;
    outputFile << "Baseline Spherical Error Probable: "
        << static_cast<int>(sigmaValue_1) << " "
        << static_cast<int>(sigmaValue_2) << " "
        << static_cast<int>(sigmaValue_3) << " "
        << static_cast<int>(sigmaValue_4) << std::endl;
    outputFile << "Solution type: " << static_cast<int>(solTypeValue) << std::endl;
    outputFile << "Receiver time of the baseline estimate: "
        << static_cast<int>(timeValue_1) << " "
        << static_cast<int>(timeValue_2) << " "
        << static_cast<int>(timeValue_3) << " "
        << static_cast<int>(timeValue_4) << std::endl;
    outputFile << "Checksum of the string: " << static_cast<int>(checkSum) << std::endl;
    outputFile << "Parameter u1 cs: " << static_cast<int>(csValue);

    outputFile.close();
}

int Var35::getCheckSum()
{
    return this->checkSum;
}

int Var35::getCsValue()
{
    return this->csValue;
}