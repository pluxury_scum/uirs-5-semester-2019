#include <iostream>
#include <fstream>
#include "../include/Var55.h"

int Var55::handleVar()
{
    std::ifstream inputFile;
    std::string inputFileName = "rks.dat";
    inputFile.open(inputFileName, std::ios::in | std::ios::binary);

    if (!inputFile.is_open())
    {
        std::cerr << "Error. File was not overwritten" << std::endl;
        exit(1);
    }
    else
    {
        int tempInt = 0; // variable for memory allocation
        std::string tempString; // char array holds all the characters of the file
        inputFile.read((char*)&tempInt, sizeof(tempInt)); // read string size from file
        tempString.resize(tempInt); // allocate memory of required size
        inputFile.read(&tempString[0], tempInt); // read string from file

        this->allValuesArr[0] = static_cast<u1>(tempString[0x9A]);
        this->allValuesArr[1] = static_cast<u1>(tempString[0x9B]);
        this->allValuesArr[2] = static_cast<u1>(tempString[0x9C]);
        this->allValuesArr[3] = static_cast<u1>(tempString[0x9D]);
        this->allValuesArr[4] = static_cast<u1>(tempString[0x9E]);
        this->allValuesArr[5] = static_cast<u1>(tempString[0x9F]);
        this->allValuesArr[6] = static_cast<u1>(tempString[0xA0]);
        this->allValuesArr[7] = static_cast<u1>(tempString[0xA1]);
        this->allValuesArr[8] = static_cast<u1>(tempString[0xA2]);
        this->allValuesArr[9] = static_cast<u1>(tempString[0xA3]);
        this->allValuesArr[10] = static_cast<u1>(tempString[0xA4]);
        this->allValuesArr[11] = static_cast<u1>(tempString[0xA5]);
        this->allValuesArr[12] = static_cast<u1>(tempString[0xA6]);
        this->allValuesArr[13] = static_cast<u1>(tempString[0xA7]);
        this->allValuesArr[14] = static_cast<u1>(tempString[0xA8]);

        this->msValue_1 = this->allValuesArr[5];
        this->msValue_2 = this->allValuesArr[6];
        this->msValue_3 = this->allValuesArr[7];
        this->msValue_4 = this->allValuesArr[8];
        this->nsValue_1 = this->allValuesArr[9];
        this->nsValue_2 = this->allValuesArr[10];
        this->nsValue_3 = this->allValuesArr[11];
        this->nsValue_4 = this->allValuesArr[12];
        this->timeScaleValue = this->allValuesArr[13];
        this->csValue = this->allValuesArr[14];

        handleCS();
        printVar();

        inputFile.close();
        return 0;
    }
}

void Var55::handleCS()
{
    this->checkSum = cs(allValuesArr, 14);
}

void Var55::printVar()
{
    std::ofstream outputFile;
    std::string outputFileName = "Var55_output.txt";
    outputFile.open(outputFileName);

    outputFile << "Var 55" << std::endl;
    outputFile << "Structure ExtEvent" << std::endl;
    outputFile << "Ms part of event time tag: "
        << static_cast<int>(msValue_1) << " "
        << static_cast<int>(msValue_2) << " "
        << static_cast<int>(msValue_3) << " "
        << static_cast<int>(msValue_4) << std::endl;
    outputFile << "Ns part of event time tag: "
        << static_cast<int>(nsValue_1) << " "
        << static_cast<int>(nsValue_2) << " "
        << static_cast<int>(nsValue_3) << " "
        << static_cast<int>(nsValue_4) << std::endl;
    outputFile << "Time scale: " << static_cast<int>(timeScaleValue) << std::endl;
    outputFile << "Checksum of the string: " << static_cast<int>(checkSum) << std::endl;
    outputFile << "Parameter u1 cs: " << static_cast<int>(csValue);

    outputFile.close();
}

int Var55::getCheckSum()
{
    return this->checkSum;
}

int Var55::getCsValue()
{
    return this->csValue;
}