#include <iostream>
#include <fstream>
#include "../include/Var29.h"

int Var29::handleVar()
{
    std::ifstream inputFile;
    std::string inputFileName = "rks.dat";
    inputFile.open(inputFileName, std::ios::in | std::ios::binary);

    if (!inputFile.is_open())
    {
        std::cerr << "Error. File was not overwritten" << std::endl;
        exit(1);
    }
    else
    {
        int tempInt = 0; // variable for memory allocation
        std::string tempString; // char array holds all the characters of the file
        inputFile.read((char*)&tempInt, sizeof(tempInt)); // read string size from file
        tempString.resize(tempInt); // allocate memory of required size
        inputFile.read(&tempString[0], tempInt); // read string from file

        this->allValuesArr[0] = static_cast<u1>(tempString[0x8EB74]);
        this->allValuesArr[1] = static_cast<u1>(tempString[0x8EB75]);
        this->allValuesArr[2] = static_cast<u1>(tempString[0x8EB76]);
        this->allValuesArr[3] = static_cast<u1>(tempString[0x8EB77]);
        this->allValuesArr[4] = static_cast<u1>(tempString[0x8EB78]);
        this->allValuesArr[5] = static_cast<u1>(tempString[0x8EB79]);
        this->allValuesArr[6] = static_cast<u1>(tempString[0x8EB7A]);
        this->allValuesArr[7] = static_cast<u1>(tempString[0x8EB7B]);
        this->allValuesArr[8] = static_cast<u1>(tempString[0x8EB7C]);
        this->allValuesArr[9] = static_cast<u1>(tempString[0x8EB7D]);
        this->allValuesArr[10] = static_cast<u1>(tempString[0x8EB7E]);
        this->allValuesArr[11] = static_cast<u1>(tempString[0x8EB7F]);
        this->allValuesArr[12] = static_cast<u1>(tempString[0x8EB80]);
        this->allValuesArr[13] = static_cast<u1>(tempString[0x8EB81]);
        this->allValuesArr[14] = static_cast<u1>(tempString[0x8EB82]);
        this->allValuesArr[15] = static_cast<u1>(tempString[0x8EB83]);
        this->allValuesArr[16] = static_cast<u1>(tempString[0x8EB84]);
        this->allValuesArr[17] = static_cast<u1>(tempString[0x8EB85]);
        this->allValuesArr[18] = static_cast<u1>(tempString[0x8EB86]);
        this->allValuesArr[19] = static_cast<u1>(tempString[0x8EB87]);
        this->allValuesArr[20] = static_cast<u1>(tempString[0x8EB88]);
        this->allValuesArr[21] = static_cast<u1>(tempString[0x8EB89]);
        this->allValuesArr[22] = static_cast<u1>(tempString[0x8EB8A]);

        this->latValue_1 = this->allValuesArr[5];
        this->latValue_2 = this->allValuesArr[6];
        this->latValue_3 = this->allValuesArr[7];
        this->latValue_4 = this->allValuesArr[8];
        this->lonValue_1 = this->allValuesArr[9];
        this->lonValue_2 = this->allValuesArr[10];
        this->lonValue_3 = this->allValuesArr[11];
        this->lonValue_4 = this->allValuesArr[12];
        this->altValue_1 = this->allValuesArr[13];
        this->altValue_2 = this->allValuesArr[14];
        this->altValue_3 = this->allValuesArr[15];
        this->altValue_4 = this->allValuesArr[16];
        this->vSigmaValue_1 = this->allValuesArr[17];
        this->vSigmaValue_2 = this->allValuesArr[18];
        this->vSigmaValue_3 = this->allValuesArr[19];
        this->vSigmaValue_4 = this->allValuesArr[20];
        this->solTypeValue = this->allValuesArr[21];
        this->csValue = this->allValuesArr[22];

        handleCS();
        printVar();

        inputFile.close();
        return 0;
    }
}

void Var29::handleCS()
{
    this->checkSum = cs(allValuesArr, 22);
}

void Var29::printVar()
{
    std::ofstream outputFile;
    std::string outputFileName = "Var29_output.txt";
    outputFile.open(outputFileName);

    outputFile << "Var 29" << std::endl;
    outputFile << "Structure GeoVel" << std::endl;
    outputFile << "Northing velocity: "
        << static_cast<int>(latValue_1) << " "
        << static_cast<int>(latValue_2) << " "
        << static_cast<int>(latValue_3) << " "
        << static_cast<int>(latValue_4) << std::endl;
    outputFile << "Easting velocity: "
        << static_cast<int>(lonValue_1) << " "
        << static_cast<int>(lonValue_2) << " "
        << static_cast<int>(lonValue_3) << " "
        << static_cast<int>(lonValue_4) << std::endl;
    outputFile << "Height velocity: "
        << static_cast<int>(altValue_1) << " "
        << static_cast<int>(altValue_2) << " "
        << static_cast<int>(altValue_3) << " "
        << static_cast<int>(altValue_4) << std::endl;
    outputFile << "Velocity SEP: "
        << static_cast<int>(vSigmaValue_1) << " "
        << static_cast<int>(vSigmaValue_2) << " "
        << static_cast<int>(vSigmaValue_3) << " "
        << static_cast<int>(vSigmaValue_4) << std::endl;
    outputFile << "Solution type: " << static_cast<int>(solTypeValue) << std::endl;
    outputFile << "Checksum of the string: " << static_cast<int>(checkSum) << std::endl;
    outputFile << "Parameter u1 cs: " << static_cast<int>(csValue);

    outputFile.close();
}

int Var29::getCheckSum()
{
    return this->checkSum;
}

int Var29::getCsValue()
{
    return this->csValue;
}