#include <iostream>
#include <fstream>
#include "../include/Var64.h"

int Var64::handleVar()
{
    std::ifstream inputFile;
    std::string inputFileName = "rks.dat";
    inputFile.open(inputFileName, std::ios::in | std::ios::binary);

    if (!inputFile.is_open())
    {
        std::cerr << "Error. File was not overwritten" << std::endl;
        exit(1);
    }
    else
    {
        int tempInt = 0; // variable for memory allocation
        std::string tempString; // char array holds all the characters of the file
        inputFile.read((char*)&tempInt, sizeof(tempInt)); // read string size from file
        tempString.resize(tempInt); // allocate memory of required size
        inputFile.read(&tempString[0], tempInt); // read string from file

        this->allValuesArr[0] = static_cast<u1>(tempString[0x8BACB]);
        this->allValuesArr[1] = static_cast<u1>(tempString[0x8BACC]);
        this->allValuesArr[2] = static_cast<u1>(tempString[0x8BACD]);
        this->allValuesArr[3] = static_cast<u1>(tempString[0x8BACE]);
        this->allValuesArr[4] = static_cast<u1>(tempString[0x8BACF]);
        this->allValuesArr[5] = static_cast<u1>(tempString[0x8BAD0]);
        this->allValuesArr[6] = static_cast<u1>(tempString[0x8BAD1]);
        this->allValuesArr[7] = static_cast<u1>(tempString[0x8BAD2]);
        this->allValuesArr[8] = static_cast<u1>(tempString[0x8BAD3]);
        this->allValuesArr[9] = static_cast<u1>(tempString[0x8BAD4]);
        this->allValuesArr[10] = static_cast<u1>(tempString[0x8BAD5]);
        this->allValuesArr[11] = static_cast<u1>(tempString[0x8BAD6]);
        this->allValuesArr[12] = static_cast<u1>(tempString[0x8BAD7]);
        this->allValuesArr[13] = static_cast<u1>(tempString[0x8BAD8]);
        this->allValuesArr[14] = static_cast<u1>(tempString[0x8BAD9]);
        this->allValuesArr[15] = static_cast<u1>(tempString[0x8BADA]);
        this->allValuesArr[16] = static_cast<u1>(tempString[0x8BADB]);
        this->allValuesArr[17] = static_cast<u1>(tempString[0x8BADC]);
        this->allValuesArr[18] = static_cast<u1>(tempString[0x8BADD]);
        this->allValuesArr[19] = static_cast<u1>(tempString[0x8BADE]);
        this->allValuesArr[20] = static_cast<u1>(tempString[0x8BADF]);
        this->allValuesArr[21] = static_cast<u1>(tempString[0x8BAE0]);
        this->allValuesArr[22] = static_cast<u1>(tempString[0x8BAE1]);
        this->allValuesArr[23] = static_cast<u1>(tempString[0x8BAE2]);
        this->allValuesArr[24] = static_cast<u1>(tempString[0x8BAE3]);
        this->allValuesArr[25] = static_cast<u1>(tempString[0x8BAE4]);
        this->allValuesArr[26] = static_cast<u1>(tempString[0x8BAE5]);
        this->allValuesArr[27] = static_cast<u1>(tempString[0x8BAE6]);
        this->allValuesArr[28] = static_cast<u1>(tempString[0x8BAE7]);
        this->allValuesArr[29] = static_cast<u1>(tempString[0x8BAE8]);
        this->allValuesArr[30] = static_cast<u1>(tempString[0x8BAE9]);
        this->allValuesArr[31] = static_cast<u1>(tempString[0x8BAEA]);
        this->allValuesArr[32] = static_cast<u1>(tempString[0x8BAEB]);

        this->xValue_1 = this->allValuesArr[5];
        this->xValue_2 = this->allValuesArr[6];
        this->xValue_3 = this->allValuesArr[7];
        this->xValue_4 = this->allValuesArr[8];
        this->xValue_5 = this->allValuesArr[9];
        this->xValue_6 = this->allValuesArr[10];
        this->xValue_7 = this->allValuesArr[11];
        this->xValue_8 = this->allValuesArr[12];
        this->yValue_1 = this->allValuesArr[13];
        this->yValue_2 = this->allValuesArr[14];
        this->yValue_3 = this->allValuesArr[15];
        this->yValue_4 = this->allValuesArr[16];
        this->yValue_5 = this->allValuesArr[17];
        this->yValue_6 = this->allValuesArr[18];
        this->yValue_7 = this->allValuesArr[19];
        this->yValue_8 = this->allValuesArr[20];
        this->zValue_1 = this->allValuesArr[21];
        this->zValue_2 = this->allValuesArr[22];
        this->zValue_3 = this->allValuesArr[23];
        this->zValue_4 = this->allValuesArr[24];
        this->zValue_5 = this->allValuesArr[25];
        this->zValue_6 = this->allValuesArr[26];
        this->zValue_7 = this->allValuesArr[27];
        this->zValue_8 = this->allValuesArr[28];
        this->idValue_1 = this->allValuesArr[29];
        this->idValue_2 = this->allValuesArr[30];
        this->solTypeValue = this->allValuesArr[31];
        this->csValue = this->allValuesArr[32];

        handleCS();
        printVar();

        inputFile.close();
        return 0;
    }
}

void Var64::handleCS()
{
    this->checkSum = cs(allValuesArr, 32);
}

void Var64::printVar()
{
    std::ofstream outputFile;
    std::string outputFileName = "Var64_output.txt";
    outputFile.open(outputFileName);

    outputFile << "Var 64" << std::endl;
    outputFile << "Structure BaseInfo" << std::endl;
    outputFile << "ECEF coordinates: " << std::endl;
    outputFile << "x: "
        << static_cast<int>(xValue_1) << " "
        << static_cast<int>(xValue_2) << " "
        << static_cast<int>(xValue_3) << " "
        << static_cast<int>(xValue_4) << " "
        << static_cast<int>(xValue_5) << " "
        << static_cast<int>(xValue_6) << " "
        << static_cast<int>(xValue_7) << " "
        << static_cast<int>(xValue_8) << std::endl;
    outputFile << "y: "
        << static_cast<int>(yValue_1) << " "
        << static_cast<int>(yValue_2) << " "
        << static_cast<int>(yValue_3) << " "
        << static_cast<int>(yValue_4) << " "
        << static_cast<int>(yValue_5) << " "
        << static_cast<int>(yValue_6) << " "
        << static_cast<int>(yValue_7) << " "
        << static_cast<int>(yValue_8) << std::endl;
    outputFile << "z: "
        << static_cast<int>(zValue_1) << " "
        << static_cast<int>(zValue_2) << " "
        << static_cast<int>(zValue_3) << " "
        << static_cast<int>(zValue_4) << " "
        << static_cast<int>(zValue_5) << " "
        << static_cast<int>(zValue_6) << " "
        << static_cast<int>(zValue_7) << " "
        << static_cast<int>(zValue_8) << std::endl;
    outputFile << "Reference station ID: "
        << static_cast<int>(idValue_1) << " "
        << static_cast<int>(idValue_2) << std::endl;
    outputFile << "Solution type: " << static_cast<int>(solTypeValue) << std::endl;
    outputFile << "Checksum of the string: " << static_cast<int>(checkSum) << std::endl;
    outputFile << "Parameter u1 cs: " << static_cast<int>(csValue);

    outputFile.close();
}

int Var64::getCheckSum()
{
    return this->checkSum;
}

int Var64::getCsValue()
{
    return this->csValue;
}