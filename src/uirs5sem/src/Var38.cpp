#include <iostream>
#include <fstream>
#include "../include/Var38.h"

int Var38::handleVar()
{
    std::ifstream inputFile;
    std::string inputFileName = "rks.dat";
    inputFile.open(inputFileName, std::ios::in | std::ios::binary);

    if (!inputFile.is_open())
    {
        std::cerr << "Error. File was not overwritten" << std::endl;
        exit(1);
    }
    else
    {
        int tempInt = 0; // variable for memory allocation
        std::string tempString; // char array holds all the characters of the file
        inputFile.read((char*)&tempInt, sizeof(tempInt)); // read string size from file
        tempString.resize(tempInt); // allocate memory of required size
        inputFile.read(&tempString[0], tempInt); // read string from file

        this->allValuesArr[0] = static_cast<u1>(tempString[0x9ED75]);
        this->allValuesArr[1] = static_cast<u1>(tempString[0x9ED76]);
        this->allValuesArr[2] = static_cast<u1>(tempString[0x9ED77]);
        this->allValuesArr[3] = static_cast<u1>(tempString[0x9ED78]);
        this->allValuesArr[4] = static_cast<u1>(tempString[0x9ED79]);
        this->allValuesArr[5] = static_cast<u1>(tempString[0x9ED7A]);
        this->allValuesArr[6] = static_cast<u1>(tempString[0x9ED7B]);
        this->allValuesArr[7] = static_cast<u1>(tempString[0x9ED7C]);
        this->allValuesArr[8] = static_cast<u1>(tempString[0x9ED7D]);
        this->allValuesArr[9] = static_cast<u1>(tempString[0x9ED7E]);

        this->ptValue_1 = this->allValuesArr[5];
        this->ptValue_2 = this->allValuesArr[6];
        this->ptValue_3 = this->allValuesArr[7];
        this->ptValue_4 = this->allValuesArr[8];
        this->csValue = this->allValuesArr[9];

        handleCS();
        printVar();

        inputFile.close();
        return 0;
    }
}

void Var38::handleCS()
{
    this->checkSum = cs(allValuesArr, 9);
}

void Var38::printVar()
{
    std::ofstream outputFile;
    std::string outputFileName = "Var38_output.txt";
    outputFile.open(outputFileName);

    outputFile << "Var 38" << std::endl;
    outputFile << "Structure PosCompTime" << std::endl;
    outputFile << "Continuous position computation time: "
        << static_cast<int>(ptValue_1) << " "
        << static_cast<int>(ptValue_2) << " "
        << static_cast<int>(ptValue_3) << " "
        << static_cast<int>(ptValue_4) << std::endl;
    outputFile << "Checksum of the string: " << static_cast<int>(checkSum) << std::endl;
    outputFile << "Parameter u1 cs: " << static_cast<int>(csValue);

    outputFile.close();
}

int Var38::getCheckSum()
{
    return this->checkSum;
}

int Var38::getCsValue()
{
    return this->csValue;
}