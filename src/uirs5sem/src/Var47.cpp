#include <iostream>
#include <fstream>
#include "../include/Var47.h"

int Var47::handleVar()
{
    std::ifstream inputFile;
    std::string inputFileName = "rks.dat";
    inputFile.open(inputFileName, std::ios::in | std::ios::binary);

    if (!inputFile.is_open())
    {
        std::cerr << "Error. File was not overwritten" << std::endl;
        exit(1);
    }
    else
    {
        int tempInt = 0; // variable for memory allocation
        std::string tempString; // char array holds all the characters of the file
        inputFile.read((char*)&tempInt, sizeof(tempInt)); // read string size from file
        tempString.resize(tempInt); // allocate memory of required size
        inputFile.read(&tempString[0], tempInt); // read string from file

        this->allValuesArr[0] = static_cast<u1>(tempString[0x3B5]);
        this->allValuesArr[1] = static_cast<u1>(tempString[0x3B6]);
        this->allValuesArr[2] = static_cast<u1>(tempString[0x3B7]);
        this->allValuesArr[3] = static_cast<u1>(tempString[0x3B8]);
        this->allValuesArr[4] = static_cast<u1>(tempString[0x3B9]);
        this->allValuesArr[5] = static_cast<u1>(tempString[0x3BA]);
        this->allValuesArr[6] = static_cast<u1>(tempString[0x3BB]);
        this->allValuesArr[7] = static_cast<u1>(tempString[0x3BC]);
        this->allValuesArr[8] = static_cast<u1>(tempString[0x3BD]);
        this->allValuesArr[9] = static_cast<u1>(tempString[0x3BE]);
        this->allValuesArr[10] = static_cast<u1>(tempString[0x3BF]);
        this->allValuesArr[11] = static_cast<u1>(tempString[0x3C0]);
        this->allValuesArr[12] = static_cast<u1>(tempString[0x3C1]);
        this->allValuesArr[13] = static_cast<u1>(tempString[0x3C2]);
        this->allValuesArr[14] = static_cast<u1>(tempString[0x3C3]);
        this->allValuesArr[15] = static_cast<u1>(tempString[0x3C4]);
        this->allValuesArr[16] = static_cast<u1>(tempString[0x3C5]);
        this->allValuesArr[17] = static_cast<u1>(tempString[0x3C6]);
        this->allValuesArr[18] = static_cast<u1>(tempString[0x3C7]);
        this->allValuesArr[19] = static_cast<u1>(tempString[0x3C8]);
        this->allValuesArr[20] = static_cast<u1>(tempString[0x3C9]);
        this->allValuesArr[21] = static_cast<u1>(tempString[0x3CA]);
        this->allValuesArr[22] = static_cast<u1>(tempString[0x3CB]);
        this->allValuesArr[23] = static_cast<u1>(tempString[0x3CC]);
        this->allValuesArr[24] = static_cast<u1>(tempString[0x3CD]);
        this->allValuesArr[25] = static_cast<u1>(tempString[0x3CE]);
        this->allValuesArr[26] = static_cast<u1>(tempString[0x3CF]);
        this->allValuesArr[27] = static_cast<u1>(tempString[0x3D0]);
        this->allValuesArr[28] = static_cast<u1>(tempString[0x3D1]);
        this->allValuesArr[29] = static_cast<u1>(tempString[0x3D2]);
        this->allValuesArr[30] = static_cast<u1>(tempString[0x3D3]);
        this->allValuesArr[31] = static_cast<u1>(tempString[0x3D4]);
        this->allValuesArr[32] = static_cast<u1>(tempString[0x3D5]);
        this->allValuesArr[33] = static_cast<u1>(tempString[0x3D6]);
        this->allValuesArr[34] = static_cast<u1>(tempString[0x3D7]);
        this->allValuesArr[35] = static_cast<u1>(tempString[0x3D8]);
        this->allValuesArr[36] = static_cast<u1>(tempString[0x3D9]);
        this->allValuesArr[37] = static_cast<u1>(tempString[0x3DA]);
        this->allValuesArr[38] = static_cast<u1>(tempString[0x3DB]);
        this->allValuesArr[39] = static_cast<u1>(tempString[0x3DC]);
        this->allValuesArr[40] = static_cast<u1>(tempString[0x3DD]);
        this->allValuesArr[41] = static_cast<u1>(tempString[0x3DE]);
        this->allValuesArr[42] = static_cast<u1>(tempString[0x3DF]);
        this->allValuesArr[43] = static_cast<u1>(tempString[0x3E0]);
        this->allValuesArr[44] = static_cast<u1>(tempString[0x3E1]);
        this->allValuesArr[45] = static_cast<u1>(tempString[0x3E2]);
        this->allValuesArr[46] = static_cast<u1>(tempString[0x3E3]);
        this->allValuesArr[47] = static_cast<u1>(tempString[0x3E4]);
        this->allValuesArr[48] = static_cast<u1>(tempString[0x3E5]);
        this->allValuesArr[49] = static_cast<u1>(tempString[0x3E6]);
        this->allValuesArr[50] = static_cast<u1>(tempString[0x3E7]);
        this->allValuesArr[51] = static_cast<u1>(tempString[0x3E8]);
        this->allValuesArr[52] = static_cast<u1>(tempString[0x3E9]);
        this->allValuesArr[53] = static_cast<u1>(tempString[0x3EA]);
        this->allValuesArr[54] = static_cast<u1>(tempString[0x3EB]);
        this->allValuesArr[55] = static_cast<u1>(tempString[0x3EC]);
        this->allValuesArr[56] = static_cast<u1>(tempString[0x3ED]);
        this->allValuesArr[57] = static_cast<u1>(tempString[0x3EE]);
        this->allValuesArr[58] = static_cast<u1>(tempString[0x3EF]);
        this->allValuesArr[59] = static_cast<u1>(tempString[0x3F0]);
        this->allValuesArr[60] = static_cast<u1>(tempString[0x3F1]);
        this->allValuesArr[61] = static_cast<u1>(tempString[0x3F2]);
        this->allValuesArr[62] = static_cast<u1>(tempString[0x3F3]);
        this->allValuesArr[63] = static_cast<u1>(tempString[0x3F4]);
        this->allValuesArr[64] = static_cast<u1>(tempString[0x3F5]);
        this->allValuesArr[65] = static_cast<u1>(tempString[0x3F6]);
        this->allValuesArr[66] = static_cast<u1>(tempString[0x3F7]);
        this->allValuesArr[67] = static_cast<u1>(tempString[0x3F8]);
        this->allValuesArr[68] = static_cast<u1>(tempString[0x3F9]);
        this->allValuesArr[69] = static_cast<u1>(tempString[0x3FA]);
        this->allValuesArr[70] = static_cast<u1>(tempString[0x3FB]);
        this->allValuesArr[71] = static_cast<u1>(tempString[0x3FC]);
        this->allValuesArr[72] = static_cast<u1>(tempString[0x3FD]);
        this->allValuesArr[73] = static_cast<u1>(tempString[0x3FE]);
        this->allValuesArr[74] = static_cast<u1>(tempString[0x3FF]);
        this->allValuesArr[75] = static_cast<u1>(tempString[0x400]);
        this->allValuesArr[76] = static_cast<u1>(tempString[0x401]);
        this->allValuesArr[77] = static_cast<u1>(tempString[0x402]);
        this->allValuesArr[78] = static_cast<u1>(tempString[0x403]);
        this->allValuesArr[79] = static_cast<u1>(tempString[0x404]);
        this->allValuesArr[80] = static_cast<u1>(tempString[0x405]);
        this->allValuesArr[81] = static_cast<u1>(tempString[0x406]);
        this->allValuesArr[82] = static_cast<u1>(tempString[0x407]);
        this->allValuesArr[83] = static_cast<u1>(tempString[0x408]);
        this->allValuesArr[84] = static_cast<u1>(tempString[0x409]);
        this->allValuesArr[85] = static_cast<u1>(tempString[0x40A]);
        this->allValuesArr[86] = static_cast<u1>(tempString[0x40B]);
        this->allValuesArr[87] = static_cast<u1>(tempString[0x40C]);
        this->allValuesArr[88] = static_cast<u1>(tempString[0x40D]);
        this->allValuesArr[89] = static_cast<u1>(tempString[0x40E]);
        this->allValuesArr[90] = static_cast<u1>(tempString[0x40F]);
        this->allValuesArr[91] = static_cast<u1>(tempString[0x410]);
        this->allValuesArr[92] = static_cast<u1>(tempString[0x411]);
        this->allValuesArr[93] = static_cast<u1>(tempString[0x412]);
        this->allValuesArr[94] = static_cast<u1>(tempString[0x413]);
        this->allValuesArr[95] = static_cast<u1>(tempString[0x414]);
        this->allValuesArr[96] = static_cast<u1>(tempString[0x415]);
        this->allValuesArr[97] = static_cast<u1>(tempString[0x416]);
        this->allValuesArr[98] = static_cast<u1>(tempString[0x417]);
        this->allValuesArr[99] = static_cast<u1>(tempString[0x418]);
        this->allValuesArr[100] = static_cast<u1>(tempString[0x419]);
        this->allValuesArr[101] = static_cast<u1>(tempString[0x41A]);
        this->allValuesArr[102] = static_cast<u1>(tempString[0x41B]);
        this->allValuesArr[103] = static_cast<u1>(tempString[0x41C]);
        this->allValuesArr[104] = static_cast<u1>(tempString[0x41D]);
        this->allValuesArr[105] = static_cast<u1>(tempString[0x41E]);
        this->allValuesArr[106] = static_cast<u1>(tempString[0x41F]);
        this->allValuesArr[107] = static_cast<u1>(tempString[0x420]);
        this->allValuesArr[108] = static_cast<u1>(tempString[0x421]);
        this->allValuesArr[109] = static_cast<u1>(tempString[0x422]);
        this->allValuesArr[110] = static_cast<u1>(tempString[0x423]);
        this->allValuesArr[111] = static_cast<u1>(tempString[0x424]);
        this->allValuesArr[112] = static_cast<u1>(tempString[0x425]);
        this->allValuesArr[113] = static_cast<u1>(tempString[0x426]);
        this->allValuesArr[114] = static_cast<u1>(tempString[0x427]);
        this->allValuesArr[115] = static_cast<u1>(tempString[0x428]);
        this->allValuesArr[116] = static_cast<u1>(tempString[0x429]);
        this->allValuesArr[117] = static_cast<u1>(tempString[0x42A]);
        this->allValuesArr[118] = static_cast<u1>(tempString[0x42B]);
        this->allValuesArr[119] = static_cast<u1>(tempString[0x42C]);
        this->allValuesArr[120] = static_cast<u1>(tempString[0x42D]);
        this->allValuesArr[121] = static_cast<u1>(tempString[0x42E]);
        this->allValuesArr[122] = static_cast<u1>(tempString[0x42F]);
        this->allValuesArr[123] = static_cast<u1>(tempString[0x430]);
        this->allValuesArr[124] = static_cast<u1>(tempString[0x431]);
        this->allValuesArr[125] = static_cast<u1>(tempString[0x432]);
        this->allValuesArr[126] = static_cast<u1>(tempString[0x433]);
        this->allValuesArr[127] = static_cast<u1>(tempString[0x434]);
        this->allValuesArr[128] = static_cast<u1>(tempString[0x435]);
        this->allValuesArr[129] = static_cast<u1>(tempString[0x436]);
        this->allValuesArr[130] = static_cast<u1>(tempString[0x437]);
        this->allValuesArr[131] = static_cast<u1>(tempString[0x438]);
        this->allValuesArr[132] = static_cast<u1>(tempString[0x439]);
        this->allValuesArr[133] = static_cast<u1>(tempString[0x43A]);
        this->allValuesArr[134] = static_cast<u1>(tempString[0x43B]);
        this->allValuesArr[135] = static_cast<u1>(tempString[0x43C]);
        this->allValuesArr[136] = static_cast<u1>(tempString[0x43D]);

        this->svValue = this->allValuesArr[5];
        this->towValue_1 = this->allValuesArr[6];
        this->towValue_2 = this->allValuesArr[7];
        this->towValue_3 = this->allValuesArr[8];
        this->towValue_4 = this->allValuesArr[9];
        this->flagsValue = this->allValuesArr[10];
        this->iodcValue_1 = this->allValuesArr[11];
        this->iodcValue_2 = this->allValuesArr[12];
        this->tocValue_1 = this->allValuesArr[13];
        this->tocValue_2 = this->allValuesArr[14];
        this->tocValue_3 = this->allValuesArr[15];
        this->tocValue_4 = this->allValuesArr[16];
        this->uraValue = this->allValuesArr[17];
        this->healthSValue = this->allValuesArr[18];
        this->wnValue_1 = this->allValuesArr[19];
        this->wnValue_2 = this->allValuesArr[20];
        this->tgdValue_1 = this->allValuesArr[21];
        this->tgdValue_2 = this->allValuesArr[22];
        this->tgdValue_3 = this->allValuesArr[23];
        this->tgdValue_4 = this->allValuesArr[24];
        this->af2Value_1 = this->allValuesArr[25];
        this->af2Value_2 = this->allValuesArr[26];
        this->af2Value_3 = this->allValuesArr[27];
        this->af2Value_4 = this->allValuesArr[28];
        this->af1Value_1 = this->allValuesArr[29];
        this->af1Value_2 = this->allValuesArr[30];
        this->af1Value_3 = this->allValuesArr[31];
        this->af1Value_4 = this->allValuesArr[32];
        this->af0Value_1 = this->allValuesArr[33];
        this->af0Value_2 = this->allValuesArr[34];
        this->af0Value_3 = this->allValuesArr[35];
        this->af0Value_4 = this->allValuesArr[36];
        this->toeValue_1 = this->allValuesArr[37];
        this->toeValue_2 = this->allValuesArr[38];
        this->toeValue_3 = this->allValuesArr[39];
        this->toeValue_4 = this->allValuesArr[40];
        this->iodeValue_1 = this->allValuesArr[41];
        this->iodeValue_2 = this->allValuesArr[42];
        this->rootAValue_1 = this->allValuesArr[43];
        this->rootAValue_2 = this->allValuesArr[44];
        this->rootAValue_3 = this->allValuesArr[45];
        this->rootAValue_4 = this->allValuesArr[46];
        this->rootAValue_5 = this->allValuesArr[47];
        this->rootAValue_6 = this->allValuesArr[48];
        this->rootAValue_7 = this->allValuesArr[49];
        this->rootAValue_8 = this->allValuesArr[50];
        this->eccValue_1 = this->allValuesArr[51];
        this->eccValue_2 = this->allValuesArr[52];
        this->eccValue_3 = this->allValuesArr[53];
        this->eccValue_4 = this->allValuesArr[54];
        this->eccValue_5 = this->allValuesArr[55];
        this->eccValue_6 = this->allValuesArr[56];
        this->eccValue_7 = this->allValuesArr[57];
        this->eccValue_8 = this->allValuesArr[58];
        this->m0Value_1 = this->allValuesArr[59];
        this->m0Value_2 = this->allValuesArr[60];
        this->m0Value_3 = this->allValuesArr[61];
        this->m0Value_4 = this->allValuesArr[62];
        this->m0Value_5 = this->allValuesArr[63];
        this->m0Value_6 = this->allValuesArr[64];
        this->m0Value_7 = this->allValuesArr[65];
        this->m0Value_8 = this->allValuesArr[66];
        this->omega0Value_1 = this->allValuesArr[67];
        this->omega0Value_2 = this->allValuesArr[68];
        this->omega0Value_3 = this->allValuesArr[69];
        this->omega0Value_4 = this->allValuesArr[70];
        this->omega0Value_5 = this->allValuesArr[71];
        this->omega0Value_6 = this->allValuesArr[72];
        this->omega0Value_7 = this->allValuesArr[73];
        this->omega0Value_8 = this->allValuesArr[74];
        this->inc0Value_1 = this->allValuesArr[75];
        this->inc0Value_2 = this->allValuesArr[76];
        this->inc0Value_3 = this->allValuesArr[77];
        this->inc0Value_4 = this->allValuesArr[78];
        this->inc0Value_5 = this->allValuesArr[79];
        this->inc0Value_6 = this->allValuesArr[80];
        this->inc0Value_7 = this->allValuesArr[81];
        this->inc0Value_8 = this->allValuesArr[82];
        this->argPerValue_1 = this->allValuesArr[83];
        this->argPerValue_2 = this->allValuesArr[84];
        this->argPerValue_3 = this->allValuesArr[85];
        this->argPerValue_4 = this->allValuesArr[86];
        this->argPerValue_5 = this->allValuesArr[87];
        this->argPerValue_6 = this->allValuesArr[88];
        this->argPerValue_7 = this->allValuesArr[89];
        this->argPerValue_8 = this->allValuesArr[90];
        this->delnValue_1 = this->allValuesArr[91];
        this->delnValue_2 = this->allValuesArr[92];
        this->delnValue_3 = this->allValuesArr[93];
        this->delnValue_4 = this->allValuesArr[94];
        this->omegaDotValue_1 = this->allValuesArr[95];
        this->omegaDotValue_2 = this->allValuesArr[96];
        this->omegaDotValue_3 = this->allValuesArr[97];
        this->omegaDotValue_4 = this->allValuesArr[98];
        this->incDotValue_1 = this->allValuesArr[99];
        this->incDotValue_2 = this->allValuesArr[100];
        this->incDotValue_3 = this->allValuesArr[101];
        this->incDotValue_4 = this->allValuesArr[102];
        this->crcValue_1 = this->allValuesArr[103];
        this->crcValue_2 = this->allValuesArr[104];
        this->crcValue_3 = this->allValuesArr[105];
        this->crcValue_4 = this->allValuesArr[106];
        this->crsValue_1 = this->allValuesArr[107];
        this->crsValue_2 = this->allValuesArr[108];
        this->crsValue_3 = this->allValuesArr[109];
        this->crsValue_4 = this->allValuesArr[110];
        this->cucValue_1 = this->allValuesArr[111];
        this->cucValue_2 = this->allValuesArr[112];
        this->cucValue_3 = this->allValuesArr[113];
        this->cucValue_4 = this->allValuesArr[114];
        this->cusValue_1 = this->allValuesArr[115];
        this->cusValue_2 = this->allValuesArr[116];
        this->cusValue_3 = this->allValuesArr[117];
        this->cusValue_4 = this->allValuesArr[118];
        this->cicValue_1 = this->allValuesArr[119];
        this->cicValue_2 = this->allValuesArr[120];
        this->cicValue_3 = this->allValuesArr[121];
        this->cicValue_4 = this->allValuesArr[122];
        this->cisValue_1 = this->allValuesArr[123];
        this->cisValue_2 = this->allValuesArr[124];
        this->cisValue_3 = this->allValuesArr[125];
        this->cisValue_4 = this->allValuesArr[126];
        this->tgd2Value_1 = this->allValuesArr[127];
        this->tgd2Value_2 = this->allValuesArr[128];
        this->tgd2Value_3 = this->allValuesArr[129];
        this->tgd2Value_4 = this->allValuesArr[130];
        this->navTypeValue = this->allValuesArr[131];
        this->DAf0Value_1 = this->allValuesArr[132];
        this->DAf0Value_2 = this->allValuesArr[133];
        this->DAf0Value_3 = this->allValuesArr[134];
        this->DAf0Value_4 = this->allValuesArr[135];
        this->csValue = this->allValuesArr[136];

        handleCS();
        printVar();

        inputFile.close();
        return 0;
    }
}

void Var47::handleCS()
{
    this->checkSum = cs(allValuesArr, 136);
}

void Var47::printVar()
{
    std::ofstream outputFile;
    std::string outputFileName = "Var47_output.txt";
    outputFile.open(outputFileName);

    outputFile << "Var 47" << std::endl;
    outputFile << "Structure BeiDouEphemeris" << std::endl;
    outputFile << "SV PRN number: " << static_cast<int>(svValue) << std::endl;
    outputFile << "Time of week: "
        << static_cast<int>(towValue_1) << " "
        << static_cast<int>(towValue_2) << " "
        << static_cast<int>(towValue_3) << " "
        << static_cast<int>(towValue_4) << std::endl;
    outputFile << "Flags: " << static_cast<int>(flagsValue) << std::endl;
    outputFile << "Issue of data, clock: "
        << static_cast<int>(iodcValue_1) << " "
        << static_cast<int>(iodcValue_2) << std::endl;
    outputFile << "Clock data reference time: "
        << static_cast<int>(tocValue_1) << " "
        << static_cast<int>(tocValue_2) << " "
        << static_cast<int>(tocValue_3) << " "
        << static_cast<int>(tocValue_4) << std::endl;
    outputFile << "User range accuracy: " << static_cast<int>(uraValue) << std::endl;
    outputFile << "Satellite health: " << static_cast<int>(uraValue) << std::endl;
    outputFile << "Week number: " << static_cast<int>(healthSValue) << std::endl;
    outputFile << "Estimated group delay differential: "
        << static_cast<int>(wnValue_1) << " "
        << static_cast<int>(wnValue_2) << std::endl;
    outputFile << "Polynomial coefficient af2: "
        << static_cast<int>(af2Value_1) << " "
        << static_cast<int>(af2Value_2) << " "
        << static_cast<int>(af2Value_3) << " "
        << static_cast<int>(af2Value_4) << std::endl;
    outputFile << "Polynomial coefficient af1: "
        << static_cast<int>(af1Value_1) << " "
        << static_cast<int>(af1Value_2) << " "
        << static_cast<int>(af1Value_3) << " "
        << static_cast<int>(af1Value_4) << std::endl;
    outputFile << "Polynomial coefficient af0: "
        << static_cast<int>(af0Value_1) << " "
        << static_cast<int>(af0Value_2) << " "
        << static_cast<int>(af0Value_3) << " "
        << static_cast<int>(af0Value_4) << std::endl;
    outputFile << "Ephemeris reference time: "
        << static_cast<int>(toeValue_1) << " "
        << static_cast<int>(toeValue_2) << " "
        << static_cast<int>(toeValue_3) << " "
        << static_cast<int>(toeValue_4) << std::endl;
    outputFile << "Issue of data, ephemeris: "
        << static_cast<int>(iodeValue_1) << " "
        << static_cast<int>(iodeValue_2) << std::endl;
    outputFile << "Square root of the semi-major axis: "
        << static_cast<int>(rootAValue_1) << " "
        << static_cast<int>(rootAValue_2) << " "
        << static_cast<int>(rootAValue_3) << " "
        << static_cast<int>(rootAValue_4) << " "
        << static_cast<int>(rootAValue_5) << " "
        << static_cast<int>(rootAValue_6) << " "
        << static_cast<int>(rootAValue_7) << " "
        << static_cast<int>(rootAValue_8) << std::endl;
    outputFile << "Eccentricity: "
        << static_cast<int>(eccValue_1) << " "
        << static_cast<int>(eccValue_2) << " "
        << static_cast<int>(eccValue_3) << " "
        << static_cast<int>(eccValue_4) << " "
        << static_cast<int>(eccValue_5) << " "
        << static_cast<int>(eccValue_6) << " "
        << static_cast<int>(eccValue_7) << " "
        << static_cast<int>(eccValue_8) << std::endl;
    outputFile << "Mean Anomaly at reference time: "
        << static_cast<int>(m0Value_1) << " "
        << static_cast<int>(m0Value_2) << " "
        << static_cast<int>(m0Value_3) << " "
        << static_cast<int>(m0Value_4) << " "
        << static_cast<int>(m0Value_5) << " "
        << static_cast<int>(m0Value_6) << " "
        << static_cast<int>(m0Value_7) << " "
        << static_cast<int>(m0Value_8) << std::endl;
    outputFile << "Longitude of ascending node of orbit plane at the start of week �wn�: "
        << static_cast<int>(omega0Value_1) << " "
        << static_cast<int>(omega0Value_2) << " "
        << static_cast<int>(omega0Value_3) << " "
        << static_cast<int>(omega0Value_4) << " "
        << static_cast<int>(omega0Value_5) << " "
        << static_cast<int>(omega0Value_6) << " "
        << static_cast<int>(omega0Value_7) << " "
        << static_cast<int>(omega0Value_8) << std::endl;
    outputFile << "Inclination angle at reference time: "
        << static_cast<int>(inc0Value_1) << " "
        << static_cast<int>(inc0Value_2) << " "
        << static_cast<int>(inc0Value_3) << " "
        << static_cast<int>(inc0Value_4) << " "
        << static_cast<int>(inc0Value_5) << " "
        << static_cast<int>(inc0Value_6) << " "
        << static_cast<int>(inc0Value_7) << " "
        << static_cast<int>(inc0Value_8) << std::endl;
    outputFile << "Argument of perigee: "
        << static_cast<int>(argPerValue_1) << " "
        << static_cast<int>(argPerValue_2) << " "
        << static_cast<int>(argPerValue_3) << " "
        << static_cast<int>(argPerValue_4) << " "
        << static_cast<int>(argPerValue_5) << " "
        << static_cast<int>(argPerValue_6) << " "
        << static_cast<int>(argPerValue_7) << " "
        << static_cast<int>(argPerValue_8) << std::endl;
    outputFile << "Mean motion difference from computed value: "
        << static_cast<int>(delnValue_1) << " "
        << static_cast<int>(delnValue_2) << " "
        << static_cast<int>(delnValue_3) << " "
        << static_cast<int>(delnValue_4) << std::endl;
    outputFile << "Rate of right ascension: "
        << static_cast<int>(omegaDotValue_1) << " "
        << static_cast<int>(omegaDotValue_2) << " "
        << static_cast<int>(omegaDotValue_3) << " "
        << static_cast<int>(omegaDotValue_4) << std::endl;
    outputFile << "Rate of inclination angle: "
        << static_cast<int>(incDotValue_1) << " "
        << static_cast<int>(incDotValue_2) << " "
        << static_cast<int>(incDotValue_3) << " "
        << static_cast<int>(incDotValue_4) << std::endl;
    outputFile << "Amplitude of the cosine harmonic correction term to the orbit radius: "
        << static_cast<int>(crcValue_1) << " "
        << static_cast<int>(crcValue_2) << " "
        << static_cast<int>(crcValue_3) << " "
        << static_cast<int>(crcValue_4) << std::endl;
    outputFile << "Amplitude of the sine harmonic correction term to the orbit radius: "
        << static_cast<int>(crsValue_1) << " "
        << static_cast<int>(crsValue_2) << " "
        << static_cast<int>(crsValue_3) << " "
        << static_cast<int>(crsValue_4) << std::endl;
    outputFile << "Amplitude of the cosine harmonic correction term to the argument of latitude: "
        << static_cast<int>(cucValue_1) << " "
        << static_cast<int>(cucValue_2) << " "
        << static_cast<int>(cucValue_3) << " "
        << static_cast<int>(cucValue_4) << std::endl;
    outputFile << "Amplitude of the sine harmonic correction term to the argument of latitude: "
        << static_cast<int>(cusValue_1) << " "
        << static_cast<int>(cusValue_2) << " "
        << static_cast<int>(cusValue_3) << " "
        << static_cast<int>(cusValue_4) << std::endl;
    outputFile << "Amplitude of the cosine harmonic correction term to the angle of inclination: "
        << static_cast<int>(cicValue_1) << " "
        << static_cast<int>(cicValue_2) << " "
        << static_cast<int>(cicValue_3) << " "
        << static_cast<int>(cicValue_4) << std::endl;
    outputFile << "Amplitude of the sine harmonic correction term to the angle of inclination: "
        << static_cast<int>(cisValue_1) << " "
        << static_cast<int>(cisValue_2) << " "
        << static_cast<int>(cisValue_3) << " "
        << static_cast<int>(cisValue_4) << std::endl;
    outputFile << "tgd2: "
        << static_cast<int>(tgdValue_1) << " "
        << static_cast<int>(tgdValue_2) << " "
        << static_cast<int>(tgdValue_3) << " "
        << static_cast<int>(tgdValue_4) << std::endl;
    outputFile << "Signal type: " << static_cast<int>(navTypeValue) << std::endl;
    outputFile << "Correction to �af0�: "
        << static_cast<int>(DAf0Value_1) << " "
        << static_cast<int>(DAf0Value_2) << " "
        << static_cast<int>(DAf0Value_3) << " "
        << static_cast<int>(DAf0Value_4) << std::endl;
    outputFile << "Checksum of the string: " << static_cast<int>(checkSum) << std::endl;
    outputFile << "Parameter u1 cs: " << static_cast<int>(csValue);

    outputFile.close();
}

int Var47::getCheckSum()
{
    return this->checkSum;
}

int Var47::getCsValue()
{
    return this->csValue;
}