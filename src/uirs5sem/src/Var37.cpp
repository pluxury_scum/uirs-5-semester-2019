#include <iostream>
#include <fstream>
#include "../include/Var37.h"

int Var37::handleVar()
{
    std::ifstream inputFile;
    std::string inputFileName = "rks.dat";
    inputFile.open(inputFileName, std::ios::in | std::ios::binary);

    if (!inputFile.is_open())
    {
        std::cerr << "Error. File was not overwritten" << std::endl;
        exit(1);
    }
    else
    {
        int tempInt = 0; // variable for memory allocation
        std::string tempString; // char array holds all the characters of the file
        inputFile.read((char*)&tempInt, sizeof(tempInt)); // read string size from file
        tempString.resize(tempInt); // allocate memory of required size
        inputFile.read(&tempString[0], tempInt); // read string from file

        this->allValuesArr[0] = static_cast<u1>(tempString[0x520E6]);
        this->allValuesArr[1] = static_cast<u1>(tempString[0x520E7]);
        this->allValuesArr[2] = static_cast<u1>(tempString[0x520E8]);
        this->allValuesArr[3] = static_cast<u1>(tempString[0x520E9]);
        this->allValuesArr[4] = static_cast<u1>(tempString[0x520EA]);
        this->allValuesArr[5] = static_cast<u1>(tempString[0x520EB]);
        this->allValuesArr[6] = static_cast<u1>(tempString[0x520EC]);
        this->allValuesArr[7] = static_cast<u1>(tempString[0x520ED]);
        this->allValuesArr[8] = static_cast<u1>(tempString[0x520EE]);
        this->allValuesArr[9] = static_cast<u1>(tempString[0x520EF]);
        this->allValuesArr[10] = static_cast<u1>(tempString[0x520F0]);
        this->allValuesArr[11] = static_cast<u1>(tempString[0x520F1]);
        this->allValuesArr[12] = static_cast<u1>(tempString[0x520F2]);
        this->allValuesArr[13] = static_cast<u1>(tempString[0x520F3]);
        this->allValuesArr[14] = static_cast<u1>(tempString[0x520F4]);
        this->allValuesArr[15] = static_cast<u1>(tempString[0x520F5]);
        this->allValuesArr[16] = static_cast<u1>(tempString[0x520F6]);
        this->allValuesArr[17] = static_cast<u1>(tempString[0x520F7]);
        this->allValuesArr[18] = static_cast<u1>(tempString[0x520F8]);
        this->allValuesArr[19] = static_cast<u1>(tempString[0x520F9]);
        this->allValuesArr[20] = static_cast<u1>(tempString[0x520FA]);
        this->allValuesArr[21] = static_cast<u1>(tempString[0x520FB]);
        this->allValuesArr[22] = static_cast<u1>(tempString[0x520FC]);
        this->allValuesArr[23] = static_cast<u1>(tempString[0x520FD]);
        this->allValuesArr[24] = static_cast<u1>(tempString[0x520FE]);
        this->allValuesArr[25] = static_cast<u1>(tempString[0x520FF]);
        this->allValuesArr[26] = static_cast<u1>(tempString[0x52100]);
        this->allValuesArr[27] = static_cast<u1>(tempString[0x52101]);
        this->allValuesArr[28] = static_cast<u1>(tempString[0x52102]);
        this->allValuesArr[29] = static_cast<u1>(tempString[0x52103]);
        this->allValuesArr[30] = static_cast<u1>(tempString[0x52104]);
        this->allValuesArr[31] = static_cast<u1>(tempString[0x52105]);
        this->allValuesArr[32] = static_cast<u1>(tempString[0x52106]);
        this->allValuesArr[33] = static_cast<u1>(tempString[0x52107]);
        this->allValuesArr[34] = static_cast<u1>(tempString[0x52108]);
        this->allValuesArr[35] = static_cast<u1>(tempString[0x52109]);
        this->allValuesArr[36] = static_cast<u1>(tempString[0x5210A]);
        this->allValuesArr[37] = static_cast<u1>(tempString[0x5210B]);
        this->allValuesArr[38] = static_cast<u1>(tempString[0x5210C]);
        this->allValuesArr[39] = static_cast<u1>(tempString[0x5210D]);
        this->allValuesArr[40] = static_cast<u1>(tempString[0x5210E]);
        this->allValuesArr[41] = static_cast<u1>(tempString[0x5210F]);

        this->q00Value_1 = this->allValuesArr[5];
        this->q00Value_2 = this->allValuesArr[6];
        this->q00Value_3 = this->allValuesArr[7];
        this->q00Value_4 = this->allValuesArr[8];
        this->q01Value_1 = this->allValuesArr[9];
        this->q01Value_2 = this->allValuesArr[10];
        this->q01Value_3 = this->allValuesArr[11];
        this->q01Value_4 = this->allValuesArr[12];
        this->q02Value_1 = this->allValuesArr[13];
        this->q02Value_2 = this->allValuesArr[14];
        this->q02Value_3 = this->allValuesArr[15];
        this->q02Value_4 = this->allValuesArr[16];
        this->q10Value_1 = this->allValuesArr[17];
        this->q10Value_2 = this->allValuesArr[18];
        this->q10Value_3 = this->allValuesArr[19];
        this->q10Value_4 = this->allValuesArr[20];
        this->q11Value_1 = this->allValuesArr[21];
        this->q11Value_2 = this->allValuesArr[22];
        this->q11Value_3 = this->allValuesArr[23];
        this->q11Value_4 = this->allValuesArr[24];
        this->q12Value_1 = this->allValuesArr[25];
        this->q12Value_2 = this->allValuesArr[26];
        this->q12Value_3 = this->allValuesArr[27];
        this->q12Value_4 = this->allValuesArr[28];
        this->q20Value_1 = this->allValuesArr[29];
        this->q20Value_2 = this->allValuesArr[30];
        this->q20Value_3 = this->allValuesArr[31];
        this->q20Value_4 = this->allValuesArr[32];
        this->q21Value_1 = this->allValuesArr[33];
        this->q21Value_2 = this->allValuesArr[34];
        this->q21Value_3 = this->allValuesArr[35];
        this->q21Value_4 = this->allValuesArr[36];
        this->q22Value_1 = this->allValuesArr[37];
        this->q22Value_2 = this->allValuesArr[38];
        this->q22Value_3 = this->allValuesArr[39];
        this->q22Value_4 = this->allValuesArr[40];
        this->csValue = this->allValuesArr[41];

        handleCS();
        printVar();

        inputFile.close();
        return 0;
    }
}

void Var37::handleCS()
{
    this->checkSum = cs(allValuesArr, 41);
}

void Var37::printVar()
{
    std::ofstream outputFile;
    std::string outputFileName = "Var37_output.txt";
    outputFile.open(outputFileName);

    outputFile << "Var 37" << std::endl;
    outputFile << "Structure FullRotationMatrix" << std::endl;
    outputFile << "Components of the rotation matrix Q:" << std::endl;
    outputFile << "q00: "
        << static_cast<int>(q00Value_1) << " "
        << static_cast<int>(q00Value_2) << " "
        << static_cast<int>(q00Value_3) << " "
        << static_cast<int>(q00Value_4) << std::endl;
    outputFile << "q01: "
        << static_cast<int>(q01Value_1) << " "
        << static_cast<int>(q01Value_2) << " "
        << static_cast<int>(q01Value_3) << " "
        << static_cast<int>(q01Value_4) << std::endl;
    outputFile << "q02: "
        << static_cast<int>(q02Value_1) << " "
        << static_cast<int>(q02Value_2) << " "
        << static_cast<int>(q02Value_3) << " "
        << static_cast<int>(q02Value_4) << std::endl;
    outputFile << "q10: "
        << static_cast<int>(q10Value_1) << " "
        << static_cast<int>(q10Value_2) << " "
        << static_cast<int>(q10Value_3) << " "
        << static_cast<int>(q10Value_4) << std::endl;
    outputFile << "q11: "
        << static_cast<int>(q11Value_1) << " "
        << static_cast<int>(q11Value_2) << " "
        << static_cast<int>(q11Value_3) << " "
        << static_cast<int>(q11Value_4) << std::endl;
    outputFile << "q12: "
        << static_cast<int>(q12Value_1) << " "
        << static_cast<int>(q12Value_2) << " "
        << static_cast<int>(q12Value_3) << " "
        << static_cast<int>(q12Value_4) << std::endl;
    outputFile << "q20: "
        << static_cast<int>(q20Value_1) << " "
        << static_cast<int>(q20Value_2) << " "
        << static_cast<int>(q20Value_3) << " "
        << static_cast<int>(q20Value_4) << std::endl;
    outputFile << "q21: "
        << static_cast<int>(q21Value_1) << " "
        << static_cast<int>(q21Value_2) << " "
        << static_cast<int>(q21Value_3) << " "
        << static_cast<int>(q21Value_4) << std::endl;
    outputFile << "q22: "
        << static_cast<int>(q22Value_1) << " "
        << static_cast<int>(q22Value_2) << " "
        << static_cast<int>(q22Value_3) << " "
        << static_cast<int>(q22Value_4) << std::endl;
    outputFile << "Checksum of the string: " << static_cast<int>(checkSum) << std::endl;
    outputFile << "Parameter u1 cs: " << static_cast<int>(csValue);

    outputFile.close();
}

int Var37::getCheckSum()
{
    return this->checkSum;
}

int Var37::getCsValue()
{
    return this->csValue;
}