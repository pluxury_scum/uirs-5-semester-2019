#include <iostream>
#include <fstream>
#include "../include/Var52.h"

int Var52::handleVar()
{
    std::ifstream inputFile;
    std::string inputFileName = "rks.dat";
    inputFile.open(inputFileName, std::ios::in | std::ios::binary);

    if (!inputFile.is_open())
    {
        std::cerr << "Error. File was not overwritten" << std::endl;
        exit(1);
    }
    else
    {
        int tempInt = 0; // variable for memory allocation
        std::string tempString; // char array holds all the characters of the file
        inputFile.read((char*)&tempInt, sizeof(tempInt)); // read string size from file
        tempString.resize(tempInt); // allocate memory of required size
        inputFile.read(&tempString[0], tempInt); // read string from file

        this->allValuesArr[0] = static_cast<u1>(tempString[0x102]);
        this->allValuesArr[1] = static_cast<u1>(tempString[0x103]);
        this->allValuesArr[2] = static_cast<u1>(tempString[0x104]);
        this->allValuesArr[3] = static_cast<u1>(tempString[0x105]);
        this->allValuesArr[4] = static_cast<u1>(tempString[0x106]);
        this->allValuesArr[5] = static_cast<u1>(tempString[0x107]);
        this->allValuesArr[6] = static_cast<u1>(tempString[0x108]);
        this->allValuesArr[7] = static_cast<u1>(tempString[0x109]);
        this->allValuesArr[8] = static_cast<u1>(tempString[0x10A]);
        this->allValuesArr[9] = static_cast<u1>(tempString[0x10B]);
        this->allValuesArr[10] = static_cast<u1>(tempString[0x10C]);
        this->allValuesArr[11] = static_cast<u1>(tempString[0x10D]);
        this->allValuesArr[12] = static_cast<u1>(tempString[0x10E]);
        this->allValuesArr[13] = static_cast<u1>(tempString[0x10F]);
        this->allValuesArr[14] = static_cast<u1>(tempString[0x110]);
        this->allValuesArr[15] = static_cast<u1>(tempString[0x111]);
        this->allValuesArr[16] = static_cast<u1>(tempString[0x112]);
        this->allValuesArr[17] = static_cast<u1>(tempString[0x113]);
        this->allValuesArr[18] = static_cast<u1>(tempString[0x114]);
        this->allValuesArr[19] = static_cast<u1>(tempString[0x115]);
        this->allValuesArr[20] = static_cast<u1>(tempString[0x116]);
        this->allValuesArr[21] = static_cast<u1>(tempString[0x117]);
        this->allValuesArr[22] = static_cast<u1>(tempString[0x118]);
        this->allValuesArr[23] = static_cast<u1>(tempString[0x119]);
        this->allValuesArr[24] = static_cast<u1>(tempString[0x11A]);
        this->allValuesArr[25] = static_cast<u1>(tempString[0x11B]);
        this->allValuesArr[26] = static_cast<u1>(tempString[0x11C]);

        this->timeValue_1 = this->allValuesArr[5];
        this->timeValue_2 = this->allValuesArr[6];
        this->timeValue_3 = this->allValuesArr[7];
        this->timeValue_4 = this->allValuesArr[8];
        this->xValue_1 = this->allValuesArr[9];
        this->xValue_2 = this->allValuesArr[10];
        this->xValue_3 = this->allValuesArr[11];
        this->xValue_4 = this->allValuesArr[12];
        this->yValue_1 = this->allValuesArr[13];
        this->yValue_2 = this->allValuesArr[14];
        this->yValue_3 = this->allValuesArr[15];
        this->yValue_4 = this->allValuesArr[16];
        this->zValue_1 = this->allValuesArr[17];
        this->zValue_2 = this->allValuesArr[18];
        this->zValue_3 = this->allValuesArr[19];
        this->zValue_4 = this->allValuesArr[20];
        this->rmsValue_1 = this->allValuesArr[21];
        this->rmsValue_2 = this->allValuesArr[22];
        this->rmsValue_3 = this->allValuesArr[23];
        this->rmsValue_4 = this->allValuesArr[24];
        this->flagsValue = this->allValuesArr[25];
        this->csValue = this->allValuesArr[26];

        handleCS();
        printVar();

        inputFile.close();
        return 0;
    }
}

void Var52::handleCS()
{
    this->checkSum = cs(allValuesArr, 26);
}

void Var52::printVar()
{
    std::ofstream outputFile;
    std::string outputFileName = "Var52_output.txt";
    outputFile.open(outputFileName);

    outputFile << "Var 52" << std::endl;
    outputFile << "Structure AngularVelocity" << std::endl;
    outputFile << "Time: "
        << static_cast<int>(timeValue_1) << " "
        << static_cast<int>(timeValue_2) << " "
        << static_cast<int>(timeValue_3) << " "
        << static_cast<int>(timeValue_4) << std::endl;
    outputFile << "X component of angular velocity: "
        << static_cast<int>(xValue_1) << " "
        << static_cast<int>(xValue_2) << " "
        << static_cast<int>(xValue_3) << " "
        << static_cast<int>(xValue_4) << std::endl;
    outputFile << "Y component of angular velocity: "
        << static_cast<int>(yValue_1) << " "
        << static_cast<int>(yValue_2) << " "
        << static_cast<int>(yValue_3) << " "
        << static_cast<int>(yValue_4) << std::endl;
    outputFile << "Z component of angular velocity: "
        << static_cast<int>(zValue_1) << " "
        << static_cast<int>(zValue_2) << " "
        << static_cast<int>(zValue_3) << " "
        << static_cast<int>(zValue_4) << std::endl;
    outputFile << "Angular velocity RMS: "
        << static_cast<int>(rmsValue_1) << " "
        << static_cast<int>(rmsValue_2) << " "
        << static_cast<int>(rmsValue_3) << " "
        << static_cast<int>(rmsValue_4) << std::endl;
    outputFile << "Flags: " << static_cast<int>(flagsValue) << std::endl;
    outputFile << "Checksum of the string: " << static_cast<int>(checkSum) << std::endl;
    outputFile << "Parameter u1 cs: " << static_cast<int>(csValue);

    outputFile.close();
}

int Var52::getCheckSum()
{
    return this->checkSum;
}

int Var52::getCsValue()
{
    return this->csValue;
}