#include <iostream>
#include <fstream>
#include "../include/Var17.h"

int Var17::handleVar()
{
    std::ifstream inputFile;
    std::string inputFileName = "rks.dat";
    inputFile.open(inputFileName, std::ios::in | std::ios::binary);

    if (!inputFile.is_open())
    {
        std::cerr << "Error. File was not overwritten" << std::endl;
        exit(1);
    }
    else
    {
        int tempInt = 0; // variable for memory allocation
        std::string tempString; // char array holds all the characters of the file
        inputFile.read((char*)&tempInt, sizeof(tempInt)); // read string size from file
        tempString.resize(tempInt); // allocate memory of required size
        inputFile.read(&tempString[0], tempInt); // read string from file

        this->allValuesArr[0] = static_cast<u1>(tempString[0x35285]);
        this->allValuesArr[1] = static_cast<u1>(tempString[0x35286]);
        this->allValuesArr[2] = static_cast<u1>(tempString[0x35287]);
        this->allValuesArr[3] = static_cast<u1>(tempString[0x35288]);
        this->allValuesArr[4] = static_cast<u1>(tempString[0x35289]);
        this->allValuesArr[5] = static_cast<u1>(tempString[0x3528A]);
        this->allValuesArr[6] = static_cast<u1>(tempString[0x3528B]);
        this->allValuesArr[7] = static_cast<u1>(tempString[0x3528C]);
        this->allValuesArr[8] = static_cast<u1>(tempString[0x3528D]);
        this->allValuesArr[9] = static_cast<u1>(tempString[0x3528E]);
        this->allValuesArr[10] = static_cast<u1>(tempString[0x3528F]);
        this->allValuesArr[11] = static_cast<u1>(tempString[0x35290]);
        this->allValuesArr[12] = static_cast<u1>(tempString[0x35291]);
        this->allValuesArr[13] = static_cast<u1>(tempString[0x35292]);
        this->allValuesArr[14] = static_cast<u1>(tempString[0x35293]);
        this->allValuesArr[15] = static_cast<u1>(tempString[0x35294]);
        this->allValuesArr[16] = static_cast<u1>(tempString[0x35295]);
        this->allValuesArr[17] = static_cast<u1>(tempString[0x35296]);
        this->allValuesArr[18] = static_cast<u1>(tempString[0x35297]);
        this->allValuesArr[19] = static_cast<u1>(tempString[0x35298]);
        this->allValuesArr[20] = static_cast<u1>(tempString[0x35299]);
        this->allValuesArr[21] = static_cast<u1>(tempString[0x3529A]);
        this->allValuesArr[22] = static_cast<u1>(tempString[0x3529B]);
        this->allValuesArr[23] = static_cast<u1>(tempString[0x3529C]);
        this->allValuesArr[24] = static_cast<u1>(tempString[0x3529D]);
        this->allValuesArr[25] = static_cast<u1>(tempString[0x3529E]);
        this->allValuesArr[26] = static_cast<u1>(tempString[0x3529F]);
        this->allValuesArr[27] = static_cast<u1>(tempString[0x352A0]);
        this->allValuesArr[28] = static_cast<u1>(tempString[0x352A1]);
        this->allValuesArr[29] = static_cast<u1>(tempString[0x352A2]);
        this->allValuesArr[30] = static_cast<u1>(tempString[0x352A3]);
        this->allValuesArr[31] = static_cast<u1>(tempString[0x352A4]);
        this->allValuesArr[32] = static_cast<u1>(tempString[0x352A5]);
        this->allValuesArr[33] = static_cast<u1>(tempString[0x352A6]);
        this->allValuesArr[34] = static_cast<u1>(tempString[0x352A7]);
        this->allValuesArr[35] = static_cast<u1>(tempString[0x352A8]);
        this->allValuesArr[36] = static_cast<u1>(tempString[0x352A9]);

        this->a0Value_1 = this->allValuesArr[5];
        this->a0Value_2 = this->allValuesArr[6];
        this->a0Value_3 = this->allValuesArr[7];
        this->a0Value_4 = this->allValuesArr[8];
        this->a0Value_5 = this->allValuesArr[9];
        this->a0Value_6 = this->allValuesArr[10];
        this->a0Value_7 = this->allValuesArr[11];
        this->a0Value_8 = this->allValuesArr[12];
        this->a1Value_1 = this->allValuesArr[13];
        this->a1Value_2 = this->allValuesArr[14];
        this->a1Value_3 = this->allValuesArr[15];
        this->a1Value_4 = this->allValuesArr[16];
        this->totValue_1 = this->allValuesArr[17];
        this->totValue_2 = this->allValuesArr[18];
        this->totValue_3 = this->allValuesArr[19];
        this->totValue_4 = this->allValuesArr[20];
        this->wntValue_1 = this->allValuesArr[21];
        this->wntValue_2 = this->allValuesArr[22];
        this->dtlsValue = this->allValuesArr[23];
        this->dnValue = this->allValuesArr[24];
        this->wnlsfValue_1 = this->allValuesArr[25];
        this->wnlsfValue_2 = this->allValuesArr[26];
        this->dtlsfValue = this->allValuesArr[27];
        this->utcsiValue = this->allValuesArr[28];
        this->towValue_1 = this->allValuesArr[29];
        this->towValue_2 = this->allValuesArr[30];
        this->towValue_3 = this->allValuesArr[31];
        this->towValue_4 = this->allValuesArr[32];
        this->wnValue_1 = this->allValuesArr[33];
        this->wnValue_2 = this->allValuesArr[34];
        this->flagsValue = this->allValuesArr[35];
        this->csValue = this->allValuesArr[36];

        handleCS();
        printVar();

        inputFile.close();
        return 0;
    }
}

void Var17::handleCS()
{
    this->checkSum = cs(allValuesArr, 36);
}

void Var17::printVar()
{
    std::ofstream outputFile;
    std::string outputFileName = "Var17_output.txt";
    outputFile.open(outputFileName);

    outputFile << "Var 17" << std::endl;
    outputFile << "Structure SbasUtcParam" << std::endl;
    outputFile << "Constant term of polynomial: "
        << static_cast<int>(a0Value_1) << " "
        << static_cast<int>(a0Value_2) << " "
        << static_cast<int>(a0Value_3) << " "
        << static_cast<int>(a0Value_4) << " "
        << static_cast<int>(a0Value_5) << " "
        << static_cast<int>(a0Value_6) << " "
        << static_cast<int>(a0Value_7) << " "
        << static_cast<int>(a0Value_8) << std::endl;
    outputFile << "First order term of polynomial: "
        << static_cast<int>(a1Value_1) << " "
        << static_cast<int>(a1Value_2) << " "
        << static_cast<int>(a1Value_3) << " "
        << static_cast<int>(a1Value_4) << std::endl;
    outputFile << "Reference time of week: "
        << static_cast<int>(totValue_1) << " "
        << static_cast<int>(totValue_2) << " "
        << static_cast<int>(totValue_3) << " "
        << static_cast<int>(totValue_4) << std::endl;
    outputFile << "Reference week number: "
        << static_cast<int>(wntValue_1) << " "
        << static_cast<int>(wntValue_2) << std::endl;
    outputFile << "Delta time due to leap seconds: " << static_cast<int>(dtlsValue) << std::endl;
    outputFile << "'Future' reference day number: " << static_cast<int>(dnValue) << std::endl;
    outputFile << "'Future' reference week number: "
        << static_cast<int>(wnlsfValue_1) << " "
        << static_cast<int>(wnlsfValue_2) << std::endl;
    outputFile << "'Future' reference delta time due to leap seconds: " << static_cast<int>(dtlsfValue) << std::endl;
    outputFile << "UTC standard identifier: " << static_cast<int>(utcsiValue) << std::endl;
    outputFile << "Reference time of week: "
        << static_cast<int>(towValue_1) << " "
        << static_cast<int>(towValue_2) << " "
        << static_cast<int>(towValue_3) << " "
        << static_cast<int>(towValue_4) << std::endl;
    outputFile << "Reference week number: "
        << static_cast<int>(wnValue_1) << " "
        << static_cast<int>(wnValue_2) << std::endl;
    outputFile << "Flags, reserved: " << static_cast<int>(flagsValue) << std::endl;
    outputFile << "Checksum of the string: " << static_cast<int>(checkSum) << std::endl;
    outputFile << "Parameter u1 cs: " << static_cast<int>(csValue);

    outputFile.close();
}

int Var17::getCheckSum()
{
    return this->checkSum;
}

int Var17::getCsValue()
{
    return this->csValue;
}