#include <iostream>
#include <fstream>
#include "../include/Var20.h"

int Var20::handleVar()
{
    std::ifstream inputFile;
    std::string inputFileName = "rks.dat";
    inputFile.open(inputFileName, std::ios::in | std::ios::binary);

    if (!inputFile.is_open())
    {
        std::cerr << "Error. File was not overwritten" << std::endl;
        exit(1);
    }
    else
    {
        int tempInt = 0; // variable for memory allocation
        std::string tempString; // char array holds all the characters of the file
        inputFile.read((char*)&tempInt, sizeof(tempInt)); // read string size from file
        tempString.resize(tempInt); // allocate memory of required size
        inputFile.read(&tempString[0], tempInt); // read string from file

        this->allValuesArr[0] = static_cast<u1>(tempString[0x7B7]);
        this->allValuesArr[1] = static_cast<u1>(tempString[0x7B8]);
        this->allValuesArr[2] = static_cast<u1>(tempString[0x7B9]);
        this->allValuesArr[3] = static_cast<u1>(tempString[0x7BA]);
        this->allValuesArr[4] = static_cast<u1>(tempString[0x7BB]);
        this->allValuesArr[5] = static_cast<u1>(tempString[0x7BC]);
        this->allValuesArr[6] = static_cast<u1>(tempString[0x7BD]);
        this->allValuesArr[7] = static_cast<u1>(tempString[0x7BE]);
        this->allValuesArr[8] = static_cast<u1>(tempString[0x7BF]);
        this->allValuesArr[9] = static_cast<u1>(tempString[0x7C0]);
        this->allValuesArr[10] = static_cast<u1>(tempString[0x7C1]);
        this->allValuesArr[11] = static_cast<u1>(tempString[0x7C2]);
        this->allValuesArr[12] = static_cast<u1>(tempString[0x7C3]);
        this->allValuesArr[13] = static_cast<u1>(tempString[0x7C4]);
        this->allValuesArr[14] = static_cast<u1>(tempString[0x7C5]);
        this->allValuesArr[15] = static_cast<u1>(tempString[0x7C6]);
        this->allValuesArr[16] = static_cast<u1>(tempString[0x7C7]);
        this->allValuesArr[17] = static_cast<u1>(tempString[0x7C8]);
        this->allValuesArr[18] = static_cast<u1>(tempString[0x7C9]);
        this->allValuesArr[19] = static_cast<u1>(tempString[0x7CA]);
        this->allValuesArr[20] = static_cast<u1>(tempString[0x7CB]);
        this->allValuesArr[21] = static_cast<u1>(tempString[0x7CC]);
        this->allValuesArr[22] = static_cast<u1>(tempString[0x7CD]);
        this->allValuesArr[23] = static_cast<u1>(tempString[0x7CE]);
        this->allValuesArr[24] = static_cast<u1>(tempString[0x7CF]);
        this->allValuesArr[25] = static_cast<u1>(tempString[0x7D0]);
        this->allValuesArr[26] = static_cast<u1>(tempString[0x7D1]);
        this->allValuesArr[27] = static_cast<u1>(tempString[0x7D2]);
        this->allValuesArr[28] = static_cast<u1>(tempString[0x7D3]);

        this->a0Value_1 = this->allValuesArr[5];
        this->a0Value_2 = this->allValuesArr[6];
        this->a0Value_3 = this->allValuesArr[7];
        this->a0Value_4 = this->allValuesArr[8];
        this->a0Value_5 = this->allValuesArr[9];
        this->a0Value_6 = this->allValuesArr[10];
        this->a0Value_7 = this->allValuesArr[11];
        this->a0Value_8 = this->allValuesArr[12];
        this->a1Value_1 = this->allValuesArr[13];
        this->a1Value_2 = this->allValuesArr[14];
        this->a1Value_3 = this->allValuesArr[15];
        this->a1Value_4 = this->allValuesArr[16];
        this->totValue_1 = this->allValuesArr[17];
        this->totValue_2 = this->allValuesArr[18];
        this->totValue_3 = this->allValuesArr[19];
        this->totValue_4 = this->allValuesArr[20];
        this->wntValue_1 = this->allValuesArr[21];
        this->wntValue_2 = this->allValuesArr[22];
        this->dtlsValue = this->allValuesArr[23];
        this->dnValue = this->allValuesArr[24];
        this->wnlsfValue_1 = this->allValuesArr[25];
        this->wnlsfValue_2 = this->allValuesArr[26];
        this->dtlsfValue = this->allValuesArr[27];
        this->csValue = this->allValuesArr[28];

        handleCS();
        printVar();

        inputFile.close();
        return 0;
    }
}

void Var20::handleCS()
{
    this->checkSum = cs(allValuesArr, 28);
}

void Var20::printVar()
{
    std::ofstream outputFile;
    std::string outputFileName = "Var20_output.txt";
    outputFile.open(outputFileName);

    outputFile << "Var 20" << std::endl;
    outputFile << "Structure BeiDouUtcParam" << std::endl;
    outputFile << "Constant term of polynomial: "
        << static_cast<int>(a0Value_1) << " "
        << static_cast<int>(a0Value_2) << " "
        << static_cast<int>(a0Value_3) << " "
        << static_cast<int>(a0Value_4) << " "
        << static_cast<int>(a0Value_5) << " "
        << static_cast<int>(a0Value_6) << " "
        << static_cast<int>(a0Value_7) << " "
        << static_cast<int>(a0Value_8) << std::endl;
    outputFile << "First order term of polynomial: "
        << static_cast<int>(a1Value_1) << " "
        << static_cast<int>(a1Value_2) << " "
        << static_cast<int>(a1Value_3) << " "
        << static_cast<int>(a1Value_4) << std::endl;
    outputFile << "Reference time of week: "
        << static_cast<int>(totValue_1) << " "
        << static_cast<int>(totValue_2) << " "
        << static_cast<int>(totValue_3) << " "
        << static_cast<int>(totValue_4) << std::endl;
    outputFile << "Reference week number: "
        << static_cast<int>(wntValue_1) << " "
        << static_cast<int>(wntValue_2) << std::endl;
    outputFile << "Delta time due to leap seconds: " << static_cast<int>(dtlsValue) << std::endl;
    outputFile << "'Future' reference day number: " << static_cast<int>(dnValue) << std::endl;
    outputFile << "'Future' reference week number: "
        << static_cast<int>(wnlsfValue_1) << " "
        << static_cast<int>(wnlsfValue_2) << std::endl;
    outputFile << "'Future' reference delta time due to leap seconds: " << static_cast<int>(dtlsfValue) << std::endl;
    outputFile << "Checksum of the string: " << static_cast<int>(checkSum) << std::endl;
    outputFile << "Parameter u1 cs: " << static_cast<int>(csValue);

    outputFile.close();
}

int Var20::getCheckSum()
{
    return this->checkSum;
}

int Var20::getCsValue()
{
    return this->csValue;
}