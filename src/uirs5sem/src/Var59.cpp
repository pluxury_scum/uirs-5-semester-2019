#include <iostream>
#include <fstream>
#include "../include/Var59.h"

int Var59::handleVar()
{
    std::ifstream inputFile;
    std::string inputFileName = "rks.dat";
    inputFile.open(inputFileName, std::ios::in | std::ios::binary);

    if (!inputFile.is_open())
    {
        std::cerr << "Error. File was not overwritten" << std::endl;
        exit(1);
    }
    else
    {
        int tempInt = 0; // variable for memory allocation
        std::string tempString; // char array holds all the characters of the file
        inputFile.read((char*)&tempInt, sizeof(tempInt)); // read string size from file
        tempString.resize(tempInt); // allocate memory of required size
        inputFile.read(&tempString[0], tempInt); // read string from file

        this->allValuesArr[0] = static_cast<u1>(tempString[0x2037]);
        this->allValuesArr[1] = static_cast<u1>(tempString[0x2038]);
        this->allValuesArr[2] = static_cast<u1>(tempString[0x2039]);
        this->allValuesArr[3] = static_cast<u1>(tempString[0x203A]);
        this->allValuesArr[4] = static_cast<u1>(tempString[0x203B]);
        this->allValuesArr[5] = static_cast<u1>(tempString[0x203C]);
        this->allValuesArr[6] = static_cast<u1>(tempString[0x203D]);
        this->allValuesArr[7] = static_cast<u1>(tempString[0x203E]);
        this->allValuesArr[8] = static_cast<u1>(tempString[0x203F]);
        this->allValuesArr[9] = static_cast<u1>(tempString[0x2040]);
        this->allValuesArr[10] = static_cast<u1>(tempString[0x2041]);
        this->allValuesArr[11] = static_cast<u1>(tempString[0x2042]);
        this->allValuesArr[12] = static_cast<u1>(tempString[0x2043]);
        this->allValuesArr[13] = static_cast<u1>(tempString[0x2044]);
        this->allValuesArr[14] = static_cast<u1>(tempString[0x2045]);
        this->allValuesArr[15] = static_cast<u1>(tempString[0x2046]);
        this->allValuesArr[16] = static_cast<u1>(tempString[0x2047]);
        this->allValuesArr[17] = static_cast<u1>(tempString[0x2048]);
        this->allValuesArr[18] = static_cast<u1>(tempString[0x2049]);
        this->allValuesArr[19] = static_cast<u1>(tempString[0x204A]);
        this->allValuesArr[20] = static_cast<u1>(tempString[0x204B]);
        this->allValuesArr[21] = static_cast<u1>(tempString[0x204C]);
        this->allValuesArr[22] = static_cast<u1>(tempString[0x204D]);
        this->allValuesArr[23] = static_cast<u1>(tempString[0x204E]);
        this->allValuesArr[24] = static_cast<u1>(tempString[0x204F]);
        this->allValuesArr[25] = static_cast<u1>(tempString[0x2050]);
        this->allValuesArr[26] = static_cast<u1>(tempString[0x2051]);
        this->allValuesArr[27] = static_cast<u1>(tempString[0x2052]);
        this->allValuesArr[28] = static_cast<u1>(tempString[0x2053]);
        this->allValuesArr[29] = static_cast<u1>(tempString[0x2054]);
        this->allValuesArr[30] = static_cast<u1>(tempString[0x2055]);
        this->allValuesArr[31] = static_cast<u1>(tempString[0x2056]);
        this->allValuesArr[32] = static_cast<u1>(tempString[0x2057]);
        this->allValuesArr[33] = static_cast<u1>(tempString[0x2058]);
        this->allValuesArr[34] = static_cast<u1>(tempString[0x2059]);
        this->allValuesArr[35] = static_cast<u1>(tempString[0x205A]);
        this->allValuesArr[36] = static_cast<u1>(tempString[0x205B]);
        this->allValuesArr[37] = static_cast<u1>(tempString[0x205C]);
        this->allValuesArr[38] = static_cast<u1>(tempString[0x205D]);
        this->allValuesArr[39] = static_cast<u1>(tempString[0x205E]);
        this->allValuesArr[40] = static_cast<u1>(tempString[0x205F]);
        this->allValuesArr[41] = static_cast<u1>(tempString[0x2060]);
        this->allValuesArr[42] = static_cast<u1>(tempString[0x2061]);
        this->allValuesArr[43] = static_cast<u1>(tempString[0x2062]);

        this->totValue_1 = this->allValuesArr[5];
        this->totValue_2 = this->allValuesArr[6];
        this->totValue_3 = this->allValuesArr[7];
        this->totValue_4 = this->allValuesArr[8];
        this->wnValue_1 = this->allValuesArr[9];
        this->wnValue_2 = this->allValuesArr[10];
        this->alpha0Value_1 = this->allValuesArr[11];
        this->alpha0Value_2 = this->allValuesArr[12];
        this->alpha0Value_3 = this->allValuesArr[13];
        this->alpha0Value_4 = this->allValuesArr[14];
        this->alpha1Value_1 = this->allValuesArr[15];
        this->alpha1Value_2 = this->allValuesArr[16];
        this->alpha1Value_3 = this->allValuesArr[17];
        this->alpha1Value_4 = this->allValuesArr[18];
        this->alpha2Value_1 = this->allValuesArr[19];
        this->alpha2Value_2 = this->allValuesArr[20];
        this->alpha2Value_3 = this->allValuesArr[21];
        this->alpha2Value_4 = this->allValuesArr[22];
        this->alpha3Value_1 = this->allValuesArr[23];
        this->alpha3Value_2 = this->allValuesArr[24];
        this->alpha3Value_3 = this->allValuesArr[25];
        this->alpha3Value_4 = this->allValuesArr[26];
        this->beta0Value_1 = this->allValuesArr[27];
        this->beta0Value_2 = this->allValuesArr[28];
        this->beta0Value_3 = this->allValuesArr[29];
        this->beta0Value_4 = this->allValuesArr[30];
        this->beta1Value_1 = this->allValuesArr[31];
        this->beta1Value_2 = this->allValuesArr[32];
        this->beta1Value_3 = this->allValuesArr[33];
        this->beta1Value_4 = this->allValuesArr[34];
        this->beta2Value_1 = this->allValuesArr[35];
        this->beta2Value_2 = this->allValuesArr[36];
        this->beta2Value_3 = this->allValuesArr[37];
        this->beta2Value_4 = this->allValuesArr[38];
        this->beta3Value_1 = this->allValuesArr[39];
        this->beta3Value_2 = this->allValuesArr[40];
        this->beta3Value_3 = this->allValuesArr[41];
        this->beta3Value_4 = this->allValuesArr[42];
        this->csValue = this->allValuesArr[43];

        handleCS();
        printVar();

        inputFile.close();
        return 0;
    }
}

void Var59::handleCS()
{
    this->checkSum = cs(allValuesArr, 43);
}

void Var59::printVar()
{
    std::ofstream outputFile;
    std::string outputFileName = "Var59_output.txt";
    outputFile.open(outputFileName);

    outputFile << "Var 59" << std::endl;
    outputFile << "Structure IonoParams" << std::endl;
    outputFile << "Time of week: "
        << static_cast<int>(totValue_1) << " "
        << static_cast<int>(totValue_2) << " "
        << static_cast<int>(totValue_3) << " "
        << static_cast<int>(totValue_4) << std::endl;
    outputFile << "Week number: "
        << static_cast<int>(wnValue_1) << " "
        << static_cast<int>(wnValue_2) << std::endl;
    outputFile << "The coefficients of a cubic equation representing the amplitude of the vertical delay: "
        << static_cast<int>(alpha0Value_1) << " "
        << static_cast<int>(alpha0Value_2) << " "
        << static_cast<int>(alpha0Value_3) << " "
        << static_cast<int>(alpha0Value_4) << " "
        << static_cast<int>(alpha1Value_1) << " "
        << static_cast<int>(alpha1Value_2) << " "
        << static_cast<int>(alpha1Value_3) << " "
        << static_cast<int>(alpha1Value_4) << " "
        << static_cast<int>(alpha2Value_1) << " "
        << static_cast<int>(alpha2Value_2) << " "
        << static_cast<int>(alpha2Value_3) << " "
        << static_cast<int>(alpha2Value_4) << " "
        << static_cast<int>(alpha3Value_1) << " "
        << static_cast<int>(alpha3Value_2) << " "
        << static_cast<int>(alpha3Value_3) << " "
        << static_cast<int>(alpha3Value_4) << std::endl;
    outputFile << "The coefficients of a cubic equation representing the period of the model: "
        << static_cast<int>(beta0Value_1) << " "
        << static_cast<int>(beta0Value_2) << " "
        << static_cast<int>(beta0Value_3) << " "
        << static_cast<int>(beta0Value_4) << " "
        << static_cast<int>(beta1Value_1) << " "
        << static_cast<int>(beta1Value_2) << " "
        << static_cast<int>(beta1Value_3) << " "
        << static_cast<int>(beta1Value_4) << " "
        << static_cast<int>(beta2Value_1) << " "
        << static_cast<int>(beta2Value_2) << " "
        << static_cast<int>(beta2Value_3) << " "
        << static_cast<int>(beta2Value_4) << " "
        << static_cast<int>(beta3Value_1) << " "
        << static_cast<int>(beta3Value_2) << " "
        << static_cast<int>(beta3Value_3) << " "
        << static_cast<int>(beta3Value_4) << std::endl;
    outputFile << "Checksum of the string: " << static_cast<int>(checkSum) << std::endl;
    outputFile << "Parameter u1 cs: " << static_cast<int>(csValue);

    outputFile.close();
}

int Var59::getCheckSum()
{
    return this->checkSum;
}

int Var59::getCsValue()
{
    return this->csValue;
}