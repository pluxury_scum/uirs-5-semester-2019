#include <iostream>
#include <fstream>
#include "../include/Var63.h"

int Var63::handleVar()
{
    std::ifstream inputFile;
    std::string inputFileName = "rks.dat";
    inputFile.open(inputFileName, std::ios::in | std::ios::binary);

    if (!inputFile.is_open())
    {
        std::cerr << "Error. File was not overwritten" << std::endl;
        exit(1);
    }
    else
    {
        int tempInt = 0; // variable for memory allocation
        std::string tempString; // char array holds all the characters of the file
        inputFile.read((char*)&tempInt, sizeof(tempInt)); // read string size from file
        tempString.resize(tempInt); // allocate memory of required size
        inputFile.read(&tempString[0], tempInt); // read string from file

        this->allValuesArr[0] = static_cast<u1>(tempString[0x130]);
        this->allValuesArr[1] = static_cast<u1>(tempString[0x131]);
        this->allValuesArr[2] = static_cast<u1>(tempString[0x132]);
        this->allValuesArr[3] = static_cast<u1>(tempString[0x133]);
        this->allValuesArr[4] = static_cast<u1>(tempString[0x134]);
        this->allValuesArr[5] = static_cast<u1>(tempString[0x135]);
        this->allValuesArr[6] = static_cast<u1>(tempString[0x136]);

        this->ltValue = this->allValuesArr[5];
        this->csValue = this->allValuesArr[6];

        handleCS();
        printVar();

        inputFile.close();
        return 0;
    }
}

void Var63::handleCS()
{
    this->checkSum = cs(allValuesArr, 6);
}

void Var63::printVar()
{
    std::ofstream outputFile;
    std::string outputFileName = "Var63_output.txt";
    outputFile.open(outputFileName);

    outputFile << "Var 63" << std::endl;
    outputFile << "Structure Latency" << std::endl;
    outputFile << "Output latency: " << static_cast<int>(ltValue) << std::endl;
    outputFile << "Checksum of the string: " << static_cast<int>(checkSum) << std::endl;
    outputFile << "Parameter u1 cs: " << static_cast<int>(csValue);

    outputFile.close();
}

int Var63::getCheckSum()
{
    return this->checkSum;
}

int Var63::getCsValue()
{
    return this->csValue;
}