#include <iostream>
#include <fstream>
#include "../include/Var49.h"

int Var49::handleVar()
{
    std::ifstream inputFile;
    std::string inputFileName = "rks.dat";
    inputFile.open(inputFileName, std::ios::in | std::ios::binary);

    if (!inputFile.is_open())
    {
        std::cerr << "Error. File was not overwritten" << std::endl;
        exit(1);
    }
    else
    {
        int tempInt = 0; // variable for memory allocation
        std::string tempString; // char array holds all the characters of the file
        inputFile.read((char*)&tempInt, sizeof(tempInt)); // read string size from file
        tempString.resize(tempInt); // allocate memory of required size
        inputFile.read(&tempString[0], tempInt); // read string from file

        this->allValuesArr[0] = static_cast<u1>(tempString[0x3B300]);
        this->allValuesArr[1] = static_cast<u1>(tempString[0x3B301]);
        this->allValuesArr[2] = static_cast<u1>(tempString[0x3B302]);
        this->allValuesArr[3] = static_cast<u1>(tempString[0x3B303]);
        this->allValuesArr[4] = static_cast<u1>(tempString[0x3B304]);
        this->allValuesArr[5] = static_cast<u1>(tempString[0x3B305]);
        this->allValuesArr[6] = static_cast<u1>(tempString[0x3B306]);
        this->allValuesArr[7] = static_cast<u1>(tempString[0x3B307]);
        this->allValuesArr[8] = static_cast<u1>(tempString[0x3B308]);
        this->allValuesArr[9] = static_cast<u1>(tempString[0x3B309]);
        this->allValuesArr[10] = static_cast<u1>(tempString[0x3B30A]);
        this->allValuesArr[11] = static_cast<u1>(tempString[0x3B30B]);
        this->allValuesArr[12] = static_cast<u1>(tempString[0x3B30C]);
        this->allValuesArr[13] = static_cast<u1>(tempString[0x3B30D]);
        this->allValuesArr[14] = static_cast<u1>(tempString[0x3B30E]);
        this->allValuesArr[15] = static_cast<u1>(tempString[0x3B30F]);
        this->allValuesArr[16] = static_cast<u1>(tempString[0x3B310]);
        this->allValuesArr[17] = static_cast<u1>(tempString[0x3B311]);
        this->allValuesArr[18] = static_cast<u1>(tempString[0x3B312]);
        this->allValuesArr[19] = static_cast<u1>(tempString[0x3B313]);
        this->allValuesArr[20] = static_cast<u1>(tempString[0x3B314]);
        this->allValuesArr[21] = static_cast<u1>(tempString[0x3B315]);
        this->allValuesArr[22] = static_cast<u1>(tempString[0x3B316]);
        this->allValuesArr[23] = static_cast<u1>(tempString[0x3B317]);
        this->allValuesArr[24] = static_cast<u1>(tempString[0x3B318]);
        this->allValuesArr[25] = static_cast<u1>(tempString[0x3B319]);
        this->allValuesArr[26] = static_cast<u1>(tempString[0x3B31A]);
        this->allValuesArr[27] = static_cast<u1>(tempString[0x3B31B]);
        this->allValuesArr[28] = static_cast<u1>(tempString[0x3B31C]);
        this->allValuesArr[29] = static_cast<u1>(tempString[0x3B31D]);
        this->allValuesArr[30] = static_cast<u1>(tempString[0x3B31E]);
        this->allValuesArr[31] = static_cast<u1>(tempString[0x3B31F]);
        this->allValuesArr[32] = static_cast<u1>(tempString[0x3B320]);
        this->allValuesArr[33] = static_cast<u1>(tempString[0x3B321]);
        this->allValuesArr[34] = static_cast<u1>(tempString[0x3B322]);
        this->allValuesArr[35] = static_cast<u1>(tempString[0x3B323]);
        this->allValuesArr[36] = static_cast<u1>(tempString[0x3B324]);
        this->allValuesArr[37] = static_cast<u1>(tempString[0x3B325]);
        this->allValuesArr[38] = static_cast<u1>(tempString[0x3B326]);
        this->allValuesArr[39] = static_cast<u1>(tempString[0x3B327]);
        this->allValuesArr[40] = static_cast<u1>(tempString[0x3B328]);
        this->allValuesArr[41] = static_cast<u1>(tempString[0x3B329]);

        this->timeValue_1 = this->allValuesArr[5];
        this->timeValue_2 = this->allValuesArr[6];
        this->timeValue_3 = this->allValuesArr[7];
        this->timeValue_4 = this->allValuesArr[8];
        this->q00Value_1 = this->allValuesArr[9];
        this->q00Value_2 = this->allValuesArr[10];
        this->q00Value_3 = this->allValuesArr[11];
        this->q00Value_4 = this->allValuesArr[12];
        this->q01Value_1 = this->allValuesArr[13];
        this->q01Value_2 = this->allValuesArr[14];
        this->q01Value_3 = this->allValuesArr[15];
        this->q01Value_4 = this->allValuesArr[16];
        this->q02Value_1 = this->allValuesArr[17];
        this->q02Value_2 = this->allValuesArr[18];
        this->q02Value_3 = this->allValuesArr[19];
        this->q02Value_4 = this->allValuesArr[20];
        this->q12Value_1 = this->allValuesArr[21];
        this->q12Value_2 = this->allValuesArr[22];
        this->q12Value_3 = this->allValuesArr[23];
        this->q12Value_4 = this->allValuesArr[24];
        this->rmsValue_1 = this->allValuesArr[25];
        this->rmsValue_2 = this->allValuesArr[26];
        this->rmsValue_3 = this->allValuesArr[27];
        this->rmsValue_4 = this->allValuesArr[28];
        this->rmsValue_5 = this->allValuesArr[29];
        this->rmsValue_6 = this->allValuesArr[30];
        this->rmsValue_7 = this->allValuesArr[31];
        this->rmsValue_8 = this->allValuesArr[32];
        this->rmsValue_9 = this->allValuesArr[33];
        this->rmsValue_10 = this->allValuesArr[34];
        this->rmsValue_11 = this->allValuesArr[35];
        this->rmsValue_12 = this->allValuesArr[36];
        this->solTypeValue_1 = this->allValuesArr[37];
        this->solTypeValue_2 = this->allValuesArr[38];
        this->solTypeValue_3 = this->allValuesArr[39];
        this->flagValue = this->allValuesArr[40];
        this->csValue = this->allValuesArr[41];

        handleCS();
        printVar();

        inputFile.close();
        return 0;
    }
}

void Var49::handleCS()
{
    this->checkSum = cs(allValuesArr, 41);
}

void Var49::printVar()
{
    std::ofstream outputFile;
    std::string outputFileName = "Var49_output.txt";
    outputFile.open(outputFileName);

    outputFile << "Var 49" << std::endl;
    outputFile << "Structure RotationMatrix" << std::endl;
    outputFile << "Receiver time: "
        << static_cast<int>(timeValue_1) << " "
        << static_cast<int>(timeValue_2) << " "
        << static_cast<int>(timeValue_3) << " "
        << static_cast<int>(timeValue_4) << std::endl;
    outputFile << "Components of the rotation matrix Q:" << std::endl;
    outputFile << "q00: "
        << static_cast<int>(q00Value_1) << " "
        << static_cast<int>(q00Value_2) << " "
        << static_cast<int>(q00Value_3) << " "
        << static_cast<int>(q00Value_4) << std::endl;
    outputFile << "q01: "
        << static_cast<int>(q01Value_1) << " "
        << static_cast<int>(q01Value_2) << " "
        << static_cast<int>(q01Value_3) << " "
        << static_cast<int>(q01Value_4) << std::endl;
    outputFile << "q02: "
        << static_cast<int>(q02Value_1) << " "
        << static_cast<int>(q02Value_2) << " "
        << static_cast<int>(q02Value_3) << " "
        << static_cast<int>(q02Value_4) << std::endl;
    outputFile << "q12: "
        << static_cast<int>(q12Value_1) << " "
        << static_cast<int>(q12Value_2) << " "
        << static_cast<int>(q12Value_3) << " "
        << static_cast<int>(q12Value_4) << std::endl;
    outputFile << "Estimated accuracy for three baseline vectors: "
        << static_cast<int>(rmsValue_1) << " "
        << static_cast<int>(rmsValue_2) << " "
        << static_cast<int>(rmsValue_3) << " "
        << static_cast<int>(rmsValue_4) << " "
        << static_cast<int>(rmsValue_5) << " "
        << static_cast<int>(rmsValue_6) << " "
        << static_cast<int>(rmsValue_7) << " "
        << static_cast<int>(rmsValue_8) << " "
        << static_cast<int>(rmsValue_9) << " "
        << static_cast<int>(rmsValue_10) << " "
        << static_cast<int>(rmsValue_11) << " "
        << static_cast<int>(rmsValue_12) << std::endl;
    outputFile << "Solution type8 for three baseline vectors: "
        << static_cast<int>(solTypeValue_1) << " "
        << static_cast<int>(solTypeValue_2) << " "
        << static_cast<int>(solTypeValue_3) << std::endl;
    outputFile << "Flag: " << static_cast<int>(flagValue) << std::endl;
    outputFile << "Checksum of the string: " << static_cast<int>(checkSum) << std::endl;
    outputFile << "Parameter u1 cs: " << static_cast<int>(csValue);

    outputFile.close();
}

int Var49::getCheckSum()
{
    return this->checkSum;
}

int Var49::getCsValue()
{
    return this->csValue;
}