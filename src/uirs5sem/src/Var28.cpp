#include <iostream>
#include <fstream>
#include "../include/Var28.h"

int Var28::handleVar()
{
    std::ifstream inputFile;
    std::string inputFileName = "rks.dat";
    inputFile.open(inputFileName, std::ios::in | std::ios::binary);

    if (!inputFile.is_open())
    {
        std::cerr << "Error. File was not overwritten" << std::endl;
        exit(1);
    }
    else
    {
        int tempInt = 0; // variable for memory allocation
        std::string tempString; // char array holds all the characters of the file
        inputFile.read((char*)&tempInt, sizeof(tempInt)); // read string size from file
        tempString.resize(tempInt); // allocate memory of required size
        inputFile.read(&tempString[0], tempInt); // read string from file

        this->allValuesArr[0] = static_cast<u1>(tempString[0x310C]);
        this->allValuesArr[1] = static_cast<u1>(tempString[0x310D]);
        this->allValuesArr[2] = static_cast<u1>(tempString[0x310E]);
        this->allValuesArr[3] = static_cast<u1>(tempString[0x310F]);
        this->allValuesArr[4] = static_cast<u1>(tempString[0x3110]);
        this->allValuesArr[5] = static_cast<u1>(tempString[0x3111]);
        this->allValuesArr[6] = static_cast<u1>(tempString[0x3112]);
        this->allValuesArr[7] = static_cast<u1>(tempString[0x3113]);
        this->allValuesArr[8] = static_cast<u1>(tempString[0x3114]);
        this->allValuesArr[9] = static_cast<u1>(tempString[0x3115]);
        this->allValuesArr[10] = static_cast<u1>(tempString[0x3116]);
        this->allValuesArr[11] = static_cast<u1>(tempString[0x3117]);
        this->allValuesArr[12] = static_cast<u1>(tempString[0x3118]);
        this->allValuesArr[13] = static_cast<u1>(tempString[0x3119]);
        this->allValuesArr[14] = static_cast<u1>(tempString[0x311A]);
        this->allValuesArr[15] = static_cast<u1>(tempString[0x311B]);
        this->allValuesArr[16] = static_cast<u1>(tempString[0x311C]);
        this->allValuesArr[17] = static_cast<u1>(tempString[0x311D]);
        this->allValuesArr[18] = static_cast<u1>(tempString[0x311E]);
        this->allValuesArr[19] = static_cast<u1>(tempString[0x311F]);
        this->allValuesArr[20] = static_cast<u1>(tempString[0x3120]);
        this->allValuesArr[21] = static_cast<u1>(tempString[0x3121]);
        this->allValuesArr[22] = static_cast<u1>(tempString[0x3122]);
        this->allValuesArr[23] = static_cast<u1>(tempString[0x3123]);
        this->allValuesArr[24] = static_cast<u1>(tempString[0x3124]);
        this->allValuesArr[25] = static_cast<u1>(tempString[0x3125]);
        this->allValuesArr[26] = static_cast<u1>(tempString[0x3126]);
        this->allValuesArr[27] = static_cast<u1>(tempString[0x3127]);
        this->allValuesArr[28] = static_cast<u1>(tempString[0x3128]);
        this->allValuesArr[29] = static_cast<u1>(tempString[0x3129]);
        this->allValuesArr[30] = static_cast<u1>(tempString[0x312A]);
        this->allValuesArr[31] = static_cast<u1>(tempString[0x312B]);
        this->allValuesArr[32] = static_cast<u1>(tempString[0x312C]);
        this->allValuesArr[33] = static_cast<u1>(tempString[0x312D]);
        this->allValuesArr[34] = static_cast<u1>(tempString[0x312E]);
        this->allValuesArr[35] = static_cast<u1>(tempString[0x312F]);
        this->allValuesArr[36] = static_cast<u1>(tempString[0x3130]);
        this->allValuesArr[37] = static_cast<u1>(tempString[0x3131]);
        this->allValuesArr[38] = static_cast<u1>(tempString[0x3132]);
        this->allValuesArr[39] = static_cast<u1>(tempString[0x3133]);
        this->allValuesArr[40] = static_cast<u1>(tempString[0x3134]);
        this->allValuesArr[41] = static_cast<u1>(tempString[0x3135]);
        this->allValuesArr[42] = static_cast<u1>(tempString[0x3136]);

        this->latValue_1 = this->allValuesArr[5];
        this->latValue_2 = this->allValuesArr[6];
        this->latValue_3 = this->allValuesArr[7];
        this->latValue_4 = this->allValuesArr[8];
        this->latValue_5 = this->allValuesArr[9];
        this->latValue_6 = this->allValuesArr[10];
        this->latValue_7 = this->allValuesArr[11];
        this->latValue_8 = this->allValuesArr[12];
        this->lonValue_1 = this->allValuesArr[13];
        this->lonValue_2 = this->allValuesArr[14];
        this->lonValue_3 = this->allValuesArr[15];
        this->lonValue_4 = this->allValuesArr[16];
        this->lonValue_5 = this->allValuesArr[17];
        this->lonValue_6 = this->allValuesArr[18];
        this->lonValue_7 = this->allValuesArr[19];
        this->lonValue_8 = this->allValuesArr[20];
        this->altValue_1 = this->allValuesArr[21];
        this->altValue_2 = this->allValuesArr[22];
        this->altValue_3 = this->allValuesArr[23];
        this->altValue_4 = this->allValuesArr[24];
        this->altValue_5 = this->allValuesArr[25];
        this->altValue_6 = this->allValuesArr[26];
        this->altValue_7 = this->allValuesArr[27];
        this->altValue_8 = this->allValuesArr[28];
        this->pSigmaValue_1 = this->allValuesArr[29];
        this->pSigmaValue_2 = this->allValuesArr[30];
        this->pSigmaValue_3 = this->allValuesArr[31];
        this->pSigmaValue_4 = this->allValuesArr[32];
        this->solTypeValue = this->allValuesArr[33];
        this->systemValue = this->allValuesArr[34];
        this->crsCodeValue_1 = this->allValuesArr[35];
        this->crsCodeValue_2 = this->allValuesArr[36];
        this->crsCodeValue_3 = this->allValuesArr[37];
        this->crsCodeValue_4 = this->allValuesArr[38];
        this->crsCodeValue_5 = this->allValuesArr[39];
        this->chIssueValue_1 = this->allValuesArr[40];
        this->chIssueValue_2 = this->allValuesArr[41];
        this->csValue = this->allValuesArr[42];

        handleCS();
        printVar();

        inputFile.close();
        return 0;
    }
}

void Var28::handleCS()
{
    this->checkSum = cs(allValuesArr, 42);
}

void Var28::printVar()
{
    std::ofstream outputFile;
    std::string outputFileName = "Var28_output.txt";
    outputFile.open(outputFileName);

    outputFile << "Var 28" << std::endl;
    outputFile << "Structure SpecificGeoPos" << std::endl;
    outputFile << "Latitude: "
        << static_cast<int>(latValue_1) << " "
        << static_cast<int>(latValue_2) << " "
        << static_cast<int>(latValue_3) << " "
        << static_cast<int>(latValue_4) << " "
        << static_cast<int>(latValue_5) << " "
        << static_cast<int>(latValue_6) << " "
        << static_cast<int>(latValue_7) << " "
        << static_cast<int>(latValue_8) << std::endl;
    outputFile << "Longitude: "
        << static_cast<int>(lonValue_1) << " "
        << static_cast<int>(lonValue_2) << " "
        << static_cast<int>(lonValue_3) << " "
        << static_cast<int>(lonValue_4) << " "
        << static_cast<int>(lonValue_5) << " "
        << static_cast<int>(lonValue_6) << " "
        << static_cast<int>(lonValue_7) << " "
        << static_cast<int>(lonValue_8) << std::endl;
    outputFile << "Ellipsoidal height: "
        << static_cast<int>(altValue_1) << " "
        << static_cast<int>(altValue_2) << " "
        << static_cast<int>(altValue_3) << " "
        << static_cast<int>(altValue_4) << " "
        << static_cast<int>(altValue_5) << " "
        << static_cast<int>(altValue_6) << " "
        << static_cast<int>(altValue_7) << " "
        << static_cast<int>(altValue_8) << std::endl;
    outputFile << "Position SEP: "
        << static_cast<int>(pSigmaValue_1) << " "
        << static_cast<int>(pSigmaValue_2) << " "
        << static_cast<int>(pSigmaValue_3) << " "
        << static_cast<int>(pSigmaValue_4) << std::endl;
    outputFile << "Solution type: " << static_cast<int>(solTypeValue) << std::endl;
    outputFile << "Coordinate system: " << static_cast<int>(systemValue) << std::endl;
    outputFile << "Name of the coordinate reference system: "
        << static_cast<int>(crsCodeValue_1) << " "
        << static_cast<int>(crsCodeValue_2) << " "
        << static_cast<int>(crsCodeValue_3) << " "
        << static_cast<int>(crsCodeValue_4) << " "
        << static_cast<int>(crsCodeValue_5) << std::endl;
    outputFile << "Counter incrementing on every potential change of user grid system: "
        << static_cast<int>(chIssueValue_1) << " "
        << static_cast<int>(chIssueValue_2) << std::endl;
    outputFile << "Checksum of the string: " << static_cast<int>(checkSum) << std::endl;
    outputFile << "Parameter u1 cs: " << static_cast<int>(csValue);

    outputFile.close();
}

int Var28::getCheckSum()
{
    return this->checkSum;
}

int Var28::getCsValue()
{
    return this->csValue;
}