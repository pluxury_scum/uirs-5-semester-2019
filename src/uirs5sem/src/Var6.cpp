#include <iostream>
#include <fstream>
#include "../include/Var6.h"

int Var6::handleVar()
{
    std::ifstream inputFile;
    std::string inputFileName = "rks.dat";
    inputFile.open(inputFileName, std::ios::in | std::ios::binary);

    if (!inputFile.is_open())
    {
        std::cerr << "Error. File was not overwritten" << std::endl;
        exit(1);
    }
    else
    {
        int tempInt = 0; // variable for memory allocation
        std::string tempString; // char array holds all the characters of the file
        inputFile.read((char*)&tempInt, sizeof(tempInt)); // read string size from file
        tempString.resize(tempInt); // allocate memory of required size
        inputFile.read(&tempString[0], tempInt); // read string from file

        this->allValuesArr[0] = static_cast<u1>(tempString[0x5CCC]);
        this->allValuesArr[1] = static_cast<u1>(tempString[0x5CCD]);
        this->allValuesArr[2] = static_cast<u1>(tempString[0x5CCE]);
        this->allValuesArr[3] = static_cast<u1>(tempString[0x5CCF]);
        this->allValuesArr[4] = static_cast<u1>(tempString[0x5CD0]);
        this->allValuesArr[5] = static_cast<u1>(tempString[0x5CD1]);
        this->allValuesArr[6] = static_cast<u1>(tempString[0x5CD2]);
        this->allValuesArr[7] = static_cast<u1>(tempString[0x5CD3]);
        this->allValuesArr[8] = static_cast<u1>(tempString[0x5CD4]);
        this->allValuesArr[9] = static_cast<u1>(tempString[0x5CD5]);

        this->accValue_1 = this->allValuesArr[5];
        this->accValue_2 = this->allValuesArr[6];
        this->accValue_3 = this->allValuesArr[7];
        this->accValue_4 = this->allValuesArr[8];
        this->csValue = this->allValuesArr[9];

        handleCS();
        printVar();

        inputFile.close();
        return 0;
    }
}

void Var6::handleCS()
{
    this->checkSum = cs(allValuesArr, 9);
}

void Var6::printVar()
{
    std::ofstream outputFile;
    std::string outputFileName = "Var6_output.txt";
    outputFile.open(outputFileName);

    outputFile << "Var 6" << std::endl;
    outputFile << "Structure RcvTimeAccuracy" << std::endl;
    outputFile << "Accuracy: "
        << static_cast<int>(accValue_1) << " "
        << static_cast<int>(accValue_2) << " "
        << static_cast<int>(accValue_3) << " "
        << static_cast<int>(accValue_4) << std::endl;
    outputFile << "Checksum of the string: " << static_cast<int>(checkSum) << std::endl;
    outputFile << "Parameter u1 cs: " << static_cast<int>(csValue);

    outputFile.close();
}

int Var6::getCheckSum()
{
    return this->checkSum;
}

int Var6::getCsValue()
{
    return this->csValue;
}