#include <iostream>
#include <fstream>
#include "../include/Var46.h"

int Var46::handleVar()
{
    std::ifstream inputFile;
    std::string inputFileName = "rks.dat";
    inputFile.open(inputFileName, std::ios::in | std::ios::binary);

    if (!inputFile.is_open())
    {
        std::cerr << "Error. File was not overwritten" << std::endl;
        exit(1);
    }
    else
    {
        int tempInt = 0; // variable for memory allocation
        std::string tempString; // char array holds all the characters of the file
        inputFile.read((char*)&tempInt, sizeof(tempInt)); // read string size from file
        tempString.resize(tempInt); // allocate memory of required size
        inputFile.read(&tempString[0], tempInt); // read string from file

        this->allValuesArr[0] = static_cast<u1>(tempString[0x31A]);
        this->allValuesArr[1] = static_cast<u1>(tempString[0x31B]);
        this->allValuesArr[2] = static_cast<u1>(tempString[0x31C]);
        this->allValuesArr[3] = static_cast<u1>(tempString[0x31D]);
        this->allValuesArr[4] = static_cast<u1>(tempString[0x31E]);
        this->allValuesArr[5] = static_cast<u1>(tempString[0x31F]);
        this->allValuesArr[6] = static_cast<u1>(tempString[0x320]);
        this->allValuesArr[7] = static_cast<u1>(tempString[0x321]);
        this->allValuesArr[8] = static_cast<u1>(tempString[0x322]);
        this->allValuesArr[9] = static_cast<u1>(tempString[0x323]);
        this->allValuesArr[10] = static_cast<u1>(tempString[0x324]);
        this->allValuesArr[11] = static_cast<u1>(tempString[0x325]);
        this->allValuesArr[12] = static_cast<u1>(tempString[0x326]);
        this->allValuesArr[13] = static_cast<u1>(tempString[0x327]);
        this->allValuesArr[14] = static_cast<u1>(tempString[0x328]);
        this->allValuesArr[15] = static_cast<u1>(tempString[0x329]);
        this->allValuesArr[16] = static_cast<u1>(tempString[0x32A]);
        this->allValuesArr[17] = static_cast<u1>(tempString[0x32B]);
        this->allValuesArr[18] = static_cast<u1>(tempString[0x32C]);
        this->allValuesArr[19] = static_cast<u1>(tempString[0x32D]);
        this->allValuesArr[20] = static_cast<u1>(tempString[0x32E]);
        this->allValuesArr[21] = static_cast<u1>(tempString[0x32F]);
        this->allValuesArr[22] = static_cast<u1>(tempString[0x330]);
        this->allValuesArr[23] = static_cast<u1>(tempString[0x331]);
        this->allValuesArr[24] = static_cast<u1>(tempString[0x332]);
        this->allValuesArr[25] = static_cast<u1>(tempString[0x333]);
        this->allValuesArr[26] = static_cast<u1>(tempString[0x334]);
        this->allValuesArr[27] = static_cast<u1>(tempString[0x335]);
        this->allValuesArr[28] = static_cast<u1>(tempString[0x336]);
        this->allValuesArr[29] = static_cast<u1>(tempString[0x337]);
        this->allValuesArr[30] = static_cast<u1>(tempString[0x338]);
        this->allValuesArr[31] = static_cast<u1>(tempString[0x339]);
        this->allValuesArr[32] = static_cast<u1>(tempString[0x33A]);
        this->allValuesArr[33] = static_cast<u1>(tempString[0x33B]);
        this->allValuesArr[34] = static_cast<u1>(tempString[0x33C]);
        this->allValuesArr[35] = static_cast<u1>(tempString[0x33D]);
        this->allValuesArr[36] = static_cast<u1>(tempString[0x33E]);
        this->allValuesArr[37] = static_cast<u1>(tempString[0x33F]);
        this->allValuesArr[38] = static_cast<u1>(tempString[0x340]);
        this->allValuesArr[39] = static_cast<u1>(tempString[0x341]);
        this->allValuesArr[40] = static_cast<u1>(tempString[0x342]);
        this->allValuesArr[41] = static_cast<u1>(tempString[0x343]);
        this->allValuesArr[42] = static_cast<u1>(tempString[0x344]);
        this->allValuesArr[43] = static_cast<u1>(tempString[0x345]);
        this->allValuesArr[44] = static_cast<u1>(tempString[0x346]);
        this->allValuesArr[45] = static_cast<u1>(tempString[0x347]);
        this->allValuesArr[46] = static_cast<u1>(tempString[0x348]);
        this->allValuesArr[47] = static_cast<u1>(tempString[0x349]);
        this->allValuesArr[48] = static_cast<u1>(tempString[0x34A]);
        this->allValuesArr[49] = static_cast<u1>(tempString[0x34B]);
        this->allValuesArr[50] = static_cast<u1>(tempString[0x34C]);
        this->allValuesArr[51] = static_cast<u1>(tempString[0x34D]);
        this->allValuesArr[52] = static_cast<u1>(tempString[0x34E]);
        this->allValuesArr[53] = static_cast<u1>(tempString[0x34F]);
        this->allValuesArr[54] = static_cast<u1>(tempString[0x350]);
        this->allValuesArr[55] = static_cast<u1>(tempString[0x351]);
        this->allValuesArr[56] = static_cast<u1>(tempString[0x352]);
        this->allValuesArr[57] = static_cast<u1>(tempString[0x353]);
        this->allValuesArr[58] = static_cast<u1>(tempString[0x354]);
        this->allValuesArr[59] = static_cast<u1>(tempString[0x355]);
        this->allValuesArr[60] = static_cast<u1>(tempString[0x356]);
        this->allValuesArr[61] = static_cast<u1>(tempString[0x357]);
        this->allValuesArr[62] = static_cast<u1>(tempString[0x358]);
        this->allValuesArr[63] = static_cast<u1>(tempString[0x359]);
        this->allValuesArr[64] = static_cast<u1>(tempString[0x35A]);
        this->allValuesArr[65] = static_cast<u1>(tempString[0x35B]);
        this->allValuesArr[66] = static_cast<u1>(tempString[0x35C]);
        this->allValuesArr[67] = static_cast<u1>(tempString[0x35D]);
        this->allValuesArr[68] = static_cast<u1>(tempString[0x35E]);
        this->allValuesArr[69] = static_cast<u1>(tempString[0x35F]);
        this->allValuesArr[70] = static_cast<u1>(tempString[0x360]);
        this->allValuesArr[71] = static_cast<u1>(tempString[0x361]);
        this->allValuesArr[72] = static_cast<u1>(tempString[0x362]);
        this->allValuesArr[73] = static_cast<u1>(tempString[0x363]);
        this->allValuesArr[74] = static_cast<u1>(tempString[0x364]);
        this->allValuesArr[75] = static_cast<u1>(tempString[0x365]);
        this->allValuesArr[76] = static_cast<u1>(tempString[0x366]);
        this->allValuesArr[77] = static_cast<u1>(tempString[0x367]);
        this->allValuesArr[78] = static_cast<u1>(tempString[0x368]);
        this->allValuesArr[79] = static_cast<u1>(tempString[0x369]);
        this->allValuesArr[80] = static_cast<u1>(tempString[0x36A]);
        this->allValuesArr[81] = static_cast<u1>(tempString[0x36B]);
        this->allValuesArr[82] = static_cast<u1>(tempString[0x36C]);
        this->allValuesArr[83] = static_cast<u1>(tempString[0x36D]);
        this->allValuesArr[84] = static_cast<u1>(tempString[0x36E]);
        this->allValuesArr[85] = static_cast<u1>(tempString[0x36F]);
        this->allValuesArr[86] = static_cast<u1>(tempString[0x370]);
        this->allValuesArr[87] = static_cast<u1>(tempString[0x371]);
        this->allValuesArr[88] = static_cast<u1>(tempString[0x372]);
        this->allValuesArr[89] = static_cast<u1>(tempString[0x373]);
        this->allValuesArr[90] = static_cast<u1>(tempString[0x374]);
        this->allValuesArr[91] = static_cast<u1>(tempString[0x375]);
        this->allValuesArr[92] = static_cast<u1>(tempString[0x376]);
        this->allValuesArr[93] = static_cast<u1>(tempString[0x377]);
        this->allValuesArr[94] = static_cast<u1>(tempString[0x378]);
        this->allValuesArr[95] = static_cast<u1>(tempString[0x379]);
        this->allValuesArr[96] = static_cast<u1>(tempString[0x37A]);
        this->allValuesArr[97] = static_cast<u1>(tempString[0x37B]);
        this->allValuesArr[98] = static_cast<u1>(tempString[0x37C]);
        this->allValuesArr[99] = static_cast<u1>(tempString[0x37D]);
        this->allValuesArr[100] = static_cast<u1>(tempString[0x37E]);
        this->allValuesArr[101] = static_cast<u1>(tempString[0x37F]);
        this->allValuesArr[102] = static_cast<u1>(tempString[0x380]);
        this->allValuesArr[103] = static_cast<u1>(tempString[0x381]);
        this->allValuesArr[104] = static_cast<u1>(tempString[0x382]);
        this->allValuesArr[105] = static_cast<u1>(tempString[0x383]);
        this->allValuesArr[106] = static_cast<u1>(tempString[0x384]);
        this->allValuesArr[107] = static_cast<u1>(tempString[0x385]);
        this->allValuesArr[108] = static_cast<u1>(tempString[0x386]);
        this->allValuesArr[109] = static_cast<u1>(tempString[0x387]);
        this->allValuesArr[110] = static_cast<u1>(tempString[0x388]);
        this->allValuesArr[111] = static_cast<u1>(tempString[0x389]);
        this->allValuesArr[112] = static_cast<u1>(tempString[0x38A]);
        this->allValuesArr[113] = static_cast<u1>(tempString[0x38B]);
        this->allValuesArr[114] = static_cast<u1>(tempString[0x38C]);
        this->allValuesArr[115] = static_cast<u1>(tempString[0x38D]);
        this->allValuesArr[116] = static_cast<u1>(tempString[0x38E]);
        this->allValuesArr[117] = static_cast<u1>(tempString[0x38F]);
        this->allValuesArr[118] = static_cast<u1>(tempString[0x390]);
        this->allValuesArr[119] = static_cast<u1>(tempString[0x391]);
        this->allValuesArr[120] = static_cast<u1>(tempString[0x392]);
        this->allValuesArr[121] = static_cast<u1>(tempString[0x393]);
        this->allValuesArr[122] = static_cast<u1>(tempString[0x394]);
        this->allValuesArr[123] = static_cast<u1>(tempString[0x395]);
        this->allValuesArr[124] = static_cast<u1>(tempString[0x396]);
        this->allValuesArr[125] = static_cast<u1>(tempString[0x397]);
        this->allValuesArr[126] = static_cast<u1>(tempString[0x398]);
        this->allValuesArr[127] = static_cast<u1>(tempString[0x399]);
        this->allValuesArr[128] = static_cast<u1>(tempString[0x39A]);
        this->allValuesArr[129] = static_cast<u1>(tempString[0x39B]);
        this->allValuesArr[130] = static_cast<u1>(tempString[0x39C]);
        this->allValuesArr[131] = static_cast<u1>(tempString[0x39D]);
        this->allValuesArr[132] = static_cast<u1>(tempString[0x39E]);
        this->allValuesArr[133] = static_cast<u1>(tempString[0x39F]);
        this->allValuesArr[134] = static_cast<u1>(tempString[0x3A0]);
        this->allValuesArr[135] = static_cast<u1>(tempString[0x3A1]);
        this->allValuesArr[136] = static_cast<u1>(tempString[0x3A2]);
        this->allValuesArr[137] = static_cast<u1>(tempString[0x3A3]);
        this->allValuesArr[138] = static_cast<u1>(tempString[0x3A4]);
        this->allValuesArr[139] = static_cast<u1>(tempString[0x3A5]);
        this->allValuesArr[140] = static_cast<u1>(tempString[0x3A6]);
        this->allValuesArr[141] = static_cast<u1>(tempString[0x3A7]);
        this->allValuesArr[142] = static_cast<u1>(tempString[0x3A8]);
        this->allValuesArr[143] = static_cast<u1>(tempString[0x3A9]);
        this->allValuesArr[144] = static_cast<u1>(tempString[0x3AA]);
        this->allValuesArr[145] = static_cast<u1>(tempString[0x3AB]);
        this->allValuesArr[146] = static_cast<u1>(tempString[0x3AC]);
        this->allValuesArr[147] = static_cast<u1>(tempString[0x3AD]);
        this->allValuesArr[148] = static_cast<u1>(tempString[0x3AE]);
        this->allValuesArr[149] = static_cast<u1>(tempString[0x3AF]);
        this->allValuesArr[150] = static_cast<u1>(tempString[0x3B0]);
        this->allValuesArr[151] = static_cast<u1>(tempString[0x3B1]);
        this->allValuesArr[152] = static_cast<u1>(tempString[0x3B2]);
        this->allValuesArr[153] = static_cast<u1>(tempString[0x3B3]);

        this->svValue = this->allValuesArr[5];
        this->towValue_1 = this->allValuesArr[6];
        this->towValue_2 = this->allValuesArr[7];
        this->towValue_3 = this->allValuesArr[8];
        this->towValue_4 = this->allValuesArr[9];
        this->flagsValue = this->allValuesArr[10];
        this->iodcValue_1 = this->allValuesArr[11];
        this->iodcValue_2 = this->allValuesArr[12];
        this->tocValue_1 = this->allValuesArr[13];
        this->tocValue_2 = this->allValuesArr[14];
        this->tocValue_3 = this->allValuesArr[15];
        this->tocValue_4 = this->allValuesArr[16];
        this->uraValue = this->allValuesArr[17];
        this->healthSValue = this->allValuesArr[18];
        this->wnValue_1 = this->allValuesArr[19];
        this->wnValue_2 = this->allValuesArr[20];
        this->tgdValue_1 = this->allValuesArr[21];
        this->tgdValue_2 = this->allValuesArr[22];
        this->tgdValue_3 = this->allValuesArr[23];
        this->tgdValue_4 = this->allValuesArr[24];
        this->af2Value_1 = this->allValuesArr[25];
        this->af2Value_2 = this->allValuesArr[26];
        this->af2Value_3 = this->allValuesArr[27];
        this->af2Value_4 = this->allValuesArr[28];
        this->af1Value_1 = this->allValuesArr[29];
        this->af1Value_2 = this->allValuesArr[30];
        this->af1Value_3 = this->allValuesArr[31];
        this->af1Value_4 = this->allValuesArr[32];
        this->af0Value_1 = this->allValuesArr[33];
        this->af0Value_2 = this->allValuesArr[34];
        this->af0Value_3 = this->allValuesArr[35];
        this->af0Value_4 = this->allValuesArr[36];
        this->toeValue_1 = this->allValuesArr[37];
        this->toeValue_2 = this->allValuesArr[38];
        this->toeValue_3 = this->allValuesArr[39];
        this->toeValue_4 = this->allValuesArr[40];
        this->iodeValue_1 = this->allValuesArr[41];
        this->iodeValue_2 = this->allValuesArr[42];
        this->rootAValue_1 = this->allValuesArr[43];
        this->rootAValue_2 = this->allValuesArr[44];
        this->rootAValue_3 = this->allValuesArr[45];
        this->rootAValue_4 = this->allValuesArr[46];
        this->rootAValue_5 = this->allValuesArr[47];
        this->rootAValue_6 = this->allValuesArr[48];
        this->rootAValue_7 = this->allValuesArr[49];
        this->rootAValue_8 = this->allValuesArr[50];
        this->eccValue_1 = this->allValuesArr[51];
        this->eccValue_2 = this->allValuesArr[52];
        this->eccValue_3 = this->allValuesArr[53];
        this->eccValue_4 = this->allValuesArr[54];
        this->eccValue_5 = this->allValuesArr[55];
        this->eccValue_6 = this->allValuesArr[56];
        this->eccValue_7 = this->allValuesArr[57];
        this->eccValue_8 = this->allValuesArr[58];
        this->m0Value_1 = this->allValuesArr[59];
        this->m0Value_2 = this->allValuesArr[60];
        this->m0Value_3 = this->allValuesArr[61];
        this->m0Value_4 = this->allValuesArr[62];
        this->m0Value_5 = this->allValuesArr[63];
        this->m0Value_6 = this->allValuesArr[64];
        this->m0Value_7 = this->allValuesArr[65];
        this->m0Value_8 = this->allValuesArr[66];
        this->omega0Value_1 = this->allValuesArr[67];
        this->omega0Value_2 = this->allValuesArr[68];
        this->omega0Value_3 = this->allValuesArr[69];
        this->omega0Value_4 = this->allValuesArr[70];
        this->omega0Value_5 = this->allValuesArr[71];
        this->omega0Value_6 = this->allValuesArr[72];
        this->omega0Value_7 = this->allValuesArr[73];
        this->omega0Value_8 = this->allValuesArr[74];
        this->inc0Value_1 = this->allValuesArr[75];
        this->inc0Value_2 = this->allValuesArr[76];
        this->inc0Value_3 = this->allValuesArr[77];
        this->inc0Value_4 = this->allValuesArr[78];
        this->inc0Value_5 = this->allValuesArr[79];
        this->inc0Value_6 = this->allValuesArr[80];
        this->inc0Value_7 = this->allValuesArr[81];
        this->inc0Value_8 = this->allValuesArr[82];
        this->argPerValue_1 = this->allValuesArr[83];
        this->argPerValue_2 = this->allValuesArr[84];
        this->argPerValue_3 = this->allValuesArr[85];
        this->argPerValue_4 = this->allValuesArr[86];
        this->argPerValue_5 = this->allValuesArr[87];
        this->argPerValue_6 = this->allValuesArr[88];
        this->argPerValue_7 = this->allValuesArr[89];
        this->argPerValue_8 = this->allValuesArr[90];
        this->delnValue_1 = this->allValuesArr[91];
        this->delnValue_2 = this->allValuesArr[92];
        this->delnValue_3 = this->allValuesArr[93];
        this->delnValue_4 = this->allValuesArr[94];
        this->omegaDotValue_1 = this->allValuesArr[95];
        this->omegaDotValue_2 = this->allValuesArr[96];
        this->omegaDotValue_3 = this->allValuesArr[97];
        this->omegaDotValue_4 = this->allValuesArr[98];
        this->incDotValue_1 = this->allValuesArr[99];
        this->incDotValue_2 = this->allValuesArr[100];
        this->incDotValue_3 = this->allValuesArr[101];
        this->incDotValue_4 = this->allValuesArr[102];
        this->crcValue_1 = this->allValuesArr[103];
        this->crcValue_2 = this->allValuesArr[104];
        this->crcValue_3 = this->allValuesArr[105];
        this->crcValue_4 = this->allValuesArr[106];
        this->crsValue_1 = this->allValuesArr[107];
        this->crsValue_2 = this->allValuesArr[108];
        this->crsValue_3 = this->allValuesArr[109];
        this->crsValue_4 = this->allValuesArr[110];
        this->cucValue_1 = this->allValuesArr[111];
        this->cucValue_2 = this->allValuesArr[112];
        this->cucValue_3 = this->allValuesArr[113];
        this->cucValue_4 = this->allValuesArr[114];
        this->cusValue_1 = this->allValuesArr[115];
        this->cusValue_2 = this->allValuesArr[116];
        this->cusValue_3 = this->allValuesArr[117];
        this->cusValue_4 = this->allValuesArr[118];
        this->cicValue_1 = this->allValuesArr[119];
        this->cicValue_2 = this->allValuesArr[120];
        this->cicValue_3 = this->allValuesArr[121];
        this->cicValue_4 = this->allValuesArr[122];
        this->cisValue_1 = this->allValuesArr[123];
        this->cisValue_2 = this->allValuesArr[124];
        this->cisValue_3 = this->allValuesArr[125];
        this->cisValue_4 = this->allValuesArr[126];
        this->bgdE1E5aValue_1 = this->allValuesArr[127];
        this->bgdE1E5aValue_2 = this->allValuesArr[128];
        this->bgdE1E5aValue_3 = this->allValuesArr[129];
        this->bgdE1E5aValue_4 = this->allValuesArr[130];
        this->bgdE1E5bValue_1 = this->allValuesArr[131];
        this->bgdE1E5bValue_2 = this->allValuesArr[132];
        this->bgdE1E5bValue_3 = this->allValuesArr[133];
        this->bgdE1E5bValue_4 = this->allValuesArr[134];
        this->ai0Value_1 = this->allValuesArr[135];
        this->ai0Value_2 = this->allValuesArr[136];
        this->ai0Value_3 = this->allValuesArr[137];
        this->ai0Value_4 = this->allValuesArr[138];
        this->ai1Value_1 = this->allValuesArr[139];
        this->ai1Value_2 = this->allValuesArr[140];
        this->ai1Value_3 = this->allValuesArr[141];
        this->ai1Value_4 = this->allValuesArr[142];
        this->ai2Value_1 = this->allValuesArr[143];
        this->ai2Value_2 = this->allValuesArr[144];
        this->ai2Value_3 = this->allValuesArr[145];
        this->ai2Value_4 = this->allValuesArr[146];
        this->sfiValue = this->allValuesArr[147];
        this->navTypeValue = this->allValuesArr[148];
        this->DAf0Value_1 = this->allValuesArr[149];
        this->DAf0Value_2 = this->allValuesArr[150];
        this->DAf0Value_3 = this->allValuesArr[151];
        this->DAf0Value_4 = this->allValuesArr[152];

        this->csValue = this->allValuesArr[153];

        handleCS();
        printVar();

        inputFile.close();
        return 0;
    }
}

void Var46::handleCS()
{
    this->checkSum = cs(allValuesArr, 153);
}

void Var46::printVar()
{
    std::ofstream outputFile;
    std::string outputFileName = "Var46_output.txt";
    outputFile.open(outputFileName);

    outputFile << "Var 46" << std::endl;
    outputFile << "Structure GALEphemeris" << std::endl;

    outputFile << "SV PRN number: " << static_cast<int>(svValue) << std::endl;
    outputFile << "Time of week: "
        << static_cast<int>(towValue_1) << " "
        << static_cast<int>(towValue_2) << " "
        << static_cast<int>(towValue_3) << " "
        << static_cast<int>(towValue_4) << std::endl;
    outputFile << "Flags: " << static_cast<int>(flagsValue) << std::endl;
    outputFile << "Issue of data, clock: "
        << static_cast<int>(iodcValue_1) << " "
        << static_cast<int>(iodcValue_2) << std::endl;
    outputFile << "Clock data reference time: "
        << static_cast<int>(tocValue_1) << " "
        << static_cast<int>(tocValue_2) << " "
        << static_cast<int>(tocValue_3) << " "
        << static_cast<int>(tocValue_4) << std::endl;
    outputFile << "User range accuracy: " << static_cast<int>(uraValue) << std::endl;
    outputFile << "Satellite health: " << static_cast<int>(uraValue) << std::endl;
    outputFile << "Week number: " << static_cast<int>(healthSValue) << std::endl;
    outputFile << "Estimated group delay differential: "
        << static_cast<int>(wnValue_1) << " "
        << static_cast<int>(wnValue_2) << std::endl;
    outputFile << "Polynomial coefficient af2: "
        << static_cast<int>(af2Value_1) << " "
        << static_cast<int>(af2Value_2) << " "
        << static_cast<int>(af2Value_3) << " "
        << static_cast<int>(af2Value_4) << std::endl;
    outputFile << "Polynomial coefficient af1: "
        << static_cast<int>(af1Value_1) << " "
        << static_cast<int>(af1Value_2) << " "
        << static_cast<int>(af1Value_3) << " "
        << static_cast<int>(af1Value_4) << std::endl;
    outputFile << "Polynomial coefficient af0: "
        << static_cast<int>(af0Value_1) << " "
        << static_cast<int>(af0Value_2) << " "
        << static_cast<int>(af0Value_3) << " "
        << static_cast<int>(af0Value_4) << std::endl;
    outputFile << "Ephemeris reference time: "
        << static_cast<int>(toeValue_1) << " "
        << static_cast<int>(toeValue_2) << " "
        << static_cast<int>(toeValue_3) << " "
        << static_cast<int>(toeValue_4) << std::endl;
    outputFile << "Issue of data, ephemeris: "
        << static_cast<int>(iodeValue_1) << " "
        << static_cast<int>(iodeValue_2) << std::endl;
    outputFile << "Square root of the semi-major axis: "
        << static_cast<int>(rootAValue_1) << " "
        << static_cast<int>(rootAValue_2) << " "
        << static_cast<int>(rootAValue_3) << " "
        << static_cast<int>(rootAValue_4) << " "
        << static_cast<int>(rootAValue_5) << " "
        << static_cast<int>(rootAValue_6) << " "
        << static_cast<int>(rootAValue_7) << " "
        << static_cast<int>(rootAValue_8) << std::endl;
    outputFile << "Eccentricity: "
        << static_cast<int>(eccValue_1) << " "
        << static_cast<int>(eccValue_2) << " "
        << static_cast<int>(eccValue_3) << " "
        << static_cast<int>(eccValue_4) << " "
        << static_cast<int>(eccValue_5) << " "
        << static_cast<int>(eccValue_6) << " "
        << static_cast<int>(eccValue_7) << " "
        << static_cast<int>(eccValue_8) << std::endl;
    outputFile << "Mean Anomaly at reference time: "
        << static_cast<int>(m0Value_1) << " "
        << static_cast<int>(m0Value_2) << " "
        << static_cast<int>(m0Value_3) << " "
        << static_cast<int>(m0Value_4) << " "
        << static_cast<int>(m0Value_5) << " "
        << static_cast<int>(m0Value_6) << " "
        << static_cast<int>(m0Value_7) << " "
        << static_cast<int>(m0Value_8) << std::endl;
    outputFile << "Longitude of ascending node of orbit plane at the start of week �wn�: "
        << static_cast<int>(omega0Value_1) << " "
        << static_cast<int>(omega0Value_2) << " "
        << static_cast<int>(omega0Value_3) << " "
        << static_cast<int>(omega0Value_4) << " "
        << static_cast<int>(omega0Value_5) << " "
        << static_cast<int>(omega0Value_6) << " "
        << static_cast<int>(omega0Value_7) << " "
        << static_cast<int>(omega0Value_8) << std::endl;
    outputFile << "Inclination angle at reference time: "
        << static_cast<int>(inc0Value_1) << " "
        << static_cast<int>(inc0Value_2) << " "
        << static_cast<int>(inc0Value_3) << " "
        << static_cast<int>(inc0Value_4) << " "
        << static_cast<int>(inc0Value_5) << " "
        << static_cast<int>(inc0Value_6) << " "
        << static_cast<int>(inc0Value_7) << " "
        << static_cast<int>(inc0Value_8) << std::endl;
    outputFile << "Argument of perigee: "
        << static_cast<int>(argPerValue_1) << " "
        << static_cast<int>(argPerValue_2) << " "
        << static_cast<int>(argPerValue_3) << " "
        << static_cast<int>(argPerValue_4) << " "
        << static_cast<int>(argPerValue_5) << " "
        << static_cast<int>(argPerValue_6) << " "
        << static_cast<int>(argPerValue_7) << " "
        << static_cast<int>(argPerValue_8) << std::endl;
    outputFile << "Mean motion difference from computed value: "
        << static_cast<int>(delnValue_1) << " "
        << static_cast<int>(delnValue_2) << " "
        << static_cast<int>(delnValue_3) << " "
        << static_cast<int>(delnValue_4) << std::endl;
    outputFile << "Rate of right ascension: "
        << static_cast<int>(omegaDotValue_1) << " "
        << static_cast<int>(omegaDotValue_2) << " "
        << static_cast<int>(omegaDotValue_3) << " "
        << static_cast<int>(omegaDotValue_4) << std::endl;
    outputFile << "Rate of inclination angle: "
        << static_cast<int>(incDotValue_1) << " "
        << static_cast<int>(incDotValue_2) << " "
        << static_cast<int>(incDotValue_3) << " "
        << static_cast<int>(incDotValue_4) << std::endl;
    outputFile << "Amplitude of the cosine harmonic correction term to the orbit radius: "
        << static_cast<int>(crcValue_1) << " "
        << static_cast<int>(crcValue_2) << " "
        << static_cast<int>(crcValue_3) << " "
        << static_cast<int>(crcValue_4) << std::endl;
    outputFile << "Amplitude of the sine harmonic correction term to the orbit radius: "
        << static_cast<int>(crsValue_1) << " "
        << static_cast<int>(crsValue_2) << " "
        << static_cast<int>(crsValue_3) << " "
        << static_cast<int>(crsValue_4) << std::endl;
    outputFile << "Amplitude of the cosine harmonic correction term to the argument of latitude: "
        << static_cast<int>(cucValue_1) << " "
        << static_cast<int>(cucValue_2) << " "
        << static_cast<int>(cucValue_3) << " "
        << static_cast<int>(cucValue_4) << std::endl;
    outputFile << "Amplitude of the sine harmonic correction term to the argument of latitude: "
        << static_cast<int>(cusValue_1) << " "
        << static_cast<int>(cusValue_2) << " "
        << static_cast<int>(cusValue_3) << " "
        << static_cast<int>(cusValue_4) << std::endl;
    outputFile << "Amplitude of the cosine harmonic correction term to the angle of inclination: "
        << static_cast<int>(cicValue_1) << " "
        << static_cast<int>(cicValue_2) << " "
        << static_cast<int>(cicValue_3) << " "
        << static_cast<int>(cicValue_4) << std::endl;
    outputFile << "Amplitude of the sine harmonic correction term to the angle of inclination: "
        << static_cast<int>(cisValue_1) << " "
        << static_cast<int>(cisValue_2) << " "
        << static_cast<int>(cisValue_3) << " "
        << static_cast<int>(cisValue_4) << std::endl;
    outputFile << "Broadcast group delay E1 - E5A: "
        << static_cast<int>(bgdE1E5aValue_1) << " "
        << static_cast<int>(bgdE1E5aValue_2) << " "
        << static_cast<int>(bgdE1E5aValue_3) << " "
        << static_cast<int>(bgdE1E5aValue_4) << std::endl;
    outputFile << "Broadcast group delay E1 - E5B: "
        << static_cast<int>(bgdE1E5bValue_1) << " "
        << static_cast<int>(bgdE1E5bValue_2) << " "
        << static_cast<int>(bgdE1E5bValue_3) << " "
        << static_cast<int>(bgdE1E5bValue_4) << std::endl;
    outputFile << "Effective ionisation level 1-st order parameter: "
        << static_cast<int>(ai0Value_1) << " "
        << static_cast<int>(ai0Value_2) << " "
        << static_cast<int>(ai0Value_3) << " "
        << static_cast<int>(ai0Value_4) << std::endl;
    outputFile << "Effective ionisation level 2-nd order parameter: "
        << static_cast<int>(ai1Value_1) << " "
        << static_cast<int>(ai1Value_2) << " "
        << static_cast<int>(ai1Value_3) << " "
        << static_cast<int>(ai1Value_4) << std::endl;
    outputFile << "Effective ionisation level 3-rd order parameter: "
        << static_cast<int>(ai2Value_1) << " "
        << static_cast<int>(ai2Value_2) << " "
        << static_cast<int>(ai2Value_3) << " "
        << static_cast<int>(ai2Value_4) << std::endl;
    outputFile << "Ionospheric disturbance flags: " << static_cast<int>(sfiValue) << std::endl;
    outputFile << "Signal type: " << static_cast<int>(navTypeValue) << std::endl;
    outputFile << "Correction to �af0�: "
        << static_cast<int>(DAf0Value_1) << " "
        << static_cast<int>(DAf0Value_2) << " "
        << static_cast<int>(DAf0Value_3) << " "
        << static_cast<int>(DAf0Value_4) << std::endl;
    outputFile << "Checksum of the string: " << static_cast<int>(checkSum) << std::endl;
    outputFile << "Parameter u1 cs: " << static_cast<int>(csValue);

    outputFile.close();
}

int Var46::getCheckSum()
{
    return this->checkSum;
}

int Var46::getCsValue()
{
    return this->csValue;
}