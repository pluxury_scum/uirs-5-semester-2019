#include <iostream>
#include <fstream>
#include "../include/Var66.h"

int Var66::handleVar()
{
    std::ifstream inputFile;
    std::string inputFileName = "rks.dat";
    inputFile.open(inputFileName, std::ios::in | std::ios::binary);

    if (!inputFile.is_open())
    {
        std::cerr << "Error. File was not overwritten" << std::endl;
        exit(1);
    }
    else
    {
        int tempInt = 0; // variable for memory allocation
        std::string tempString; // char array holds all the characters of the file
        inputFile.read((char*)&tempInt, sizeof(tempInt)); // read string size from file
        tempString.resize(tempInt); // allocate memory of required size
        inputFile.read(&tempString[0], tempInt); // read string from file

        this->allValuesArr[0] = static_cast<u1>(tempString[0x37B30]);
        this->allValuesArr[1] = static_cast<u1>(tempString[0x37B31]);
        this->allValuesArr[2] = static_cast<u1>(tempString[0x37B32]);
        this->allValuesArr[3] = static_cast<u1>(tempString[0x37B33]);
        this->allValuesArr[4] = static_cast<u1>(tempString[0x37B34]);
        this->allValuesArr[5] = static_cast<u1>(tempString[0x37B35]);
        this->allValuesArr[6] = static_cast<u1>(tempString[0x37B36]);
        this->allValuesArr[7] = static_cast<u1>(tempString[0x37B37]);
        this->allValuesArr[8] = static_cast<u1>(tempString[0x37B38]);
        this->allValuesArr[9] = static_cast<u1>(tempString[0x37B39]);

        this->ttValue_1 = this->allValuesArr[5];
        this->ttValue_2 = this->allValuesArr[6];
        this->ttValue_3 = this->allValuesArr[7];
        this->ttValue_4 = this->allValuesArr[8];
        this->csValue = this->allValuesArr[9];

        handleCS();
        printVar();

        inputFile.close();
        return 0;
    }
}

void Var66::handleCS()
{
    this->checkSum = cs(allValuesArr, 9);
}

void Var66::printVar()
{
    std::ofstream outputFile;
    std::string outputFileName = "Var66_output.txt";
    outputFile.open(outputFileName);

    outputFile << "Var 66" << std::endl;
    outputFile << "Structure TrackingTime" << std::endl;
    outputFile << "Tracking time: "
        << static_cast<int>(ttValue_1) << " "
        << static_cast<int>(ttValue_2) << " "
        << static_cast<int>(ttValue_3) << " "
        << static_cast<int>(ttValue_4) << std::endl;
    outputFile << "Checksum of the string: " << static_cast<int>(checkSum) << std::endl;
    outputFile << "Parameter u1 cs: " << static_cast<int>(csValue);

    outputFile.close();
}

int Var66::getCheckSum()
{
    return this->checkSum;
}

int Var66::getCsValue()
{
    return this->csValue;
}