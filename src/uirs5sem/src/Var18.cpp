#include <iostream>
#include <fstream>
#include "../include/Var18.h"

int Var18::handleVar()
{
    std::ifstream inputFile;
    std::string inputFileName = "rks.dat";
    inputFile.open(inputFileName, std::ios::in | std::ios::binary);

    if (!inputFile.is_open())
    {
        std::cerr << "Error. File was not overwritten" << std::endl;
        exit(1);
    }
    else
    {
        int tempInt = 0; // variable for memory allocation
        std::string tempString; // char array holds all the characters of the file
        inputFile.read((char*)&tempInt, sizeof(tempInt)); // read string size from file
        tempString.resize(tempInt); // allocate memory of required size
        inputFile.read(&tempString[0], tempInt); // read string from file

        this->allValuesArr[0] = static_cast<u1>(tempString[0x789]);
        this->allValuesArr[1] = static_cast<u1>(tempString[0x78A]);
        this->allValuesArr[2] = static_cast<u1>(tempString[0x78B]);
        this->allValuesArr[3] = static_cast<u1>(tempString[0x78C]);
        this->allValuesArr[4] = static_cast<u1>(tempString[0x78D]);
        this->allValuesArr[5] = static_cast<u1>(tempString[0x78E]);
        this->allValuesArr[6] = static_cast<u1>(tempString[0x78F]);
        this->allValuesArr[7] = static_cast<u1>(tempString[0x790]);
        this->allValuesArr[8] = static_cast<u1>(tempString[0x791]);
        this->allValuesArr[9] = static_cast<u1>(tempString[0x792]);
        this->allValuesArr[10] = static_cast<u1>(tempString[0x793]);
        this->allValuesArr[11] = static_cast<u1>(tempString[0x794]);
        this->allValuesArr[12] = static_cast<u1>(tempString[0x795]);
        this->allValuesArr[13] = static_cast<u1>(tempString[0x796]);
        this->allValuesArr[14] = static_cast<u1>(tempString[0x797]);
        this->allValuesArr[15] = static_cast<u1>(tempString[0x798]);
        this->allValuesArr[16] = static_cast<u1>(tempString[0x799]);
        this->allValuesArr[17] = static_cast<u1>(tempString[0x79A]);
        this->allValuesArr[18] = static_cast<u1>(tempString[0x79B]);
        this->allValuesArr[19] = static_cast<u1>(tempString[0x79C]);
        this->allValuesArr[20] = static_cast<u1>(tempString[0x79D]);
        this->allValuesArr[21] = static_cast<u1>(tempString[0x79E]);
        this->allValuesArr[22] = static_cast<u1>(tempString[0x79F]);
        this->allValuesArr[23] = static_cast<u1>(tempString[0x7A0]);
        this->allValuesArr[24] = static_cast<u1>(tempString[0x7A1]);
        this->allValuesArr[25] = static_cast<u1>(tempString[0x7A2]);
        this->allValuesArr[26] = static_cast<u1>(tempString[0x7A3]);
        this->allValuesArr[27] = static_cast<u1>(tempString[0x7A4]);
        this->allValuesArr[28] = static_cast<u1>(tempString[0x7A5]);
        this->allValuesArr[29] = static_cast<u1>(tempString[0x7A6]);
        this->allValuesArr[30] = static_cast<u1>(tempString[0x7A7]);
        this->allValuesArr[31] = static_cast<u1>(tempString[0x7A8]);
        this->allValuesArr[32] = static_cast<u1>(tempString[0x7A9]);
        this->allValuesArr[33] = static_cast<u1>(tempString[0x7AA]);
        this->allValuesArr[34] = static_cast<u1>(tempString[0x7AB]);
        this->allValuesArr[35] = static_cast<u1>(tempString[0x7AC]);
        this->allValuesArr[36] = static_cast<u1>(tempString[0x7AD]);
        this->allValuesArr[37] = static_cast<u1>(tempString[0x7AE]);
        this->allValuesArr[38] = static_cast<u1>(tempString[0x7AF]);
        this->allValuesArr[39] = static_cast<u1>(tempString[0x7B0]);
        this->allValuesArr[40] = static_cast<u1>(tempString[0x7B1]);
        this->allValuesArr[41] = static_cast<u1>(tempString[0x7B2]);
        this->allValuesArr[42] = static_cast<u1>(tempString[0x7B3]);
        this->allValuesArr[43] = static_cast<u1>(tempString[0x7B4]);
        this->allValuesArr[44] = static_cast<u1>(tempString[0x7B5]);

        this->a0Value_1 = this->allValuesArr[5];
        this->a0Value_2 = this->allValuesArr[6];
        this->a0Value_3 = this->allValuesArr[7];
        this->a0Value_4 = this->allValuesArr[8];
        this->a0Value_5 = this->allValuesArr[9];
        this->a0Value_6 = this->allValuesArr[10];
        this->a0Value_7 = this->allValuesArr[11];
        this->a0Value_8 = this->allValuesArr[12];
        this->a1Value_1 = this->allValuesArr[13];
        this->a1Value_2 = this->allValuesArr[14];
        this->a1Value_3 = this->allValuesArr[15];
        this->a1Value_4 = this->allValuesArr[16];
        this->totValue_1 = this->allValuesArr[17];
        this->totValue_2 = this->allValuesArr[18];
        this->totValue_3 = this->allValuesArr[19];
        this->totValue_4 = this->allValuesArr[20];
        this->wntValue_1 = this->allValuesArr[21];
        this->wntValue_2 = this->allValuesArr[22];
        this->dtlsValue = this->allValuesArr[23];
        this->dnValue = this->allValuesArr[24];
        this->wnlsfValue_1 = this->allValuesArr[25];
        this->wnlsfValue_2 = this->allValuesArr[26];
        this->dtlsfValue = this->allValuesArr[27];
        this->a0gValue_1 = this->allValuesArr[28];
        this->a0gValue_2 = this->allValuesArr[29];
        this->a0gValue_3 = this->allValuesArr[30];
        this->a0gValue_4 = this->allValuesArr[31];
        this->a1gValue_1 = this->allValuesArr[32];
        this->a1gValue_2 = this->allValuesArr[33];
        this->a1gValue_3 = this->allValuesArr[34];
        this->a1gValue_4 = this->allValuesArr[35];
        this->t0gValue_1 = this->allValuesArr[36];
        this->t0gValue_2 = this->allValuesArr[37];
        this->t0gValue_3 = this->allValuesArr[38];
        this->t0gValue_4 = this->allValuesArr[39];
        this->wn0gValue_1 = this->allValuesArr[40];
        this->wn0gValue_2 = this->allValuesArr[41];
        this->flagsValue_1 = this->allValuesArr[42];
        this->flagsValue_2 = this->allValuesArr[43];
        this->csValue = this->allValuesArr[44];

        handleCS();
        printVar();

        inputFile.close();
        return 0;
    }
}

void Var18::handleCS()
{
    this->checkSum = cs(allValuesArr, 44);
}

void Var18::printVar()
{
    std::ofstream outputFile;
    std::string outputFileName = "Var18_output.txt";
    outputFile.open(outputFileName);

    outputFile << "Var 18" << std::endl;
    outputFile << "Structure GalUtcGpsParam" << std::endl;
    outputFile << "Constant term of polynomial: "
        << static_cast<int>(a0Value_1) << " "
        << static_cast<int>(a0Value_2) << " "
        << static_cast<int>(a0Value_3) << " "
        << static_cast<int>(a0Value_4) << " "
        << static_cast<int>(a0Value_5) << " "
        << static_cast<int>(a0Value_6) << " "
        << static_cast<int>(a0Value_7) << " "
        << static_cast<int>(a0Value_8) << std::endl;
    outputFile << "First order term of polynomial: "
        << static_cast<int>(a1Value_1) << " "
        << static_cast<int>(a1Value_2) << " "
        << static_cast<int>(a1Value_3) << " "
        << static_cast<int>(a1Value_4) << std::endl;
    outputFile << "Reference time of week: "
        << static_cast<int>(totValue_1) << " "
        << static_cast<int>(totValue_2) << " "
        << static_cast<int>(totValue_3) << " "
        << static_cast<int>(totValue_4) << std::endl;
    outputFile << "Reference week number: "
        << static_cast<int>(wntValue_1) << " "
        << static_cast<int>(wntValue_2) << std::endl;
    outputFile << "Delta time due to leap seconds: " << static_cast<int>(dtlsValue) << std::endl;
    outputFile << "'Future' reference day number: " << static_cast<int>(dnValue) << std::endl;
    outputFile << "'Future' reference week number: "
        << static_cast<int>(wnlsfValue_1) << " "
        << static_cast<int>(wnlsfValue_2) << std::endl;
    outputFile << "'Future' reference delta time due to leap seconds: " << static_cast<int>(dtlsfValue) << std::endl;
    outputFile << "Constant term of time offset: "
        << static_cast<int>(a0gValue_1) << " "
        << static_cast<int>(a0gValue_2) << " "
        << static_cast<int>(a0gValue_3) << " "
        << static_cast<int>(a0gValue_4) << std::endl;
    outputFile << "Rate of time offset: "
        << static_cast<int>(a1gValue_1) << " "
        << static_cast<int>(a1gValue_2) << " "
        << static_cast<int>(a1gValue_3) << " "
        << static_cast<int>(a1gValue_4) << std::endl;
    outputFile << "Reference time of week: "
        << static_cast<int>(t0gValue_1) << " "
        << static_cast<int>(t0gValue_2) << " "
        << static_cast<int>(t0gValue_3) << " "
        << static_cast<int>(t0gValue_4) << std::endl;
    outputFile << "Reference week number: "
        << static_cast<int>(wn0gValue_1) << " "
        << static_cast<int>(wn0gValue_2) << std::endl;
    outputFile << "Flags of data availability: "
        << static_cast<int>(flagsValue_1) << " "
        << static_cast<int>(flagsValue_2) << std::endl;
    outputFile << "Checksum of the string: " << static_cast<int>(checkSum) << std::endl;
    outputFile << "Parameter u1 cs: " << static_cast<int>(csValue);

    outputFile.close();
}

int Var18::getCheckSum()
{
    return this->checkSum;
}

int Var18::getCsValue()
{
    return this->csValue;
}