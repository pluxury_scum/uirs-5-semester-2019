#include <iostream>
#include <fstream>
#include "../include/Var2.h"

int Var2::handleVar()
{
    std::ifstream inputFile;
    std::string inputFileName = "rks.dat";
    inputFile.open(inputFileName, std::ios::in | std::ios::binary);

    if (!inputFile.is_open())
    {
        std::cerr << "Error. File was not overwritten" << std::endl;
        exit(1);
    }
    else
    {
        int tempInt = 0; // variable for memory allocation
        std::string tempString; // char array holds all the characters of the file
        inputFile.read((char*)&tempInt, sizeof(tempInt)); // read string size from file
        tempString.resize(tempInt); // allocate memory of required size
        inputFile.read(&tempString[0], tempInt); // read string from file

        this->allValuesArr[0] = static_cast<u1>(tempString[0xC9BE8]);
        this->allValuesArr[1] = static_cast<u1>(tempString[0xC9BE9]);
        this->allValuesArr[2] = static_cast<u1>(tempString[0xC9BEA]);
        this->allValuesArr[3] = static_cast<u1>(tempString[0xC9BEB]);
        this->allValuesArr[4] = static_cast<u1>(tempString[0xC9BEC]);
        this->allValuesArr[5] = static_cast<u1>(tempString[0xC9BED]);
        this->allValuesArr[6] = static_cast<u1>(tempString[0xC9BEE]);
        this->allValuesArr[7] = static_cast<u1>(tempString[0xC9BEF]);
        this->allValuesArr[8] = static_cast<u1>(tempString[0xC9BF0]);
        this->allValuesArr[9] = static_cast<u1>(tempString[0xC9BF1]);

        this->todValue_1 = this->allValuesArr[5];
        this->todValue_2 = this->allValuesArr[6];
        this->todValue_3 = this->allValuesArr[7];
        this->todValue_4 = this->allValuesArr[8];
        this->csValue = this->allValuesArr[9];

        handleCS();
        printVar();

        inputFile.close();
        return 0;
    }
}

void Var2::handleCS()
{
    this->checkSum = cs(allValuesArr, 9);
}

void Var2::printVar()
{
    std::ofstream outputFile;
    std::string outputFileName = "Var2_output.txt";
    outputFile.open(outputFileName);

    outputFile << "Var 2" << std::endl;
    outputFile << "Structure EpochTime" << std::endl;
    outputFile << "Tr modulo 1 day: "
        << static_cast<int>(todValue_1) << " "
        << static_cast<int>(todValue_2) << " "
        << static_cast<int>(todValue_3) << " "
        << static_cast<int>(todValue_4) << std::endl;
    outputFile << "Checksum of the string: " << static_cast<int>(checkSum) << std::endl;
    outputFile << "Parameter u1 cs: " << static_cast<int>(csValue);

    outputFile.close();
}

int Var2::getCheckSum()
{
    return this->checkSum;
}

int Var2::getCsValue()
{
    return this->csValue;
}