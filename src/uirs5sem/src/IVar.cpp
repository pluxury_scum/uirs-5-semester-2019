#include "../include/IVar.h"

u1 IVar::cs(u1 const* src, int count)
{
    u1 res = 0;

    while (count--)
        res = ROT_LEFT(res) ^ *src++;

    return ROT_LEFT(res);
}