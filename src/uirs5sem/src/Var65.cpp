#include <iostream>
#include <fstream>
#include "../include/Var65.h"

int Var65::handleVar()
{
    std::ifstream inputFile;
    std::string inputFileName = "rks.dat";
    inputFile.open(inputFileName, std::ios::in | std::ios::binary);

    if (!inputFile.is_open())
    {
        std::cerr << "Error. File was not overwritten" << std::endl;
        exit(1);
    }
    else
    {
        int tempInt = 0; // variable for memory allocation
        std::string tempString; // char array holds all the characters of the file
        inputFile.read((char*)&tempInt, sizeof(tempInt)); // read string size from file
        tempString.resize(tempInt); // allocate memory of required size
        inputFile.read(&tempString[0], tempInt); // read string from file

        this->allValuesArr[0] = static_cast<u1>(tempString[0x34FF5]);
        this->allValuesArr[1] = static_cast<u1>(tempString[0x34FF6]);
        this->allValuesArr[2] = static_cast<u1>(tempString[0x34FF7]);
        this->allValuesArr[3] = static_cast<u1>(tempString[0x34FF8]);
        this->allValuesArr[4] = static_cast<u1>(tempString[0x34FF9]);
        this->allValuesArr[5] = static_cast<u1>(tempString[0x34FFA]);
        this->allValuesArr[6] = static_cast<u1>(tempString[0x34FFB]);
        this->allValuesArr[7] = static_cast<u1>(tempString[0x34FFC]);
        this->allValuesArr[8] = static_cast<u1>(tempString[0x34FFD]);
        this->allValuesArr[9] = static_cast<u1>(tempString[0x34FFE]);
        this->allValuesArr[10] = static_cast<u1>(tempString[0x34FFF]);

        this->dataValue_1 = this->allValuesArr[5];
        this->dataValue_2 = this->allValuesArr[6];
        this->dataValue_3 = this->allValuesArr[7];
        this->dataValue_4 = this->allValuesArr[8];
        this->dataValue_5 = this->allValuesArr[9];
        this->csValue = this->allValuesArr[10];

        handleCS();
        printVar();

        inputFile.close();
        return 0;
    }
}

void Var65::handleCS()
{
    this->checkSum = cs(allValuesArr, 10);
}

void Var65::printVar()
{
    std::ofstream outputFile;
    std::string outputFileName = "Var65_output.txt";
    outputFile.open(outputFileName);

    outputFile << "Var 65" << std::endl;
    outputFile << "Structure Security" << std::endl;
    outputFile << "Tracking time: "
        << static_cast<int>(dataValue_1) << " "
        << static_cast<int>(dataValue_2) << " "
        << static_cast<int>(dataValue_3) << " "
        << static_cast<int>(dataValue_4) << " "
        << static_cast<int>(dataValue_5) << std::endl;
    outputFile << "Checksum of the string: " << static_cast<int>(checkSum) << std::endl;
    outputFile << "Parameter u1 cs: " << static_cast<int>(csValue);

    outputFile.close();
}

int Var65::getCheckSum()
{
    return this->checkSum;
}

int Var65::getCsValue()
{
    return this->csValue;
}