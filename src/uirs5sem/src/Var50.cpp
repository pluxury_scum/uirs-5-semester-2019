#include <iostream>
#include <fstream>
#include "../include/Var50.h"

int Var50::handleVar()
{
    std::ifstream inputFile;
    std::string inputFileName = "rks.dat";
    inputFile.open(inputFileName, std::ios::in | std::ios::binary);

    if (!inputFile.is_open())
    {
        std::cerr << "Error. File was not overwritten" << std::endl;
        exit(1);
    }
    else
    {
        int tempInt = 0; // variable for memory allocation
        std::string tempString; // char array holds all the characters of the file
        inputFile.read((char*)&tempInt, sizeof(tempInt)); // read string size from file
        tempString.resize(tempInt); // allocate memory of required size
        inputFile.read(&tempString[0], tempInt); // read string from file

        this->allValuesArr[0] = static_cast<u1>(tempString[0x18CE]);
        this->allValuesArr[1] = static_cast<u1>(tempString[0x18CF]);
        this->allValuesArr[2] = static_cast<u1>(tempString[0x18D0]);
        this->allValuesArr[3] = static_cast<u1>(tempString[0x18D1]);
        this->allValuesArr[4] = static_cast<u1>(tempString[0x18D2]);
        this->allValuesArr[5] = static_cast<u1>(tempString[0x18D3]);
        this->allValuesArr[6] = static_cast<u1>(tempString[0x18D4]);
        this->allValuesArr[7] = static_cast<u1>(tempString[0x18D5]);
        this->allValuesArr[8] = static_cast<u1>(tempString[0x18D6]);
        this->allValuesArr[9] = static_cast<u1>(tempString[0x18D7]);
        this->allValuesArr[10] = static_cast<u1>(tempString[0x18D8]);
        this->allValuesArr[11] = static_cast<u1>(tempString[0x18D9]);
        this->allValuesArr[12] = static_cast<u1>(tempString[0x18DA]);
        this->allValuesArr[13] = static_cast<u1>(tempString[0x18DB]);
        this->allValuesArr[14] = static_cast<u1>(tempString[0x18DC]);
        this->allValuesArr[15] = static_cast<u1>(tempString[0x18DD]);
        this->allValuesArr[16] = static_cast<u1>(tempString[0x18DE]);
        this->allValuesArr[17] = static_cast<u1>(tempString[0x18DF]);
        this->allValuesArr[18] = static_cast<u1>(tempString[0x18E0]);
        this->allValuesArr[19] = static_cast<u1>(tempString[0x18E1]);
        this->allValuesArr[20] = static_cast<u1>(tempString[0x18E2]);
        this->allValuesArr[21] = static_cast<u1>(tempString[0x18E3]);
        this->allValuesArr[22] = static_cast<u1>(tempString[0x18E4]);
        this->allValuesArr[23] = static_cast<u1>(tempString[0x18E5]);
        this->allValuesArr[24] = static_cast<u1>(tempString[0x18E6]);
        this->allValuesArr[25] = static_cast<u1>(tempString[0x18E7]);
        this->allValuesArr[26] = static_cast<u1>(tempString[0x18E8]);
        this->allValuesArr[27] = static_cast<u1>(tempString[0x18E9]);
        this->allValuesArr[28] = static_cast<u1>(tempString[0x18EA]);
        this->allValuesArr[29] = static_cast<u1>(tempString[0x18EB]);
        this->allValuesArr[30] = static_cast<u1>(tempString[0x18EC]);
        this->allValuesArr[31] = static_cast<u1>(tempString[0x18ED]);
        this->allValuesArr[32] = static_cast<u1>(tempString[0x18EE]);
        this->allValuesArr[33] = static_cast<u1>(tempString[0x18EF]);
        this->allValuesArr[34] = static_cast<u1>(tempString[0x18F0]);
        this->allValuesArr[35] = static_cast<u1>(tempString[0x18F1]);
        this->allValuesArr[36] = static_cast<u1>(tempString[0x18F2]);
        this->allValuesArr[37] = static_cast<u1>(tempString[0x18F3]);
        this->allValuesArr[38] = static_cast<u1>(tempString[0x18F4]);
        this->allValuesArr[39] = static_cast<u1>(tempString[0x18F5]);
        this->allValuesArr[40] = static_cast<u1>(tempString[0x18F6]);
        this->allValuesArr[41] = static_cast<u1>(tempString[0x18F7]);
        this->allValuesArr[42] = static_cast<u1>(tempString[0x18F8]);
        this->allValuesArr[43] = static_cast<u1>(tempString[0x18F9]);
        this->allValuesArr[44] = static_cast<u1>(tempString[0x18FA]);
        this->allValuesArr[45] = static_cast<u1>(tempString[0x18FB]);
        this->allValuesArr[46] = static_cast<u1>(tempString[0x18FC]);
        this->allValuesArr[47] = static_cast<u1>(tempString[0x18FD]);
        this->allValuesArr[48] = static_cast<u1>(tempString[0x18FE]);
        this->allValuesArr[49] = static_cast<u1>(tempString[0x18FF]);
        this->allValuesArr[50] = static_cast<u1>(tempString[0x1900]);
        this->allValuesArr[51] = static_cast<u1>(tempString[0x1901]);
        this->allValuesArr[52] = static_cast<u1>(tempString[0x1902]);
        this->allValuesArr[53] = static_cast<u1>(tempString[0x1903]);
        this->allValuesArr[54] = static_cast<u1>(tempString[0x1904]);
        this->allValuesArr[55] = static_cast<u1>(tempString[0x1905]);
        this->allValuesArr[56] = static_cast<u1>(tempString[0x1906]);
        this->allValuesArr[57] = static_cast<u1>(tempString[0x1907]);
        this->allValuesArr[58] = static_cast<u1>(tempString[0x1908]);
        this->allValuesArr[59] = static_cast<u1>(tempString[0x1909]);
        this->allValuesArr[60] = static_cast<u1>(tempString[0x190A]);
        this->allValuesArr[61] = static_cast<u1>(tempString[0x190B]);
        this->allValuesArr[62] = static_cast<u1>(tempString[0x190C]);
        this->allValuesArr[63] = static_cast<u1>(tempString[0x190D]);
        this->allValuesArr[64] = static_cast<u1>(tempString[0x190E]);
        this->allValuesArr[65] = static_cast<u1>(tempString[0x190F]);
        this->allValuesArr[66] = static_cast<u1>(tempString[0x1910]);
        this->allValuesArr[67] = static_cast<u1>(tempString[0x1911]);
        this->allValuesArr[68] = static_cast<u1>(tempString[0x1912]);
        this->allValuesArr[69] = static_cast<u1>(tempString[0x1913]);
        this->allValuesArr[70] = static_cast<u1>(tempString[0x1914]);
        this->allValuesArr[71] = static_cast<u1>(tempString[0x1915]);
        this->allValuesArr[72] = static_cast<u1>(tempString[0x1916]);
        this->allValuesArr[73] = static_cast<u1>(tempString[0x1917]);
        this->allValuesArr[74] = static_cast<u1>(tempString[0x1918]);
        this->allValuesArr[75] = static_cast<u1>(tempString[0x1919]);
        this->allValuesArr[76] = static_cast<u1>(tempString[0x191A]);
        this->allValuesArr[77] = static_cast<u1>(tempString[0x191B]);

        this->timeValue_1 = this->allValuesArr[5];
        this->timeValue_2 = this->allValuesArr[6];
        this->timeValue_3 = this->allValuesArr[7];
        this->timeValue_4 = this->allValuesArr[8];
        this->q00Value_1 = this->allValuesArr[9];
        this->q00Value_2 = this->allValuesArr[10];
        this->q00Value_3 = this->allValuesArr[11];
        this->q00Value_4 = this->allValuesArr[12];
        this->q01Value_1 = this->allValuesArr[13];
        this->q01Value_2 = this->allValuesArr[14];
        this->q01Value_3 = this->allValuesArr[15];
        this->q01Value_4 = this->allValuesArr[16];
        this->q02Value_1 = this->allValuesArr[17];
        this->q02Value_2 = this->allValuesArr[18];
        this->q02Value_3 = this->allValuesArr[19];
        this->q02Value_4 = this->allValuesArr[20];
        this->q12Value_1 = this->allValuesArr[21];
        this->q12Value_2 = this->allValuesArr[22];
        this->q12Value_3 = this->allValuesArr[23];
        this->q12Value_4 = this->allValuesArr[24];
        this->rmsValue_1 = this->allValuesArr[25];
        this->rmsValue_2 = this->allValuesArr[26];
        this->rmsValue_3 = this->allValuesArr[27];
        this->rmsValue_4 = this->allValuesArr[28];
        this->rmsValue_5 = this->allValuesArr[29];
        this->rmsValue_6 = this->allValuesArr[30];
        this->rmsValue_7 = this->allValuesArr[31];
        this->rmsValue_8 = this->allValuesArr[32];
        this->rmsValue_9 = this->allValuesArr[33];
        this->rmsValue_10 = this->allValuesArr[34];
        this->rmsValue_11 = this->allValuesArr[35];
        this->rmsValue_12 = this->allValuesArr[36];
        this->solTypeValue_1 = this->allValuesArr[37];
        this->solTypeValue_2 = this->allValuesArr[38];
        this->solTypeValue_3 = this->allValuesArr[39];
        this->flagValue = this->allValuesArr[40];
        this->bl0Value_1 = this->allValuesArr[41];
        this->bl0Value_2 = this->allValuesArr[42];
        this->bl0Value_3 = this->allValuesArr[43];
        this->bl0Value_4 = this->allValuesArr[44];
        this->bl0Value_5 = this->allValuesArr[45];
        this->bl0Value_6 = this->allValuesArr[46];
        this->bl0Value_7 = this->allValuesArr[47];
        this->bl0Value_8 = this->allValuesArr[48];
        this->bl0Value_9 = this->allValuesArr[49];
        this->bl0Value_10 = this->allValuesArr[50];
        this->bl0Value_11 = this->allValuesArr[51];
        this->bl0Value_12 = this->allValuesArr[52];
        this->bl1Value_1 = this->allValuesArr[53];
        this->bl1Value_2 = this->allValuesArr[54];
        this->bl1Value_3 = this->allValuesArr[55];
        this->bl1Value_4 = this->allValuesArr[56];
        this->bl1Value_5 = this->allValuesArr[57];
        this->bl1Value_6 = this->allValuesArr[58];
        this->bl1Value_7 = this->allValuesArr[59];
        this->bl1Value_8 = this->allValuesArr[60];
        this->bl1Value_9 = this->allValuesArr[61];
        this->bl1Value_10 = this->allValuesArr[62];
        this->bl1Value_11 = this->allValuesArr[63];
        this->bl1Value_12 = this->allValuesArr[64];
        this->bl2Value_1 = this->allValuesArr[65];
        this->bl2Value_2 = this->allValuesArr[66];
        this->bl2Value_3 = this->allValuesArr[67];
        this->bl2Value_4 = this->allValuesArr[68];
        this->bl2Value_5 = this->allValuesArr[69];
        this->bl2Value_6 = this->allValuesArr[70];
        this->bl2Value_7 = this->allValuesArr[71];
        this->bl2Value_8 = this->allValuesArr[72];
        this->bl2Value_9 = this->allValuesArr[73];
        this->bl2Value_10 = this->allValuesArr[74];
        this->bl2Value_11 = this->allValuesArr[75];
        this->bl2Value_12 = this->allValuesArr[76];
        this->csValue = this->allValuesArr[77];

        handleCS();
        printVar();

        inputFile.close();
        return 0;
    }
}

void Var50::handleCS()
{
    this->checkSum = cs(allValuesArr, 77);
}

void Var50::printVar()
{
    std::ofstream outputFile;
    std::string outputFileName = "Var50_output.txt";
    outputFile.open(outputFileName);

    outputFile << "Var 50" << std::endl;
    outputFile << "Structure RotationMatrixAndVectors" << std::endl;
    outputFile << "Receiver time: "
        << static_cast<int>(timeValue_1) << " "
        << static_cast<int>(timeValue_2) << " "
        << static_cast<int>(timeValue_3) << " "
        << static_cast<int>(timeValue_4) << std::endl;
    outputFile << "Components of the rotation matrix Q:" << std::endl;
    outputFile << "q00: "
        << static_cast<int>(q00Value_1) << " "
        << static_cast<int>(q00Value_2) << " "
        << static_cast<int>(q00Value_3) << " "
        << static_cast<int>(q00Value_4) << std::endl;
    outputFile << "q01: "
        << static_cast<int>(q01Value_1) << " "
        << static_cast<int>(q01Value_2) << " "
        << static_cast<int>(q01Value_3) << " "
        << static_cast<int>(q01Value_4) << std::endl;
    outputFile << "q02: "
        << static_cast<int>(q02Value_1) << " "
        << static_cast<int>(q02Value_2) << " "
        << static_cast<int>(q02Value_3) << " "
        << static_cast<int>(q02Value_4) << std::endl;
    outputFile << "q12: "
        << static_cast<int>(q12Value_1) << " "
        << static_cast<int>(q12Value_2) << " "
        << static_cast<int>(q12Value_3) << " "
        << static_cast<int>(q12Value_4) << std::endl;
    outputFile << "Estimated accuracy for three baseline vectors: "
        << static_cast<int>(rmsValue_1) << " "
        << static_cast<int>(rmsValue_2) << " "
        << static_cast<int>(rmsValue_3) << " "
        << static_cast<int>(rmsValue_4) << " "
        << static_cast<int>(rmsValue_5) << " "
        << static_cast<int>(rmsValue_6) << " "
        << static_cast<int>(rmsValue_7) << " "
        << static_cast<int>(rmsValue_8) << " "
        << static_cast<int>(rmsValue_9) << " "
        << static_cast<int>(rmsValue_10) << " "
        << static_cast<int>(rmsValue_11) << " "
        << static_cast<int>(rmsValue_12) << std::endl;
    outputFile << "Solution type8 for three baseline vectors: "
        << static_cast<int>(solTypeValue_1) << " "
        << static_cast<int>(solTypeValue_2) << " "
        << static_cast<int>(solTypeValue_3) << std::endl;
    outputFile << "Flag: " << static_cast<int>(flagValue) << std::endl;
    outputFile << "Baseline vector M-S0 in the current epoch: "
        << static_cast<int>(bl0Value_1) << " "
        << static_cast<int>(bl0Value_2) << " "
        << static_cast<int>(bl0Value_3) << " "
        << static_cast<int>(bl0Value_4) << " "
        << static_cast<int>(bl0Value_5) << " "
        << static_cast<int>(bl0Value_6) << " "
        << static_cast<int>(bl0Value_7) << " "
        << static_cast<int>(bl0Value_8) << " "
        << static_cast<int>(bl0Value_9) << " "
        << static_cast<int>(bl0Value_10) << " "
        << static_cast<int>(bl0Value_11) << " "
        << static_cast<int>(bl0Value_12) << std::endl;
    outputFile << "Baseline vector M-S1 in the current epoch: "
        << static_cast<int>(bl1Value_1) << " "
        << static_cast<int>(bl1Value_2) << " "
        << static_cast<int>(bl1Value_3) << " "
        << static_cast<int>(bl1Value_4) << " "
        << static_cast<int>(bl1Value_5) << " "
        << static_cast<int>(bl1Value_6) << " "
        << static_cast<int>(bl1Value_7) << " "
        << static_cast<int>(bl1Value_8) << " "
        << static_cast<int>(bl1Value_9) << " "
        << static_cast<int>(bl1Value_10) << " "
        << static_cast<int>(bl1Value_11) << " "
        << static_cast<int>(bl1Value_12) << std::endl;
    outputFile << "Baseline vector M-S2 in the current epoch: "
        << static_cast<int>(bl2Value_1) << " "
        << static_cast<int>(bl2Value_2) << " "
        << static_cast<int>(bl2Value_3) << " "
        << static_cast<int>(bl2Value_4) << " "
        << static_cast<int>(bl2Value_5) << " "
        << static_cast<int>(bl2Value_6) << " "
        << static_cast<int>(bl2Value_7) << " "
        << static_cast<int>(bl2Value_8) << " "
        << static_cast<int>(bl2Value_9) << " "
        << static_cast<int>(bl2Value_10) << " "
        << static_cast<int>(bl2Value_11) << " "
        << static_cast<int>(bl2Value_12) << std::endl;
    outputFile << "Checksum of the string: " << static_cast<int>(checkSum) << std::endl;
    outputFile << "Parameter u1 cs: " << static_cast<int>(csValue);

    outputFile.close();
}

int Var50::getCheckSum()
{
    return this->checkSum;
}

int Var50::getCsValue()
{
    return this->csValue;
}