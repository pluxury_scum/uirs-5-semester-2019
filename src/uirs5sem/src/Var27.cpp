#include <iostream>
#include <fstream>
#include "../include/Var27.h"

int Var27::handleVar()
{
    std::ifstream inputFile;
    std::string inputFileName = "rks.dat";
    inputFile.open(inputFileName, std::ios::in | std::ios::binary);

    if (!inputFile.is_open())
    {
        std::cerr << "Error. File was not overwritten" << std::endl;
        exit(1);
    }
    else
    {
        int tempInt = 0; // variable for memory allocation
        std::string tempString; // char array holds all the characters of the file
        inputFile.read((char*)&tempInt, sizeof(tempInt)); // read string size from file
        tempString.resize(tempInt); // allocate memory of required size
        inputFile.read(&tempString[0], tempInt); // read string from file

        this->allValuesArr[0] = static_cast<u1>(tempString[0x8BB7B]);
        this->allValuesArr[1] = static_cast<u1>(tempString[0x8BB7C]);
        this->allValuesArr[2] = static_cast<u1>(tempString[0x8BB7D]);
        this->allValuesArr[3] = static_cast<u1>(tempString[0x8BB7E]);
        this->allValuesArr[4] = static_cast<u1>(tempString[0x8BB7F]);
        this->allValuesArr[5] = static_cast<u1>(tempString[0x8BB80]);
        this->allValuesArr[6] = static_cast<u1>(tempString[0x8BB81]);
        this->allValuesArr[7] = static_cast<u1>(tempString[0x8BB82]);
        this->allValuesArr[8] = static_cast<u1>(tempString[0x8BB83]);
        this->allValuesArr[9] = static_cast<u1>(tempString[0x8BB84]);
        this->allValuesArr[10] = static_cast<u1>(tempString[0x8BB85]);
        this->allValuesArr[11] = static_cast<u1>(tempString[0x8BB86]);
        this->allValuesArr[12] = static_cast<u1>(tempString[0x8BB87]);
        this->allValuesArr[13] = static_cast<u1>(tempString[0x8BB88]);
        this->allValuesArr[14] = static_cast<u1>(tempString[0x8BB89]);
        this->allValuesArr[15] = static_cast<u1>(tempString[0x8BB8A]);
        this->allValuesArr[16] = static_cast<u1>(tempString[0x8BB8B]);
        this->allValuesArr[17] = static_cast<u1>(tempString[0x8BB8C]);
        this->allValuesArr[18] = static_cast<u1>(tempString[0x8BB8D]);
        this->allValuesArr[19] = static_cast<u1>(tempString[0x8BB8E]);
        this->allValuesArr[20] = static_cast<u1>(tempString[0x8BB8F]);
        this->allValuesArr[21] = static_cast<u1>(tempString[0x8BB90]);
        this->allValuesArr[22] = static_cast<u1>(tempString[0x8BB91]);
        this->allValuesArr[23] = static_cast<u1>(tempString[0x8BB92]);
        this->allValuesArr[24] = static_cast<u1>(tempString[0x8BB93]);
        this->allValuesArr[25] = static_cast<u1>(tempString[0x8BB94]);
        this->allValuesArr[26] = static_cast<u1>(tempString[0x8BB95]);
        this->allValuesArr[27] = static_cast<u1>(tempString[0x8BB96]);
        this->allValuesArr[28] = static_cast<u1>(tempString[0x8BB97]);
        this->allValuesArr[29] = static_cast<u1>(tempString[0x8BB98]);
        this->allValuesArr[30] = static_cast<u1>(tempString[0x8BB99]);
        this->allValuesArr[31] = static_cast<u1>(tempString[0x8BB9A]);
        this->allValuesArr[32] = static_cast<u1>(tempString[0x8BB9B]);
        this->allValuesArr[33] = static_cast<u1>(tempString[0x8BB9C]);
        this->allValuesArr[34] = static_cast<u1>(tempString[0x8BB9D]);

        this->latValue_1 = this->allValuesArr[5];
        this->latValue_2 = this->allValuesArr[6];
        this->latValue_3 = this->allValuesArr[7];
        this->latValue_4 = this->allValuesArr[8];
        this->latValue_5 = this->allValuesArr[9];
        this->latValue_6 = this->allValuesArr[10];
        this->latValue_7 = this->allValuesArr[11];
        this->latValue_8 = this->allValuesArr[12];
        this->lonValue_1 = this->allValuesArr[13];
        this->lonValue_2 = this->allValuesArr[14];
        this->lonValue_3 = this->allValuesArr[15];
        this->lonValue_4 = this->allValuesArr[16];
        this->lonValue_5 = this->allValuesArr[17];
        this->lonValue_6 = this->allValuesArr[18];
        this->lonValue_7 = this->allValuesArr[19];
        this->lonValue_8 = this->allValuesArr[20];
        this->altValue_1 = this->allValuesArr[21];
        this->altValue_2 = this->allValuesArr[22];
        this->altValue_3 = this->allValuesArr[23];
        this->altValue_4 = this->allValuesArr[24];
        this->altValue_5 = this->allValuesArr[25];
        this->altValue_6 = this->allValuesArr[26];
        this->altValue_7 = this->allValuesArr[27];
        this->altValue_8 = this->allValuesArr[28];
        this->pSigmaValue_1 = this->allValuesArr[29];
        this->pSigmaValue_2 = this->allValuesArr[30];
        this->pSigmaValue_3 = this->allValuesArr[31];
        this->pSigmaValue_4 = this->allValuesArr[32];
        this->solTypeValue = this->allValuesArr[33];
        this->csValue = this->allValuesArr[34];

        handleCS();
        printVar();

        inputFile.close();
        return 0;
    }
}

void Var27::handleCS()
{
    this->checkSum = cs(allValuesArr, 34);
}

void Var27::printVar()
{
    std::ofstream outputFile;
    std::string outputFileName = "Var27_output.txt";
    outputFile.open(outputFileName);

    outputFile << "Var 27" << std::endl;
    outputFile << "Structure GeoPos" << std::endl;
    outputFile << "Latitude: "
        << static_cast<int>(latValue_1) << " "
        << static_cast<int>(latValue_2) << " "
        << static_cast<int>(latValue_3) << " "
        << static_cast<int>(latValue_4) << " "
        << static_cast<int>(latValue_5) << " "
        << static_cast<int>(latValue_6) << " "
        << static_cast<int>(latValue_7) << " "
        << static_cast<int>(latValue_8) << std::endl;
    outputFile << "Longitude: "
        << static_cast<int>(lonValue_1) << " "
        << static_cast<int>(lonValue_2) << " "
        << static_cast<int>(lonValue_3) << " "
        << static_cast<int>(lonValue_4) << " "
        << static_cast<int>(lonValue_5) << " "
        << static_cast<int>(lonValue_6) << " "
        << static_cast<int>(lonValue_7) << " "
        << static_cast<int>(lonValue_8) << std::endl;
    outputFile << "Ellipsoidal height: "
        << static_cast<int>(altValue_1) << " "
        << static_cast<int>(altValue_2) << " "
        << static_cast<int>(altValue_3) << " "
        << static_cast<int>(altValue_4) << " "
        << static_cast<int>(altValue_5) << " "
        << static_cast<int>(altValue_6) << " "
        << static_cast<int>(altValue_7) << " "
        << static_cast<int>(altValue_8) << std::endl;
    outputFile << "Position SEP: "
        << static_cast<int>(pSigmaValue_1) << " "
        << static_cast<int>(pSigmaValue_2) << " "
        << static_cast<int>(pSigmaValue_3) << " "
        << static_cast<int>(pSigmaValue_4) << std::endl;
    outputFile << "Solution type: " << static_cast<int>(solTypeValue) << std::endl;
    outputFile << "Checksum of the string: " << static_cast<int>(checkSum) << std::endl;
    outputFile << "Parameter u1 cs: " << static_cast<int>(csValue);

    outputFile.close();
}

int Var27::getCheckSum()
{
    return this->checkSum;
}

int Var27::getCsValue()
{
    return this->csValue;
}