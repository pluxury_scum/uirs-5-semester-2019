#include <iostream>
#include <fstream>
#include "../include/Var45.h"

int Var45::handleVar()
{
    std::ifstream inputFile;
    std::string inputFileName = "rks.dat";
    inputFile.open(inputFileName, std::ios::in | std::ios::binary);

    if (!inputFile.is_open())
    {
        std::cerr << "Error. File was not overwritten" << std::endl;
        exit(1);
    }
    else
    {
        int tempInt = 0; // variable for memory allocation
        std::string tempString; // char array holds all the characters of the file
        inputFile.read((char*)&tempInt, sizeof(tempInt)); // read string size from file
        tempString.resize(tempInt); // allocate memory of required size
        inputFile.read(&tempString[0], tempInt); // read string from file

        this->allValuesArr[0] = static_cast<u1>(tempString[0x1B90]);
        this->allValuesArr[1] = static_cast<u1>(tempString[0x1B91]);
        this->allValuesArr[2] = static_cast<u1>(tempString[0x1B92]);
        this->allValuesArr[3] = static_cast<u1>(tempString[0x1B93]);
        this->allValuesArr[4] = static_cast<u1>(tempString[0x1B94]);
        this->allValuesArr[5] = static_cast<u1>(tempString[0x1B95]);
        this->allValuesArr[6] = static_cast<u1>(tempString[0x1B96]);
        this->allValuesArr[7] = static_cast<u1>(tempString[0x1B97]);
        this->allValuesArr[8] = static_cast<u1>(tempString[0x1B98]);
        this->allValuesArr[9] = static_cast<u1>(tempString[0x1B99]);
        this->allValuesArr[10] = static_cast<u1>(tempString[0x1B9A]);
        this->allValuesArr[11] = static_cast<u1>(tempString[0x1B9B]);
        this->allValuesArr[12] = static_cast<u1>(tempString[0x1B9C]);
        this->allValuesArr[13] = static_cast<u1>(tempString[0x1B9D]);
        this->allValuesArr[14] = static_cast<u1>(tempString[0x1B9E]);
        this->allValuesArr[15] = static_cast<u1>(tempString[0x1B9F]);
        this->allValuesArr[16] = static_cast<u1>(tempString[0x1BA0]);
        this->allValuesArr[17] = static_cast<u1>(tempString[0x1BA1]);
        this->allValuesArr[18] = static_cast<u1>(tempString[0x1BA2]);
        this->allValuesArr[19] = static_cast<u1>(tempString[0x1BA3]);
        this->allValuesArr[20] = static_cast<u1>(tempString[0x1BA4]);
        this->allValuesArr[21] = static_cast<u1>(tempString[0x1BA5]);
        this->allValuesArr[22] = static_cast<u1>(tempString[0x1BA6]);
        this->allValuesArr[23] = static_cast<u1>(tempString[0x1BA7]);
        this->allValuesArr[24] = static_cast<u1>(tempString[0x1BA8]);
        this->allValuesArr[25] = static_cast<u1>(tempString[0x1BA9]);
        this->allValuesArr[26] = static_cast<u1>(tempString[0x1BAA]);
        this->allValuesArr[27] = static_cast<u1>(tempString[0x1BAB]);
        this->allValuesArr[28] = static_cast<u1>(tempString[0x1BAC]);
        this->allValuesArr[29] = static_cast<u1>(tempString[0x1BAD]);
        this->allValuesArr[30] = static_cast<u1>(tempString[0x1BAE]);
        this->allValuesArr[31] = static_cast<u1>(tempString[0x1BAF]);
        this->allValuesArr[32] = static_cast<u1>(tempString[0x1BB0]);
        this->allValuesArr[33] = static_cast<u1>(tempString[0x1BB1]);
        this->allValuesArr[34] = static_cast<u1>(tempString[0x1BB2]);
        this->allValuesArr[35] = static_cast<u1>(tempString[0x1BB3]);
        this->allValuesArr[36] = static_cast<u1>(tempString[0x1BB4]);
        this->allValuesArr[37] = static_cast<u1>(tempString[0x1BB5]);
        this->allValuesArr[38] = static_cast<u1>(tempString[0x1BB6]);
        this->allValuesArr[39] = static_cast<u1>(tempString[0x1BB7]);
        this->allValuesArr[40] = static_cast<u1>(tempString[0x1BB8]);
        this->allValuesArr[41] = static_cast<u1>(tempString[0x1BB9]);
        this->allValuesArr[42] = static_cast<u1>(tempString[0x1BBA]);
        this->allValuesArr[43] = static_cast<u1>(tempString[0x1BBB]);
        this->allValuesArr[44] = static_cast<u1>(tempString[0x1BBC]);
        this->allValuesArr[45] = static_cast<u1>(tempString[0x1BBD]);
        this->allValuesArr[46] = static_cast<u1>(tempString[0x1BBE]);
        this->allValuesArr[47] = static_cast<u1>(tempString[0x1BBF]);
        this->allValuesArr[48] = static_cast<u1>(tempString[0x1BC0]);
        this->allValuesArr[49] = static_cast<u1>(tempString[0x1BC1]);
        this->allValuesArr[50] = static_cast<u1>(tempString[0x1BC2]);
        this->allValuesArr[51] = static_cast<u1>(tempString[0x1BC3]);
        this->allValuesArr[52] = static_cast<u1>(tempString[0x1BC4]);
        this->allValuesArr[53] = static_cast<u1>(tempString[0x1BC5]);
        this->allValuesArr[54] = static_cast<u1>(tempString[0x1BC6]);
        this->allValuesArr[55] = static_cast<u1>(tempString[0x1BC7]);

        this->waasPrnValue = this->allValuesArr[5];
        this->gpsPrnValue = this->allValuesArr[6];
        this->idValue = this->allValuesArr[7];
        this->healthSValue = this->allValuesArr[8];
        this->todValue_1 = this->allValuesArr[9];
        this->todValue_2 = this->allValuesArr[10];
        this->todValue_3 = this->allValuesArr[11];
        this->todValue_4 = this->allValuesArr[12];
        this->xgValue_1 = this->allValuesArr[13];
        this->xgValue_2 = this->allValuesArr[14];
        this->xgValue_3 = this->allValuesArr[15];
        this->xgValue_4 = this->allValuesArr[16];
        this->xgValue_5 = this->allValuesArr[17];
        this->xgValue_6 = this->allValuesArr[18];
        this->xgValue_7 = this->allValuesArr[19];
        this->xgValue_8 = this->allValuesArr[20];
        this->ygValue_1 = this->allValuesArr[21];
        this->ygValue_2 = this->allValuesArr[22];
        this->ygValue_3 = this->allValuesArr[23];
        this->ygValue_4 = this->allValuesArr[24];
        this->ygValue_5 = this->allValuesArr[25];
        this->ygValue_6 = this->allValuesArr[26];
        this->ygValue_7 = this->allValuesArr[27];
        this->ygValue_8 = this->allValuesArr[28];
        this->zgValue_1 = this->allValuesArr[29];
        this->zgValue_2 = this->allValuesArr[30];
        this->zgValue_3 = this->allValuesArr[31];
        this->zgValue_4 = this->allValuesArr[32];
        this->zgValue_5 = this->allValuesArr[33];
        this->zgValue_6 = this->allValuesArr[34];
        this->zgValue_7 = this->allValuesArr[35];
        this->zgValue_8 = this->allValuesArr[36];
        this->vxgValue_1 = this->allValuesArr[37];
        this->vxgValue_2 = this->allValuesArr[38];
        this->vxgValue_3 = this->allValuesArr[39];
        this->vxgValue_4 = this->allValuesArr[40];
        this->vygValue_1 = this->allValuesArr[41];
        this->vygValue_2 = this->allValuesArr[42];
        this->vygValue_3 = this->allValuesArr[43];
        this->vygValue_4 = this->allValuesArr[44];
        this->vzgValue_1 = this->allValuesArr[45];
        this->vzgValue_2 = this->allValuesArr[46];
        this->vzgValue_3 = this->allValuesArr[47];
        this->vzgValue_4 = this->allValuesArr[48];
        this->towValue_1 = this->allValuesArr[49];
        this->towValue_2 = this->allValuesArr[50];
        this->towValue_3 = this->allValuesArr[51];
        this->towValue_4 = this->allValuesArr[52];
        this->wnValue_1 = this->allValuesArr[53];
        this->wnValue_2 = this->allValuesArr[54];
        this->csValue = this->allValuesArr[55];

        handleCS();
        printVar();

        inputFile.close();
        return 0;
    }
}

void Var45::handleCS()
{
    this->checkSum = cs(allValuesArr, 55);
}

void Var45::printVar()
{
    std::ofstream outputFile;
    std::string outputFileName = "Var45_output.txt";
    outputFile.open(outputFileName);

    outputFile << "Var 45" << std::endl;
    outputFile << "Structure SBASAlmanac" << std::endl;
    outputFile << "SBAS SV PRN number: " << static_cast<int>(waasPrnValue) << std::endl;
    outputFile << "GPS SV PRN associated with SBAS SV: " << static_cast<int>(gpsPrnValue) << std::endl;
    outputFile << "Data ID: " << static_cast<int>(idValue) << std::endl;
    outputFile << "Satellite health: " << static_cast<int>(healthSValue) << std::endl;
    outputFile << "Time of the day: "
        << static_cast<int>(todValue_1) << " "
        << static_cast<int>(todValue_2) << " "
        << static_cast<int>(todValue_3) << " "
        << static_cast<int>(todValue_4) << std::endl;
    outputFile << "ECEF coordinates:" << std::endl;
    outputFile << "xg: "
        << static_cast<int>(xgValue_1) << " "
        << static_cast<int>(xgValue_2) << " "
        << static_cast<int>(xgValue_3) << " "
        << static_cast<int>(xgValue_4) << " "
        << static_cast<int>(xgValue_5) << " "
        << static_cast<int>(xgValue_6) << " "
        << static_cast<int>(xgValue_7) << " "
        << static_cast<int>(xgValue_8) << std::endl;
    outputFile << "yg: "
        << static_cast<int>(ygValue_1) << " "
        << static_cast<int>(ygValue_2) << " "
        << static_cast<int>(ygValue_3) << " "
        << static_cast<int>(ygValue_4) << " "
        << static_cast<int>(ygValue_5) << " "
        << static_cast<int>(ygValue_6) << " "
        << static_cast<int>(ygValue_7) << " "
        << static_cast<int>(ygValue_8) << std::endl;
    outputFile << "zg: "
        << static_cast<int>(zgValue_1) << " "
        << static_cast<int>(zgValue_2) << " "
        << static_cast<int>(zgValue_3) << " "
        << static_cast<int>(zgValue_4) << " "
        << static_cast<int>(zgValue_5) << " "
        << static_cast<int>(zgValue_6) << " "
        << static_cast<int>(zgValue_7) << " "
        << static_cast<int>(zgValue_8) << std::endl;
    outputFile << "ECEF velocity:" << std::endl;
    outputFile << "vxg: "
        << static_cast<int>(vxgValue_1) << " "
        << static_cast<int>(vxgValue_2) << " "
        << static_cast<int>(vxgValue_3) << " "
        << static_cast<int>(vxgValue_4) << std::endl;
    outputFile << "vyg: "
        << static_cast<int>(vygValue_1) << " "
        << static_cast<int>(vygValue_2) << " "
        << static_cast<int>(vygValue_3) << " "
        << static_cast<int>(vygValue_4) << std::endl;
    outputFile << "vzg: "
        << static_cast<int>(vzgValue_1) << " "
        << static_cast<int>(vzgValue_2) << " "
        << static_cast<int>(vzgValue_3) << " "
        << static_cast<int>(vzgValue_4) << std::endl;
    outputFile << "Time of GPS week almanac was received at: "
        << static_cast<int>(towValue_1) << " "
        << static_cast<int>(towValue_2) << " "
        << static_cast<int>(towValue_3) << " "
        << static_cast<int>(towValue_4) << std::endl;
    outputFile << "Rate of coarse satellite clock correction to GLONASS time scale: "
        << static_cast<int>(wnValue_1) << " "
        << static_cast<int>(wnValue_2) << std::endl;
    outputFile << "Checksum of the string: " << static_cast<int>(checkSum) << std::endl;
    outputFile << "Parameter u1 cs: " << static_cast<int>(csValue);

    outputFile.close();
}

int Var45::getCheckSum()
{
    return this->checkSum;
}

int Var45::getCsValue()
{
    return this->csValue;
}