#include <iostream>
#include <fstream>
#include "../include/Var25.h"

int Var25::handleVar()
{
    std::ifstream inputFile;
    std::string inputFileName = "rks.dat";
    inputFile.open(inputFileName, std::ios::in | std::ios::binary);

    if (!inputFile.is_open())
    {
        std::cerr << "Error. File was not overwritten" << std::endl;
        exit(1);
    }
    else
    {
        int tempInt = 0; // variable for memory allocation
        std::string tempString; // char array holds all the characters of the file
        inputFile.read((char*)&tempInt, sizeof(tempInt)); // read string size from file
        tempString.resize(tempInt); // allocate memory of required size
        inputFile.read(&tempString[0], tempInt); // read string from file

        this->allValuesArr[0] = static_cast<u1>(tempString[0x12493]);
        this->allValuesArr[1] = static_cast<u1>(tempString[0x12494]);
        this->allValuesArr[2] = static_cast<u1>(tempString[0x12495]);
        this->allValuesArr[3] = static_cast<u1>(tempString[0x12496]);
        this->allValuesArr[4] = static_cast<u1>(tempString[0x12497]);
        this->allValuesArr[5] = static_cast<u1>(tempString[0x12498]);
        this->allValuesArr[6] = static_cast<u1>(tempString[0x12499]);
        this->allValuesArr[7] = static_cast<u1>(tempString[0x1249A]);
        this->allValuesArr[8] = static_cast<u1>(tempString[0x1249B]);
        this->allValuesArr[9] = static_cast<u1>(tempString[0x1249C]);
        this->allValuesArr[10] = static_cast<u1>(tempString[0x1249D]);
        this->allValuesArr[11] = static_cast<u1>(tempString[0x1249E]);
        this->allValuesArr[12] = static_cast<u1>(tempString[0x1249F]);
        this->allValuesArr[13] = static_cast<u1>(tempString[0x124A0]);
        this->allValuesArr[14] = static_cast<u1>(tempString[0x124A1]);
        this->allValuesArr[15] = static_cast<u1>(tempString[0x124A2]);
        this->allValuesArr[16] = static_cast<u1>(tempString[0x124A3]);
        this->allValuesArr[17] = static_cast<u1>(tempString[0x124A4]);
        this->allValuesArr[18] = static_cast<u1>(tempString[0x124A5]);
        this->allValuesArr[19] = static_cast<u1>(tempString[0x124A6]);
        this->allValuesArr[20] = static_cast<u1>(tempString[0x124A7]);
        this->allValuesArr[21] = static_cast<u1>(tempString[0x124A8]);
        this->allValuesArr[22] = static_cast<u1>(tempString[0x124A9]);
        this->allValuesArr[23] = static_cast<u1>(tempString[0x124AA]);
        this->allValuesArr[24] = static_cast<u1>(tempString[0x124AB]);
        this->allValuesArr[25] = static_cast<u1>(tempString[0x124AC]);
        this->allValuesArr[26] = static_cast<u1>(tempString[0x124AD]);
        this->allValuesArr[27] = static_cast<u1>(tempString[0x124AE]);
        this->allValuesArr[28] = static_cast<u1>(tempString[0x124AF]);
        this->allValuesArr[29] = static_cast<u1>(tempString[0x124B0]);
        this->allValuesArr[30] = static_cast<u1>(tempString[0x124B1]);
        this->allValuesArr[31] = static_cast<u1>(tempString[0x124B2]);
        this->allValuesArr[32] = static_cast<u1>(tempString[0x124B3]);
        this->allValuesArr[33] = static_cast<u1>(tempString[0x124B4]);
        this->allValuesArr[34] = static_cast<u1>(tempString[0x124B5]);
        this->allValuesArr[35] = static_cast<u1>(tempString[0x124B6]);
        this->allValuesArr[36] = static_cast<u1>(tempString[0x124B7]);
        this->allValuesArr[37] = static_cast<u1>(tempString[0x124B8]);
        this->allValuesArr[38] = static_cast<u1>(tempString[0x124B9]);
        this->allValuesArr[39] = static_cast<u1>(tempString[0x124BA]);
        this->allValuesArr[40] = static_cast<u1>(tempString[0x124BB]);
        this->allValuesArr[41] = static_cast<u1>(tempString[0x124BC]);
        this->allValuesArr[42] = static_cast<u1>(tempString[0x124BD]);

        this->xValue_1 = this->allValuesArr[5];
        this->xValue_2 = this->allValuesArr[6];
        this->xValue_3 = this->allValuesArr[7];
        this->xValue_4 = this->allValuesArr[8];
        this->xValue_5 = this->allValuesArr[9];
        this->xValue_6 = this->allValuesArr[10];
        this->xValue_7 = this->allValuesArr[11];
        this->xValue_8 = this->allValuesArr[12];
        this->yValue_1 = this->allValuesArr[13];
        this->yValue_2 = this->allValuesArr[14];
        this->yValue_3 = this->allValuesArr[15];
        this->yValue_4 = this->allValuesArr[16];
        this->yValue_5 = this->allValuesArr[17];
        this->yValue_6 = this->allValuesArr[18];
        this->yValue_7 = this->allValuesArr[19];
        this->yValue_8 = this->allValuesArr[20];
        this->zValue_1 = this->allValuesArr[21];
        this->zValue_2 = this->allValuesArr[22];
        this->zValue_3 = this->allValuesArr[23];
        this->zValue_4 = this->allValuesArr[24];
        this->zValue_5 = this->allValuesArr[25];
        this->zValue_6 = this->allValuesArr[26];
        this->zValue_7 = this->allValuesArr[27];
        this->zValue_8 = this->allValuesArr[28];
        this->pSigmaValue_1 = this->allValuesArr[29];
        this->pSigmaValue_2 = this->allValuesArr[30];
        this->pSigmaValue_3 = this->allValuesArr[31];
        this->pSigmaValue_4 = this->allValuesArr[32];
        this->solTypeValue = this->allValuesArr[33];
        this->systemValue = this->allValuesArr[34];
        this->crsCodeValue_1 = this->allValuesArr[35];
        this->crsCodeValue_2 = this->allValuesArr[36];
        this->crsCodeValue_3 = this->allValuesArr[37];
        this->crsCodeValue_4 = this->allValuesArr[38];
        this->crsCodeValue_5 = this->allValuesArr[39];
        this->chIssueValue_1 = this->allValuesArr[40];
        this->chIssueValue_2 = this->allValuesArr[41];
        this->csValue = this->allValuesArr[42];

        handleCS();
        printVar();

        inputFile.close();
        return 0;
    }
}

void Var25::handleCS()
{
    this->checkSum = cs(allValuesArr, 42);
}

void Var25::printVar()
{
    std::ofstream outputFile;
    std::string outputFileName = "Var25_output.txt";
    outputFile.open(outputFileName);

    outputFile << "Var 25" << std::endl;
    outputFile << "Structure SpecificCrtPos" << std::endl;
    outputFile << "Cartesian coordinates:" << std::endl;
    outputFile << "x: "
        << static_cast<int>(xValue_1) << " "
        << static_cast<int>(xValue_2) << " "
        << static_cast<int>(xValue_3) << " "
        << static_cast<int>(xValue_4) << " "
        << static_cast<int>(xValue_5) << " "
        << static_cast<int>(xValue_6) << " "
        << static_cast<int>(xValue_7) << " "
        << static_cast<int>(xValue_8) << std::endl;
    outputFile << "y: "
        << static_cast<int>(yValue_1) << " "
        << static_cast<int>(yValue_2) << " "
        << static_cast<int>(yValue_3) << " "
        << static_cast<int>(yValue_4) << " "
        << static_cast<int>(yValue_5) << " "
        << static_cast<int>(yValue_6) << " "
        << static_cast<int>(yValue_7) << " "
        << static_cast<int>(yValue_8) << std::endl;
    outputFile << "z: "
        << static_cast<int>(zValue_1) << " "
        << static_cast<int>(zValue_2) << " "
        << static_cast<int>(zValue_3) << " "
        << static_cast<int>(zValue_4) << " "
        << static_cast<int>(zValue_5) << " "
        << static_cast<int>(zValue_6) << " "
        << static_cast<int>(zValue_7) << " "
        << static_cast<int>(zValue_8) << std::endl;
    outputFile << "Position SEP6: "
        << static_cast<int>(pSigmaValue_1) << " "
        << static_cast<int>(pSigmaValue_2) << " "
        << static_cast<int>(pSigmaValue_3) << " "
        << static_cast<int>(pSigmaValue_4) << std::endl;
    outputFile << "Solution type : " << static_cast<int>(solTypeValue) << std::endl;
    outputFile << "Source of position: " << static_cast<int>(systemValue) << std::endl;
    outputFile << "Name of the coordinate reference system: "
        << static_cast<int>(crsCodeValue_1) << " "
        << static_cast<int>(crsCodeValue_2) << " "
        << static_cast<int>(crsCodeValue_3) << " "
        << static_cast<int>(crsCodeValue_4) << " "
        << static_cast<int>(crsCodeValue_5) << std::endl;
    outputFile << "Counter incrementing on every potential change of user grid system: "
        << static_cast<int>(chIssueValue_1) << " "
        << static_cast<int>(chIssueValue_2) << " " << std::endl;
    outputFile << "Checksum of the string: " << static_cast<int>(checkSum) << std::endl;
    outputFile << "Parameter u1 cs: " << static_cast<int>(csValue);

    outputFile.close();
}

int Var25::getCheckSum()
{
    return this->checkSum;
}

int Var25::getCsValue()
{
    return this->csValue;
}