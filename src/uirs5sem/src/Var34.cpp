#include <iostream>
#include <fstream>
#include "../include/Var34.h"

int Var34::handleVar()
{
    std::ifstream inputFile;
    std::string inputFileName = "rks.dat";
    inputFile.open(inputFileName, std::ios::in | std::ios::binary);

    if (!inputFile.is_open())
    {
        std::cerr << "Error. File was not overwritten" << std::endl;
        exit(1);
    }
    else
    {
        int tempInt = 0; // variable for memory allocation
        std::string tempString; // char array holds all the characters of the file
        inputFile.read((char*)&tempInt, sizeof(tempInt)); // read string size from file
        tempString.resize(tempInt); // allocate memory of required size
        inputFile.read(&tempString[0], tempInt); // read string from file

        this->allValuesArr[0] = static_cast<u1>(tempString[0x70C1F]);
        this->allValuesArr[1] = static_cast<u1>(tempString[0x70C20]);
        this->allValuesArr[2] = static_cast<u1>(tempString[0x70C21]);
        this->allValuesArr[3] = static_cast<u1>(tempString[0x70C22]);
        this->allValuesArr[4] = static_cast<u1>(tempString[0x70C23]);
        this->allValuesArr[5] = static_cast<u1>(tempString[0x70C24]);
        this->allValuesArr[6] = static_cast<u1>(tempString[0x70C25]);
        this->allValuesArr[7] = static_cast<u1>(tempString[0x70C26]);
        this->allValuesArr[8] = static_cast<u1>(tempString[0x70C27]);
        this->allValuesArr[9] = static_cast<u1>(tempString[0x70C28]);
        this->allValuesArr[10] = static_cast<u1>(tempString[0x70C29]);
        this->allValuesArr[11] = static_cast<u1>(tempString[0x70C2A]);
        this->allValuesArr[12] = static_cast<u1>(tempString[0x70C2B]);
        this->allValuesArr[13] = static_cast<u1>(tempString[0x70C2C]);
        this->allValuesArr[14] = static_cast<u1>(tempString[0x70C2D]);
        this->allValuesArr[15] = static_cast<u1>(tempString[0x70C2E]);
        this->allValuesArr[16] = static_cast<u1>(tempString[0x70C2F]);
        this->allValuesArr[17] = static_cast<u1>(tempString[0x70C30]);
        this->allValuesArr[18] = static_cast<u1>(tempString[0x70C31]);
        this->allValuesArr[19] = static_cast<u1>(tempString[0x70C32]);
        this->allValuesArr[20] = static_cast<u1>(tempString[0x70C33]);
        this->allValuesArr[21] = static_cast<u1>(tempString[0x70C34]);
        this->allValuesArr[22] = static_cast<u1>(tempString[0x70C35]);
        this->allValuesArr[23] = static_cast<u1>(tempString[0x70C36]);
        this->allValuesArr[24] = static_cast<u1>(tempString[0x70C37]);
        this->allValuesArr[25] = static_cast<u1>(tempString[0x70C38]);
        this->allValuesArr[26] = static_cast<u1>(tempString[0x70C39]);
        this->allValuesArr[27] = static_cast<u1>(tempString[0x70C3A]);
        this->allValuesArr[28] = static_cast<u1>(tempString[0x70C3B]);
        this->allValuesArr[29] = static_cast<u1>(tempString[0x70C3C]);
        this->allValuesArr[30] = static_cast<u1>(tempString[0x70C3D]);
        this->allValuesArr[31] = static_cast<u1>(tempString[0x70C3E]);
        this->allValuesArr[32] = static_cast<u1>(tempString[0x70C3F]);
        this->allValuesArr[33] = static_cast<u1>(tempString[0x70C40]);
        this->allValuesArr[34] = static_cast<u1>(tempString[0x70C41]);
        this->allValuesArr[35] = static_cast<u1>(tempString[0x70C42]);
        this->allValuesArr[36] = static_cast<u1>(tempString[0x70C43]);
        this->allValuesArr[37] = static_cast<u1>(tempString[0x70C44]);
        this->allValuesArr[38] = static_cast<u1>(tempString[0x70C45]);
        this->allValuesArr[39] = static_cast<u1>(tempString[0x70C46]);
        this->allValuesArr[40] = static_cast<u1>(tempString[0x70C47]);
        this->allValuesArr[41] = static_cast<u1>(tempString[0x70C48]);
        this->allValuesArr[42] = static_cast<u1>(tempString[0x70C49]);
        this->allValuesArr[43] = static_cast<u1>(tempString[0x70C4A]);
        this->allValuesArr[44] = static_cast<u1>(tempString[0x70C4B]);
        this->allValuesArr[45] = static_cast<u1>(tempString[0x70C4C]);
        this->allValuesArr[46] = static_cast<u1>(tempString[0x70C4D]);

        this->xxValue_1 = this->allValuesArr[5];
        this->xxValue_2 = this->allValuesArr[6];
        this->xxValue_3 = this->allValuesArr[7];
        this->xxValue_4 = this->allValuesArr[8];
        this->yyValue_1 = this->allValuesArr[9];
        this->yyValue_2 = this->allValuesArr[10];
        this->yyValue_3 = this->allValuesArr[11];
        this->yyValue_4 = this->allValuesArr[12];
        this->zzValue_1 = this->allValuesArr[13];
        this->zzValue_2 = this->allValuesArr[14];
        this->zzValue_3 = this->allValuesArr[15];
        this->zzValue_4 = this->allValuesArr[16];
        this->ttValue_1 = this->allValuesArr[17];
        this->ttValue_2 = this->allValuesArr[18];
        this->ttValue_3 = this->allValuesArr[19];
        this->ttValue_4 = this->allValuesArr[20];
        this->xyValue_1 = this->allValuesArr[21];
        this->xyValue_2 = this->allValuesArr[22];
        this->xyValue_3 = this->allValuesArr[23];
        this->xyValue_4 = this->allValuesArr[24];
        this->xzValue_1 = this->allValuesArr[25];
        this->xzValue_2 = this->allValuesArr[26];
        this->xzValue_3 = this->allValuesArr[27];
        this->xzValue_4 = this->allValuesArr[28];
        this->xtValue_1 = this->allValuesArr[29];
        this->xtValue_2 = this->allValuesArr[30];
        this->xtValue_3 = this->allValuesArr[31];
        this->xtValue_4 = this->allValuesArr[32];
        this->yzValue_1 = this->allValuesArr[33];
        this->yzValue_2 = this->allValuesArr[34];
        this->yzValue_3 = this->allValuesArr[35];
        this->yzValue_4 = this->allValuesArr[36];
        this->ytValue_1 = this->allValuesArr[37];
        this->ytValue_2 = this->allValuesArr[38];
        this->ytValue_3 = this->allValuesArr[39];
        this->ytValue_4 = this->allValuesArr[40];
        this->ztValue_1 = this->allValuesArr[41];
        this->ztValue_2 = this->allValuesArr[42];
        this->ztValue_3 = this->allValuesArr[43];
        this->ztValue_4 = this->allValuesArr[44];
        this->solTypeValue = this->allValuesArr[45];
        this->csValue = this->allValuesArr[46];

        handleCS();
        printVar();

        inputFile.close();
        return 0;
    }
}

void Var34::handleCS()
{
    this->checkSum = cs(allValuesArr, 46);
}

void Var34::printVar()
{
    std::ofstream outputFile;
    std::string outputFileName = "Var34_output.txt";
    outputFile.open(outputFileName);

    outputFile << "Var 34" << std::endl;
    outputFile << "Structure VelCov" << std::endl;
    outputFile << "xx: "
        << static_cast<int>(xxValue_1) << " "
        << static_cast<int>(xxValue_2) << " "
        << static_cast<int>(xxValue_3) << " "
        << static_cast<int>(xxValue_4) << std::endl;
    outputFile << "yy: "
        << static_cast<int>(yyValue_1) << " "
        << static_cast<int>(yyValue_2) << " "
        << static_cast<int>(yyValue_3) << " "
        << static_cast<int>(yyValue_4) << std::endl;
    outputFile << "zz: "
        << static_cast<int>(zzValue_1) << " "
        << static_cast<int>(zzValue_2) << " "
        << static_cast<int>(zzValue_3) << " "
        << static_cast<int>(zzValue_4) << std::endl;
    outputFile << "tt: "
        << static_cast<int>(ttValue_1) << " "
        << static_cast<int>(ttValue_2) << " "
        << static_cast<int>(ttValue_3) << " "
        << static_cast<int>(ttValue_4) << std::endl;
    outputFile << "xy: "
        << static_cast<int>(xyValue_1) << " "
        << static_cast<int>(xyValue_2) << " "
        << static_cast<int>(xyValue_3) << " "
        << static_cast<int>(xyValue_4) << std::endl;
    outputFile << "xz: "
        << static_cast<int>(xzValue_1) << " "
        << static_cast<int>(xzValue_2) << " "
        << static_cast<int>(xzValue_3) << " "
        << static_cast<int>(xzValue_4) << std::endl;
    outputFile << "xt: "
        << static_cast<int>(xtValue_1) << " "
        << static_cast<int>(xtValue_2) << " "
        << static_cast<int>(xtValue_3) << " "
        << static_cast<int>(xtValue_4) << std::endl;
    outputFile << "yz: "
        << static_cast<int>(yzValue_1) << " "
        << static_cast<int>(yzValue_2) << " "
        << static_cast<int>(yzValue_3) << " "
        << static_cast<int>(yzValue_4) << std::endl;
    outputFile << "yt: "
        << static_cast<int>(ytValue_1) << " "
        << static_cast<int>(ytValue_2) << " "
        << static_cast<int>(ytValue_3) << " "
        << static_cast<int>(ytValue_4) << std::endl;
    outputFile << "zt: "
        << static_cast<int>(ztValue_1) << " "
        << static_cast<int>(ztValue_2) << " "
        << static_cast<int>(ztValue_3) << " "
        << static_cast<int>(ztValue_4) << std::endl;
    outputFile << "Solution type: " << static_cast<int>(solTypeValue) << std::endl;
    outputFile << "Checksum of the string: " << static_cast<int>(checkSum) << std::endl;
    outputFile << "Parameter u1 cs: " << static_cast<int>(csValue);

    outputFile.close();
}

int Var34::getCheckSum()
{
    return this->checkSum;
}

int Var34::getCsValue()
{
    return this->csValue;
}