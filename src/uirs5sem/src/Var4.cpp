#include <iostream>
#include <fstream>
#include "../include/Var4.h"

int Var4::handleVar()
{
    std::ifstream inputFile;
    std::string inputFileName = "rks.dat";
    inputFile.open(inputFileName, std::ios::in | std::ios::binary);

    if (!inputFile.is_open())
    {
        std::cerr << "Error. File was not overwritten" << std::endl;
        exit(1);
    }
    else
    {
        int tempInt = 0; // variable for memory allocation
        std::string tempString; // char array holds all the characters of the file
        inputFile.read((char*)&tempInt, sizeof(tempInt)); // read string size from file
        tempString.resize(tempInt); // allocate memory of required size
        inputFile.read(&tempString[0], tempInt); // read string from file

        this->allValuesArr[0] = static_cast<u1>(tempString[0x19C9]);
        this->allValuesArr[1] = static_cast<u1>(tempString[0x19CA]);
        this->allValuesArr[2] = static_cast<u1>(tempString[0x19CB]);
        this->allValuesArr[3] = static_cast<u1>(tempString[0x19CC]);
        this->allValuesArr[4] = static_cast<u1>(tempString[0x19CD]);
        this->allValuesArr[5] = static_cast<u1>(tempString[0x19CE]);
        this->allValuesArr[6] = static_cast<u1>(tempString[0x19CF]);
        this->allValuesArr[7] = static_cast<u1>(tempString[0x19D0]);
        this->allValuesArr[8] = static_cast<u1>(tempString[0x19D1]);
        this->allValuesArr[9] = static_cast<u1>(tempString[0x19D2]);
        this->allValuesArr[10] = static_cast<u1>(tempString[0x19D3]);
        this->allValuesArr[11] = static_cast<u1>(tempString[0x19D4]);
        this->allValuesArr[12] = static_cast<u1>(tempString[0x19D5]);
        this->allValuesArr[13] = static_cast<u1>(tempString[0x19D6]);
        this->allValuesArr[14] = static_cast<u1>(tempString[0x19D7]);
        this->allValuesArr[15] = static_cast<u1>(tempString[0x19D8]);
        this->allValuesArr[16] = static_cast<u1>(tempString[0x19D9]);
        this->allValuesArr[17] = static_cast<u1>(tempString[0x19DA]);
        this->allValuesArr[18] = static_cast<u1>(tempString[0x19DB]);
        this->allValuesArr[19] = static_cast<u1>(tempString[0x19DC]);
        this->allValuesArr[20] = static_cast<u1>(tempString[0x19DD]);
        this->allValuesArr[21] = static_cast<u1>(tempString[0x19DE]);

        this->valValue_1 = this->allValuesArr[5];
        this->valValue_2 = this->allValuesArr[6];
        this->valValue_3 = this->allValuesArr[7];
        this->valValue_4 = this->allValuesArr[8];
        this->valValue_5 = this->allValuesArr[9];
        this->valValue_6 = this->allValuesArr[10];
        this->valValue_7 = this->allValuesArr[11];
        this->valValue_8 = this->allValuesArr[12];
        this->svalValue_1 = this->allValuesArr[13];
        this->svalValue_2 = this->allValuesArr[14];
        this->svalValue_3 = this->allValuesArr[15];
        this->svalValue_4 = this->allValuesArr[16];
        this->svalValue_5 = this->allValuesArr[17];
        this->svalValue_6 = this->allValuesArr[18];
        this->svalValue_7 = this->allValuesArr[19];
        this->svalValue_8 = this->allValuesArr[20];
        this->csValue = this->allValuesArr[21];

        handleCS();
        printVar();

        inputFile.close();
        return 0;
    }
}

void Var4::handleCS()
{
    this->checkSum = cs(allValuesArr, 21);
}

void Var4::printVar()
{
    std::ofstream outputFile;
    std::string outputFileName = "Var4_output.txt";
    outputFile.open(outputFileName);

    outputFile << "Var 4" << std::endl;
    outputFile << "Structure RcvTimeOffset" << std::endl;
    outputFile << "(Trr - Tr): "
        << static_cast<int>(valValue_1) << " "
        << static_cast<int>(valValue_2) << " "
        << static_cast<int>(valValue_3) << " "
        << static_cast<int>(valValue_4) << " "
        << static_cast<int>(valValue_5) << " "
        << static_cast<int>(valValue_6) << " "
        << static_cast<int>(valValue_7) << " "
        << static_cast<int>(valValue_8) << std::endl;
    outputFile << "Smoothed (Trr - Tr): "
        << static_cast<int>(svalValue_1) << " "
        << static_cast<int>(svalValue_2) << " "
        << static_cast<int>(svalValue_3) << " "
        << static_cast<int>(svalValue_4) << " "
        << static_cast<int>(svalValue_5) << " "
        << static_cast<int>(svalValue_6) << " "
        << static_cast<int>(svalValue_7) << " "
        << static_cast<int>(svalValue_8) << std::endl;
    outputFile << "Checksum of the string: " << static_cast<int>(checkSum) << std::endl;
    outputFile << "Parameter u1 cs: " << static_cast<int>(csValue);

    outputFile.close();
}

int Var4::getCheckSum()
{
    return this->checkSum;
}

int Var4::getCsValue()
{
    return this->csValue;
}