#include <iostream>
#include <fstream>
#include "../include/Var56.h"

int Var56::handleVar()
{
    std::ifstream inputFile;
    std::string inputFileName = "rks.dat";
    inputFile.open(inputFileName, std::ios::in | std::ios::binary);

    if (!inputFile.is_open())
    {
        std::cerr << "Error. File was not overwritten" << std::endl;
        exit(1);
    }
    else
    {
        int tempInt = 0; // variable for memory allocation
        std::string tempString; // char array holds all the characters of the file
        inputFile.read((char*)&tempInt, sizeof(tempInt)); // read string size from file
        tempString.resize(tempInt); // allocate memory of required size
        inputFile.read(&tempString[0], tempInt); // read string from file

        this->allValuesArr[0] = static_cast<u1>(tempString[0x7DB]);
        this->allValuesArr[1] = static_cast<u1>(tempString[0x7DC]);
        this->allValuesArr[2] = static_cast<u1>(tempString[0x7DD]);
        this->allValuesArr[3] = static_cast<u1>(tempString[0x7DE]);
        this->allValuesArr[4] = static_cast<u1>(tempString[0x7DF]);
        this->allValuesArr[5] = static_cast<u1>(tempString[0x7E0]);
        this->allValuesArr[6] = static_cast<u1>(tempString[0x7E1]);
        this->allValuesArr[7] = static_cast<u1>(tempString[0x7E2]);
        this->allValuesArr[8] = static_cast<u1>(tempString[0x7E3]);
        this->allValuesArr[9] = static_cast<u1>(tempString[0x7E4]);

        this->offsValue_1 = this->allValuesArr[5];
        this->offsValue_2 = this->allValuesArr[6];
        this->offsValue_3 = this->allValuesArr[7];
        this->offsValue_4 = this->allValuesArr[8];
        this->csValue = this->allValuesArr[9];

        handleCS();
        printVar();

        inputFile.close();
        return 0;
    }
}

void Var56::handleCS()
{
    this->checkSum = cs(allValuesArr, 9);
}

void Var56::printVar()
{
    std::ofstream outputFile;
    std::string outputFileName = "Var56_output.txt";
    outputFile.open(outputFileName);

    outputFile << "Var 56" << std::endl;
    outputFile << "Structure PPSOffset" << std::endl;
    outputFile << "PPS offset in nanoseconds: "
        << static_cast<int>(offsValue_1) << " "
        << static_cast<int>(offsValue_2) << " "
        << static_cast<int>(offsValue_3) << " "
        << static_cast<int>(offsValue_4) << std::endl;
    outputFile << "Checksum of the string: " << static_cast<int>(checkSum) << std::endl;
    outputFile << "Parameter u1 cs: " << static_cast<int>(csValue);

    outputFile.close();
}

int Var56::getCheckSum()
{
    return this->checkSum;
}

int Var56::getCsValue()
{
    return this->csValue;
}