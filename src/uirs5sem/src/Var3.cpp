#include <iostream>
#include <fstream>
#include "../include/Var3.h"

int Var3::handleVar()
{
    std::ifstream inputFile;
    std::string inputFileName = "rks.dat";
    inputFile.open(inputFileName, std::ios::in | std::ios::binary);

    if (!inputFile.is_open())
    {
        std::cerr << "Error. File was not overwritten" << std::endl;
        exit(1);
    }
    else
    {
        int tempInt = 0; // variable for memory allocation
        std::string tempString; // char array holds all the characters of the file
        inputFile.read((char*)&tempInt, sizeof(tempInt)); // read string size from file
        tempString.resize(tempInt); // allocate memory of required size
        inputFile.read(&tempString[0], tempInt); // read string from file

        this->allValuesArr[0] = static_cast<u1>(tempString[0x22F]);
        this->allValuesArr[1] = static_cast<u1>(tempString[0x230]);
        this->allValuesArr[2] = static_cast<u1>(tempString[0x231]);
        this->allValuesArr[3] = static_cast<u1>(tempString[0x232]);
        this->allValuesArr[4] = static_cast<u1>(tempString[0x233]);
        this->allValuesArr[5] = static_cast<u1>(tempString[0x234]);
        this->allValuesArr[6] = static_cast<u1>(tempString[0x235]);
        this->allValuesArr[7] = static_cast<u1>(tempString[0x236]);
        this->allValuesArr[8] = static_cast<u1>(tempString[0x237]);
        this->allValuesArr[9] = static_cast<u1>(tempString[0x238]);
        this->allValuesArr[10] = static_cast<u1>(tempString[0x239]);

        this->yearValue_1 = allValuesArr[5];
        this->yearValue_2 = allValuesArr[6];
        this->monthValue = allValuesArr[7];
        this->dayValue = allValuesArr[8];
        this->baseValue = allValuesArr[9];
        this->csValue = this->allValuesArr[10];

        handleCS();
        printVar();

        inputFile.close();
        return 0;
    }
}

void Var3::handleCS()
{
    this->checkSum = cs(allValuesArr, 10);
}

void Var3::printVar()
{
    std::ofstream outputFile;
    std::string outputFileName = "Var3_output.txt";
    outputFile.open(outputFileName);

    outputFile << "Var 3" << std::endl;
    outputFile << "Structure RcvDate" << std::endl;
    outputFile << "Current year: "
        << static_cast<int>(yearValue_1) << " "
        << static_cast<int>(yearValue_2) << std::endl;
    outputFile << "Current month: " << static_cast<int>(monthValue) << std::endl;
    outputFile << "Current day: " << static_cast<int>(dayValue) << std::endl;
    outputFile << "Receiver reference time: " << static_cast<int>(baseValue) << std::endl;
    outputFile << "Checksum of the string: " << static_cast<int>(checkSum) << std::endl;
    outputFile << "Parameter u1 cs: " << static_cast<int>(csValue);

    outputFile.close();
}

int Var3::getCheckSum()
{
    return this->checkSum;
}

int Var3::getCsValue()
{
    return this->csValue;
}