# GREIS format C++ parser

Student project.<br />
A parser for some GNSS components.

## GREIS Specification
[GREIS](http://www.javad.com/downloads/javadgnss/manuals/GREIS/GREIS_Reference_Guide.pdf)

## Format specifications
```cpp
struct NAME {LENGTH}
{
    TYPE FIELD[COUNT]; // DESCRIPTION
    …
    TYPE FIELD[COUNT]; // DESCRIPTION
};
```
where:<br />
NAME – the name assigned to this format. It could be used in other format definitions
as the TYPE of a field.<br />
LENGTH – the length in bytes of entire sequence. For a fixed length format, it is a
number, for a variable length message, it may be either an arithmetic expression
depending on some other variable parameters or just the string var.<br />
TYPE FIELD[COUNT] – field descriptor. It describes a sequence of COUNT elements of
the same TYPE which is assigned the name FIELD. The TYPE could be either one of
the primary field types described below, or a NAME of another format. When
[COUNT] is absent, the field consists of exactly one element. When COUNT is absent
(i.e., there are only empty square brackets, []), it means that the field consists of
unspecified number of elements.<br />
DESCRIPTION – description of the field along with its measurement units and allowed
range of values, where appropriate. Measurement units are surrounded by square
brackets.

## Primary field types
| Type Name | Meaning | Length in Bytes |
| ------ | ------ | ------ |
| a1 | ASCII character | 1 |
| i1 | signed integer | 1 |
| i2 | signed integer | 2 |
| i4 | signed integer | 4 |
| u1 | unsigned integer | 1 |
| u2 | unsigned integer | 2 |
| u4 | unsigned integer | 4 |
| f4 | IEEE-754 single precision floating point | 4 |
| f8 | IEEE-754 double precision floating point | 8 |
| str | zero-terminated sequence of ASCII characters | variable |

## Listing of the variants

#### Var1
```cpp
struct RcvTime {5}
{
    u4 tod; // Tr modulo 1 day (86400000 ms) [ms]
    u1 cs; // Checksum
};
```
#### Var2
```cpp
struct EpochTime {5}
{
    u4 tod; // Tr modulo 1 day (86400000 ms) [ms]
    u1 cs; // Checksum
};
```
#### Var3
```cpp
struct RcvDate {6}
{
    u2 year; // Current year [1…65534][]
    u1 month; // Current month [1…12] []
    u1 day; // Current day [1…31] []
    u1 base; // Receiver reference time [enumerated]
    // 0 — GPS
    // 1 — UTC_USNO
    // 2 — GLONASS
    // 3 — UTC_SU
    // 4…254 — Reserved
    u1 cs; // Checksum
};
```
#### Var4
```cpp
struct RcvTimeOffset {17}
{
    f8 val; // Trr - Tr [s]
    f8 sval; // Smoothed (Trr - Tr) [s]
    u1 cs; // Checksum
};
```
#### Var5
```cpp
struct RcvTimeOffsetDot {9}
{
    f4 val; // Derivative of (Trr - Tr) [s/s]
    f4 sval; // Smoothed derivative of (Trr - Tr) [s/s]
    u1 cs; // Checksum
};
```
#### Var6
```cpp
struct RcvTimeAccuracy {5}
{
    f4 acc; // Accuracy [s]
    u1 cs; // Checksum
};
```
#### Var7
```cpp
struct GPSTime {7}
{
    u4 tow; // Time of week [ms]
    u2 wn; // GPS week number (modulo 1024) []
    u1 cs; // Checksum
};
```
#### Var8
```cpp
struct RcvGPSTimeOffset {17}
{
    f8 val; // (Tg - Tr) [s]
    f8 sval; // Smoothed (Tg - Tr) [s]
    u1 cs; // Checksum
};
```
#### Var9
```cpp
struct GLOTime {7}
{
    u4 tod; // time of day [ms]
    u2 dn; // GLONASS day number (modulo 4 years
    // starting from 1996) []
    u1 cs; // Checksum
};
```
#### Var10
```cpp
struct RcvGLOTimeOffset {17}
{
    f8 val; // (Tn - Tr) [s]
    f8 sval; // Smoothed (Tn - Tr) [s]
    u1 cs; // Checksum
};
```
#### Var11
```cpp
struct RcvGALTimeOffset {17}
{
    f8 val; // (Te - Tr) [s]
    f8 sval; // Smoothed (Te - Tr) [s]
    u1 cs; // Checksum
};
```
#### Var12
```cpp
struct RcvSBASTimeOffset {17}
{
    f8 val; // (Tw - Tr) [s]
    f8 sval; // Smoothed (Tw - Tr) [s]
    u1 cs; // Checksum
};
```
#### Var13
```cpp
struct RcvQZSSTimeOffset {17}
{
    f8 val; // (Tq - Tr) [s]
    f8 sval; // Smoothed (Tq - Tr) [s]
    u1 cs; // Checksum
};
```
#### Var14
```cpp
struct RcvBeiDouTimeOffset {17}
{
    f8 val; // (Tb - Tr) [s]
    f8 sval; // Smoothed (Tb - Tr) [s]
    u1 cs; // Checksum
};
```
#### Var15
```cpp
struct RcvIrnssTimeOffset {17}
{
    f8 val; // (Ti - Tr) [s]
    f8 sval; // Smoothed (Ti - Tr) [s]
    u1 cs; // Checksum
};
```
#### Var16
```cpp
struct GpsUtcParam {24}
{
    UtcOffs utc; // GPS UTC time offset parameters
    u1 cs; // Checksum
};

struct UtcOffs {23}
{
    f8 a0; // Constant term of polynomial [s]
    f4 a1; // First order term of polynomial [s/s]
    u4 tot; // Reference time of week [s]
    u2 wnt; // Reference week number []
    i1 dtls; // Delta time due to leap seconds [s]
    u1 dn; // 'Future' reference day number [1…7] []
    u2 wnlsf; // 'Future' reference week number []
    i1 dtlsf; // 'Future' delta time due to leap seconds [s]
};
```
#### Var17
```cpp
struct SbasUtcParam {32}
{
    UtcOffs utc; // SBAS to UTC time offset parameters
    i1 utcsi; // UTC Standard Identifier[]
    u4 tow; // Reference time of week [s]
    u2 wn; // Reference week number []
    u1 flags; // Flags, reserved (always 0)
    u1 cs; // Checksum
};

struct UtcOffs {23}
{
    f8 a0; // Constant term of polynomial [s]
    f4 a1; // First order term of polynomial [s/s]
    u4 tot; // Reference time of week [s]
    u2 wnt; // Reference week number []
    i1 dtls; // Delta time due to leap seconds [s]
    u1 dn; // 'Future' reference day number [1…7] []
    u2 wnlsf; // 'Future' reference week number []
    i1 dtlsf; // 'Future' delta time due to leap seconds [s]
};
```
#### Var18
```cpp
struct GalUtcGpsParam {40}
{
    UtcOffs utc; // GALILEO to UTC time offset parameters
    // GALILEO to GPS time offset parameters
    f4 a0g; // Constant term of time offset [s]
    f4 a1g; // Rate of time offset [s/s]
    u4 t0g; // Reference time of week
    u2 wn0g; // Reference week number
    u2 flags; // Flags of data availability [bitfield]
    // 0 - GGTO availability
    // 1…15 - reserved
    u1 cs; // Checksum
};

struct UtcOffs {23}
{
    f8 a0; // Constant term of polynomial [s]
    f4 a1; // First order term of polynomial [s/s]
    u4 tot; // Reference time of week [s]
    u2 wnt; // Reference week number []
    i1 dtls; // Delta time due to leap seconds [s]
    u1 dn; // 'Future' reference day number [1…7] []
    u2 wnlsf; // 'Future' reference week number []
    i1 dtlsf; // 'Future' delta time due to leap seconds [s]
};
```
#### Var19
```cpp
struct QzssUtcParam {24}
{
    UtcOffs utc; // QZSS UTC time offset parameters
    u1 cs; // Checksum
};

struct UtcOffs {23}
{
    f8 a0; // Constant term of polynomial [s]
    f4 a1; // First order term of polynomial [s/s]
    u4 tot; // Reference time of week [s]
    u2 wnt; // Reference week number []
    i1 dtls; // Delta time due to leap seconds [s]
    u1 dn; // 'Future' reference day number [1…7] []
    u2 wnlsf; // 'Future' reference week number []
    i1 dtlsf; // 'Future' delta time due to leap seconds [s]
};
```
#### Var20
```cpp
struct BeiDouUtcParam {24}
{
    UtcOffs utc; // BeiDou UTC time offset parameters
    u1 cs; // Checksum
};

struct UtcOffs {23}
{
    f8 a0; // Constant term of polynomial [s]
    f4 a1; // First order term of polynomial [s/s]
    u4 tot; // Reference time of week [s]
    u2 wnt; // Reference week number []
    i1 dtls; // Delta time due to leap seconds [s]
    u1 dn; // 'Future' reference day number [1…7] []
    u2 wnlsf; // 'Future' reference week number []
    i1 dtlsf; // 'Future' delta time due to leap seconds [s]
};
```
#### Var21
```cpp
struct IrnssUtcParam {24}
{
    UtcOffs utc; // IRNSS UTC time offset parameters
    u1 cs; // Checksum
};

struct UtcOffs {23}
{
    f8 a0; // Constant term of polynomial [s]
    f4 a1; // First order term of polynomial [s/s]
    u4 tot; // Reference time of week [s]
    u2 wnt; // Reference week number []
    i1 dtls; // Delta time due to leap seconds [s]
    u1 dn; // 'Future' reference day number [1…7] []
    u2 wnlsf; // 'Future' reference week number []
    i1 dtlsf; // 'Future' delta time due to leap seconds [s]
};
```
#### Var22
```cpp
struct GloUtcGpsParam {27}
{
    f8 tauSys; // Time correction to GLONASS time scale (vs. UTC(SU))
    // tauSys = Tutc(su) - Tglo [s]
    f4 tauGps; // tauGps = Tgps - Tglo [s]
    f4 B1; // Coefficient for calculation of UT1
    f4 B2; // Coefficient for calculation of UT1
    u1 KP; // Leap second information
    u1 N4; // Number of 4-year cycle [1…31]
    i2 Dn; // Day number within 4-year period []
    i2 Nt; // Current day number at the decoding time
    u1 cs; // Checksum
};
```
#### Var23
```cpp
struct SolutionTime {6}
{
    u4 time; // Solution time. Tr modulo 1 day (86400000 ms)[ms]
    u1 solType; // Solution type
    u1 cs; // Checksum
};
```
#### Var24
```cpp
struct Pos {30}
{
    f8 x, y, z; // Cartesian coordinates [m]
    f4 pSigma; // Position SEP6 [m]
    u1 solType; // Solution type
    u1 cs; // Checksum
};
```
#### Var25
```cpp
struct SpecificCrtPos {38}
{
    f8 x, y, z; // Cartesian coordinates [m]
    f4 pSigma; // Position SEP [m]
    u1 solType; // Solution type
    u1 system; // Source of position
    // 0 - WGS
    // 1 - Local
    a1 crsCode[5]; // Name of the coordinate reference system
    u2 chIssue; // Counter incrementing on every potential change
    // of user grid system
    u1 cs; // Checksum
};
```
#### Var26
```cpp
struct Vel {18}
{
    f4 x, y, z; // Cartesian velocity vector [m/s]
    f4 vSigma; // Velocity SEP [m/s]
    u1 solType; // Solution type
    u1 cs; // Checksum
};
```
#### Var27
```cpp
struct GeoPos {30}
{
    f8 lat; // Latitude [rad]
    f8 lon; // Longitude [rad]
    f8 alt; // Ellipsoidal height [m]
    f4 pSigma; // Position SEP [m]
    u1 solType; // Solution type
    u1 cs; // Checksum
};
```
#### Var28
```cpp
struct SpecificGeoPos {38}
{
    f8 lat; // Latitude [rad]
    f8 lon; // Longitude [rad]
    f8 alt; // Ellipsoidal height [m]
    f4 pSigma; // Position SEP [m]
    u1 solType; // Solution type
    u1 system; // Coordinate system
    // 0 - WGS
    // 1 - Local
    a1 crsCode[5]; // Name of the coordinate reference system
    u2 chIssue; // Counter incrementing on every potential change
    // of user grid system
    u1 cs; // Checksum
};
```
#### Var29
```cpp
struct GeoVel {18}
{
    f4 lat; // Northing velocity [m/s]
    f4 lon; // Easting velocity [m/s]
    f4 alt; // Height velocity [m/s]
    f4 vSigma; // Velocity SEP [m/s]
    u1 solType; // Solution type
    u1 cs; // Checksum
};
```
#### Var30
```cpp
struct LocalPlanePos {45}
{
    f8 n; // Northern coordinate [m]
    f8 e; // Eastern coordinate [m]
    f8 u; // Altitude above local ellipsoid [m]
    f8 sep; // Geoid separation relatively to local ellipsoid [m]
    f4 pSigma; // Position SEP [m]
    u1 solType; // Solution type
    u1 grid; // Grid source
    // 0 - none
    // 1 - predefined grid
    // 2 - user defined grid
    // 3 - result of localization
    // 4 - grid got from external source
    u1 geoid; // Geoid source
    // 0 - none
    // 1 - predefined geoid
    // 2 - user defined geoid
    // 4 - geoid got from external source
    u2 prj; // EPSG code of used projection
    u1 gridZone; // Grid zone for global systems UTM and UPS, 0 otherwise
    u2 chIssue; // Counter incrementing on every potential change
    // of user grid system
    u1 cs; // Checksum
};
```
#### Var31
```cpp
struct RSLocalPlanePos {42}
{
    f8 n; // Northern coordinate [m]
    f8 e; // Eastern coordinate [m]
    f8 u; // Altitude above local ellipsoid [m]
    f8 sep; // Geoid separation relatively to local ellipsoid [m]
    f4 pSigma; // Position SEP [m]
    u1 solType; // Solution type
    u1 grid; // Grid source
    // 0 - none
    // 1 - predefined grid
    // 2 - user defined grid
    // 3 - result of localization
    // 4 - grid got from external source
    u1 geoid; // Geoid source
    // 0 - none
    // 1 - predefined geoid
    // 2 - user defined geoid
    // 4 - geoid got from external source
    u2 prj; // EPSG code of used projection
    u1 cs; // Checksum
};
```
#### Var32
```cpp
struct Dops {18}
{
    f4 hdop; // Horizontal dilution of precision (HDOP)[]
    f4 vdop; // Vertical dilution of precision (VDOP) []
    f4 tdop; // Time dilution of precision (TDOP) []
    u1 solType; // Solution type
    f4 edop; // East dilution of precision (eDOP) []
    u1 cs; // Checksum
};
```
#### Var33
```cpp
struct PosCov {42}
{
    f4 xx; // [m^2]
    f4 yy; // [m^2]
    f4 zz; // [m^2]
    f4 tt; // [m^2]
    f4 xy; // [m^2]
    f4 xz; // [m^2]
    f4 xt; // [m^2]
    f4 yz; // [m^2]
    f4 yt; // [m^2]
    f4 zt; // [m^2]
    u1 solType; // Solution type
    u1 cs; // Checksum
};
```
#### Var34
```cpp
struct VelCov {42}
{
    f4 xx; // [(m/s)^2]
    f4 yy; // [(m/s)^2]
    f4 zz; // [(m/s)^2]
    f4 tt; // [(m/s)^2]
    f4 xy; // [(m/s)^2]
    f4 xz; // [(m/s)^2]
    f4 xt; // [(m/s)^2]
    f4 yz; // [(m/s)^2]
    f4 yt; // [(m/s)^2]
    f4 zt; // [(m/s)^2]
    u1 solType; // Solution type
    u1 cs; // Checksum
};
```
#### Var35
```cpp
struct Baseline {34}
{
    f8 x, y, z; // Calculated baseline vector coordinates [m]
    f4 sigma; // Baseline Spherical Error Probable (SEP) [m]
    u1 solType; // Solution type
    i4 time; // receiver time of the baseline estimate [s]
    u1 cs; // Checksum
};
```
#### Var36
```cpp
struct Baselines {52}
{
    f4 bl0[3]; // baseline vector M-S0 [m]
    f4 bl1[3]; // baseline vector M-S1 [m]
    f4 bl2[3]; // baseline vector M-S2 [m]
    f4 rms[3]; // estimated accuracies for baseline vectors [m]
    u1 solType[3]; // solution types for baseline vectors
    u1 cs; // Checksum
};
```
#### Var37
```cpp
struct FullRotationMatrix {37}
{
    f4 q00, q01, q02; // components of the rotation matrix Q []
    f4 q10, q11, q12; // components of the rotation matrix Q []
    f4 q20, q21, q22; // components of the rotation matrix Q []
    u1 cs; // Checksum
};
```
#### Var38
```cpp
struct PosCompTime {5}
{
    u4 pt; // Continuous position computation time [s]
    u1 cs; // Checksum
};
```
#### Var39
```cpp
struct GPSAlm {47}
{
    u1 sv; // SV PRN number within the range [1…37]
    i2 wna; // Almanac reference week []
    i4 toa; // Almanac reference time of week [s]
    u1 healthA; // Health summary (from almanac), [bitfield]
    // 0…4 - code for health of SV signal components
    // 5…7 - navigation data health indicators
    u1 healthS; // Satellite health (page 25 of subframe 5) []
    u1 config; // Satellite configuration (page 25 of subframe 4)
    // [bitfield]:
    // 0…2 - satellite configuration
    // 3 - anti-spoofing flag
    // 4…7 - reserved
    //======= Clock data =======
    f4 af1; // Polynomial coefficient [s/s]
    f4 af0; // Polynomial coefficient [s]
    //===== Ephemeris data =====
    //--- Keplerian orbital parameters ---
    f4 rootA; // Square root of the semi-major axis [m^0.5]
    f4 ecc; // Eccentricity []
    f4 m0; // Mean Anomaly at reference time [semi-circles]
    f4 omega0; // Longitude of ascending node of orbit plane
    // at the start of week ‘wna’ [semi-circles]
    f4 argPer; // Argument of perigee [semi-circles]
    //--- Corrections to orbital parameters ---
    f4 deli; // Correction to inclination angle [semi-circles]
    f4 omegaDot; // Rate of right ascension [semi-circle/s]
    u1 cs; // Checksum
};
```
#### Var40
```cpp
struct GALAlm {49}
{
    // GPS-alike data
    GPSAlm gps; // Without ‘cs’ field, gps.sv within the range [1…30]
    // GALILEO-Specific data
    i2 iod; // Issue of almanac data []
    u1 cs; // Checksum
};

struct GPSAlm {47}
{
    u1 sv; // SV PRN number within the range [1…37]
    i2 wna; // Almanac reference week []
    i4 toa; // Almanac reference time of week [s]
    u1 healthA; // Health summary (from almanac), [bitfield]
    // 0…4 - code for health of SV signal components
    // 5…7 - navigation data health indicators
    u1 healthS; // Satellite health (page 25 of subframe 5) []
    u1 config; // Satellite configuration (page 25 of subframe 4)
    // [bitfield]:
    // 0…2 - satellite configuration
    // 3 - anti-spoofing flag
    // 4…7 - reserved
    //======= Clock data =======
    f4 af1; // Polynomial coefficient [s/s]
    f4 af0; // Polynomial coefficient [s]
    //===== Ephemeris data =====
    //--- Keplerian orbital parameters ---
    f4 rootA; // Square root of the semi-major axis [m^0.5]
    f4 ecc; // Eccentricity []
    f4 m0; // Mean Anomaly at reference time [semi-circles]
    f4 omega0; // Longitude of ascending node of orbit plane
    // at the start of week ‘wna’ [semi-circles]
    f4 argPer; // Argument of perigee [semi-circles]
    //--- Corrections to orbital parameters ---
    f4 deli; // Correction to inclination angle [semi-circles]
    f4 omegaDot; // Rate of right ascension [semi-circle/s]
    u1 cs; // Checksum
};
```
#### Var41
```cpp
struct QZSSAlm {47}
{
    // GPS-alike data
    GPSAlm gps; // ‘gps.sv’ within the range [193…199]
};

struct GPSAlm {47}
{
    u1 sv; // SV PRN number within the range [1…37]
    i2 wna; // Almanac reference week []
    i4 toa; // Almanac reference time of week [s]
    u1 healthA; // Health summary (from almanac), [bitfield]
    // 0…4 - code for health of SV signal components
    // 5…7 - navigation data health indicators
    u1 healthS; // Satellite health (page 25 of subframe 5) []
    u1 config; // Satellite configuration (page 25 of subframe 4)
    // [bitfield]:
    // 0…2 - satellite configuration
    // 3 - anti-spoofing flag
    // 4…7 - reserved
    //======= Clock data =======
    f4 af1; // Polynomial coefficient [s/s]
    f4 af0; // Polynomial coefficient [s]
    //===== Ephemeris data =====
    //--- Keplerian orbital parameters ---
    f4 rootA; // Square root of the semi-major axis [m^0.5]
    f4 ecc; // Eccentricity []
    f4 m0; // Mean Anomaly at reference time [semi-circles]
    f4 omega0; // Longitude of ascending node of orbit plane
    // at the start of week ‘wna’ [semi-circles]
    f4 argPer; // Argument of perigee [semi-circles]
    //--- Corrections to orbital parameters ---
    f4 deli; // Correction to inclination angle [semi-circles]
    f4 omegaDot; // Rate of right ascension [semi-circle/s]
    u1 cs; // Checksum
};
```
#### Var42
```cpp
struct BeiDouAlm {47}
{
    // GPS-alike data
    GPSAlm gps; // ‘gps.sv’ within the range [1…30]
};

struct GPSAlm {47}
{
    u1 sv; // SV PRN number within the range [1…37]
    i2 wna; // Almanac reference week []
    i4 toa; // Almanac reference time of week [s]
    u1 healthA; // Health summary (from almanac), [bitfield]
    // 0…4 - code for health of SV signal components
    // 5…7 - navigation data health indicators
    u1 healthS; // Satellite health (page 25 of subframe 5) []
    u1 config; // Satellite configuration (page 25 of subframe 4)
    // [bitfield]:
    // 0…2 - satellite configuration
    // 3 - anti-spoofing flag
    // 4…7 - reserved
    //======= Clock data =======
    f4 af1; // Polynomial coefficient [s/s]
    f4 af0; // Polynomial coefficient [s]
    //===== Ephemeris data =====
    //--- Keplerian orbital parameters ---
    f4 rootA; // Square root of the semi-major axis [m^0.5]
    f4 ecc; // Eccentricity []
    f4 m0; // Mean Anomaly at reference time [semi-circles]
    f4 omega0; // Longitude of ascending node of orbit plane
    // at the start of week ‘wna’ [semi-circles]
    f4 argPer; // Argument of perigee [semi-circles]
    //--- Corrections to orbital parameters ---
    f4 deli; // Correction to inclination angle [semi-circles]
    f4 omegaDot; // Rate of right ascension [semi-circle/s]
    u1 cs; // Checksum
};
```
#### Var43
```cpp
struct IrnssAlm {47}
{
    // GPS-alike data
    GPSAlm gps; // ‘gps.sv’ within the range [1…30]
};

struct GPSAlm {47}
{
    u1 sv; // SV PRN number within the range [1…37]
    i2 wna; // Almanac reference week []
    i4 toa; // Almanac reference time of week [s]
    u1 healthA; // Health summary (from almanac), [bitfield]
    // 0…4 - code for health of SV signal components
    // 5…7 - navigation data health indicators
    u1 healthS; // Satellite health (page 25 of subframe 5) []
    u1 config; // Satellite configuration (page 25 of subframe 4)
    // [bitfield]:
    // 0…2 - satellite configuration
    // 3 - anti-spoofing flag
    // 4…7 - reserved
    //======= Clock data =======
    f4 af1; // Polynomial coefficient [s/s]
    f4 af0; // Polynomial coefficient [s]
    //===== Ephemeris data =====
    //--- Keplerian orbital parameters ---
    f4 rootA; // Square root of the semi-major axis [m^0.5]
    f4 ecc; // Eccentricity []
    f4 m0; // Mean Anomaly at reference time [semi-circles]
    f4 omega0; // Longitude of ascending node of orbit plane
    // at the start of week ‘wna’ [semi-circles]
    f4 argPer; // Argument of perigee [semi-circles]
    //--- Corrections to orbital parameters ---
    f4 deli; // Correction to inclination angle [semi-circles]
    f4 omegaDot; // Rate of right ascension [semi-circle/s]
    u1 cs; // Checksum
};
```
#### Var44
```cpp
struct GLOAlmanac {47 | 52}
{
    u1 sv; // Satellite orbit slot number within [1…32] []
    i1 frqNum; // Satellite frequency channel number [-7…24] []
    i2 dna; // Day number within 4-year period starting
    // with the leap year []
    f4 tlam; // Time of the first ascending node passage
    // on day ‘dna’ [s]
    u1 flags; // Satellite flags [bitfield]:
    // 0 - health: 1 - healthy SV, as specified
    // by ‘Cn’, 0 - unhealthy
    // 1 - SVs type: 0 - GLONASS, 1 - GLONASS-M
    // 2…7 - reserved
    //======= Clock data =======
    f4 tauN; // Coarse time correction to SV clock
    // with respect to GLONASS system time [s]
    f8 tauSys; // Correction to GLONASS system time with respect
    // to UTC(SU) [s]
    //===== Ephemeris data =====
    f4 ecc; // Eccentricity at reference time ‘tlam’ []
    f4 lambda; // Longitude of ascending node
    // at reference time ‘tlam’ [semi-circles]
    f4 argPer; // Argument of perigee
    // at reference time ‘tlam’ [semi-circles]
    f4 delT; // Correction to mean Draconic period
    // at reference time ‘tlam’ [s/period]
    f4 delTdt; // Rate of change of Draconic period [s/period^2]
    f4 deli; // Correction to inclination
    // at reference time ‘tlam’[semi-circles]
    u1 n4; // Number of 4-year period []
    // --- Optional data block ---
    u1 reserved; // <reserved>
    f4 gammaN; // Rate of coarse satellite clock correction to
    // GLONASS time scale [s/s]
    // --- End of optional data block ---
    u1 cs; // Checksum
};
```
#### Var45
```cpp
struct SBASAlmanac {51}
{
    u1 waasPrn; // SBAS SV PRN number within [120…142]
    u1 gpsPrn; // GPS SV PRN associated with SBAS SV
    u1 id; // Data ID
    u1 healthS; // Satellite health [bitfield]:
    // 0 - 0–Ranging on, 1–off
    // 1 - 0–Corrections on, 1-off
    // 2 - 0–Broadcast Integrity on, 1-off
    // 3 - reserved
    // 4…7 - are set to zero
    u4 tod; // Time of the day [s]
    f8 xg, yg, zg; // ECEF coordinates [m]
    f4 vxg, vyg, vzg; // ECEF velocity [m/s]
    u4 tow; // time of GPS week almanac was received at
    u2 wn; // GPS week this almanac was received at
    u1 cs; // Checksum
};
```
#### Var46
```cpp
struct GALEphemeris {149}
{
    GpsEphReqData req; // GPS required data, ‘req.sv’ within the range [1…30]
    // --- GALILEO-specific data block ---
    f4 bgdE1E5a; // broadcast group delay E1 - E5A [s]
    f4 bgdE1E5b; // broadcast group delay E1 - E5B [s]
    f4 ai0; // Effective ionisation level 1-st order parameter []
    f4 ai1; // Effective ionisation level 2-nd order parameter []
    f4 ai2; // Effective ionisation level 3-rd order parameter []
    u1 sfi; // Ionospheric disturbance flags [bitfield]
    u1 navType; // Signal type [bitfield]:
    // 0 - GALILEO E1B(INAV)
    // 1 - GALILEO E5A(FNAV)
    // 2 - GALILEO E5B(INAV)
    // 3 - GIOVE E1B (historical)
    // 4 - GIOVE E5A (historical)
    // 5 - <reserved>
    // 6 - GALILEO E6
    f4 DAf0; // correction to ‘af0’. Exact term = af0 + DAF0
    // --- End of GALILEO-specific data block ---
    u1 cs; // Checksum
};

struct GpsEphReqData {122} 
{
    u1 sv; // SV PRN number within the range [1…37]
    u4 tow; // Time of week [s]
    u1 flags; // Flags (see GPS ICD for details)[bitfield]:
    // 0 - curve fit interval
    // 1 - data flag for L2 P-code
    // 2,3 - code on L2 channel
    // 4 - anti-spoof (A-S) flag (from HOW)
    // 5 - ‘Alert’ flag (from HOW)
    // 6 - ephemeris was retrieved from non-volatile memory
    // 7 - reserved
    //===== Clock data (Subframe 1) =====
    i2 iodc; // Issue of data, clock []
    i4 toc; // Clock data reference time [s]
    i1 ura; // User range accuracy []
    u1 healthS; // Satellite health []
    i2 wn; // Week number []
    f4 tgd; // Estimated group delay differential [s]
    f4 af2; // Polynomial coefficient [s/(s^2)]
    f4 af1; // Polynomial coefficient [s/s]
    f4 af0; // Polynomial coefficient [s]
    //===== Ephemeris data (Subframes 2 and 3) =====
    i4 toe; // Ephemeris reference time [s]
    i2 iode; // Issue of data, ephemeris []
    //--- Keplerian orbital parameters ---
    f8 rootA; // Square root of the semi-major axis [m^0.5]
    f8 ecc; // Eccentricity []
    f8 m0; // Mean Anomaly at reference time (wn,toe)
    // [semi-circles]
    f8 omega0; // Longitude of ascending node of orbit plane at the
    // start of week ‘wn’ [semi-circles]
    f8 inc0; // Inclination angle at reference time [semi-circles]
    f8 argPer; // Argument of perigee [semi-circles]
    //--- Corrections to orbital parameters ---
    f4 deln; // Mean motion difference from computed value
    // [semi-circle/s]
    f4 omegaDot; // Rate of right ascension [semi-circle/s]
    f4 incDot; // Rate of inclination angle [semi-circle/s]
    f4 crc; // Amplitude of the cosine harmonic correction term
    // to the orbit radius [m]
    f4 crs; // Amplitude of the sine harmonic correction term
    // to the orbit radius [m]
    f4 cuc; // Amplitude of the cosine harmonic correction term
    // to the argument of latitude [rad]
    f4 cus; // Amplitude of the sine harmonic correction term
    // to the argument of latitude [rad]
    f4 cic; // Amplitude of the cosine harmonic correction term
    // to the angle of inclination [rad]
    f4 cis; // Amplitude of the sine harmonic correction term
    // to the angle of inclination [rad]
};
```
#### Var47
```cpp
struct BeiDouEphemeris {132}
{
    GpsEphReqData req; // GPS required data, ‘req.sv’ within the range [1…30]
    // --- BeiDou-specific data block ---
    f4 tgd2; //
    u1 navType; // Signal type[bitfield]
    // 0 - B1
    // 1 - B2
    // 2 - B3
    f4 DAf0; // correction to ‘af0’. Exact term = af0 + DAF0
    // --- End of BeiDou-specific data block ---
    u1 cs; // Checksum
};

struct GpsEphReqData {122} 
{
    u1 sv; // SV PRN number within the range [1…37]
    u4 tow; // Time of week [s]
    u1 flags; // Flags (see GPS ICD for details)[bitfield]:
    // 0 - curve fit interval
    // 1 - data flag for L2 P-code
    // 2,3 - code on L2 channel
    // 4 - anti-spoof (A-S) flag (from HOW)
    // 5 - ‘Alert’ flag (from HOW)
    // 6 - ephemeris was retrieved from non-volatile memory
    // 7 - reserved
    //===== Clock data (Subframe 1) =====
    i2 iodc; // Issue of data, clock []
    i4 toc; // Clock data reference time [s]
    i1 ura; // User range accuracy []
    u1 healthS; // Satellite health []
    i2 wn; // Week number []
    f4 tgd; // Estimated group delay differential [s]
    f4 af2; // Polynomial coefficient [s/(s^2)]
    f4 af1; // Polynomial coefficient [s/s]
    f4 af0; // Polynomial coefficient [s]
    //===== Ephemeris data (Subframes 2 and 3) =====
    i4 toe; // Ephemeris reference time [s]
    i2 iode; // Issue of data, ephemeris []
    //--- Keplerian orbital parameters ---
    f8 rootA; // Square root of the semi-major axis [m^0.5]
    f8 ecc; // Eccentricity []
    f8 m0; // Mean Anomaly at reference time (wn,toe)
    // [semi-circles]
    f8 omega0; // Longitude of ascending node of orbit plane at the
    // start of week ‘wn’ [semi-circles]
    f8 inc0; // Inclination angle at reference time [semi-circles]
    f8 argPer; // Argument of perigee [semi-circles]
    //--- Corrections to orbital parameters ---
    f4 deln; // Mean motion difference from computed value
    // [semi-circle/s]
    f4 omegaDot; // Rate of right ascension [semi-circle/s]
    f4 incDot; // Rate of inclination angle [semi-circle/s]
    f4 crc; // Amplitude of the cosine harmonic correction term
    // to the orbit radius [m]
    f4 crs; // Amplitude of the sine harmonic correction term
    // to the orbit radius [m]
    f4 cuc; // Amplitude of the cosine harmonic correction term
    // to the argument of latitude [rad]
    f4 cus; // Amplitude of the sine harmonic correction term
    // to the argument of latitude [rad]
    f4 cic; // Amplitude of the cosine harmonic correction term
    // to the angle of inclination [rad]
    f4 cis; // Amplitude of the sine harmonic correction term
    // to the angle of inclination [rad]
};
```
#### Var48
```cpp
struct MDM_Spectrum {9} 
{ 
    i4 frq; // Current frequency [Hz]
    i4 pwr; // Current signal (or noise) power [dBm]
    u1 cs; // Checksum
};
```
#### Var49
```cpp
struct RotationMatrix {37}
{
    u4 time; // receiver time [ms]
    f4 q00, q01, q02, q12; // components of the rotation matrix Q []
    f4 rms[3]; // estimated accuracy for three baseline vectors [m]
    u1 solType[3]; // solution type8 for three baseline vectors
    u1 flag; // 0 – components of matrix Q are invalid, 1 - valid
    u1 cs; // Checksum
};
```
#### Var50
```cpp
struct RotationMatrixAndVectors {73}
{
    u4 time; // receiver time [ms]
    f4 q00, q01, q02, q12; // components of the rotation matrix Q []
    f4 rms[3]; // estimated accuracy for three baseline vectors [m]
    u1 solType[3]; // solution type8 for three baseline vectors
    u1 flag; // 0 – components of matrix Q are invalid, 1 - valid
    f4 bl0[3]; // baseline vector M-S0 in the current epoch [m]
    f4 bl1[3]; // baseline vector M-S1 in the current epoch [m]
    f4 bl2[3]; // baseline vector M-S2 in the current epoch [m]
    u1 cs; // Checksum
};
```
#### Var51
```cpp
struct RotationAngles {33}
{
    u4 time; // Receiver time [ms]
    f4 p,r,h; // Pitch ,roll , heading angles [deg]
    f4 sp,sr,sh; // Pitch, roll, heading angles RMS [deg]
    u1 solType[3]; // Solution type for 3 base lines
    u1 flags; // flags [bitfield]:
    // 0: 0 - no data available
    // 1 - data are valid
    // 7…1: reserved
    u1 cs; // Checksum
};
```
#### Var52
```cpp
struct AngularVelocity {22}
{
    u4 time; // receiver time [ms]
    f4 x; // X component of angular velocity [rad/s]
    f4 y; // Y component of angular velocity [rad/s]
    f4 z; // Z component of angular velocity [rad/s]
    f4 rms; // Angular velocity RMS [rad/s]
    u1 flags; // flags [bitfield]:
    // 0: 0 - no data available
    // 1 - data are valid
    // 7…1: reserved
    u1 cs; // Checksum
};
```
#### Var53
```cpp
struct InertialMeasurements {25}
{
    f4 accelerations[3]; // ax,ay,az [m/sec2]
    f4 angularVelocities[3]; // wx,wy,wz [rad/sec]
    u1 cs;
};
```
#### Var54
```cpp
struct AccMag {38}
{
    u4 time; // receiver time [ms]
    f4 accelerations[3]; // ax, ay, az [cm/sec2]
    f4 induction[3]; // bx, by, bz
    f4 magnitude; // Value of magnetic field
    f4 temperature; // Temperature of magnetic sensor [deg C]
    u1 calibrated; // 1 - calibrated, 0 - not calibrated
    u1 cs; // Checksum
};
```
#### Var55
```cpp
struct ExtEvent {10}
{
    i4 ms; // ms part of event time tag
    i4 ns; // ns part of event time tag
    u1 timeScale; // time scale
    u1 cs; // Checksum
};
```
#### Var56
```cpp
struct PPSOffset {5}
{
    f4 offs; // PPS offset in nanoseconds
    u1 cs; // Checksum
};
```
#### Var57
```cpp
struct RcvTimeOffsAtPPS {10}
{
    f8 offs; // [Tpps-Tr] offset [s]
    u1 timeScale; // time scale
    u1 cs; // Checksum
};
```
#### Var58
```cpp
struct HeadAndPitch {10}
{
    f4 heading; // Heading of the baseline between the base and the
    // rover receiver [degrees]
    f4 pitch; // Pitch of the baseline between the base and the
    // rover receiver [degrees]
    u1 solType; // Solution type
    u1 cs; // Checksum
};
```
#### Var59
```cpp
struct IonoParams {39}
{
    u4 tot; // Time of week [s]
    u2 wn; // Week number (taken from the first subframe)
    // The coefficients of a cubic equation representing
    // the amplitude of the vertical delay
    f4 alpha0; // [s]
    f4 alpha1; // [s/semicircles]
    f4 alpha2; // [s/semicircles2]
    f4 alpha3; // [s/semicircles3]
    // The coefficients of a cubic equation representing
    // the period of the model
    f4 beta0; // [s]
    f4 beta1; // [s/semicircles]
    f4 beta2; // [s/semicircles2]
    f4 beta3; // [s/semicircles3]
    u1 cs; // Checksum
};
```
#### Var60
```cpp
struct QzssIonoParams {39}
{
    IonoParams par;
};

struct IonoParams {39}
{
    u4 tot; // Time of week [s]
    u2 wn; // Week number (taken from the first subframe)
    // The coefficients of a cubic equation representing
    // the amplitude of the vertical delay
    f4 alpha0; // [s]
    f4 alpha1; // [s/semicircles]
    f4 alpha2; // [s/semicircles2]
    f4 alpha3; // [s/semicircles3]
    // The coefficients of a cubic equation representing
    // the period of the model
    f4 beta0; // [s]
    f4 beta1; // [s/semicircles]
    f4 beta2; // [s/semicircles2]
    f4 beta3; // [s/semicircles3]
    u1 cs; // Checksum
};
```
#### Var61
```cpp
struct BeiDouIonoParams {39}
{
    IonoParams par;
};

struct IonoParams {39}
{
    u4 tot; // Time of week [s]
    u2 wn; // Week number (taken from the first subframe)
    // The coefficients of a cubic equation representing
    // the amplitude of the vertical delay
    f4 alpha0; // [s]
    f4 alpha1; // [s/semicircles]
    f4 alpha2; // [s/semicircles2]
    f4 alpha3; // [s/semicircles3]
    // The coefficients of a cubic equation representing
    // the period of the model
    f4 beta0; // [s]
    f4 beta1; // [s/semicircles]
    f4 beta2; // [s/semicircles2]
    f4 beta3; // [s/semicircles3]
    u1 cs; // Checksum
};
```
#### Var62
```cpp
struct IrnssIonoParams {39}
{
    IonoParams par;
};

struct IonoParams {39}
{
    u4 tot; // Time of week [s]
    u2 wn; // Week number (taken from the first subframe)
    // The coefficients of a cubic equation representing
    // the amplitude of the vertical delay
    f4 alpha0; // [s]
    f4 alpha1; // [s/semicircles]
    f4 alpha2; // [s/semicircles2]
    f4 alpha3; // [s/semicircles3]
    // The coefficients of a cubic equation representing
    // the period of the model
    f4 beta0; // [s]
    f4 beta1; // [s/semicircles]
    f4 beta2; // [s/semicircles2]
    f4 beta3; // [s/semicircles3]
    u1 cs; // Checksum
};
```
#### Var63
```cpp
struct Latency {2}
{
    u1 lt; // output latency [ms]
    u1 cs; // Checksum
};
```
#### Var64
```cpp
struct BaseInfo {28}
{
    f8 x, y, z; // ECEF coordinates [m]
    u2 id; // Reference station ID
    u1 solType; // Solution type
    u1 cs; // Checksum
};
```
#### Var65
```cpp
struct Security {6}
{
    u1 data[5]; // Opaque data
    u1 cs; // Checksum
};
```
#### Var66
```cpp
struct TrackingTime {5}
{
    u4 tt; // tracking time [s]
    u1 cs; // Checksum
};
```
#### Var67
```cpp
struct RcvOscOffs {5}
{
    f4 val; // Oscillator offset [s/s]
    u1 cs; // Checksum
};
```
#### Var68
```cpp
struct EpochEnd {1}
{
    u1 cs; // Checksum
};
```

## How to use
In the main function write variants you want to be in output. <br /><br />
*For this example variants 1, 2, 3 were chosen:*
```cpp
int main()
{
    std::unique_ptr<VarFactory1> varfactory1(new VarFactory1);
    std::unique_ptr<VarFactory2> varfactory2(new VarFactory2);
    std::unique_ptr<VarFactory3> varfactory3(new VarFactory3);

    std::vector<IVar*> v;
    v.push_back(varfactory1->createVar());
    v.push_back(varfactory2->createVar());
    v.push_back(varfactory3->createVar());
    
    for (int i = 0; i < v.size(); i++)
    {
        v[i]->handleVar();
    }

    return 0;
}
```

## Tests
Tests folder can be easily located in **uirs5semester2019\src**. <br />
Each variant has its own test, checking its checksum. <br /><br />
*Test example for 1 variant:*
```cpp
#include "pch.h"
#include "../../uirs5sem/include/Var1.h"
#include "../../uirs5sem/src/Var1.cpp"

TEST(Var1Test, isCheckSumOK)
{
	Var1 var1;
	int checksum = var1.getCheckSum();
	int csvalue = var1.getCsValue();
	EXPECT_EQ(checksum, csvalue);
}
```

## What is a checksum
A checksum is a sequence of numbers and letters used to check data for errors.<br />
If you know the checksum of an original file, you can use a checksum utility to confirm your copy is identical.

## How to compute a checksum
In this case we use **CRC-8** algorithm to compute checksums for each variant.

## License
This project is licensed under the MIT License - see the LICENSE.md file for details.